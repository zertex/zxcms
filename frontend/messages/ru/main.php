<?php

return [
	'Contacts' => 'Контакты',
	'Thank you for contacting us. We will respond to you as soon as possible.' => 'Сообщение отправлено. Мы свяжемся с вами при первой возможности.',
	'There was an error sending your message.' => 'Ошибка отправки сообщения.',
	'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.' => 'Если у вас возникли любые вопросы к нам, заполните пожалуйста форму. Спасибо.',
	'Submit' => 'Отправить',
	'Get in touch' => 'Обратная связь',
	'Your name' => 'Ваше имя',
	'Subject' => 'Тема сообщения',
	'Message' => 'Сообщение',
	'Verification Code' => 'Код проверки',
	'Contact Details' => 'Контакты',
	'Address' => 'Адрес',
	'Phone' => 'Телефон',
];