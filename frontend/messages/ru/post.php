<?php

return [
	'Home' => 'Главная',
	'News' => 'Новости',
	'Articles' => 'Статьи',
	'Blog' => 'Блог',
	'Events' => 'События',
	'Gallery' => 'Фотогалерея',
	'Read more' => 'Подробнее',
	'Share' => 'Поделиться',
	'All news' => 'Все новости',
	'In The Spotlight' => 'В центре внимания',
	'Follow us' => 'Поделиться',
	'Categories' => 'Категории',
	'Tags' => 'Теги',
	'Related' => 'Похожие',
	'Comments: {count}' => 'Комментариев: {count}',
	'Comment' => 'Комментарий',
	'Submit' => 'Отправить',
	'Reply' => 'Ответить',
	'Send comment' => 'Комментировать',
	'Please {login} for writing a comment.' => 'Пожалуйста {login}, чтобы добавить комментарий.',
	'Latest comments' => 'Последние комментарии',
];