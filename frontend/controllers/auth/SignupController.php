<?php

namespace frontend\controllers\auth;

use core\services\auth\SignupService;
use DomainException;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use core\forms\auth\SignupForm;
use yii\web\Controller;
use yii\web\Response;

class SignupController extends Controller
{
    public $layout = 'auth';

    private SignupService $service;

    public function __construct($id, $module, SignupService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['request', 'confirm'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|Response
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function actionRequest(): string|Response
    {
        $form = new SignupForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->signup($form);
                Yii::$app->session->setFlash('success', Yii::t('auth', 'Check your email for further instructions.'));

                //return $this->goHome();
                return $this->redirect(['auth/auth/login']);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('request', [
            'model' => $form,
        ]);
    }

    /**
     * @param $token
     *
     * @return Response
     */
    public function actionConfirm($token): Response
    {
        try {
            $this->service->confirm($token);
            Yii::$app->session->setFlash('success', Yii::t('auth', 'Your email is confirmed.'));

            return $this->redirect(['auth/auth/login']);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->goHome();
    }
}
