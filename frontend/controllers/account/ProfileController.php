<?php
/**
 * Created by Error202
 * Date: 22.08.2017
 */

namespace frontend\controllers\account;

use DomainException;
use frontend\components\FrontendController;
use core\services\user\ProfileService;
use core\forms\user\ProfileEditForm;
use core\entities\user\User;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use Yii;
use yii\web\Response;

class ProfileController extends FrontendController
{
    public $layout = 'profile';

    private ProfileService $service;

    public function __construct($id, $module, ProfileService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only'  => ['index'],
                'rules' => [
                    [
                        'actions' => ['edit', 'social'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|Response
     * @throws NotFoundHttpException|Exception
     */
    public function actionEdit(): Response|string
    {
        $user = $this->findModel(Yii::$app->user->id);

        $form = new ProfileEditForm($user);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($user->id, $form);
                Yii::$app->session->setFlash('success', Yii::t('user', 'Profile is saved.'));

                return $this->redirect(['/account/profile/edit']);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('edit', [
            'model' => $form,
            'user'  => $user,
        ]);
    }

    public function actionSocial(): string
    {
        return $this->render('social');
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): User
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
