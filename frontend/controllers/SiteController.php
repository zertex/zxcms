<?php

namespace frontend\controllers;

use core\forms\SubscribeForm;
use core\services\newsletter\Newsletter;
use frontend\components\FrontendController;
use RuntimeException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\components\SiteAccess;
use Yii;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    public Newsletter $newletter;

    public function __construct(string $id, $module, Newsletter $newsletter, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->newletter = $newsletter;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only'  => ['subscribe'],
                'rules' => [
                    [
                        'actions' => ['subscribe'],
                        'allow'   => true,
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            SiteAccess::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'transparent'     => true,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $this->layout = 'home';

        return $this->render('index');
    }

    public function actionSubscribe(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $form                       = new SubscribeForm();
        if (Yii::$app->request->isAjax && $form->load(Yii::$app->request->post())) {
            try {
                $this->newletter->subscribe($form->email);

                return ['result' => 'success'];
            } catch (RuntimeException $e) {
                return ['result' => 'error', 'message' => $e->getMessage()];
            }
        }

        return ['result' => 'error', 'message' => 'Request error'];
    }
}
