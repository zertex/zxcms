<?php
namespace frontend\models;

use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use core\entities\user\User;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public string $password;
    private ?User $user;

    /**
     * Creates a form model given a token.
     * @param string $token
     * @param array $config
     */
    public function __construct(string $token, array $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidArgumentException('Password reset token cannot be blank.');
        }
        $this->user = User::findByPasswordResetToken($token);
        if (!$this->user) {
            throw new InvalidArgumentException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function resetPassword(): bool
    {
        $user = $this->user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
