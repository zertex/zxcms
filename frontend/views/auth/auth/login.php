<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

use core\forms\auth\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = Yii::t('auth', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Login-->
<section class="section section-variant-1 bg-gray-100">
	<div class="container">
		<div class="row row-50 justify-content-center">
			<div class="col-md-10 col-lg-8 col-xl-6">
				<div class="card-login-register" id="card-l-r">
					<div class="card-top-panel">
						<div class="card-top-panel-left">
							<h5 class="card-title card-title-login"><?= Yii::t('auth', 'Sign in') ?></h5>
							<h5 class="card-title card-title-register"><?= Yii::t('auth', 'Register') ?></h5>
						</div>
						<div class="card-top-panel-right"><span class="card-subtitle"><span class="card-subtitle-login"><?= Yii::t('auth', 'Register') ?></span><span class="card-subtitle-register"><?= Yii::t('auth', 'Sign in') ?></span></span>
							<button class="card-toggle" data-custom-toggle="#card-l-r"><span class="card-toggle-circle"></span></button>
						</div>
					</div>
					<div class="card-form card-form-login">

						<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

						<div class="form-wrap">
							<?= $form->field($model, 'username', [
								'template' => '{input}{error}',
								'errorOptions' => [ 'class' => 'form-validation' ]
							])->textInput([
								'class' => 'form-input form-control-has-validation form-control-last-child',
								'placeholder' => Yii::t('auth', 'Username'),
							])->label(false) ?>
						</div>

						<div class="form-wrap">
							<?= $form->field($model, 'password', [
								'template' => '{input}{error}',
								'errorOptions' => [ 'class' => 'form-validation' ]
							])->passwordInput([
								'class' => 'form-input form-control-has-validation form-control-last-child',
								'placeholder' => Yii::t('auth', 'Password'),
							])->label(false) ?>
						</div>


						<?= $form->field($model, 'rememberMe', [
							'labelOptions' => [
								'class' => 'checkbox-inline checkbox-inline-lg checkbox-light',
								'style' => 'font-weight: normal; text-transform: none',
							]
						])->checkbox([
							'class' => 'checkbox-custom',
						]) ?>

						<div style="color:#999;margin:1em 0">
							If you forgot your password you can <?= Html::a('reset it', ['auth/reset/request']) ?>.
						</div>

						<div>
							<?= Html::submitButton(Yii::t('auth', 'Sign in'), [
								'class' => 'button button-lg button-primary button-block',
								'name' => 'login-button'
							]) ?>
						</div>

						<?php ActiveForm::end(); ?>

						<div class="group-sm group-sm-justify group-middle">
							<a class="button button-google button-icon button-icon-left button-round" href="#">
								<span class="icon fa fa-google-plus"></span>
								<span>Google+</span>
							</a>

							<a class="button button-twitter button-icon button-icon-left button-round" href="#">
								<span class="icon fa fa-twitter"></span>
								<span>Twitter</span>
							</a>

							<a class="button button-facebook button-icon button-icon-left button-round" href="#">
								<span class="icon fa fa-facebook"></span>
								<span>Facebook</span>
							</a>
						</div>
					</div>
					<div class="card-form card-form-register">



						<form class="rd-form rd-mailform" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php" novalidate="novalidate">
							<div class="form-wrap">
								<label class="form-label rd-input-label" for="form-register-email">E-mail</label>
								<input class="form-input form-control-has-validation form-control-last-child" id="form-register-email" name="email" data-constraints="@Email @Required" type="email"><span class="form-validation"></span>
							</div>
							<div class="form-wrap">
								<label class="form-label rd-input-label" for="form-login-name-2">Login</label>
								<input class="form-input form-control-has-validation form-control-last-child" id="form-login-name-2" name="form-input" data-constraints="@Required" type="text"><span class="form-validation"></span>
							</div>
							<div class="form-wrap">
								<label class="form-label rd-input-label" for="form-login-password-2">Password</label>
								<input class="form-input form-control-has-validation form-control-last-child" id="form-login-password-2" name="password" data-constraints="@Required" type="password"><span class="form-validation"></span>
							</div>
							<div class="form-wrap">
								<label class="form-label rd-input-label" for="form-login-password-3">Repeat Password</label>
								<input class="form-input form-control-has-validation form-control-last-child" id="form-login-password-3" name="password" data-constraints="@Required" type="password"><span class="form-validation"></span>
							</div>
							<button class="button button-lg button-primary button-block" type="submit"><?= Yii::t('auth', 'Register now') ?></button>
						</form>
						<div class="group-sm group-sm-justify group-middle"><a class="button button-google button-icon button-icon-left button-round" href="#"><span class="icon fa fa-google-plus"></span><span>Google+</span></a><a class="button button-twitter button-icon button-icon-left button-round" href="#"><span class="icon fa fa-twitter"></span><span>Twitter</span></a><a class="button button-facebook button-icon button-icon-left button-round" href="#"><span class="icon fa fa-facebook"></span><span>Facebook</span></a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>












































<section class="section section-variant-1 bg-gray-100">
	<div class="container">
		<div class="row row-50 justify-content-center">
			<div class="col-md-12 col-lg-12 col-xl-12">


<div class="row">
    <div class="col-sm-6">
	    <div class="card-login-register" id="card-2-r">
		    <div class="card-top-panel">
			    <div class="card-top-panel-left">
				    <h5 class="card-title card-title-login"><?= Yii::t('auth', 'New Customer') ?></h5>
			    </div>
			    <div class="card-top-panel-right">
				    <span class="card-subtitle">
					    <span class="card-subtitle-login"><?= Yii::t('auth', 'Register') ?></span>
				    </span>
			    </div>
		    </div>
		    <div class="card-form card-form-login">
			    <?= Html::a(Yii::t('auth', 'Register now'), ['/auth/signup/request'], [
				    'class' => 'button button-lg button-primary button-block',
				    'name' => 'register-button'
			    ]) ?>

		    </div>
        </div>

        <div class="card-login-register" id="card-3-r" style="margin-top:20px">
	        <div class="card-top-panel">
		        <div class="card-top-panel-left">
			        <h5 class="card-title card-title-login"><?= Yii::t('auth', 'Social login') ?></h5>
		        </div>
	        </div>
	        <div class="card-form card-form-login">

			        <?php $authAuthChoice = AuthChoice::begin([
				        'baseAuthUrl' => ['site/auth'],
				        'options' => [
				            'class' => 'group-sm group-sm-justify group-middle'
				        ]
			        ]); ?>

				        <?php foreach ($authAuthChoice->getClients() as $client): ?>
					        <?= $authAuthChoice->clientLink($client, null, [
					            'class' => 'button button-' . $client->getId() . ' button-icon button-icon-left button-round',
					        ]) ?>
				        <?php endforeach; ?>

			        <?php AuthChoice::end(); ?>

	        </div>
        </div>

    </div>
    <div class="col-sm-6">

	    <div class="card-login-register" id="card-l-r">
		    <div class="card-top-panel">
			    <div class="card-top-panel-left">
				    <h5 class="card-title card-title-login"><?= Yii::t('auth', 'Sign in') ?></h5>
			    </div>
			    <div class="card-top-panel-right">
				    <span class="card-subtitle">
					    <span class="card-subtitle-login"><?= Yii::t('auth', 'Returning customer') ?></span>
				    </span>
			    </div>
		    </div>
		    <div class="card-form card-form-login">

	        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

	        <div class="form-wrap">
		        <?= $form->field($model, 'username', [
			        'template' => '{input}{error}',
			        'errorOptions' => [ 'class' => 'form-validation' ]
		        ])->textInput([
			        'class' => 'form-input form-control-has-validation form-control-last-child',
			        'placeholder' => Yii::t('auth', 'Username'),
		        ])->label(false) ?>
	        </div>

	        <div class="form-wrap">
		        <?= $form->field($model, 'password', [
			        'template' => '{input}{error}',
			        'errorOptions' => [ 'class' => 'form-validation' ]
		        ])->passwordInput([
			        'class' => 'form-input form-control-has-validation form-control-last-child',
			        'placeholder' => Yii::t('auth', 'Password'),
		        ])->label(false) ?>
	        </div>


	        <?= $form->field($model, 'rememberMe', [
		        'labelOptions' => [
			        'class' => 'checkbox-inline checkbox-inline-lg checkbox-light',
			        'style' => 'font-weight: normal; text-transform: none',
		        ]
	        ])->checkbox([
		        'class' => 'checkbox-custom',
	        ])->label(Yii::t('auth', 'Remember Me')) ?>

	        <div style="color:#999;margin:1em 0">
		        If you forgot your password you can <?= Html::a('reset it', ['auth/reset/request']) ?>.
	        </div>

	        <div>
		        <?= Html::submitButton(Yii::t('auth', 'Sign in'), [
			        'class' => 'button button-lg button-primary button-block',
			        'name' => 'login-button'
		        ]) ?>
	        </div>

	        <?php ActiveForm::end(); ?>

		    </div>
	    </div>

    </div>
</div>


			</div>
		</div>
	</div>
</section>