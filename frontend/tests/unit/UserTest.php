<?php
/**
 * Created by Error202
 * Date: 13.08.2018
 */

namespace frontend\tests\unit;

use Codeception\Test\Unit;
use core\entities\Meta;
use frontend\tests\UnitTester;
use frontend\tests\fixtures\UserFixture;

class UserTest extends Unit
{
	/**
	 * @var UnitTester
	 */
	protected $tester;

	public function _fixtures()
	{
		return [
			'users' => UserFixture::class,
			//'dataFile' => '/app/frontend/tests/fixtures/data/user.php',
			//'dataFile' => codecept_data_dir() . 'user.php'
		];
	}

	protected function _before() {

	}

	protected function _after() {

	}

	public function testFindUserByUsername()
	{
		$user = $this->tester->grabFixture('users', 'user1');
		//$user = $this->users['user1'];
		//expect_that($user = User::findByUsername('Error202'));
		//expect($user->id)->equals(1);



		$this->assertEquals(1, $user->id);
		$this->assertEquals('Error202', $user->username);
		//expect_not(User::findByUsername('Error203'));
	}

	/*public function testValidateUser()
	{
		$user = User::findByUsername('Error202');
		expect_that($user->validatePassword('4220618'));
		expect_not($user->validatePassword('123456'));
	}*/

	public function testMeta()
	{
		$meta = new Meta('test', 'desc', 'keys');
		expect($meta->title)->equals('test');

	}

	// test
	/*public function testTest()
	{
		$user = $this->tester->grabFixture('users', 'user1');
	}*/

}