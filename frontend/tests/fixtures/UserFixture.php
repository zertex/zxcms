<?php
/**
 * Created by Error202
 * Date: 13.08.2018
 */

namespace frontend\tests\fixtures;


use core\entities\user\User;
use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
	public $modelClass = User::class;
}