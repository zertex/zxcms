<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-frontend',
    'language'            => 'ru',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => [
        'log',
        'common\bootstrap\SetUp',
        'frontend\bootstrap\SetUp',
    ],
    'aliases'             => [
        '@staticRoot' => $params['staticPath'],
        '@static'     => $params['staticHostInfo'],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components'          => [
        'request'            => [
            'baseUrl'             => '',
            'csrfParam'           => '_csrf-frontend',
            'cookieValidationKey' => $params['cookieValidationKey'],
        ],
        'user'               => [
            'identityClass'   => 'common\auth\Identity',
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity', 'httpOnly' => true, 'domain' => $params['cookieDomain']],
            'loginUrl'        => ['auth/auth/login'],
        ],
        'session'            => [
            'name'          => '_session',
            'class'         => 'yii\web\DbSession',
            'writeCallback' => function ($session) {
                return [
                    'user_id' => Yii::$app->user->id
                ];
            },
            'cookieParams'  => [
                'domain'   => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],
        'log'                => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler'       => [
            'errorAction' => 'site/error',
        ],
        'backendUrlManager'  => require __DIR__ . '/../../backend/config/urlManager.php',
        'frontendUrlManager' => require __DIR__ . '/urlManager.php',
        'urlManager'         => function () {
            return Yii::$app->get('frontendUrlManager');
        },
        'i18n'               => [
            'translations' => [
                'post'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'slider' => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'auth'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'user'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'main'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
            ],
        ],
        'view'               => [
            'theme' => [
                'basePath' => '@webroot/themes/sport',
                'baseUrl'  => '@web/themes/sport',
                'pathMap'  => [
                    '@frontend/views'   => '@webroot/themes/sport',
                    '@frontend/widgets' => '@webroot/themes/sport/widgets',
                ],
            ],
        ],
        'assetManager'       => [
            'bundles' => [
                'yii\web\JqueryAsset'                => [
                    'sourcePath' => '@frontend/assets/libs/jquery321',   // do not publish the bundle
                    'js'         => [
                        YII_ENV_DEV ? 'jquery-3.2.1.js' : 'jquery-3.2.1.min.js'
                    ],
                ],
                'yii\bootstrap\BootstrapAsset'       => [
                    'sourcePath' => '@frontend/assets/libs/bootstrap4/css',   // do not publish the bundle
                    'css'        => [
                        YII_ENV_DEV ? 'bootstrap.css' : 'bootstrap.min.css'
                    ],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => '@frontend/assets/libs/bootstrap4/js',   // do not publish the bundle
                    'js'         => [
                        YII_ENV_DEV ? 'bootstrap.js' : 'bootstrap.min.js'
                    ],
                    'depends'    => [
                        'yii\web\JqueryAsset',
                        'yii\bootstrap\BootstrapAsset',
                    ],
                ],
            ],
        ],
    ],
    'params'              => $params,
];
