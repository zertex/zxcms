<?php
return [
    'id' => 'app-frontend-tests',
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
        'user' => [
	        'identityClass' => 'common\auth\Identity',
	        'enableAutoLogin' => true,
	        'identityCookie' => ['name' => '_identity', 'httpOnly' => true, 'domain' => $params['cookieDomain']],
	        'loginUrl' => ['auth/auth/login'],
        ],
    ],
];
