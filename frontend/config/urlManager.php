<?php

/** @var array $params */

return [
    //'class' => 'yii\web\UrlManager',
	'class' => \frontend\config\LanguageUrlManager::class,
    'hostInfo' => $params['frontendHostInfo'],
    'baseUrl' => '',
    //'suffix' => '/',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'cache' => false,
    'rules' => [
        '' => 'site/index',
        'contact' => 'contact/index',
        'signup' => 'auth/signup/request',
        'signup/<_a:[\w-]+>' => 'auth/signup/<_a>',
        '<_a:login|logout>' => 'auth/auth/<_a>',

	    //['class' => \frontend\components\LanguageUrlRule::class],
        //['pattern' => 'yandex-market', 'route' => 'market/index', 'suffix' => '.xml'],

        //['pattern' => 'sitemap', 'route' => 'sitemap/index', 'suffix' => '.xml'],
        //['pattern' => 'sitemap-<target:[a-z-]+>-<start:\d+>', 'route' => 'sitemap/<target>', 'suffix' => '.xml'],
        //['pattern' => 'sitemap-<target:[a-z-]+>', 'route' => 'sitemap/<target>', 'suffix' => '.xml'],

        //'blog' => 'blog/post/index',
        //'blog/tag/<slug:[\w\-]+>' => 'blog/post/tag',
        //'blog/<id:\d+>' => 'blog/post/post',
        //'blog/<id:\d+>/comment' => 'blog/post/comment',
        //'blog/<slug:[\w\-]+>' => 'blog/post/category',

        /*'cabinet' => 'cabinet/default/index',
        'cabinet/<_c:[\w\-]+>' => 'cabinet/<_c>/index',
        'cabinet/<_c:[\w\-]+>/<id:\d+>' => 'cabinet/<_c>/view',
        'cabinet/<_c:[\w\-]+>/<_a:[\w-]+>' => 'cabinet/<_c>/<_a>',
        'cabinet/<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => 'cabinet/<_c>/<_a>',*/

	    //'post/<_c:[\w\-]+>' => 'post/',
	/*    ['class' => 'frontend\urls\PostMainUrlRule'],
	    ['class' => 'frontend\urls\PostUrlRule'],
	    ['class' => 'frontend\urls\PostCategoryUrlRule'],
	    ['class' => 'frontend\urls\PageUrlRule'],*/

	/* Moved to end bootstrap - SetUp.php
        '<_c:[\w\-]+>' => '<_c>/index',
        '<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
        '<_c:[\w\-]+>/<_a:[\w-]+>' => '<_c>/<_a>',
        '<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_c>/<_a>',*/
    ],
];