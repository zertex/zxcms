<?php
/**
 * Created by Error202
 * Date: 23.08.2018
 */

namespace frontend\config;


use core\helpers\LanguageHelper;
use yii\web\UrlManager;
use Yii;

class LanguageUrlManager extends UrlManager
{
	public function init()
	{
		if (basename(Yii::$app->getBasePath()) !== 'frontend') {
			return parent::init();
		}
		LanguageHelper::setLanguage(Yii::$app->request->getUrl());
		$langPrefix = Yii::$app->language . '/';
		$finalRules[$langPrefix] = '';

		foreach ($this->rules as $rule => $path) {
			if ( is_array($path) && isset($path['pattern']) && isset($path[0]) ) {
				$finalRules[$langPrefix . ltrim($path['pattern'], '/')] = $path[0];
			}
			else {
				$finalRules[$langPrefix . ltrim($rule, '/')] = $path;
			}
		}
		$this->rules = array_merge_recursive($finalRules, $this->rules);
		return parent::init();
	}

	public function createUrl( $params )
	{
		$url = parent::createUrl( $params );
		return LanguageHelper::addLangToUrl($url, isset($params['language']) ? $params['language'] : null);
	}
}