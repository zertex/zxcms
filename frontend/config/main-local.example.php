<?php

$config = [
	'components' => [
		'authClientCollection' => [
			'class' => 'yii\authclient\Collection',
			'clients' => [
				/*
					Register app: https://oauth.yandex.ru/client/new
					Callback URL: http://site.name/auth/network/auth?authclient=yandex
				*/
				'yandex' => [
					'class' => 'yii\authclient\clients\Yandex',
					'clientId' => '',
					'clientSecret' => '',
				],
				'google' => [
					'class' => 'yii\authclient\clients\Google',
					'clientId' => '',
					'clientSecret' => '',
				],
				'facebook' => [
					'class' => 'yii\authclient\clients\Facebook',
					'clientId' => '',
					'clientSecret' => '',
				],
				'vk' => [
					'class' => 'yii\authclient\clients\VKontakte',
					'clientId' => '',
					'clientSecret' => '',
				],
			],
		]
	],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
