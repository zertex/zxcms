<?php

namespace frontend\bootstrap;

use core\entities\Settings;
use core\helpers\LanguageHelper;
use Yii;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = Yii::$container;

        $container->set(Breadcrumbs::class, function ($container, $params, $args) {
            return new Breadcrumbs(ArrayHelper::merge([
                'homeLink' => [
                    //'label' => '<span class="fa fa-home"></span>',
                    'label'  => Yii::t('post', 'Home'),
                    'encode' => false,
                    'url'    => Yii::$app->homeUrl,
                ],
            ], $args));
        });

        // Load settings
        $settings = Settings::find()->with('translations')->andWhere(['active' => 1])->all();
        $settings_array = $settings ? ArrayHelper::map($settings, 'key', function ($el) {
            return $el->translation->value ?? '';
        }, 'section') : [];
        $app->params['settings'] = $settings_array;

        // Add finish UrlRules
        $app->getUrlManager()->addRules([
            '<_c:[\w\-]+>'                       => '<_c>/index',
            '<_c:[\w\-]+>/<id:\d+>'              => '<_c>/view',
            '<_c:[\w\-]+>/<_a:[\w-]+>'           => '<_c>/<_a>',
            '<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_c>/<_a>',
        ]);

        // redefine home url
        Yii::$app->homeUrl = LanguageHelper::addLangToUrl(Yii::$app->homeUrl);
    }
}
