<?php
/**
 * Created by Error202
 * Date: 27.01.2018
 */

namespace frontend\widgets\slider;

use core\entities\Slider;
use yii\base\Widget;

class SliderWidget extends Widget
{
	public string $tag;
	public string $classes;
	public array $attrs;

	public function run(): string
	{
		$slider = Slider::find()->orderBy(['sort' => SORT_ASC])->all();

		return $this->render('slider', [
			'slider' => $slider,
			'tag' => $this->tag ?: 'div',
			'classes' => $this->classes,
			'attrs' => $this->attrs,
		]);
	}
}