<?php

use core\entities\Slider;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $slider Slider[]
 * @var $tag string
 * @var $classes string
 * @var $attrs string
 */

$js = <<<JS
	var s = $(".swiper-slider");
    var pag = s.find(".swiper-pagination"),
      next = s.find(".swiper-button-next"),
      prev = s.find(".swiper-button-prev"),
      bar = s.find(".swiper-scrollbar"),
      swiperSlide = s.find(".swiper-slide"),
      autoplay = false;
    for (var j = 0; j < swiperSlide.length; j++) {
      var sthis = $(swiperSlide[j]),
        url;

      if (url = sthis.attr("data-slide-bg")) {
        sthis.css({
          "background-image": "url(" + url + ")",
          "background-size": "cover"
        })
      }
    }
    swiperSlide.end()
      .find("[data-caption-animate]")
      .addClass("not-animated")
      .end();
    
    s.swiper({
          autoplay: s.attr('data-autoplay') ? s.attr('data-autoplay') === "false" || s.attr('data-autoplay-hover') === "true" ? undefined : s.attr('data-autoplay') : 5000,
          direction: s.attr('data-direction') ? s.attr('data-direction') : "horizontal",
          effect: s.attr('data-slide-effect') ? s.attr('data-slide-effect') : "slide",
          speed: s.attr('data-slide-speed') ? s.attr('data-slide-speed') : 600,
          keyboardControl: s.attr('data-keyboard') === "true",
          mousewheelControl: s.attr('data-mousewheel') === "true",
          mousewheelReleaseOnEdges: s.attr('data-mousewheel-releaase') === "true",
          lazyLoading: s.attr('data-lazy-loading') === "true",
          nextButton: next.length ? next.get(0) : null,
          prevButton: prev.length ? prev.get(0) : null,
          pagination: pag.length ? pag.get(0) : null,
          paginationClickable: pag.length ? pag.attr("data-clickable") !== "false" : false,
          paginationBulletRender: pag.length ? pag.attr("data-index-bullet") === "true" ? function (swiper, index, className) {
            return '<span class="' + className + '"><span>' + ((index + 1) < 10 ? ('0' + (index + 1) ): (index + 1))  + '</span></span>';
          } : null : null,
          scrollbar: bar.length ? bar.get(0) : null,
          scrollbarDraggable: bar.length ? bar.attr("data-draggable") !== "false" : true,
          scrollbarHide: bar.length ? bar.attr("data-draggable") === "false" : false,
          //loop: isNoviBuilder ? false : s.attr('data-loop') !== "false",
          loop: s.attr('data-loop') !== "false",
          //simulateTouch: s.attr('data-simulate-touch') && !isNoviBuilder ? s.attr('data-simulate-touch') === "true" : false,
          simulateTouch: s.attr('data-simulate-touch') ? s.attr('data-simulate-touch') === "true" : false,
          onTransitionStart: function (swiper) {
            toggleSwiperInnerVideos(swiper);
          },
          onTransitionEnd: function (swiper) {
            toggleSwiperCaptionAnimation(swiper);
          },
          onInit: function (swiper) {
            toggleSwiperInnerVideos(swiper);
            toggleSwiperCaptionAnimation(swiper);

            if (swiper.container.data('autoplay-hover') === true) {
              var hoverEvent;

              swiper.container.mouseenter(function (e) {
                hoverEvent = setInterval(function () {
                  swiper.slideNext();
                }, $(swiper.container).data('autoplay'));
              });

              swiper.container.mouseleave(function (e) {
                clearInterval(hoverEvent);
              });
            }

            //initLightGalleryItem(s.find('[data-lightgallery="item"]'), 'lightGallery-in-carousel');
          }
        });
    




	win.on("resize", (function (s) {
      return function () {
        var mh = getSwiperHeight(s, "min-height"),
          h = getSwiperHeight(s, "height");
        if (h) {
          s.css("height", mh ? mh > h ? mh : h : h);
        }
      }
    })(s)).trigger("resize");
	
	window.setTimeout(function() {
	  win.trigger("resize");
	}, 100);
JS;
$this->registerJs($js, $this::POS_READY);


$js2 = <<<JS2
	var win = $(window);

	function getSwiperHeight(object, attr) {
      var val = object.attr("data-" + attr),
        dim;

      if (!val) {
        return undefined;
      }

      dim = val.match(/(px)|(%)|(vh)|(vw)$/i);

      if (dim.length) {
        switch (dim[0]) {
          case "px":
            return parseFloat(val);
          case "vh":
            return win.height() * (parseFloat(val) / 100);
          case "vw":
            return win.width() * (parseFloat(val) / 100);
          case "%":
            return object.width() * (parseFloat(val) / 100);
        }
      } else {
        return undefined;
      }
    }
    
    function toggleSwiperInnerVideos(swiper) {
      var prevSlide = $(swiper.slides[swiper.previousIndex]),
        nextSlide = $(swiper.slides[swiper.activeIndex]),
        videos,
        videoItems = prevSlide.find("video");

      for (var i = 0; i < videoItems.length; i++) {
        videoItems[i].pause();
      }

      videos = nextSlide.find("video");
      if (videos.length) {
        videos.get(0).play();
      }
    }
    
    function toggleSwiperCaptionAnimation(swiper) {
      var prevSlide = $(swiper.container).find("[data-caption-animate]"),
        nextSlide = $(swiper.slides[swiper.activeIndex]).find("[data-caption-animate]"),
        delay,
        duration,
        nextSlideItem,
        prevSlideItem;

      for (var i = 0; i < prevSlide.length; i++) {
        prevSlideItem = $(prevSlide[i]);

        prevSlideItem.removeClass("animated")
          .removeClass(prevSlideItem.attr("data-caption-animate"))
          .addClass("not-animated");
      }


      var tempFunction = function (nextSlideItem, duration) {
        return function () {
          nextSlideItem
            .removeClass("not-animated")
            .addClass(nextSlideItem.attr("data-caption-animate"))
            .addClass("animated");
          if (duration) {
            nextSlideItem.css('animation-duration', duration + 'ms');
          }
        };
      };

      for (var i = 0; i < nextSlide.length; i++) {
        nextSlideItem = $(nextSlide[i]);
        delay = nextSlideItem.attr("data-caption-delay");
        duration = nextSlideItem.attr('data-caption-duration');
        //if (!isNoviBuilder) {
          setTimeout(tempFunction(nextSlideItem, duration), parseInt(delay, 10));
        //} else {
        //  nextSlideItem.removeClass("not-animated")
        //}
      }
    }
JS2;
$this->registerJs($js2, $this::POS_END);
?>

<<?= $tag ?> class="<?= $classes ?>" <?= $attrs ?>>
	<div class="swiper-wrapper" style="transition-duration: 0ms;">

		<?php foreach ($slider as $slide): ?>

			<div class="swiper-slide swiper-slide-active"
			     data-slide-bg="<?= Html::encode($slide->getThumbFileUrl('image', '1920_671')) ?>"
			     style="background-image: url(&quot;<?= Html::encode($slide->getThumbFileUrl('image', '1920_671')) ?>&quot;); background-size: cover; width: 1903px; transform: translate3d(-1903px, 0px, 0px); opacity: 1; transition-duration: 0ms;"
			     data-swiper-slide-index="0">
				<div class="container">
					<div class="swiper-slide-caption">
						<h1 data-caption-animate="fadeInUp" data-caption-delay="100" class="fadeInUp animated">
							<?= str_replace(PHP_EOL, "<br>", Html::encode($slide->title)) ?></h1>
						<h4 data-caption-animate="fadeInUp" data-caption-delay="200" class="fadeInUp animated">
							<?= str_replace(PHP_EOL, "<br>", Html::encode($slide->tagline)) ?>
						</h4><a
							class="button button-gray-outline fadeInUp animated" data-caption-animate="fadeInUp"
							data-caption-delay="300" href="#"><?= Yii::t('slider', 'Read more') ?></a>
					</div>
				</div>
			</div>

		<?php endforeach; ?>

	</div>
	<div class="swiper-button swiper-button-prev"></div>
	<div class="swiper-button swiper-button-next"></div>
	<div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets">
		<span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span>
		<span class="swiper-pagination-bullet"></span>
	</div>
</<?= $tag ?>>
