<?php
/**
 * Created by Error202
 * Date: 30.01.2018
 */

namespace frontend\widgets\post;


use core\entities\post\PostCategory;
use yii\base\Widget;

class PostCategoriesWidget extends Widget
{
	public int $type_id;

	public function run(): string
    {
		$categories = PostCategory::find()->byType($this->type_id)->all();

		if (count($categories) < 2) {
			return '';
		}

		return $this->render('categories', [
			'categories' => $categories,
		]);
	}
}
