<?php
/**
 * Created by Error202
 * Date: 10.02.2018
 */

namespace frontend\widgets\post;

use core\entities\post\PostComment;
use yii\base\Widget;

class LastCommentsWidget extends Widget
{
	public int $count;

	public function __construct( array $config = [] ) {
		parent::__construct( $config );
		$this->count = $this->count ?: 3;
	}

	public function run(): string
    {
		$comments = PostComment::find()->orderBy(['created_at' => SORT_DESC])->limit($this->count)->all();

		return $this->render('comments', [
			'comments' => $comments,
		]);
	}
}
