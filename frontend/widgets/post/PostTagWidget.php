<?php
/**
 * Created by Error202
 * Date: 30.01.2018
 */

namespace frontend\widgets\post;

use core\entities\post\Post;
use yii\base\Widget;

class PostTagWidget extends Widget
{
	public int $type_id;

	public function run(): string
    {

		// tags from categories with assignments, ordered by count
		$tags = Post::find()->select('t.name as name, count(t.id) as cnt')
			->from('posts as p, post_tag_assignments as a, post_tags as t')
			->andWhere(['p.type_id' => $this->type_id])
			->andWhere('p.id = a.post_id')
			->andWhere('t.id = a.tag_id')
			->groupBy('t.id')
			->orderBy('cnt DESC')
			->limit(20)
			->asArray()
			->all();

		return $this->render('tags', [
			'tags' => $tags,
		]);
	}
}
