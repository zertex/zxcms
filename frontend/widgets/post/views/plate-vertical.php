<?php

use core\entities\post\Post;
use yii\web\View;

/**
 * @var $this View
 * @var $view string
 * @var $posts Post[]
 * @var $url string
 */
?>

	<?php foreach ($posts as $post): ?>

		<?= $this->render($view . '_item', [
			'post' => $post,
			'url' => $url,
		]) ?>

	<?php endforeach; ?>
