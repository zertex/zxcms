<?php

use core\entities\post\PostComment;
use yii\web\View;

/**
 * @var $this View
 * @var $comments PostComment[]
 */
?>

<div class="block-aside-item">
	<!-- Heading Component-->
	<article class="heading-component">
		<div class="heading-component-inner">
			<h5 class="heading-component-title">
				<?= Yii::t('post', 'Latest comments') ?>
			</h5>
		</div>
	</article>

	<!-- List Comments Classic-->
	<div class="list-comments-classic">


<?php foreach ( $comments as $comment ): ?>

	<?= $this->render( 'comments_item', [
		'comment' => $comment,
	] ) ?>

<?php endforeach; ?>

	</div>
</div>