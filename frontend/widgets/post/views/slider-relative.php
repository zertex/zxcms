<?php

use core\entities\post\Post;
use yii\web\View;

/**
 * @var $this View
 * @var $view string
 * @var $posts Post[]
 */
?>

<div class="row">
	<div class="col-sm-12 owl-carousel-outer-navigation">
		<!-- Heading Component-->
		<article class="heading-component">
			<div class="heading-component-inner">
				<h5 class="heading-component-title"><?= Yii::t('post', 'Related') ?>
				</h5>
				<div class="owl-carousel-arrows-outline">
					<div class="owl-nav">
						<button class="owl-arrow owl-arrow-prev" id="relative-button-prev"></button>
						<button class="owl-arrow owl-arrow-next" id="relative-button-next"></button>
					</div>
				</div>
			</div>
		</article>

		<!-- Blog Carousel-->
		<div id="relative_carousel" class="owl-carousel row">
				<?php foreach ($posts as $post): ?>

					<?= $this->render($view . '_item', [
						'post' => $post
					]) ?>

				<?php endforeach; ?>
		</div>
	</div>
</div>