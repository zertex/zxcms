<?php

use core\entities\post\Post;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $post Post
 */

$url = Url::to(['post/post', 'id' =>$post->id]);
?>

<div class="news col-sm-4 main_news_homepage item_1">
	<article class="post-carmen">

		<img src="<?= Html::encode($post->getThumbFileUrl('image', 'home_slider')) ?>" alt="<?= Html::encode($post->title) ?>" />

		<div class="post-carmen-header">
			<?php if (!$post->video): ?>
			<!-- Badge-->
				<div class="badge badge-secondary"><?= $post->category->name ?></div>
			<?php else: ?>
				<a class="post-video-button" href="<?= $url ?>" ><span class="icon fa fa-play"></span></a>
			<?php endif; ?>
		</div>
		<div class="post-carmen-main">
			<h4 class="post-carmen-title">
				<?= Html::a($post->title, $url) ?>
			</h4>
			<div class="post-carmen-meta">
				<div class="post-carmen-time"><span
						class="icon fa fa-calendar"></span>
					<time datetime="<?= date('Y') ?>"><?= Yii::$app->formatter->asDate($post->published_at, 'php:d F, Y') ?></time>
				</div>
				<div class="post-carmen-comment"><span
						class="icon fa fa-comment-o"></span><a href="<?= $url . '#comments' ?>"><?= $post->comments_count ?></a>
				</div>
				<div class="post-carmen-view"><span class="icon fa fa-eye"></span><?= $post->views ?></div>
			</div>
		</div>
	</article>
</div>
