<?php

use core\entities\post\Post;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\View;

/**
 * @var $this View
 * @var $post Post
 */
?>

<div class="col-md-12">
	<!-- Post Future-->
	<article class="post-future post-future-horizontal">
		<a class="post-future-figure" href="#">
			<img src="<?= Html::encode($post->getThumbFileUrl('image', '370_325')) ?>" alt="<?= Html::encode($post->title) ?>" />
		</a>
		<div class="post-future-main">
			<h4 class="post-future-title">
				<?= Html::a(StringHelper::truncateWords(Html::encode($post->title), 8, '...'), '#') ?>
			</h4>
			<div class="post-future-meta">
				<!-- Badge-->
				<div class="badge badge-secondary"><?= $post->category->name ?></div>
				<div class="post-future-time"><span class="icon fa fa-calendar"></span>
					<time datetime="<?= date('Y') ?>"><?= Yii::$app->formatter->asDate($post->published_at, 'php:d F, Y') ?></time>
				</div>
			</div>
			<hr>
			<div class="post-future-text">
				<p><?= StringHelper::truncateWords(Html::encode($post->description), 16, '...') ?></p>
			</div>
		</div>
	</article>
</div>
