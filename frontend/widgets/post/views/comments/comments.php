<?php

use core\entities\post\Post;
use core\forms\post\PostCommentForm;
use frontend\widgets\post\CommentView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var $post Post
 * @var $this yii\web\View
 * @var $items CommentView[]
 * @var $count integer
 * @var $commentForm PostCommentForm
 */

$css = <<<CSS
a.reply-block-start {
    display: block;
    position: relative;
    top: -100px;
    visibility: hidden;
}
CSS;
$this->registerCss( $css );
?>

<div class="row">
	<div class="col-sm-12">
		<!-- Heading Component-->
		<article class="heading-component">
			<div class="heading-component-inner">
				<h5 class="heading-component-title">
					<?= Yii::t( 'post', 'Comments: {count}', [ 'count' => $post->comments_count ] ) ?>
				</h5>
				<?php if ( ! Yii::$app->user->isGuest ): ?>
					<a id="send-comment" href="#reply-block-start"
					   class="pull-right"><?= Yii::t( 'post', 'Send comment' ) ?></a>
				<?php endif; ?>

			</div>
		</article>

		<?php if ( count( $items ) > 0 ): ?>
			<div class="blog-post-comments">
				<?php foreach ( $items as $item ): ?>
					<?= $this->render( '_comment', [ 'item' => $item, 'child' => 0 ] ) ?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<?php if ( ! Yii::$app->user->isGuest ): ?>
			<div id="reply-container">
				<a name="reply-block-start" id="reply-block-start" class="reply-block-start"></a>
				<div id="reply-block" class="leave-reply">
					<?php $form = ActiveForm::begin( [
						'action' => [ 'comment', 'id' => $post->id ],
					] ); ?>

					<?= Html::activeHiddenInput( $commentForm, 'parentId' ) ?>
					<?= $form->field( $commentForm, 'text' )->textarea( [ 'rows' => 5 ] )->label( Yii::t( 'post', 'Comment' ) ) ?>

					<div class="form-group">
						<?= Html::submitButton( Yii::t( 'post', 'Submit' ), [ 'class' => 'btn btn-primary' ] ) ?>
					</div>

					<?php ActiveForm::end(); ?>
				</div>
			</div>
		<?php else: ?>
			<div style="text-align: center; padding: 20px;">
					<?= Yii::t('post', 'Please {login} for writing a comment.', ['login' => Html::a(Yii::t('auth', 'Log in'), ['/auth/auth/login'])]) ?>
			</div>
		<?php endif; ?>


	</div>
</div>

<?php $this->registerJs( "
    jQuery(document).on('click', '.blog-post-comments .comment-reply', function () {
        var link = jQuery(this);
        var form = jQuery('#reply-block');
        var comment = link.closest('.post-comment');
        jQuery('#postcommentform-parentid').val(comment.data('id'));
        form.detach().appendTo(comment.find('.reply-block:first'));
        return false;
    });
    
    jQuery(document).on('click', '#send-comment', function () {
        var form = jQuery('#reply-block');
        jQuery('#postcommentform-parentid').val('');
        form.detach().appendTo('#reply-container');
        //return false;
    });
" ); ?>


 