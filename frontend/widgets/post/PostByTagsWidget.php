<?php
/**
 * Created by Error202
 * Date: 24.01.2018
 */

namespace frontend\widgets\post;

use core\entities\post\Post;
use core\entities\post\PostTagAssignment;
use core\repositories\post\read\PostReadRepository;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class PostByTagsWidget extends Widget
{
	public int $count;
	public $view;
	public Post $post;

	public PostReadRepository $posts;

	public function __construct(PostReadRepository $posts, $config = [])
	{
		parent::__construct($config);
		$this->count = $this->count ?: 5;
		$this->posts = $posts;
	}

	public function run(): string
	{
		$tag_ids = ArrayHelper::getColumn(PostTagAssignment::find()->andWhere(['post_id' => $this->post->id])->all(), 'tag_id');
		$posts = $this->posts->getByTagsId($this->post, $tag_ids, $this->count)->getModels();

		return $this->render($this->view, [
			'posts' => $posts,
			'view' => $this->view,
			'url' => Url::canonical(),
		]);
	}
}