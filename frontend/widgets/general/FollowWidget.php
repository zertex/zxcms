<?php
/**
 * Created by Error202
 * Date: 29.01.2018
 */

namespace frontend\widgets\general;

use yii\helpers\Url;
use yii\base\Widget;

class FollowWidget extends Widget
{
	public function run(): string
	{
		$url = Url::canonical();

		return $this->render('follow_us', [
			'url' => $url,
		]);
	}
}
