<?php
/**
 * Created by Error202
 * Date: 10.02.2018
 */

namespace frontend\widgets\general;

use core\forms\SubscribeForm;
use yii\base\Widget;

class SubscribeWidget extends Widget
{
	public $view;

	public function run() {
		$form = new SubscribeForm();

		return $this->render($this->view, [
			'model' => $form,
		]);
	}
}