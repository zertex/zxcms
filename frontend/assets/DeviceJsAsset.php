<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class DeviceJsAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/libs/devicejs';
    public $js = [
	    YII_ENV_DEV ? 'device.js' : 'device.min.js'
    ];
}
