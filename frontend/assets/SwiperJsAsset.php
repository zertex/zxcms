<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class SwiperJsAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/libs/swiper342/js';
    public $js = [
	    YII_ENV_DEV ? 'swiper.jquery.js' : 'swiper.jquery.min.js'
    ];
}
