<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class MigrateAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-migrate';
    public $js = [
	    YII_ENV_DEV ? 'jquery-migrate.js' : 'jquery-migrate.min.js'
    ];
}
