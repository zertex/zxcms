<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class MaterialDesignAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/libs/materialdesignfonts2119';
    public $css = [
	    YII_ENV_DEV ? 'css/materialdesignicons.css' : 'css/materialdesignicons.min.css'
    ];
}
