<?php
/**
 * Created by Error202
 * Date: 02.02.2018
 */

namespace frontend\components;

use core\entities\user\User;
use yii\base\ActionFilter;
use Yii;
use yii\helpers\Url;

class SiteAccess extends ActionFilter
{
	public function beforeAction($action): bool
    {
		if (!isset(Yii::$app->user->identity->user)) {
			return true;
		}

		/* @var $user User */
		$user = Yii::$app->user->identity->user;

		if (empty($user->email) || $user->email_confirm_token || empty($user->username)) {

			if (empty($user->email)) {
				Yii::$app->session->addFlash('error', Yii::t('auth', 'Specify email please'));
			}

			if ($user->email_confirm_token) {
				Yii::$app->session->addFlash('error', Yii::t('auth', 'Confirm email please'));
			}

			if (empty($user->username)) {
				Yii::$app->session->addFlash('error', Yii::t('auth', 'Specify username please'));
			}

			Yii::$app->response->redirect(Url::to([
				'/account/profile/edit',
				'return' => Yii::$app->request->url,
			]));
			return false;
		}
		return true;
	}
}
