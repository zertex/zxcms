<?php
/**
 * Created by Error202
 * Date: 23.07.2018
 */

namespace frontend\components;

use yii\base\Theme;
use yii\web\Controller;
use Yii;

class FrontendController extends Controller
{
    public function init()
    {
        parent::init();

        // language
        $languages          = \Yii::$app->params['translatedLanguages'];
        $language           = Yii::$app->request->get('language');
        Yii::$app->language = $language && in_array($language, $languages) ? $language : Yii::$app->language;

        // themes
        $theme                 = Yii::$app->params['settings']['design']['theme'] ?? 'start';
        Yii::$app->view->theme = new Theme([
            'basePath' => '@webroot/themes/' . $theme,
            'baseUrl'  => '@web/themes/' . $theme,
            'pathMap'  => [
                '@common/modules'   => '@webroot/themes/' . $theme . '/modules',
                '@frontend/views'   => '@webroot/themes/' . $theme,
                '@frontend/widgets' => '@webroot/themes/' . $theme . '/widgets',
            ],

        ]);

        // site name
        Yii::$app->name = Yii::$app->params['settings']['site']['name'];
    }
}
