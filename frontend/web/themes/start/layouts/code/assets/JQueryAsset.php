<?php
/**
 * Created by Error202
 * Date: 21.06.2018
 */

namespace frontend\web\themes\start\layouts\code\assets;


use yii\web\AssetBundle;

class JQueryAsset extends AssetBundle
{
	public $basePath = '@webroot/themes/start/vendor/jquery';
	public $baseUrl = '@web/themes/start/vendor/jquery';
	public $css = [];
	public $js = [
		'jquery.min.js',
	];
	public $depends = [
	];
}