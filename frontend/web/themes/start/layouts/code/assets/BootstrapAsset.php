<?php
/**
 * Created by Error202
 * Date: 21.06.2018
 */

namespace frontend\web\themes\start\layouts\code\assets;


use yii\web\AssetBundle;

class BootstrapAsset extends AssetBundle
{
	public $basePath = '@webroot/themes/start/vendor/bootstrap';
	public $baseUrl = '@web/themes/start/vendor/bootstrap';
	public $css = [
		'css/bootstrap.min.css',
	];
	public $js = [
		'js/bootstrap.bundle.min.js',
	];
	public $depends = [
	];
}