<?php $this->beginContent('@frontend/web/themes/start/layouts/main.php') ?>

<!-- Page Content -->
<div class="container">

	<?= \yii\widgets\Breadcrumbs::widget([
		'tag' => 'ul',
		'itemTemplate' => '<li class="breadcrumb-item">{link}</li>' . "\n",
		'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>' . "\n",
		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>

	<div class="row">

		<!-- Page Content -->
		<div class="col-md-12">

			<?= $content ?>

		</div>

	</div>
	<!-- /.row -->

</div>
<!-- /.container -->
<?php $this->endContent() ?>

