<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */


$this->title = Yii::t('blog', 'Blog');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1 class="my-4"><?= Yii::t('blog_public', 'Blog') ?></h1>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider
]) ?>