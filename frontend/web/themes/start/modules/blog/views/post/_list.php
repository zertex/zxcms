<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */

?>

<?= \yii\widgets\ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "<div class='row row-30'>{items}</div>\n<nav class='pagination-wrap' aria-label='Page navigation'>{pager}</nav>",
    'itemView' => '_post',
    'pager' => [
        //'pagination' => null,
	    'maxButtonCount' => 7,
	    'pageCssClass' => 'page-item',
	    'options' => [
	        'class' => 'pagination',
	    ],
	    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link disabled'],
	    'activePageCssClass' => 'active',
	    'linkOptions' => [
	    	'class' => 'page-link',
	    ],
	    'prevPageLabel' => '<span class="fa fa-angle-left"></span>',
	    'nextPageLabel' => '<span class="fa fa-angle-right"></span>',
    ],
    'itemOptions' => [
	    'tag' => false
    ],
	'options' => [
		'tag' => false,
		'class' => 'row row-30',
		'id' => false,
	],
]) ?>

