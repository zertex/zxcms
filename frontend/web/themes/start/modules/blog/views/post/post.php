<?php

/* @var $this yii\web\View */
/* @var $post \common\modules\blog\entities\BlogPost */

use common\modules\blog\widgets\CommentsWidget;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $post->getSeoTitle();

$this->registerMetaTag(['name' =>'description', 'content' => $post->translation->meta_description]);
$this->registerMetaTag(['name' =>'keywords', 'content' => $post->translation->meta_keywords]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('blog_public', 'Blog'), 'url' => ['/blog/post/index']];
$this->params['breadcrumbs'][] = ['label' => $post->category->translation->name, 'url' => ['/blog/post/category', 'id' => $post->category->id]];
$this->params['breadcrumbs'][] = $post->translation->title;

$this->params['active_category'] = $post->category;

$tagLinks = [];
foreach ($post->tags as $tag) {
	$tagLinks[] = '<li>' . Html::a(Html::encode($tag->name), ['tag', 'id' => $tag->id]) . '</li>';
}

$url = Url::to(['/blog/post/post', 'id' =>$post->id]);
$url_full = Yii::$app->params['frontendHostInfo'] . Url::to(['/blog/post/post', 'id' =>$post->id]);
?>

<!-- Post Content Column -->

			<!-- Title -->
			<h1 class="mt-4"><?= Html::encode($post->translation->title) ?></h1>

			<hr>

			<!-- Date/Time -->
			<time class="blog-post-time" datetime="<?= date('Y') ?>"><?= Yii::$app->formatter->asDate($post->published_at, 'php:d F, Y') ?></time>

			<hr>

			<!-- Preview Image -->
			<?php if ($post->video): ?>
				<!-- <iframe style="width: 100%" width="560" height="315" src="https://www.youtube.com/embed/< ?= \core\helpers\PostHelper::parseYoutubeUrl($post->video) ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
				<?= \core\widgets\VideoWidget::widget([
					'src' => $post->video,
				]) ?>
			<?php else: ?>
			<img class="img-fluid rounded" src="<?= Html::encode($post->getThumbFileUrl('image', 'blog_post')) ?>" alt="">
			<?php endif; ?>
			<hr>

			<!-- Post Content -->
			<p class="lead"><?= Html::encode($post->translation->description) ?></p>

			<!-- < ?= $post->content ?> -->

			<?= Yii::$app->formatter->asHtml($post->translation->content, [
			'Attr.AllowedRel' => array('nofollow'),
			'HTML.SafeObject' => true,
			'Output.FlashCompat' => true,
			'HTML.SafeIframe' => true,
			'URI.SafeIframeRegexp'=>'%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
			]) ?>

<?= \common\modules\blog\widgets\TagWidget::widget([
	'post_id' => $post->id
]) ?>

<hr>

<?= CommentsWidget::widget([
'post' => $post,
]) ?>








<!--
<div class="blog-post">

	<div class="badge badge-secondary">< ?= $post->category->name ?>
	</div>
	<h3 class="blog-post-title">< ?= Html::encode($post->title) ?></h3>
	<div class="blog-post-header">
		<div class="blog-post-meta">
			<time class="blog-post-time" datetime="< ?= date('Y') ?>"><span class="icon fa fa-calendar"></span>< ?= Yii::$app->formatter->asDate($post->published_at, 'php:d F, Y') ?></time>
			<div class="blog-post-comment"><span class="icon fa fa-comment"></span>< ?= $post->comments_count ?></div>
			<div class="blog-post-view"><span class="icon fa fa-eye"></span>< ?= $post->views ?></div>
		</div>
	</div>
	<div class="blog-post-author-quote">
		<p>< ?= $post->description ?></p>
	</div>
	<div class="blog-post-share">
		<p>< ?= Yii::t('blog', 'Share') ?></p>
		<ul class="group">
			<li><a target="_blank" class="icon fa fa-facebook" href="https://www.facebook.com/sharer.php?src=sp&u=< ?= urlencode($url_full) ?>"></a></li>
			<li><a target="_blank" class="icon fa fa-odnoklassniki" href="https://connect.ok.ru/offer?url=< ?= urlencode($url_full) ?>"></a></li>
			<li><a target="_blank" class="icon fa fa-google-plus" href="https://plus.google.com/share?url=< ?= urlencode($url_full) ?>"></a></li>
			<li><a target="_blank" class="icon fa fa-vk" href="https://vk.com/share.php?url=< ?= urlencode($url_full) ?>"></a></li>
		</ul>
	</div>
	<div class="blog-post-content">

		<img src="< ?= Html::encode($post->getThumbFileUrl('image', '683_407')) ?>" alt="< ?= Html::encode($post->title) ?>" width="683" height="407" />

		< ?= Yii::$app->formatter->asHtml($post->content, [
			'Attr.AllowedRel' => array('nofollow'),
			'HTML.SafeObject' => true,
			'Output.FlashCompat' => true,
			'HTML.SafeIframe' => true,
			'URI.SafeIframeRegexp'=>'%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
		]) ?>

	</div>
</div>

< ?= \frontend\widgets\post\PostByTagsWidget::widget([
	'count' => 10,
	'view' => 'slider-relative',
	'post' => $post,
]) ?>


<ul class="list-tags" style="margin-top:20px">< ?= implode(', ', $tagLinks) ?></ul>

< ?= CommentsWidget::widget([
    'post' => $post,
]) ?>

-->
