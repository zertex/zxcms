<?php $this->beginContent('@frontend/web/themes/start/layouts/main.php') ?>

<!-- Page Content -->
<div class="container">

    <?= \yii\widgets\Breadcrumbs::widget([
        'tag'                => 'ul',
        'itemTemplate'       => '<li class="breadcrumb-item">{link}</li>' . "\n",
        'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>' . "\n",
        'links'              => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

    <div class="row">

        <!-- Page Content -->
        <div class="col-md-8">

            <?= $content ?>

        </div>

        <!-- Sidebar -->
        <div class="col-md-4">

            <!-- Search Widget -->
            <div class="card my-4">
                <h5 class="card-header">Search</h5>
                <div class="card-body">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Go!</button>
                </span>
                    </div>
                </div>
            </div>

            <?= \common\modules\shop\widgets\ShopCategoriesWidget::widget([
                'active' => $this->params['active_category'] ?? null
            ]) ?>

            <!-- Side Widget -->
            <div class="card my-4">
                <h5 class="card-header">Side Widget</h5>
                <div class="card-body">
                    You can put anything you want inside of these side widgets. They are easy to use, and feature the
                    new Bootstrap 4 card containers!
                </div>
            </div>

        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
<?php $this->endContent() ?>

