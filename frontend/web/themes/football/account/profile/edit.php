<?php

/* @var $this yii\web\View */
/* @var $model \core\forms\user\ProfileEditForm */
/* @var $user \core\entities\user\User */

use kartik\form\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('user', 'Edit Profile');
//$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['cabinet/default/index']];
$this->params['breadcrumbs'][] = Yii::t('user', 'Profile');

?>

<article class="heading-component">
	<div class="heading-component-inner">
		<h5 class="heading-component-title"><?= $this->title ?></h5>
	</div>
</article>

<div class="user-update">

    <div class="row">

        <div class="col-sm-12">

            <?php $form = ActiveForm::begin(['id' => 'form-profile']); ?>

	        <div class="form-wrap">
		        <?= $form->field($model, 'username', [
			        'template' => '{input}{error}',
			        'errorOptions' => [ 'class' => 'form-validation' ]
		        ])->textInput([
			        'class' => 'form-input form-control-has-validation form-control-last-child',
			        'placeholder' => Yii::t('auth', 'Username'),
		        ])->label(false) ?>
	        </div>

	        <div class="form-wrap">
		        <?= $form->field($model, 'email', [
			        'template' => '{input}{error}',
			        'errorOptions' => [ 'class' => 'form-validation' ]
		        ])->textInput([
			        'class' => 'form-input form-control-has-validation form-control-last-child',
			        'placeholder' => Yii::t('auth', 'E-mail'),
		        ])->label(false) ?>
	        </div>

	        <div class="form-wrap">
		        <?= $form->field($model, 'password', [
			        'template' => '{input}{error}',
			        'errorOptions' => [ 'class' => 'form-validation' ]
		        ])->passwordInput([
			        'class' => 'form-input form-control-has-validation form-control-last-child',
			        'placeholder' => Yii::t('auth', 'New password'),
		        ])->label(false) ?>
	        </div>

	        <div>
		        <?= Html::submitButton(Yii::t('user', 'Save'), [
			        'class' => 'button button-lg button-primary',
			        'name' => 'profile-save-button'
		        ]) ?>
	        </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
