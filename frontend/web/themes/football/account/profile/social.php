<?php

use yii\authclient\widgets\AuthChoice;

/* @var $this yii\web\View */


$this->title = Yii::t('user', 'Social Networks');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Profile'), 'url' => ['/users/account/profile/edit']];
$this->params['breadcrumbs'][] = $this->title;
?>

<article class="heading-component">
	<div class="heading-component-inner">
		<h5 class="heading-component-title"><?= $this->title ?></h5>
	</div>
</article>


<div class="user-update">

    <div class="row">
        <div class="col-sm-12">

            <p><?= Yii::t('user', 'Specify a social network to associate with the account.') ?></p>

	        <?php $authAuthChoice = AuthChoice::begin([
		        'baseAuthUrl' => ['/auth/network/auth'],
		        'options' => [
			        'class' => 'group-sm group-sm-justify group-middle'
		        ]
	        ]); ?>

	        <?php foreach ($authAuthChoice->getClients() as $client): ?>
		        <?= $authAuthChoice->clientLink($client, null, [
			        'class' => 'button button-' . $client->getId() . ' button-icon button-icon-left button-round',
		        ]) ?>
	        <?php endforeach; ?>

	        <?php AuthChoice::end(); ?>

        </div>
    </div>

</div>
