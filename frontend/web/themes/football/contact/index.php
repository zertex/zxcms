<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \core\forms\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = Yii::t('main', 'Contacts');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Section Contact-->
<section class="section section-variant-1 bg-gray-100">
	<div class="container">
		<div class="row row-50">
			<div class="col-lg-7 col-xl-8">
				<!-- Heading Component-->
				<article class="heading-component">
					<div class="heading-component-inner">
						<h5 class="heading-component-title"><?= Yii::t('main', 'Get in touch') ?>
						</h5>
					</div>
				</article>

				<?= \frontend\widgets\Alert::widget() ?>

				<?php $form = ActiveForm::begin([
					'id' => 'contact-form',
					'options' => [
						'class' => 'rd-form rd-mailform',
					],
					'fieldConfig' => [
						'options' => [
							'tag' => false,
						],
					],
					//'enableClientValidation'=>false,
				]); ?>
				<!-- <form class="rd-form rd-mailform" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php"> -->
					<div class="row row-10 row-narrow">
						<div class="col-md-6">
							<!--<div class="form-wrap">
								<label class="form-label" for="form-user-name-1">Your Name</label>
								<input class="form-input" id="form-user-name-1" type="text" name="name" data-constraints="@Required">
							</div>-->

							<div class="form-wrap">
								<div class="field-contactform-name">
									<?= $form->field($model, 'name', [
										'template' => '{input}{error}',
										'errorOptions' => [ 'class' => 'form-validation' ],
									])->textInput([
										'autofocus' => true,
										'class' => 'form-input',
										'placeholder' => Yii::t('main', 'Your name'),
									])->label(false) ?>
								</div>
							</div>

						</div>
						<div class="col-md-6">
							<!-- <div class="form-wrap">
								<label class="form-label" for="form-user-phone">Phone</label>
								<input class="form-input" id="form-user-phone" type="text" name="phone" data-constraints="@Numeric @Required">
							</div> -->

							<div class="form-wrap">
								<div class="field-contactform-subject">
									<?= $form->field($model, 'subject', [
										'template' => '{input}{error}',
										'errorOptions' => [ 'class' => 'form-validation' ]
									])->textInput([
										'class' => 'form-input form-control-has-validation form-control-last-child',
										'placeholder' => Yii::t('main', 'Subject'),
									])->label(false) ?>
								</div>
							</div>
						</div>
						<div class="col-sm-12">
							<!--<div class="form-wrap">
								<label class="form-label" for="form-message">Message</label>
								<textarea class="form-input" id="form-message" name="message" data-constraints="@Required"></textarea>
							</div> -->

							<div class="form-wrap">
								<div class="field-contactform-body">
									<?= $form->field($model, 'body', [
										'template' => '{input}{error}',
										'errorOptions' => [ 'class' => 'form-validation' ]
									])->textarea([
										'class' => 'form-input form-control-has-validation form-control-last-child',
										'placeholder' => Yii::t('main', 'Message'),
									])->label(false) ?>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<!--< ?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
								'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
							]) ?> -->
							<div class="form-wrap">
								<div class="field-contactform-verifycode">
									<?= $form->field($model, 'verifyCode', [
										'errorOptions' => [ 'class' => 'form-validation' ],
									])->widget(Captcha::className(), [
										'options' => [
											'class' => 'form-input form-control-has-validation form-control-last-child',
											'placeholder' => Yii::t('main', 'Verification Code'),
										],
										'template' => '<div class="row"><div class="col-lg-3" style="text-align: center">{image}</div><div class="col-lg-9">{input}</div></div>',
									])->label(false) ?>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!--<div class="form-wrap">
								<label class="form-label" for="form-email">E-mail</label>
								<input class="form-input" id="form-email" type="email" name="email" data-constraints="@Email @Required">
							</div>-->

							<div class="form-wrap">
								<div class="field-contactform-email">
									<?= $form->field($model, 'email', [
										'template' => '{input}{error}',
										'errorOptions' => [ 'class' => 'form-validation' ]
									])->textInput([
										'type' => 'email',
										'class' => 'form-input form-control-has-validation form-control-last-child',
										'placeholder' => Yii::t('main', 'E-mail'),
									])->label(false) ?>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<?= Html::submitButton(Yii::t('main', 'Submit'), [
								'class' => 'button button-lg button-primary button-block',
								'name' => 'submit-button'
							]) ?>
							<!-- <button class="button button-lg button-primary button-block" type="submit">Send message</button> -->
						</div>
					</div>
				<?php ActiveForm::end(); ?>
			</div>
			<div class="col-lg-5 col-xl-4">
				<!-- Heading Component-->
				<article class="heading-component">
					<div class="heading-component-inner">
						<h5 class="heading-component-title"><?= Yii::t('main', 'Contact Details') ?>
						</h5>
					</div>
				</article>

				<div class="contact-list">
					<dl>
						<dt><?= Yii::t('main', 'E-mail') ?></dt>
						<dd><span class="icon icon-primary mdi mdi-email-outline"></span><a class="link" href="mailto:#">info@oursite.ru</a></dd>
					</dl>
					<dl>
						<dt><?= Yii::t('main', 'Address') ?></dt>
						<dd><span class="icon icon-primary mdi mdi-map-marker"></span><a class="link" href="#">117422, Россия, г.Млсква, ул.Ясная, д.1</a></dd>
					</dl>
					<dl>
						<dt><?= Yii::t('main', 'Phone') ?></dt>
						<dd><span class="icon icon-primary mdi mdi-phone"></span><a class="link link-md" href="callto:#">+7 499 111 11 11</a></dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
</section>
















<!--
<section class="section section-variant-1 bg-gray-100">
	<div class="container">
		<div class="row row-50 justify-content-center">
			<div class="col-md-10 col-lg-8 col-xl-6">


				<div class="row">
					<div class="col-sm-12">

						<div class="card-contacts" id="card-l-r">
							<div class="card-top-panel">
								<div class="card-top-panel-left">
									<h5 class="card-title card-title-login"><?= Yii::t('main', 'Contact form') ?></h5>
								</div>
							</div>
							<div class="card-form card-form-login">
								<p>
									<?= Yii::t('main', 'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.') ?>
								</p>
								<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>


                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'subject') ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

								<div>
									<?= Html::submitButton(Yii::t('main', 'Submit'), [
										'class' => 'button button-lg button-primary button-block',
										'name' => 'signup-button'
									]) ?>
								</div>

								<?php ActiveForm::end(); ?>

							</div>
						</div>

					</div>
				</div>


			</div>
		</div>
	</div>
-->