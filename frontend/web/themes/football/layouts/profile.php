<?php

use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $content string
 */

?>
<?php $this->beginContent('@frontend/web/themes/sport/layouts/main.php') ?>


	<!-- Section Breadcrumbs-->
	<section class="section parallax-container breadcrumbs-wrap" data-parallax-img="/themes/sport/images/news.jpg">
		<div class="parallax-content breadcrumbs-custom context-dark">
			<div class="container">
				<h3 class="breadcrumbs-custom-title"><?= Yii::$app->user->identity->user->username ?></h3>
				<?= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					'options' => ['class' => 'breadcrumbs-custom-path'],
					'encodeLabels' => false,
				]) ?>
			</div>
		</div>
	</section>

	<!-- Content-->
	<section class="section section-sm bg-gray-100">
		<div class="container">
			<div class="row row-30">
				<div class="col-lg-8">
					<?= \frontend\widgets\Alert::widget() ?>
					<?= $content ?>
				</div>
				<div class="col-lg-4">
					<div class="block-aside">

						<div class="block-aside-item">
							<!-- Heading Component-->
							<article class="heading-component">
								<div class="heading-component-inner">
									<h5 class="heading-component-title"><?= Yii::t('user', 'User menu') ?>
									</h5>
								</div>
							</article>

							<!--Block Categories-->
							<div class="block-categories">
								<ul class="list-marked list-marked-categories">
									<li><a href="<?= Html::encode(Url::to(['/account/profile/edit'])) ?>"><?= Yii::t('user', 'Private') ?></a></li>
									<li><a href="<?= Html::encode(Url::to(['/account/profile/social'])) ?>"><?= Yii::t('user', 'Social Networks') ?></a></li>
									<li><a href="<?= Html::encode(Url::to(['/auth/auth/logout'])) ?>"><?= Yii::t('auth', 'Sign out') ?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php $this->endContent() ?>