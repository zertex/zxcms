<?php

use yii\widgets\Breadcrumbs;

/**
 * @var $this \yii\web\View
 * @var $content string
 */

//$this->params['breadcrumbs'][] = Yii::t('post', 'News');
?>
<?php $this->beginContent('@frontend/web/themes/sport/layouts/main.php') ?>


	<!-- Section Breadcrumbs-->
	<section class="section parallax-container breadcrumbs-wrap" data-parallax-img="/themes/sport/images/news.jpg">
		<div class="parallax-content breadcrumbs-custom context-dark">
			<div class="container">
				<h3 class="breadcrumbs-custom-title"><?= Yii::t('post', 'News') ?></h3>
				<?= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					'options' => ['class' => 'breadcrumbs-custom-path'],
					'encodeLabels' => false,
				]) ?>
			</div>
		</div>
	</section>

	<!-- News 2-->
	<section class="section section-sm bg-gray-100">
		<div class="container">
			<div class="row row-30">
				<div class="col-lg-8">
					<?= $content ?>
				</div>
				<div class="col-lg-4">
					<!-- Blog Aside-->
					<div class="block-aside">

						<?= \frontend\widgets\post\PostCategoriesWidget::widget([
							'type_id' => 2,
						]) ?>


						<!-- Right news block-->
						<?= \frontend\widgets\post\PostWidget::widget([
							'type' => 2,
							'filter' => \frontend\widgets\post\PostWidget::FILTER_LAST,
							'view' => 'news',
							'count' => 4
						]) ?>

						<?= \frontend\widgets\general\FollowWidget::widget() ?>

						<?= \frontend\widgets\post\LastCommentsWidget::widget([
							'count' => 3
						]) ?>

						<?= \frontend\widgets\general\SubscribeWidget::widget([
							'view' => 'subscribe',
						]) ?>

						<?= \frontend\widgets\post\PostTagWidget::widget([
							'type_id' => 2,
						]) ?>


					</div>
				</div>
			</div>
		</div>
	</section>

<?php $this->endContent() ?>