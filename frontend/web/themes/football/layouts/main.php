<?php
/**
 * @var $this \yii\web\View
 */
use frontend\assets\AppAsset;

AppAsset::register( $this );
rmrevin\yii\fontawesome\cdn\AssetBundle::register( $this );

$js = <<<JS
		var events_slider = $('#homepage_carousel__2').bxSlider({
			infiniteLoop: true,
			hideControlOnEnd: true,
			minSlides: 1,
			maxSlides: 1,
			moveSlides: 1,
			slideMargin: 0,
			pager: false,
			prevText: '',
			nextText: '',
			controls: false
		});
		
		$('#event-arrow-prev').on('click', function(){
            events_slider.goToPrevSlide();
            return false;
        });
        
        $('#event-arrow-next').on('click', function(){
            events_slider.goToNextSlide();
            return false;
        })
JS;
$this->registerJs($js, $this::POS_READY)
?>
<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html class="wide wow-animation desktop landscape rd-navbar-static-linked" lang="<?= Yii::$app->language ?>">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<!-- Site Title-->
		<title>Home</title>
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<link rel="icon" href="favicon.ico" type="image/x-icon">

		<?php $this->head() ?>
		<!-- Stylesheets-->
		<link rel="stylesheet" type="text/css" href="/themes/sport/images/Home_files/css.css">

		<!--[if lt IE 10]>
		<div
			style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;">
			<a href="http://windows.microsoft.com/en-US/internet-explorer/"><img
				src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
				alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a>
		</div>
		<script src="js/html5shiv.min.js"></script>
		<![endif]-->

		<link id="main-styles-link" rel="stylesheet" href="/themes/sport/images/Home_files/style.css">
		<script type="text/javascript" async="" src="/themes/sport/images/Home_files/ec.js"></script>
		<script type="text/javascript" async="" src="/themes/sport/images/Home_files/analytics.js"></script>
	</head>
	<body class="animsition-overlay" style="animation-duration: 800ms;">
	<div class="preloader loaded">
		<div class="preloader-body">
			<div class="preloader-item"></div>
		</div>
	</div>
	<!-- Page-->
	<div class="page">
		<!-- Page Header-->
		<header class="section page-header rd-navbar-dark">
			<!-- RD Navbar-->
			<div class="rd-navbar-wrap" style="height: 249.033px;">
				<nav class="rd-navbar rd-navbar-classic rd-navbar-original rd-navbar-static"
				     data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed"
				     data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed"
				     data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static"
				     data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static"
				     data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="166px"
				     data-xl-stick-up-offset="166px" data-xxl-stick-up-offset="166px" data-lg-stick-up="true"
				     data-xl-stick-up="true" data-xxl-stick-up="true">
					<div class="rd-navbar-panel">
						<!-- RD Navbar Toggle-->
						<button class="rd-navbar-toggle toggle-original" data-rd-navbar-toggle=".rd-navbar-main">
							<span></span></button>
						<!-- RD Navbar Panel-->
						<div class="rd-navbar-panel-inner container">
							<div
								class="rd-navbar-collapse rd-navbar-panel-item rd-navbar-panel-item-left toggle-original-elements">

								<!-- BX Slider - events-->
								<div class="owl-carousel-navbar owl-carousel-inline-outer">
									<div class="owl-inline-nav">
										<button class="owl-arrow owl-arrow-prev" id="event-arrow-prev"></button>
										<button class="owl-arrow owl-arrow-next" id="event-arrow-next"></button>
									</div>
									<div class="owl-carousel-inline-wrap">
										<div id="homepage_carousel__2" class="homepage_event_carousel row">
											<div class="owl-item cloned events col-sm-4 main_events_homepage item_1"
											     style="width: 1110px; margin-right: 10px;">
												<article class="post-inline">
													<time class="post-inline-time" datetime="2017">April 15,
														2017
													</time>
													<p class="post-inline-title">Atletico vs Dream Team</p>
												</article>
											</div>

											<div class="owl-item cloned events col-sm-4 main_events_homepage item_2"
											     style="width: 1110px; margin-right: 10px;">
												<article class="post-inline">
													<time class="post-inline-time" datetime="2017">April 14,
														2018
													</time>
													<p class="post-inline-title">Atletico vs Dream 2018</p>
												</article>
											</div>
										</div>
									</div>


								</div>


							</div>
							<div class="rd-navbar-panel-item rd-navbar-panel-item-right">
								<ul class="list-inline list-inline-bordered">
									<?php if (Yii::$app->user->isGuest): ?>
									<li>
										<a class="link link-icon link-icon-left link-classic" href="<?= \yii\helpers\Url::to(['/auth/auth/login']) ?>">
											<span class="icon fa fa-user"></span>
											<span class="link-icon-text"><?= Yii::t('auth', 'Sign in') ?></span>
										</a>
									</li>
									<?php else: ?>
									<li>
										<a class="link link-icon link-icon-left link-classic" href="<?= \yii\helpers\Url::to(['/account/profile/edit']) ?>">
											<span class="icon fa fa-user"></span>
											<span class="link-icon-text"><?= Yii::$app->user->identity->user->username ?></span>
										</a>
									</li>
									<li>
										<a class="link link-icon link-icon-left link-classic" href="<?= \yii\helpers\Url::to(['/auth/auth/logout']) ?>">
											<span class="icon fa fa-sign-out"></span>
											<span class="link-icon-text"><?= Yii::t('auth', 'Sign out') ?></span>
										</a>
									</li>
									<?php endif; ?>
								</ul>
							</div>
							<div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1 toggle-original"
							     data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
						</div>
					</div>
					<div class="rd-navbar-main toggle-original-elements">
						<div class="rd-navbar-main-top">
							<div class="rd-navbar-main-container container">
								<!-- RD Navbar Brand-->
								<div class="rd-navbar-brand"><a class="brand link-circle"
								                                href="https://livedemo00.template-help.com/wt_63853/soccer/index.html"><img
											class="brand-logo "
											src="/themes/sport/images/Home_files/logo-soccer-default-129x129.png" alt=""
											width="129" height="129"></a>
								</div>
								<!-- RD Navbar List-->
								<ul class="rd-navbar-list">
									<li class="rd-navbar-list-item"><a class="rd-navbar-list-link" href="#"><img
												src="/themes/sport/images/Home_files/partners-1-inverse-75x42.png"
												alt="" width="75" height="42"></a></li>
									<li class="rd-navbar-list-item"><a class="rd-navbar-list-link" href="#"><img
												src="/themes/sport/images/Home_files/partners-2-inverse-78x41.png"
												alt="" width="78" height="41"></a></li>
									<li class="rd-navbar-list-item"><a class="rd-navbar-list-link" href="#"><img
												src="/themes/sport/images/Home_files/partners-3-inverse-65x44.png"
												alt="" width="65" height="44"></a></li>
								</ul>
								<!-- RD Navbar Search-->
								<div class="rd-navbar-search toggle-original-elements">
									<button class="rd-navbar-search-toggle toggle-original"
									        data-rd-navbar-toggle=".rd-navbar-search"><span></span></button>
									<form class="rd-search" action="search-results.html"
									      data-search-live="rd-search-results-live" method="GET">
										<div class="form-wrap">
											<label class="form-label rd-input-label" for="rd-navbar-search-form-input">Enter
												your search request here...</label>
											<input class="rd-navbar-search-form-input form-input"
											       id="rd-navbar-search-form-input" name="s" autocomplete="off"
											       type="text">
											<div class="rd-search-results-live cleared"
											     id="rd-search-results-live"></div>
										</div>
										<button class="rd-search-form-submit fa fa-search" type="submit"></button>
									</form>
								</div>
							</div>
						</div>
						<div class="rd-navbar-main-bottom rd-navbar-darker">
							<div class="rd-navbar-main-container container">
								<!-- RD Navbar Nav-->
								<ul class="rd-navbar-nav">
									<li class="rd-nav-item active"><a class="rd-nav-link"
									                                  href="https://livedemo00.template-help.com/wt_63853/soccer/index.html">Главная</a>
									</li>
									<li class="rd-nav-item rd-navbar--has-megamenu rd-navbar-submenu"><a
											class="rd-nav-link" href="#">Услуги</a>
										<!-- RD Navbar Megamenu-->
										<article
											class="rd-menu rd-navbar-megamenu rd-megamenu-2-columns context-light rd-navbar-open-right">
											<div class="rd-megamenu-main">
												<div class="rd-megamenu-item rd-megamenu-item-nav">
													<!-- Heading Component-->
													<article class="heading-component heading-component-simple">
														<div class="heading-component-inner">
															<h5 class="heading-component-title">Elements
															</h5>
														</div>
													</article>

													<div class="rd-megamenu-list-outer">
														<ul class="rd-megamenu-list">
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/shortcodes.html">Shortcodes</a>
															</li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/typography.html">Typography</a>
															</li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/blog-elements.html">Blog
																	Elements</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/sport-elements.html">Sport
																	Elements</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/shop-elements.html">Shop
																	Elements</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/404-error-page.html">404
																	Error Page</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/privacy-policy.html">Privacy
																	Policy</a></li>
														</ul>
														<ul class="rd-megamenu-list">
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/about-us.html">About
																	Us</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/gallery.html">Gallery</a>
															</li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/albums.html">Albums</a>
															</li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/search-results.html">Search
																	Results</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/coming-soon.html">Coming
																	Soon</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/contact-us.html">Contact
																	Us</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/login-and-register.html">Login
																	and Register</a></li>
														</ul>
													</div>
												</div>
												<div class="rd-megamenu-item rd-megamenu-item-content">
													<!-- Heading Component-->
													<article class="heading-component heading-component-simple">
														<div class="heading-component-inner">
															<h5 class="heading-component-title">Latest News
															</h5><a class="button button-xs button-gray-outline"
															        href="https://livedemo00.template-help.com/wt_63853/soccer/news-1.html">See
																all News</a>
														</div>
													</article>

													<div class="row row-20">
														<div class="col-lg-6">
															<!-- Post Classic-->
															<article class="post-classic">
																<div class="post-classic-aside"><a
																		class="post-classic-figure"
																		href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
																			src="/themes/sport/images/Home_files/megamenu-post-1-93x94.jpg"
																			alt="" width="93" height="94"></a></div>
																<div class="post-classic-main">
																	<!-- Badge-->
																	<div class="badge badge-secondary">The Team
																	</div>
																	<p class="post-classic-title"><a
																			href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Raheem
																			Sterling turns the tide for Manchester</a>
																	</p>
																	<div class="post-classic-time"><span
																			class="icon fa fa-calendar"></span>
																		<time datetime="2017">April 15, 2017</time>
																	</div>
																</div>
															</article>
														</div>
														<div class="col-lg-6">
															<!-- Post Classic-->
															<article class="post-classic">
																<div class="post-classic-aside"><a
																		class="post-classic-figure"
																		href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
																			src="/themes/sport/images/Home_files/megamenu-post-2-93x94.jpg"
																			alt="" width="93" height="94"></a></div>
																<div class="post-classic-main">
																	<!-- Badge-->
																	<div class="badge badge-primary">The League
																	</div>
																	<p class="post-classic-title"><a
																			href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Prem
																			in 90 seconds: Chelsea's crisis is over!</a>
																	</p>
																	<div class="post-classic-time"><span
																			class="icon fa fa-calendar"></span>
																		<time datetime="2017">April 15, 2017</time>
																	</div>
																</div>
															</article>
														</div>
														<div class="col-lg-6">
															<!-- Post Classic-->
															<article class="post-classic">
																<div class="post-classic-aside"><a
																		class="post-classic-figure"
																		href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
																			src="/themes/sport/images/Home_files/megamenu-post-3-93x94.jpg"
																			alt="" width="93" height="94"></a></div>
																<div class="post-classic-main">
																	<!-- Badge-->
																	<div class="badge badge-primary">The League
																	</div>
																	<p class="post-classic-title"><a
																			href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Good
																			vibes back at struggling Schalke</a></p>
																	<div class="post-classic-time"><span
																			class="icon fa fa-calendar"></span>
																		<time datetime="2017">April 15, 2017</time>
																	</div>
																</div>
															</article>
														</div>
														<div class="col-lg-6">
															<!-- Post Classic-->
															<article class="post-classic">
																<div class="post-classic-aside"><a
																		class="post-classic-figure"
																		href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
																			src="/themes/sport/images/Home_files/megamenu-post-4-93x94.jpg"
																			alt="" width="93" height="94"></a></div>
																<div class="post-classic-main">
																	<!-- Badge-->
																	<div class="badge badge-primary">The League
																	</div>
																	<p class="post-classic-title"><a
																			href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Liverpool
																			in desperate need of backup players</a></p>
																	<div class="post-classic-time"><span
																			class="icon fa fa-calendar"></span>
																		<time datetime="2017">April 15, 2017</time>
																	</div>
																</div>
															</article>
														</div>
													</div>
												</div>
											</div>
											<!-- Event Teaser-->
											<article class="event-teaser rd-megamenu-footer">
												<div class="event-teaser-header">
													<div class="event-teaser-caption">
														<h5 class="event-teaser-title">Final Europa League 2017</h5>
														<time class="event-teaser-time" datetime="2017">Saturday, April
															14, 2017
														</time>
													</div>
													<div class="event-teaser-teams">
														<div class="event-teaser-team">
															<div
																class="unit unit-spacing-xs unit-horizontal align-items-center">
																<div class="unit-left"><img
																		class="event-teaser-team-image"
																		src="/themes/sport/images/Home_files/team-bavaria-fc-59x54.png"
																		alt="" width="59" height="54">
																</div>
																<div class="unit-body">
																	<p class="heading-7">Bavaria</p>
																	<p class="text-style-1">Germany</p>
																</div>
															</div>
														</div>
														<div class="event-teaser-team-divider"><span
																class="event-teaser-team-divider-text">VS</span></div>
														<div class="event-teaser-team">
															<div
																class="unit unit-spacing-xs unit-horizontal align-items-center">
																<div class="unit-left"><img
																		class="event-teaser-team-image"
																		src="/themes/sport/images/Home_files/team-atletico-50x50.png"
																		alt="" width="50" height="50">
																</div>
																<div class="unit-body">
																	<p class="heading-7">Atletico</p>
																	<p class="text-style-1">USA</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="event-teaser-countdown event-teaser-highlighted">
													<!-- Countdown-->
													<div class="countdown countdown-classic is-countdown"
													     data-type="until" data-time="31 Dec 2018 16:00"
													     data-format="dhms" data-style="short"><span
															class="countdown-row countdown-show4"><span
																class="countdown-section"><span
																	class="countdown-amount">347</span><span
																	class="countdown-period">Days</span></span><span
																class="countdown-section"><span
																	class="countdown-amount">16</span><span
																	class="countdown-period">Hors</span></span><span
																class="countdown-section"><span
																	class="countdown-amount">8</span><span
																	class="countdown-period">Mins</span></span><span
																class="countdown-section"><span
																	class="countdown-amount">7</span><span
																	class="countdown-period">Secs</span></span></span>
													</div>
												</div>
												<div class="event-teaser-aside"><a class="event-teaser-link" href="#">Buy
														Tickets</a></div>
											</article>
										</article>
									</li>
									<li class="rd-nav-item rd-navbar--has-dropdown rd-navbar-submenu"><a
											class="rd-nav-link" href="#">Новости</a>
										<!-- RD Navbar Dropdown-->
										<ul class="rd-menu rd-navbar-dropdown">
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/blog-elements.html">Blog
													Elements</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-1.html">News
													1</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-2.html">News
													2</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-3.html">News
													3</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-4.html">News
													4</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-5.html">News
													5</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Blog
													post</a></li>
										</ul>
									</li>
									<li class="rd-nav-item rd-navbar--has-dropdown rd-navbar-submenu"><a
											class="rd-nav-link" href="#">Team</a>
										<!-- RD Navbar Dropdown-->
										<ul class="rd-menu rd-navbar-dropdown">
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/game-overview.html">Game
													Overview</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/roster.html">Roster</a>
											</li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/standings.html">Standings</a>
											</li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/latest-results-1.html">Latest
													Results 1</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/latest-results-2.html">Latest
													Results 2</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/game-schedule.html">Game
													schedule</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">Player
													Page</a></li>
										</ul>
									</li>
									<li class="rd-nav-item rd-navbar--has-dropdown rd-navbar-submenu"><a
											class="rd-nav-link" href="#">Shop</a>
										<!-- RD Navbar Dropdown-->
										<ul class="rd-menu rd-navbar-dropdown">
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/shop-elements.html">Shop
													Elements</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/grid-shop.html">Grid
													Shop</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/list-shop.html">List
													Shop</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/fullwidth-shop.html">Fullwidth
													Shop</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Product
													Page</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html">Shopping
													Cart</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/checkout.html">Checkout</a>
											</li>
										</ul>
									</li>
								</ul>
								<div class="rd-navbar-main-element">
									<ul class="list-inline list-inline-sm">
										<li><a class="icon icon-xs icon-light fa fa-facebook" href="#"></a></li>
										<li><a class="icon icon-xs icon-light fa fa-twitter" href="#"></a></li>
										<li><a class="icon icon-xs icon-light fa fa-google-plus" href="#"></a></li>
										<li><a class="icon icon-xs icon-light fa fa-instagram" href="#"></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</nav>
			</div>
		</header>

		<?= $content ?>

		<!-- Page Footer-->
		<footer class="section footer-classic footer-classic-dark context-dark">
			<div class="footer-classic-main">
				<div class="container">
					<p class="heading-7"><?= Yii::t('user', 'Subscribe to our Newsletter') ?></p>
					<!-- RD Mailform-->
					<?= \frontend\widgets\general\SubscribeWidget::widget([
						'view' => 'bottom_subscribe',
					]) ?>

					<div class="row row-50">
						<div class="col-lg-5 text-center text-sm-left">
							<article
								class="unit unit-sm-horizontal unit-middle justify-content-center justify-content-sm-start footer-classic-info">
								<div class="unit-left"><a class="brand brand-md link-circle"
								                          href="https://livedemo00.template-help.com/wt_63853/soccer/index.html"><img
											class="brand-logo "
											src="/themes/sport/images/Home_files/logo-soccer-default-129x129.png" alt=""
											width="129" height="129"></a>
								</div>
								<div class="unit-body">
									<p>Atletico website offers you the latest news about our team as well as updates on
										our matches and other events.</p>
								</div>
							</article>
							<ul class="list-inline list-inline-bordered list-inline-bordered-lg">
								<li>
									<div class="unit unit-horizontal unit-middle">
										<div class="unit-left">
											<svg class="svg-color-primary svg-sizing-35" x="0px" y="0px" width="27px"
											     height="27px" viewBox="0 0 27 27" preserveAspectRatio="none">
												<path
													d="M2,26c0,0.553,0.447,1,1,1h5c0.553,0,1-0.447,1-1v-8.185c-0.373-0.132-0.711-0.335-1-0.595V19 H6v-1v-1v-1H5v1v2H3v-9H2v1H1V9V8c0-0.552,0.449-1,1-1h1h1h3h0.184c0.078-0.218,0.173-0.426,0.297-0.617C8.397,5.751,9,4.696,9,3.5 C9,1.567,7.434,0,5.5,0S2,1.567,2,3.5C2,4.48,2.406,5.364,3.056,6H3H2C0.895,6,0,6.895,0,8v7c0,1.104,0.895,2,2,2V26z M8,26H6v-6h2 V26z M5,26H3v-6h2V26z M3,3.5C3,2.121,4.121,1,5.5,1S8,2.121,8,3.5S6.879,6,5.5,6S3,4.879,3,3.5 M1,15v-3h1v4 C1.449,16,1,15.552,1,15"></path>
												<path
													d="M11.056,6H11h-1C8.895,6,8,6.895,8,8v7c0,1.104,0.895,2,2,2v9c0,0.553,0.447,1,1,1h5 c0.553,0,1-0.447,1-1v-9c1.104,0,2-0.896,2-2V8c0-1.105-0.896-2-2-2h-1h-0.056C16.594,5.364,17,4.48,17,3.5 C17,1.567,15.434,0,13.5,0S10,1.567,10,3.5C10,4.48,10.406,5.364,11.056,6 M10,15v1c-0.551,0-1-0.448-1-1v-3h1V15z M11,20h2v6h-2 V20z M16,26h-2v-6h2V26z M17,16v-1v-3h1v3C18,15.552,17.551,16,17,16 M17,7c0.551,0,1,0.448,1,1v1v1v1h-1v-1h-1v5v4h-2v-1v-1v-1h-1 v1v1v1h-2v-4v-5h-1v1H9v-1V9V8c0-0.552,0.449-1,1-1h1h1h3h1H17z M13.5,1C14.879,1,16,2.121,16,3.5C16,4.879,14.879,6,13.5,6 S11,4.879,11,3.5C11,2.121,12.121,1,13.5,1"></path>
												<polygon
													points="15,13 14,13 14,9 13,9 12,9 12,10 13,10 13,13 12,13 12,14 13,14 14,14 15,14 	"></polygon>
												<polygon
													points="7,14 7,13 5,13 5,12 6,12 7,12 7,10 7,9 6,9 4,9 4,10 6,10 6,11 5,11 4,11 4,12 4,13 4,14 5,14"></polygon>
												<polygon
													points="20,10 22,10 22,11 21,11 21,12 22,12 22,13 20,13 20,14 22,14 23,14 23,13 23,12 23,11 23,10 23,9 22,9 20,9 	"></polygon>
												<path
													d="M19.519,6.383C19.643,6.574,19.738,6.782,19.816,7H20h3h1h1c0.551,0,1,0.448,1,1v3h-1v-1h-1v9 h-2v-2v-1h-1v1v2h-2v-1.78c-0.289,0.26-0.627,0.463-1,0.595V26c0,0.553,0.447,1,1,1h5c0.553,0,1-0.447,1-1v-9c1.104,0,2-0.896,2-2 V8c0-1.105-0.896-2-2-2h-1h-0.056C24.594,5.364,25,4.48,25,3.5C25,1.567,23.434,0,21.5,0S18,1.567,18,3.5 c0,0.736,0.229,1.418,0.617,1.981C18.861,5.834,19.166,6.14,19.519,6.383 M19,20h2v6h-2V20z M24,26h-2v-6h2V26z M26,15 c0,0.552-0.449,1-1,1v-4h1V15z M21.5,1C22.879,1,24,2.121,24,3.5C24,4.879,22.879,6,21.5,6C20.121,6,19,4.879,19,3.5 C19,2.121,20.121,1,21.5,1"></path>
											</svg>
										</div>
										<div class="unit-body">
											<h6>Join Our Team</h6><a class="link" href="mailto:#">team@demolink.org</a>
										</div>
									</div>
								</li>
								<li>
									<div class="unit unit-horizontal unit-middle">
										<div class="unit-left">
											<svg class="svg-color-primary svg-sizing-35" x="0px" y="0px" width="72px"
											     height="72px" viewBox="0 0 72 72">
												<path
													d="M36.002,0c-0.41,0-0.701,0.184-0.931,0.332c-0.23,0.149-0.4,0.303-0.4,0.303l-9.251,8.18H11.58 c-1.236,0-1.99,0.702-2.318,1.358c-0.329,0.658-0.326,1.3-0.326,1.3v11.928l-8.962,7.936V66c0,0.015-0.038,1.479,0.694,2.972 C1.402,70.471,3.006,72,5.973,72h30.03h30.022c2.967,0,4.571-1.53,5.306-3.028c0.736-1.499,0.702-2.985,0.702-2.985V31.338 l-8.964-7.936V11.475c0,0,0.004-0.643-0.324-1.3c-0.329-0.658-1.092-1.358-2.328-1.358H46.575l-9.251-8.18 c0,0-0.161-0.154-0.391-0.303C36.703,0.184,36.412,0,36.002,0z M36.002,3.325c0.49,0,0.665,0.184,0.665,0.184l6,5.306h-6.665 h-6.665l6-5.306C35.337,3.51,35.512,3.325,36.002,3.325z M12.081,11.977h23.92H59.92v9.754v2.121v14.816L48.511,48.762 l-10.078-8.911c0,0-0.307-0.279-0.747-0.548s-1.022-0.562-1.684-0.562c-0.662,0-1.245,0.292-1.686,0.562 c-0.439,0.268-0.747,0.548-0.747,0.548l-10.078,8.911L12.082,38.668V23.852v-2.121v-9.754H12.081z M8.934,26.867v9.015 l-5.091-4.507L8.934,26.867z M63.068,26.867l5.091,4.509l-5.091,4.507V26.867z M69.031,34.44v31.559 c0,0.328-0.103,0.52-0.162,0.771L50.685,50.684L69.031,34.44z M2.971,34.448l18.348,16.235L3.133,66.77 c-0.059-0.251-0.162-0.439-0.162-0.769C2.971,66.001,2.971,34.448,2.971,34.448z M36.002,41.956c0.264,0,0.437,0.057,0.546,0.104 c0.108,0.047,0.119,0.059,0.119,0.059l30.147,26.675c-0.3,0.054-0.79,0.207-0.79,0.207H36.002H5.98H5.972 c-0.003,0-0.488-0.154-0.784-0.207l30.149-26.675c0,0,0.002-0.011,0.109-0.059C35.555,42.013,35.738,41.956,36.002,41.956z"></path>
											</svg>
										</div>
										<div class="unit-body">
											<h6>Contact Us</h6><a class="link" href="mailto:#">team@demolink.org</a>
										</div>
									</div>
								</li>
							</ul>
							<div class="group-md group-middle">
								<div class="group-item">
									<ul class="list-inline list-inline-xs">
										<li><a class="icon icon-corporate fa fa-facebook" href="#"></a></li>
										<li><a class="icon icon-corporate fa fa-twitter" href="#"></a></li>
										<li><a class="icon icon-corporate fa fa-google-plus" href="#"></a></li>
										<li><a class="icon icon-corporate fa fa-instagram" href="#"></a></li>
									</ul>
								</div>
								<a class="button button-sm button-gray-outline"
								   href="https://livedemo00.template-help.com/wt_63853/soccer/contact-us.html">Get in
									Touch</a>
							</div>
						</div>
						<div class="col-lg-7">
							<h5>Popular News</h5>
							<div class="divider-small divider-secondary"></div>
							<div class="row row-20">
								<div class="col-sm-6">
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-aside"><a class="post-classic-figure"
										                                   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
													src="/themes/sport/images/Home_files/footer-soccer-post-1-93x87.jpg"
													alt="" width="93" height="87"></a></div>
										<div class="post-classic-main">
											<!-- Badge-->
											<div class="badge badge-secondary">The Team
											</div>
											<p class="post-classic-title"><a
													href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Bundy
													stymies Blue Jays and Orioles hit 2 HRs</a></p>
											<div class="post-classic-time"><span class="icon fa fa-calendar"></span>
												<time datetime="2017">April 15, 2017</time>
											</div>
										</div>
									</article>
								</div>
								<div class="col-sm-6">
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-aside"><a class="post-classic-figure"
										                                   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
													src="/themes/sport/images/Home_files/footer-soccer-post-2-93x87.jpg"
													alt="" width="93" height="87"></a></div>
										<div class="post-classic-main">
											<!-- Badge-->
											<div class="badge badge-red">Hot<span class="icon fa fa-bolt"></span>
											</div>
											<p class="post-classic-title"><a
													href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Good
													vibes back at struggling Schalke</a></p>
											<div class="post-classic-time"><span class="icon fa fa-calendar"></span>
												<time datetime="2017">April 15, 2017</time>
											</div>
										</div>
									</article>
								</div>
								<div class="col-sm-6">
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-aside"><a class="post-classic-figure"
										                                   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
													src="/themes/sport/images/Home_files/footer-soccer-post-3-93x87.jpg"
													alt="" width="93" height="87"></a></div>
										<div class="post-classic-main">
											<!-- Badge-->
											<div class="badge badge-primary">The League
											</div>
											<p class="post-classic-title"><a
													href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Prem
													in 90 seconds: Chelsea's crisis is over!</a></p>
											<div class="post-classic-time"><span class="icon fa fa-calendar"></span>
												<time datetime="2017">April 15, 2017</time>
											</div>
										</div>
									</article>
								</div>
								<div class="col-sm-6">
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-aside"><a class="post-classic-figure"
										                                   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
													src="/themes/sport/images/Home_files/footer-soccer-post-4-93x87.jpg"
													alt="" width="93" height="87"></a></div>
										<div class="post-classic-main">
											<!-- Badge-->
											<div class="badge badge-primary">The League
											</div>
											<p class="post-classic-title"><a
													href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Liverpool
													in desperate need of backup players</a></p>
											<div class="post-classic-time"><span class="icon fa fa-calendar"></span>
												<time datetime="2017">April 15, 2017</time>
											</div>
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-classic-aside footer-classic-darken">
				<div class="container">
					<div class="layout-justify">
						<!-- Rights-->
						<p class="rights"><span>Athletico FC</span><span>&nbsp;©&nbsp;</span><span
								class="copyright-year">2018</span><span>.&nbsp;</span><a class="link-underline"
						                                                                 href="https://livedemo00.template-help.com/wt_63853/soccer/privacy-policy.html">Privacy
								Policy</a></p>
						<nav class="nav-minimal">
							<ul class="nav-minimal-list">
								<li class="active"><a
										href="https://livedemo00.template-help.com/wt_63853/soccer/index.html">Home</a>
								</li>
								<li><a href="#">Features </a></li>
								<li><a href="#">Statistics </a></li>
								<li><a href="https://livedemo00.template-help.com/wt_63853/soccer/roster.html">Team</a>
								</li>
								<li><a href="https://livedemo00.template-help.com/wt_63853/soccer/news-1.html">News</a>
								</li>
								<li>
									<a href="https://livedemo00.template-help.com/wt_63853/soccer/grid-shop.html">Shop</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</footer>
		<!-- Modal Video-->
		<div class="modal modal-video fade" id="modal1" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">×</span></button>
					</div>
					<div class="modal-body">
						<div class="embed-responsive embed-responsive-16by9">
							<!-- <iframe class="embed-responsive-item" src="Home_files/GRlsN4GyPxo.html" width="560" height="315"></iframe> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--PANEL-->
	<div class="layout-panel-wrap">
		<div class="layout-panel">
			<div class="layout-panel-content">
				<h6>Choose your color scheme:</h6>
				<div class="theme-switcher-list">
					<button class="theme-switcher-list-item active-theme" data-theme-name="soccer">Soccer</button>
					<button class="theme-switcher-list-item" data-theme-name="baseball">Baseball</button>
					<button class="theme-switcher-list-item" data-theme-name="basketball">Basketball</button>
					<button class="theme-switcher-list-item" data-theme-name="billiards">Billiards</button>
					<button class="theme-switcher-list-item" data-theme-name="bowling">Bowling</button>
					<button class="theme-switcher-list-item" data-theme-name="rugby">Rugby</button>
				</div>
			</div>
		</div>
	</div>
	<!--END PANEL-->
	<!-- Global Mailform Output-->
	<div class="snackbars" id="form-output-global"></div>

	<?php $this->endBody() ?>
	<!-- Javascript-->
	<script src="/themes/sport/images/Home_files/core.js"></script>
	<script src="/themes/sport/images/Home_files/script.js"></script>
	<span role="status" aria-live="polite" class="select2-hidden-accessible"></span><a href="#" id="ui-to-top"
	                                                                                   class="ui-to-top fa fa-angle-up"></a>
	</body>
	</html>
<?php $this->endPage() ?>