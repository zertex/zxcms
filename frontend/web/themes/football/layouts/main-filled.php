<?php
/**
 * @var $this \yii\web\View
 */
use frontend\assets\AppAsset;

AppAsset::register( $this );
rmrevin\yii\fontawesome\cdn\AssetBundle::register( $this );


$js = <<<JS
	jQuery(document).ready(function($) {
		if (device.desktop() || device.tablet()) {
			var news_slider = $('#homepage_carousel__1').bxSlider({
				infiniteLoop: true,
				hideControlOnEnd: true,
				minSlides: 3,
				maxSlides: 3,
				moveSlides: 1,
				slideMargin: 0,
				pager: false,
				prevText: '',
				nextText: '',
				controls: false
			});
			
			$('#news-arrow-prev').on('click', function(){
      			news_slider.goToPrevSlide();
      			return false;
    		});
    		
    		$('#news-arrow-next').on('click', function(){
      			news_slider.goToNextSlide();
      			return false;
    		})
		}
		
		var events_slider = $('#homepage_carousel__2').bxSlider({
			infiniteLoop: true,
			hideControlOnEnd: true,
			minSlides: 1,
			maxSlides: 1,
			moveSlides: 1,
			slideMargin: 0,
			pager: false,
			prevText: '',
			nextText: '',
			controls: false
		});
		
		$('#event-arrow-prev').on('click', function(){
            events_slider.goToPrevSlide();
            return false;
        });
        
        $('#event-arrow-next').on('click', function(){
            events_slider.goToNextSlide();
            return false;
        })
		
	});

JS;
$this->registerJs($js, $this::POS_READY);

?>
<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html class="wide wow-animation desktop landscape rd-navbar-static-linked" lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<!-- Site Title-->
		<title>Home</title>
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<link rel="icon" href="favicon.ico" type="image/x-icon">

		<?php $this->head() ?>
		<!-- Stylesheets-->
		<link rel="stylesheet" type="text/css" href="/themes/sport/images/Home_files/css.css">

		<!--[if lt IE 10]>
		<div
			style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;">
			<a href="http://windows.microsoft.com/en-US/internet-explorer/"><img
				src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820"
				alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a>
		</div>
		<script src="js/html5shiv.min.js"></script>
		<![endif]-->

		<link id="main-styles-link" rel="stylesheet" href="/themes/sport/images/Home_files/style.css">
		<script type="text/javascript" async="" src="/themes/sport/images/Home_files/ec.js"></script>
		<script type="text/javascript" async="" src="/themes/sport/images/Home_files/analytics.js"></script>
	</head>
	<body class="animsition-overlay" style="animation-duration: 800ms;">
	<div class="preloader loaded">
		<div class="preloader-body">
			<div class="preloader-item"></div>
		</div>
	</div>
	<!-- Page-->
	<div class="page">
		<!-- Page Header-->
		<header class="section page-header rd-navbar-dark">
			<!-- RD Navbar-->
			<div class="rd-navbar-wrap" style="height: 249.033px;">
				<nav class="rd-navbar rd-navbar-classic rd-navbar-original rd-navbar-static"
				     data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed"
				     data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed"
				     data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static"
				     data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static"
				     data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="166px"
				     data-xl-stick-up-offset="166px" data-xxl-stick-up-offset="166px" data-lg-stick-up="true"
				     data-xl-stick-up="true" data-xxl-stick-up="true">
					<div class="rd-navbar-panel">
						<!-- RD Navbar Toggle-->
						<button class="rd-navbar-toggle toggle-original" data-rd-navbar-toggle=".rd-navbar-main">
							<span></span></button>
						<!-- RD Navbar Panel-->
						<div class="rd-navbar-panel-inner container">
							<div
								class="rd-navbar-collapse rd-navbar-panel-item rd-navbar-panel-item-left toggle-original-elements">

								<!-- BX Slider - events-->
								<div class="owl-carousel-navbar owl-carousel-inline-outer">
									<div class="owl-inline-nav">
										<button class="owl-arrow owl-arrow-prev" id="event-arrow-prev"></button>
										<button class="owl-arrow owl-arrow-next" id="event-arrow-next"></button>
									</div>
									<div class="owl-carousel-inline-wrap">
										<div id="homepage_carousel__2" class="homepage_event_carousel row">
											<div class="owl-item cloned events col-sm-4 main_events_homepage item_1"
											     style="width: 1110px; margin-right: 10px;">
												<article class="post-inline">
													<time class="post-inline-time" datetime="2017">April 15,
														2017
													</time>
													<p class="post-inline-title">Atletico vs Dream Team</p>
												</article>
											</div>

											<div class="owl-item cloned events col-sm-4 main_events_homepage item_2"
											     style="width: 1110px; margin-right: 10px;">
												<article class="post-inline">
													<time class="post-inline-time" datetime="2017">April 14,
														2018
													</time>
													<p class="post-inline-title">Atletico vs Dream 2018</p>
												</article>
											</div>
										</div>
									</div>


								</div>


							</div>
							<div class="rd-navbar-panel-item rd-navbar-panel-item-right">
								<ul class="list-inline list-inline-bordered">
									<li><a class="link link-icon link-icon-left link-classic"
									       href="https://livedemo00.template-help.com/wt_63853/soccer/login-and-register.html"><span
												class="icon fa fa-user"></span><span
												class="link-icon-text">Вход</span></a></li>
								</ul>
							</div>
							<div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1 toggle-original"
							     data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
						</div>
					</div>
					<div class="rd-navbar-main toggle-original-elements">
						<div class="rd-navbar-main-top">
							<div class="rd-navbar-main-container container">
								<!-- RD Navbar Brand-->
								<div class="rd-navbar-brand"><a class="brand link-circle"
								                                href="https://livedemo00.template-help.com/wt_63853/soccer/index.html"><img
											class="brand-logo "
											src="/themes/sport/images/Home_files/logo-soccer-default-129x129.png" alt=""
											width="129" height="129"></a>
								</div>
								<!-- RD Navbar List-->
								<ul class="rd-navbar-list">
									<li class="rd-navbar-list-item"><a class="rd-navbar-list-link" href="#"><img
												src="/themes/sport/images/Home_files/partners-1-inverse-75x42.png"
												alt="" width="75" height="42"></a></li>
									<li class="rd-navbar-list-item"><a class="rd-navbar-list-link" href="#"><img
												src="/themes/sport/images/Home_files/partners-2-inverse-78x41.png"
												alt="" width="78" height="41"></a></li>
									<li class="rd-navbar-list-item"><a class="rd-navbar-list-link" href="#"><img
												src="/themes/sport/images/Home_files/partners-3-inverse-65x44.png"
												alt="" width="65" height="44"></a></li>
								</ul>
								<!-- RD Navbar Search-->
								<div class="rd-navbar-search toggle-original-elements">
									<button class="rd-navbar-search-toggle toggle-original"
									        data-rd-navbar-toggle=".rd-navbar-search"><span></span></button>
									<form class="rd-search" action="search-results.html"
									      data-search-live="rd-search-results-live" method="GET">
										<div class="form-wrap">
											<label class="form-label rd-input-label" for="rd-navbar-search-form-input">Enter
												your search request here...</label>
											<input class="rd-navbar-search-form-input form-input"
											       id="rd-navbar-search-form-input" name="s" autocomplete="off"
											       type="text">
											<div class="rd-search-results-live cleared"
											     id="rd-search-results-live"></div>
										</div>
										<button class="rd-search-form-submit fa fa-search" type="submit"></button>
									</form>
								</div>
							</div>
						</div>
						<div class="rd-navbar-main-bottom rd-navbar-darker">
							<div class="rd-navbar-main-container container">
								<!-- RD Navbar Nav-->
								<ul class="rd-navbar-nav">
									<li class="rd-nav-item active"><a class="rd-nav-link"
									                                  href="https://livedemo00.template-help.com/wt_63853/soccer/index.html">Главная</a>
									</li>
									<li class="rd-nav-item rd-navbar--has-megamenu rd-navbar-submenu"><a
											class="rd-nav-link" href="#">Услуги</a>
										<!-- RD Navbar Megamenu-->
										<article
											class="rd-menu rd-navbar-megamenu rd-megamenu-2-columns context-light rd-navbar-open-right">
											<div class="rd-megamenu-main">
												<div class="rd-megamenu-item rd-megamenu-item-nav">
													<!-- Heading Component-->
													<article class="heading-component heading-component-simple">
														<div class="heading-component-inner">
															<h5 class="heading-component-title">Elements
															</h5>
														</div>
													</article>

													<div class="rd-megamenu-list-outer">
														<ul class="rd-megamenu-list">
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/shortcodes.html">Shortcodes</a>
															</li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/typography.html">Typography</a>
															</li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/blog-elements.html">Blog
																	Elements</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/sport-elements.html">Sport
																	Elements</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/shop-elements.html">Shop
																	Elements</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/404-error-page.html">404
																	Error Page</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/privacy-policy.html">Privacy
																	Policy</a></li>
														</ul>
														<ul class="rd-megamenu-list">
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/about-us.html">About
																	Us</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/gallery.html">Gallery</a>
															</li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/albums.html">Albums</a>
															</li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/search-results.html">Search
																	Results</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/coming-soon.html">Coming
																	Soon</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/contact-us.html">Contact
																	Us</a></li>
															<li class="rd-megamenu-list-item"><a
																	class="rd-megamenu-list-link"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/login-and-register.html">Login
																	and Register</a></li>
														</ul>
													</div>
												</div>
												<div class="rd-megamenu-item rd-megamenu-item-content">
													<!-- Heading Component-->
													<article class="heading-component heading-component-simple">
														<div class="heading-component-inner">
															<h5 class="heading-component-title">Latest News
															</h5><a class="button button-xs button-gray-outline"
															        href="https://livedemo00.template-help.com/wt_63853/soccer/news-1.html">See
																all News</a>
														</div>
													</article>

													<div class="row row-20">
														<div class="col-lg-6">
															<!-- Post Classic-->
															<article class="post-classic">
																<div class="post-classic-aside"><a
																		class="post-classic-figure"
																		href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
																			src="/themes/sport/images/Home_files/megamenu-post-1-93x94.jpg"
																			alt="" width="93" height="94"></a></div>
																<div class="post-classic-main">
																	<!-- Badge-->
																	<div class="badge badge-secondary">The Team
																	</div>
																	<p class="post-classic-title"><a
																			href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Raheem
																			Sterling turns the tide for Manchester</a>
																	</p>
																	<div class="post-classic-time"><span
																			class="icon fa fa-calendar"></span>
																		<time datetime="2017">April 15, 2017</time>
																	</div>
																</div>
															</article>
														</div>
														<div class="col-lg-6">
															<!-- Post Classic-->
															<article class="post-classic">
																<div class="post-classic-aside"><a
																		class="post-classic-figure"
																		href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
																			src="/themes/sport/images/Home_files/megamenu-post-2-93x94.jpg"
																			alt="" width="93" height="94"></a></div>
																<div class="post-classic-main">
																	<!-- Badge-->
																	<div class="badge badge-primary">The League
																	</div>
																	<p class="post-classic-title"><a
																			href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Prem
																			in 90 seconds: Chelsea's crisis is over!</a>
																	</p>
																	<div class="post-classic-time"><span
																			class="icon fa fa-calendar"></span>
																		<time datetime="2017">April 15, 2017</time>
																	</div>
																</div>
															</article>
														</div>
														<div class="col-lg-6">
															<!-- Post Classic-->
															<article class="post-classic">
																<div class="post-classic-aside"><a
																		class="post-classic-figure"
																		href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
																			src="/themes/sport/images/Home_files/megamenu-post-3-93x94.jpg"
																			alt="" width="93" height="94"></a></div>
																<div class="post-classic-main">
																	<!-- Badge-->
																	<div class="badge badge-primary">The League
																	</div>
																	<p class="post-classic-title"><a
																			href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Good
																			vibes back at struggling Schalke</a></p>
																	<div class="post-classic-time"><span
																			class="icon fa fa-calendar"></span>
																		<time datetime="2017">April 15, 2017</time>
																	</div>
																</div>
															</article>
														</div>
														<div class="col-lg-6">
															<!-- Post Classic-->
															<article class="post-classic">
																<div class="post-classic-aside"><a
																		class="post-classic-figure"
																		href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
																			src="/themes/sport/images/Home_files/megamenu-post-4-93x94.jpg"
																			alt="" width="93" height="94"></a></div>
																<div class="post-classic-main">
																	<!-- Badge-->
																	<div class="badge badge-primary">The League
																	</div>
																	<p class="post-classic-title"><a
																			href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Liverpool
																			in desperate need of backup players</a></p>
																	<div class="post-classic-time"><span
																			class="icon fa fa-calendar"></span>
																		<time datetime="2017">April 15, 2017</time>
																	</div>
																</div>
															</article>
														</div>
													</div>
												</div>
											</div>
											<!-- Event Teaser-->
											<article class="event-teaser rd-megamenu-footer">
												<div class="event-teaser-header">
													<div class="event-teaser-caption">
														<h5 class="event-teaser-title">Final Europa League 2017</h5>
														<time class="event-teaser-time" datetime="2017">Saturday, April
															14, 2017
														</time>
													</div>
													<div class="event-teaser-teams">
														<div class="event-teaser-team">
															<div
																class="unit unit-spacing-xs unit-horizontal align-items-center">
																<div class="unit-left"><img
																		class="event-teaser-team-image"
																		src="/themes/sport/images/Home_files/team-bavaria-fc-59x54.png"
																		alt="" width="59" height="54">
																</div>
																<div class="unit-body">
																	<p class="heading-7">Bavaria</p>
																	<p class="text-style-1">Germany</p>
																</div>
															</div>
														</div>
														<div class="event-teaser-team-divider"><span
																class="event-teaser-team-divider-text">VS</span></div>
														<div class="event-teaser-team">
															<div
																class="unit unit-spacing-xs unit-horizontal align-items-center">
																<div class="unit-left"><img
																		class="event-teaser-team-image"
																		src="/themes/sport/images/Home_files/team-atletico-50x50.png"
																		alt="" width="50" height="50">
																</div>
																<div class="unit-body">
																	<p class="heading-7">Atletico</p>
																	<p class="text-style-1">USA</p>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="event-teaser-countdown event-teaser-highlighted">
													<!-- Countdown-->
													<div class="countdown countdown-classic is-countdown"
													     data-type="until" data-time="31 Dec 2018 16:00"
													     data-format="dhms" data-style="short"><span
															class="countdown-row countdown-show4"><span
																class="countdown-section"><span
																	class="countdown-amount">347</span><span
																	class="countdown-period">Days</span></span><span
																class="countdown-section"><span
																	class="countdown-amount">16</span><span
																	class="countdown-period">Hors</span></span><span
																class="countdown-section"><span
																	class="countdown-amount">8</span><span
																	class="countdown-period">Mins</span></span><span
																class="countdown-section"><span
																	class="countdown-amount">7</span><span
																	class="countdown-period">Secs</span></span></span>
													</div>
												</div>
												<div class="event-teaser-aside"><a class="event-teaser-link" href="#">Buy
														Tickets</a></div>
											</article>
										</article>
									</li>
									<li class="rd-nav-item rd-navbar--has-dropdown rd-navbar-submenu"><a
											class="rd-nav-link" href="#">Новости</a>
										<!-- RD Navbar Dropdown-->
										<ul class="rd-menu rd-navbar-dropdown">
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/blog-elements.html">Blog
													Elements</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-1.html">News
													1</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-2.html">News
													2</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-3.html">News
													3</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-4.html">News
													4</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/news-5.html">News
													5</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Blog
													post</a></li>
										</ul>
									</li>
									<li class="rd-nav-item rd-navbar--has-dropdown rd-navbar-submenu"><a
											class="rd-nav-link" href="#">Team</a>
										<!-- RD Navbar Dropdown-->
										<ul class="rd-menu rd-navbar-dropdown">
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/game-overview.html">Game
													Overview</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/roster.html">Roster</a>
											</li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/standings.html">Standings</a>
											</li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/latest-results-1.html">Latest
													Results 1</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/latest-results-2.html">Latest
													Results 2</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/game-schedule.html">Game
													schedule</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">Player
													Page</a></li>
										</ul>
									</li>
									<li class="rd-nav-item rd-navbar--has-dropdown rd-navbar-submenu"><a
											class="rd-nav-link" href="#">Shop</a>
										<!-- RD Navbar Dropdown-->
										<ul class="rd-menu rd-navbar-dropdown">
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/shop-elements.html">Shop
													Elements</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/grid-shop.html">Grid
													Shop</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/list-shop.html">List
													Shop</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/fullwidth-shop.html">Fullwidth
													Shop</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Product
													Page</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html">Shopping
													Cart</a></li>
											<li class="rd-dropdown-item"><a class="rd-dropdown-link"
											                                href="https://livedemo00.template-help.com/wt_63853/soccer/checkout.html">Checkout</a>
											</li>
										</ul>
									</li>
								</ul>
								<div class="rd-navbar-main-element">
									<ul class="list-inline list-inline-sm">
										<li><a class="icon icon-xs icon-light fa fa-facebook" href="#"></a></li>
										<li><a class="icon icon-xs icon-light fa fa-twitter" href="#"></a></li>
										<li><a class="icon icon-xs icon-light fa fa-google-plus" href="#"></a></li>
										<li><a class="icon icon-xs icon-light fa fa-instagram" href="#"></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</nav>
			</div>
		</header>

		<!-- Swiper-->

		<?= \frontend\widgets\slider\SliderWidget::widget([
			'tag' => 'section',
			'classes' => 'section swiper-container swiper-slider swiper-classic bg-gray-2 swiper-container-horizontal swiper-container-fade',
			'attrs' => 'data-loop="true" data-autoplay="false" data-simulate-touch="false" data-lazy-loading="true" data-slide-effect="fade"',
		]) ?>

		<!--

		<section class="section swiper-container swiper-slider swiper-classic bg-gray-2 swiper-container-horizontal swiper-container-fade"
			data-loop="true" data-autoplay="false" data-simulate-touch="false" data-lazy-loading="true"
			data-slide-effect="fade">
			<div class="swiper-wrapper" style="transition-duration: 0ms;">
				<div class="swiper-slide swiper-slide-duplicate swiper-slide-prev swiper-slide-duplicate-next"
				     data-slide-bg="/themes/sport/images/Home_files/landing-soccer-slider-1-slide-2-1920x671.jpg"
				     style="background-image: url(&quot;/themes/sport/images/Home_files/landing-soccer-slider-1-slide-2-1920x671.jpg&quot;); background-size: cover; width: 1903px; transform: translate3d(0px, 0px, 0px); opacity: 1; transition-duration: 0ms;"
				     data-swiper-slide-index="1">
					<div class="container">
						<div class="swiper-slide-caption">
							<h1 data-caption-animate="fadeInUp" data-caption-delay="100" class="not-animated">We are
								<br> pros</h1>
							<h4 data-caption-animate="fadeInUp" data-caption-delay="200" class="not-animated">In
								everything concerning soccer</h4><a class="button button-gray-outline not-animated"
							                                        data-caption-animate="fadeInUp"
							                                        data-caption-delay="300" href="#">Read More</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide swiper-slide-active"
				     data-slide-bg="/themes/sport/images/Home_files/landing-soccer-slider-1-slide-1-1920x671.jpg"
				     style="background-image: url(&quot;/themes/sport/images/Home_files/landing-soccer-slider-1-slide-1-1920x671.jpg&quot;); background-size: cover; width: 1903px; transform: translate3d(-1903px, 0px, 0px); opacity: 1; transition-duration: 0ms;"
				     data-swiper-slide-index="0">
					<div class="container">
						<div class="swiper-slide-caption">
							<h1 data-caption-animate="fadeInUp" data-caption-delay="100" class="fadeInUp animated">We
								play <br> Soccer</h1>
							<h4 data-caption-animate="fadeInUp" data-caption-delay="200" class="fadeInUp animated">like
								no one else in the united states</h4><a
								class="button button-gray-outline fadeInUp animated" data-caption-animate="fadeInUp"
								data-caption-delay="300" href="#">Read More</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide swiper-slide-next swiper-slide-duplicate-prev"
				     data-slide-bg="/themes/sport/images/Home_files/landing-soccer-slider-1-slide-2-1920x671.jpg"
				     style="background-image: url(&quot;/themes/sport/images/Home_files/landing-soccer-slider-1-slide-2-1920x671.jpg&quot;); background-size: cover; width: 1903px; transform: translate3d(-3806px, 0px, 0px); opacity: 0; transition-duration: 0ms;"
				     data-swiper-slide-index="1">
					<div class="container">
						<div class="swiper-slide-caption">
							<h1 data-caption-animate="fadeInUp" data-caption-delay="100" class="not-animated">We are
								<br> pros</h1>
							<h4 data-caption-animate="fadeInUp" data-caption-delay="200" class="not-animated">In
								everything concerning soccer</h4><a class="button button-gray-outline not-animated"
							                                        data-caption-animate="fadeInUp"
							                                        data-caption-delay="300"
							                                        href="https://livedemo00.template-help.com/wt_63853/soccer/about-us.html">Read
								More</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-active"
				     data-slide-bg="/themes/sport/images/Home_files/landing-soccer-slider-1-slide-1-1920x671.jpg"
				     style="background-image: url(&quot;/themes/sport/images/Home_files/landing-soccer-slider-1-slide-1-1920x671.jpg&quot;); background-size: cover; width: 1903px; transform: translate3d(-5709px, 0px, 0px); opacity: 0; transition-duration: 0ms;"
				     data-swiper-slide-index="0">
					<div class="container">
						<div class="swiper-slide-caption">
							<h1 data-caption-animate="fadeInUp" data-caption-delay="100" class="not-animated">We play
								<br> Soccer</h1>
							<h4 data-caption-animate="fadeInUp" data-caption-delay="200" class="not-animated">like no
								one else in the united states</h4><a class="button button-gray-outline not-animated"
							                                         data-caption-animate="fadeInUp"
							                                         data-caption-delay="300"
							                                         href="https://livedemo00.template-help.com/wt_63853/soccer/about-us.html">Read
								More</a>
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-button swiper-button-prev"></div>
			<div class="swiper-button swiper-button-next"></div>
			<div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets">
				<span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span>
				<span class="swiper-pagination-bullet"></span>
			</div>
		</section>

		-->

		<!-- Latest News -->
		<section class="section section-md bg-gray-100">
			<div class="container">
				<div class="row row-50">
					<div class="col-md-12">

						<!-- Heading  -->
						<article class="heading-component">
							<div class="heading-component-inner">
								<h5 class="heading-component-title">Новости
								</h5>
								<div class="owl-carousel-arrows-outline">
									<div class="owl-nav">
										<button class="owl-arrow owl-arrow-prev" id="news-arrow-prev"></button>
										<button class="owl-arrow owl-arrow-next" id="news-arrow-next"></button>
									</div>
								</div>
							</div>
						</article>

						<?= \frontend\widgets\post\PostWidget::widget([
							'count' => 15,
							'view' => 'slider',
							'filter' => \frontend\widgets\post\PostWidget::FILTER_LAST,
							'type' => 2,
						]) ?>



					</div>
				</div>
			</div>
		</section>






		<!-- Upcoming mutch-->
		<section class="section section-md bg-gray-100">
			<div class="container">
				<div class="row row-50">

					<div class="col-lg-8">
						<div class="main-component">
							<!-- Heading Component-->
							<article class="heading-component">
								<div class="heading-component-inner">
									<h5 class="heading-component-title">Upcoming Match
									</h5><a class="button button-xs button-gray-outline"
									        href="https://livedemo00.template-help.com/wt_63853/soccer/sport-elements.html">Calendar</a>
								</div>
							</article>

							<!-- Game Result Bug-->
							<article class="game-result">
								<div class="game-info game-info-creative">
									<p class="game-info-subtitle">Real Stadium -
										<time datetime="08:30"> 08:30 PM</time>
									</p>
									<h3 class="game-info-title">Champions league semi-final 2017</h3>
									<div class="game-info-main">
										<div class="game-info-team game-info-team-first">
											<figure><img src="/themes/sport/images/Home_files/team-atletico-100x100.png"
											             alt="" width="100" height="100">
											</figure>
											<div class="game-result-team-name">Atletico</div>
											<div class="game-result-team-country">Italy</div>
										</div>
										<div class="game-info-middle game-info-middle-vertical">
											<time class="time-big" datetime="2017-04-17"><span
													class="heading-3">Fri 19</span> May 2017
											</time>
											<div class="game-result-divider-wrap"><span class="game-info-team-divider">VS</span>
											</div>
											<div class="group-sm">
												<div class="button button-sm button-share-outline">Share
													<ul class="game-info-share">
														<li class="game-info-share-item"><a class="icon fa fa-facebook"
														                                    href="#"></a></li>
														<li class="game-info-share-item"><a class="icon fa fa-twitter"
														                                    href="#"></a></li>
														<li class="game-info-share-item"><a
																class="icon fa fa-google-plus" href="#"></a></li>
														<li class="game-info-share-item"><a class="icon fa fa-instagram"
														                                    href="#"></a></li>
													</ul>
												</div>
												<a class="button button-sm button-primary" href="#">Buy tickets</a>
											</div>
										</div>
										<div class="game-info-team game-info-team-second">
											<figure><img
													src="/themes/sport/images/Home_files/team-bavaria-fc-113x106.png"
													alt="" width="113" height="106">
											</figure>
											<div class="game-result-team-name">Celta Vigo</div>
											<div class="game-result-team-country">Spain</div>
										</div>
									</div>
								</div>
								<div class="game-info-countdown">
									<div class="countdown countdown-bordered is-countdown" data-type="until"
									     data-time="31 Dec 2018 16:00" data-format="dhms" data-style="short"><span
											class="countdown-row countdown-show4"><span class="countdown-section"><span
													class="countdown-amount">347</span><span class="countdown-period">Days</span></span><span
												class="countdown-section"><span class="countdown-amount">16</span><span
													class="countdown-period">Hors</span></span><span
												class="countdown-section"><span class="countdown-amount">8</span><span
													class="countdown-period">Mins</span></span><span
												class="countdown-section"><span class="countdown-amount">7</span><span
													class="countdown-period">Secs</span></span></span></div>
								</div>
							</article>
						</div>
						<div class="main-component">
							<!-- Heading Component-->
							<article class="heading-component">
								<div class="heading-component-inner">
									<h5 class="heading-component-title">Популярные новости
									</h5><a class="button button-xs button-gray-outline"
									        href="https://livedemo00.template-help.com/wt_63853/soccer/news-1.html">Все новости</a>
								</div>
							</article>

							<div class="row row-30">

								<!-- 2 popular news-->
								<?= \frontend\widgets\post\PostWidget::widget([
									'count' => 2,
									'view' => 'plate-vertical',
									'filter' => \frontend\widgets\post\PostWidget::FILTER_POPULAR,
									'type' => 2, // news
								]) ?>

								<div class="col-md-12">
									<!-- Post Gloria-->
									<article class="post-gloria"><img
											src="/themes/sport/images/Home_files/post-gloria-1-769x429.jpg" alt=""
											width="769" height="429">
										<div class="post-gloria-main">
											<h3 class="post-gloria-title"><a
													href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Premier
													League Winners and Losers: a quick look</a></h3>
											<div class="post-gloria-meta">
												<!-- Badge-->
												<div class="badge badge-primary">The League
												</div>
												<div class="post-gloria-time"><span class="icon fa fa-calendar"></span>
													<time datetime="2017">April 15, 2017</time>
												</div>
											</div>
											<div class="post-gloria-text">
												<svg version="1.1" x="0px" y="0px" width="6.888px" height="4.68px"
												     viewBox="0 0 6.888 4.68" enable-background="new 0 0 6.888 4.68"
												     xml:space="preserve">
                            <path d="M1.584,0h1.8L2.112,4.68H0L1.584,0z M5.112,0h1.776L5.64,4.68H3.528L5.112,0z"></path>
                          </svg>
												<p>During this year’s premier league, we are glad to announce that there
													are new players who are...</p>
											</div>
											<a class="button"
											   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Read
												more</a>
										</div>
									</article>
								</div>

								<!-- 2 last news-->
								<?= \frontend\widgets\post\PostWidget::widget([
									'count' => 2,
									'view' => 'plate-vertical',
									'filter' => \frontend\widgets\post\PostWidget::FILTER_LAST,
									'type' => 2, // news
								]) ?>

								<!-- 2 last articles-->
								<?= \frontend\widgets\post\PostWidget::widget([
									'count' => 2,
									'view' => 'plate-horizontal',
									'filter' => \frontend\widgets\post\PostWidget::FILTER_LAST,
									'type' => 3, // articles
								]) ?>

							</div>
						</div>
						<div class="main-component">
							<!-- Heading Component-->
							<article class="heading-component">
								<div class="heading-component-inner">
									<h5 class="heading-component-title">Top Players
									</h5><a class="button button-xs button-gray-outline"
									        href="https://livedemo00.template-help.com/wt_63853/soccer/roster.html">See
										all team</a>
								</div>
							</article>

							<div class="row row-30">
								<div class="col-md-6">
									<!-- Player Info Modern-->
									<div class="player-info-modern"><a class="player-info-modern-figure"
									                                   href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html"><img
												src="/themes/sport/images/Home_files/player-1-368x286.png" alt=""
												width="368" height="286"></a>
										<div class="player-info-modern-footer">
											<div class="player-info-modern-number">
												<p>05</p>
											</div>
											<div class="player-info-modern-content">
												<div class="player-info-modern-title">
													<h5>
														<a href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">Jack
															Windsor</a></h5>
													<p>Midfielder</p>
												</div>
												<div class="player-info-modern-progress">
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Pass Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div>
														<span class="progress-value">80</span>
													</article>
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Shots Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div>
														<span class="progress-value">60</span>
													</article>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<!-- Player Info Modern-->
									<div class="player-info-modern"><a class="player-info-modern-figure"
									                                   href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html"><img
												src="/themes/sport/images/Home_files/player-2-368x286.png" alt=""
												width="368" height="286"></a>
										<div class="player-info-modern-footer">
											<div class="player-info-modern-number">
												<p>21</p>
											</div>
											<div class="player-info-modern-content">
												<div class="player-info-modern-title">
													<h5>
														<a href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">Joe
															Perkins</a></h5>
													<p>Midfielder</p>
												</div>
												<div class="player-info-modern-progress">
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Pass Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div>
														<span class="progress-value">95</span>
													</article>
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Shots Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div>
														<span class="progress-value">70</span>
													</article>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<!-- Player Info Modern-->
									<div class="player-info-modern"><a class="player-info-modern-figure"
									                                   href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html"><img
												src="/themes/sport/images/Home_files/player-3-368x286.png" alt=""
												width="368" height="286"></a>
										<div class="player-info-modern-footer">
											<div class="player-info-modern-number">
												<p>21</p>
											</div>
											<div class="player-info-modern-content">
												<div class="player-info-modern-title">
													<h5>
														<a href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">David
															Hawkins</a></h5>
													<p>Defender</p>
												</div>
												<div class="player-info-modern-progress">
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Pass Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div>
														<span class="progress-value">90</span>
													</article>
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Shots Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div>
														<span class="progress-value">75</span>
													</article>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<!-- Player Info Modern-->
									<div class="player-info-modern"><a class="player-info-modern-figure"
									                                   href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html"><img
												src="/themes/sport/images/Home_files/player-4-368x286.png" alt=""
												width="368" height="286"></a>
										<div class="player-info-modern-footer">
											<div class="player-info-modern-number">
												<p>21</p>
											</div>
											<div class="player-info-modern-content">
												<div class="player-info-modern-title">
													<h5>
														<a href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">Harry
															Stevenson</a></h5>
													<p>Goalkeeper</p>
												</div>
												<div class="player-info-modern-progress">
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Pass Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div>
														<span class="progress-value">55</span>
													</article>
													<!-- Linear progress bar-->
													<article class="progress-linear progress-bar-modern">
														<div class="progress-header">
															<p>Shots Acc</p>
														</div>
														<div class="progress-bar-linear-wrap">
															<div class="progress-bar-linear"></div>
														</div>
														<span class="progress-value">95</span>
													</article>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Aside Block-->
					<div class="col-lg-4">
						<aside class="aside-components">
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">В центре внимания
										</h5><a class="button button-xs button-gray-outline"
										        href="https://livedemo00.template-help.com/wt_63853/soccer/news-1.html">Новости</a>
									</div>
								</article>

								<!-- Right news block-->
								<?= \frontend\widgets\post\PostWidget::widget([
									'type' => 2,
									'filter' => \frontend\widgets\post\PostWidget::FILTER_LAST,
									'view' => 'right-simple',
									'count' => 4
								]) ?>


							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">latest Games results
										</h5>
									</div>
								</article>

								<!-- Game Result Classic-->
								<article class="game-result game-result-classic">
									<div class="game-result-main">
										<div class="game-result-team game-result-team-first">
											<figure class="game-result-team-figure game-result-team-figure-big"><img
													src="/themes/sport/images/Home_files/team-atletico-55x55.png" alt=""
													width="55" height="55">
											</figure>
											<div class="game-result-team-name">Atletico</div>
											<div class="game-result-team-country">USA</div>
										</div>
										<div class="game-result-middle">
											<div class="game-result-score-wrap">
												<div class="game-result-score game-result-team-win">2<span
														class="game-result-team-label game-result-team-label-top">Win</span>
												</div>
												<div class="game-result-score-divider">
													<svg x="0px" y="0px" width="7px" height="21px" viewBox="0 0 7 21"
													     enable-background="new 0 0 7 21" xml:space="preserve">
                              <g>
	                              <circle cx="3.5" cy="3.5" r="3"></circle>
	                              <path
		                              d="M3.5,1C4.879,1,6,2.122,6,3.5S4.879,6,3.5,6S1,4.878,1,3.5S2.122,1,3.5,1 M3.5,0C1.567,0,0,1.567,0,3.5S1.567,7,3.5,7      S7,5.433,7,3.5S5.433,0,3.5,0L3.5,0z"></path>
                              </g>
														<g>
															<circle cx="3.695" cy="17.5" r="3"></circle>
															<path
																d="M3.695,15c1.378,0,2.5,1.122,2.5,2.5S5.073,20,3.695,20s-2.5-1.122-2.5-2.5S2.316,15,3.695,15 M3.695,14      c-1.933,0-3.5,1.567-3.5,3.5s1.567,3.5,3.5,3.5s3.5-1.567,3.5-3.5S5.628,14,3.695,14L3.695,14z"></path>
														</g>
                            </svg>
												</div>
												<div class="game-result-score">1
												</div>
											</div>
											<div class="game-results-status">Home</div>
										</div>
										<div class="game-result-team game-result-team-second">
											<figure class="game-result-team-figure game-result-team-figure-big"><img
													src="/themes/sport/images/Home_files/team-real-madrid-41x59.png"
													alt="" width="41" height="59">
											</figure>
											<div class="game-result-team-name">Real madrid</div>
											<div class="game-result-team-country">Spain</div>
										</div>
									</div>
									<div class="game-result-footer">
										<ul class="game-result-details">
											<li>New Yorkers Stadium</li>
											<li>
												<time datetime="2017-04-14">April 14, 2017</time>
											</li>
										</ul>
									</div>
								</article>
								<!-- Game Result Classic-->
								<article class="game-result game-result-classic">
									<div class="game-result-main">
										<div class="game-result-team game-result-team-first">
											<figure class="game-result-team-figure game-result-team-figure-big"><img
													src="/themes/sport/images/Home_files/team-bavaria-fc-56x52.png"
													alt="" width="56" height="52">
											</figure>
											<div class="game-result-team-name">Bavaria FC</div>
											<div class="game-result-team-country">Germany</div>
										</div>
										<div class="game-result-middle">
											<div class="game-result-score-wrap">
												<div class="game-result-score">2
												</div>
												<div class="game-result-score-divider">
													<svg x="0px" y="0px" width="7px" height="21px" viewBox="0 0 7 21"
													     enable-background="new 0 0 7 21" xml:space="preserve">
                              <g>
	                              <circle cx="3.5" cy="3.5" r="3"></circle>
	                              <path
		                              d="M3.5,1C4.879,1,6,2.122,6,3.5S4.879,6,3.5,6S1,4.878,1,3.5S2.122,1,3.5,1 M3.5,0C1.567,0,0,1.567,0,3.5S1.567,7,3.5,7      S7,5.433,7,3.5S5.433,0,3.5,0L3.5,0z"></path>
                              </g>
														<g>
															<circle cx="3.695" cy="17.5" r="3"></circle>
															<path
																d="M3.695,15c1.378,0,2.5,1.122,2.5,2.5S5.073,20,3.695,20s-2.5-1.122-2.5-2.5S2.316,15,3.695,15 M3.695,14      c-1.933,0-3.5,1.567-3.5,3.5s1.567,3.5,3.5,3.5s3.5-1.567,3.5-3.5S5.628,14,3.695,14L3.695,14z"></path>
														</g>
                            </svg>
												</div>
												<div class="game-result-score game-result-team-win">3<span
														class="game-result-team-label game-result-team-label-top">Win</span>
												</div>
											</div>
											<div class="game-results-status">Away</div>
										</div>
										<div class="game-result-team game-result-team-second">
											<figure class="game-result-team-figure game-result-team-figure-big"><img
													src="/themes/sport/images/Home_files/team-atletico-55x55.png" alt=""
													width="55" height="55">
											</figure>
											<div class="game-result-team-name">Atletico</div>
											<div class="game-result-team-country">USA</div>
										</div>
									</div>
									<div class="game-result-footer">
										<ul class="game-result-details">
											<li>New Yorkers Stadium</li>
											<li>
												<time datetime="2017-04-14">April 14, 2017</time>
											</li>
										</ul>
									</div>
								</article>
								<!-- Game Result Classic-->
								<article class="game-result game-result-classic">
									<div class="game-result-main">
										<div class="game-result-team game-result-team-first">
											<figure class="game-result-team-figure game-result-team-figure-big"><img
													src="/themes/sport/images/Home_files/team-atletico-55x55.png" alt=""
													width="55" height="55">
											</figure>
											<div class="game-result-team-name">Atletico</div>
											<div class="game-result-team-country">USA</div>
										</div>
										<div class="game-result-middle">
											<div class="game-result-score-wrap">
												<div class="game-result-score game-result-team-win">4<span
														class="game-result-team-label game-result-team-label-top">Win</span>
												</div>
												<div class="game-result-score-divider">
													<svg x="0px" y="0px" width="7px" height="21px" viewBox="0 0 7 21"
													     enable-background="new 0 0 7 21" xml:space="preserve">
                              <g>
	                              <circle cx="3.5" cy="3.5" r="3"></circle>
	                              <path
		                              d="M3.5,1C4.879,1,6,2.122,6,3.5S4.879,6,3.5,6S1,4.878,1,3.5S2.122,1,3.5,1 M3.5,0C1.567,0,0,1.567,0,3.5S1.567,7,3.5,7      S7,5.433,7,3.5S5.433,0,3.5,0L3.5,0z"></path>
                              </g>
														<g>
															<circle cx="3.695" cy="17.5" r="3"></circle>
															<path
																d="M3.695,15c1.378,0,2.5,1.122,2.5,2.5S5.073,20,3.695,20s-2.5-1.122-2.5-2.5S2.316,15,3.695,15 M3.695,14      c-1.933,0-3.5,1.567-3.5,3.5s1.567,3.5,3.5,3.5s3.5-1.567,3.5-3.5S5.628,14,3.695,14L3.695,14z"></path>
														</g>
                            </svg>
												</div>
												<div class="game-result-score">1
												</div>
											</div>
											<div class="game-results-status">Home</div>
										</div>
										<div class="game-result-team game-result-team-second">
											<figure class="game-result-team-figure game-result-team-figure-big"><img
													src="/themes/sport/images/Home_files/team-sevilla-57x46.png" alt=""
													width="57" height="46">
											</figure>
											<div class="game-result-team-name">Sevilla</div>
											<div class="game-result-team-country">Spain</div>
										</div>
									</div>
									<div class="game-result-footer">
										<ul class="game-result-details">
											<li>New Yorkers Stadium</li>
											<li>
												<time datetime="2017-04-14">April 14, 2017</time>
											</li>
										</ul>
									</div>
								</article>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Follow us
										</h5>
									</div>
								</article>

								<!-- Buttons Media-->
								<div class="group-sm group-flex"><a class="button-media button-media-facebook" href="#">
										<h4 class="button-media-title">50k</h4>
										<p class="button-media-action">Like<span
												class="icon fa fa-plus-circle icon-sm"></span></p><span
											class="button-media-icon fa fa-facebook"></span></a><a
										class="button-media button-media-twitter" href="#">
										<h4 class="button-media-title">120k</h4>
										<p class="button-media-action">Follow<span
												class="icon fa fa-plus-circle icon-sm"></span></p><span
											class="button-media-icon fa fa-twitter"></span></a><a
										class="button-media button-media-google" href="#">
										<h4 class="button-media-title">15k</h4>
										<p class="button-media-action">Follow<span
												class="icon fa fa-plus-circle icon-sm"></span></p><span
											class="button-media-icon fa fa-google"></span></a><a
										class="button-media button-media-instagram" href="#">
										<h4 class="button-media-title">85k</h4>
										<p class="button-media-action">Follow<span
												class="icon fa fa-plus-circle icon-sm"></span></p><span
											class="button-media-icon fa fa-instagram"></span></a></div>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Our Awards
										</h5>
									</div>
								</article>

								<!-- Owl Carousel-->
								<div class="owl-carousel owl-carousel-dots-modern awards-carousel owl-loaded owl-drag"
								     data-items="1" data-dots="true" data-nav="false" data-stage-padding="0"
								     data-loop="true" data-margin="0" data-mouse-drag="true">
									<!-- Awards Item-->

									<!-- Awards Item-->

									<!-- Awards Item-->

									<div class="owl-stage-outer">
										<div class="owl-stage"
										     style="transform: translate3d(-739px, 0px, 0px); transition: all 0s ease 0s; width: 2590px;">
											<div class="owl-item cloned" style="width: 369.983px;">
												<div class="awards-item">
													<div class="awards-item-main">
														<h4 class="awards-item-title"><span
																class="text-accent">Best</span>Forward
														</h4>
														<div class="divider"></div>
														<h5 class="awards-item-time">June 2015</h5>
													</div>
													<div class="awards-item-aside"><img
															src="/themes/sport/images/Home_files/thumbnail-minimal-2-68x126.png"
															alt="" width="68" height="126">
													</div>
												</div>
											</div>
											<div class="owl-item cloned" style="width: 369.983px;">
												<div class="awards-item">
													<div class="awards-item-main">
														<h4 class="awards-item-title"><span
																class="text-accent">Best</span>Coach
														</h4>
														<div class="divider"></div>
														<h5 class="awards-item-time">November 2016</h5>
													</div>
													<div class="awards-item-aside"><img
															src="/themes/sport/images/Home_files/thumbnail-minimal-3-73x135.png"
															alt="" width="73" height="135">
													</div>
												</div>
											</div>
											<div class="owl-item active" style="width: 369.983px;">
												<div class="awards-item">
													<div class="awards-item-main">
														<h4 class="awards-item-title"><span
																class="text-accent">World</span>Champions
														</h4>
														<div class="divider"></div>
														<h5 class="awards-item-time">December 2014</h5>
													</div>
													<div class="awards-item-aside"><img
															src="/themes/sport/images/Home_files/thumbnail-minimal-1-67x147.png"
															alt="" width="67" height="147">
													</div>
												</div>
											</div>
											<div class="owl-item" style="width: 369.983px;">
												<div class="awards-item">
													<div class="awards-item-main">
														<h4 class="awards-item-title"><span
																class="text-accent">Best</span>Forward
														</h4>
														<div class="divider"></div>
														<h5 class="awards-item-time">June 2015</h5>
													</div>
													<div class="awards-item-aside"><img
															src="/themes/sport/images/Home_files/thumbnail-minimal-2-68x126.png"
															alt="" width="68" height="126">
													</div>
												</div>
											</div>
											<div class="owl-item" style="width: 369.983px;">
												<div class="awards-item">
													<div class="awards-item-main">
														<h4 class="awards-item-title"><span
																class="text-accent">Best</span>Coach
														</h4>
														<div class="divider"></div>
														<h5 class="awards-item-time">November 2016</h5>
													</div>
													<div class="awards-item-aside"><img
															src="/themes/sport/images/Home_files/thumbnail-minimal-3-73x135.png"
															alt="" width="73" height="135">
													</div>
												</div>
											</div>
											<div class="owl-item cloned" style="width: 369.983px;">
												<div class="awards-item">
													<div class="awards-item-main">
														<h4 class="awards-item-title"><span
																class="text-accent">World</span>Champions
														</h4>
														<div class="divider"></div>
														<h5 class="awards-item-time">December 2014</h5>
													</div>
													<div class="awards-item-aside"><img
															src="/themes/sport/images/Home_files/thumbnail-minimal-1-67x147.png"
															alt="" width="67" height="147">
													</div>
												</div>
											</div>
											<div class="owl-item cloned" style="width: 369.983px;">
												<div class="awards-item">
													<div class="awards-item-main">
														<h4 class="awards-item-title"><span
																class="text-accent">Best</span>Forward
														</h4>
														<div class="divider"></div>
														<h5 class="awards-item-time">June 2015</h5>
													</div>
													<div class="awards-item-aside"><img
															src="/themes/sport/images/Home_files/thumbnail-minimal-2-68x126.png"
															alt="" width="68" height="126">
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="owl-nav disabled">
										<div class="owl-prev"></div>
										<div class="owl-next"></div>
									</div>
									<div class="owl-dots">
										<div class="owl-dot active"><span></span></div>
										<div class="owl-dot"><span></span></div>
										<div class="owl-dot"><span></span></div>
									</div>
								</div>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Standings
										</h5><a class="button button-xs button-gray-outline"
										        href="https://livedemo00.template-help.com/wt_63853/soccer/standings.html">Full
											Standings</a>
									</div>
								</article>

								<!-- Table team-->
								<div class="table-custom-responsive">
									<table class="table-custom table-standings table-classic">
										<thead>
										<tr>
											<th colspan="2">Team Position</th>
											<th>W</th>
											<th>L</th>
											<th>PTS</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td><span>1</span></td>
											<td class="team-inline">
												<div class="team-figure"><img
														src="/themes/sport/images/Home_files/team-atletico-37x37.png"
														alt="" width="37" height="37">
												</div>
												<div class="team-title">
													<div class="team-name">Atletico</div>
													<div class="team-country">USA</div>
												</div>
											</td>
											<td>153</td>
											<td>30</td>
											<td>186</td>
										</tr>
										<tr>
											<td><span>2</span></td>
											<td class="team-inline">
												<div class="team-figure"><img
														src="/themes/sport/images/Home_files/team-sevilla-45x35.png"
														alt="" width="45" height="35">
												</div>
												<div class="team-title">
													<div class="team-name">Sevilla</div>
													<div class="team-country">Spain</div>
												</div>
											</td>
											<td>120</td>
											<td>30</td>
											<td>186</td>
										</tr>
										<tr>
											<td><span>3</span></td>
											<td class="team-inline">
												<div class="team-figure"><img
														src="/themes/sport/images/Home_files/team-real-madrid-29x43.png"
														alt="" width="29" height="43">
												</div>
												<div class="team-title">
													<div class="team-name">Real Madrid</div>
													<div class="team-country">Spain</div>
												</div>
											</td>
											<td>100</td>
											<td>30</td>
											<td>186</td>
										</tr>
										<tr>
											<td><span>4</span></td>
											<td class="team-inline">
												<div class="team-figure"><img
														src="/themes/sport/images/Home_files/team-celta-vigo-37x34.png"
														alt="" width="37" height="34">
												</div>
												<div class="team-title">
													<div class="team-name">Celta Vigo</div>
													<div class="team-country">Italy</div>
												</div>
											</td>
											<td>98</td>
											<td>30</td>
											<td>186</td>
										</tr>
										<tr>
											<td><span>5</span></td>
											<td class="team-inline">
												<div class="team-figure"><img
														src="/themes/sport/images/Home_files/team-barcelona-36x31.png"
														alt="" width="36" height="31">
												</div>
												<div class="team-title">
													<div class="team-name">Barcelona</div>
													<div class="team-country">Spain</div>
												</div>
											</td>
											<td>98</td>
											<td>30</td>
											<td>186</td>
										</tr>
										<tr>
											<td><span>6</span></td>
											<td class="team-inline">
												<div class="team-figure"><img
														src="/themes/sport/images/Home_files/team-bavaria-fc-39x37.png"
														alt="" width="39" height="37">
												</div>
												<div class="team-title">
													<div class="team-name">Bavaria FC</div>
													<div class="team-country">Germany</div>
												</div>
											</td>
											<td>98</td>
											<td>30</td>
											<td>186</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Gallery
										</h5>
									</div>
								</article>

								<article class="instafeed" data-instafeed-tagname="tm_63853_football"
								         data-instafeed-get="tagged" data-instafeed-sort="least-liked"
								         data-lightgallery="group">
									<div class="row row-10 row-narrow">
										<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
												class="thumbnail-creative" data-lightgallery="item"
												href="https://scontent.cdninstagram.com/vp/0f10e089d45d8d6c8becfc00330900e3/5AEA53D0/t51.2885-15/s640x640/sh0.08/e35/21480085_1618930664797383_1913910776549081088_n.jpg"
												data-images-standard_resolution-url="href"><img
													src="/themes/sport/images/Home_files/21480085_1618930664797383_1913910776549081088_n.jpg"
													alt="" data-images-thumbnail-url="src">
												<div class="thumbnail-creative-overlay"></div>
											</a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
												class="thumbnail-creative" data-lightgallery="item"
												href="https://scontent.cdninstagram.com/vp/93fa1fe65dab2d35141b5fd27728c6b2/5ADD7B6A/t51.2885-15/s640x640/sh0.08/e35/21568673_150711905520712_2006047875271753728_n.jpg"
												data-images-standard_resolution-url="href"><img
													src="/themes/sport/images/Home_files/21568673_150711905520712_2006047875271753728_n.jpg"
													alt="" data-images-thumbnail-url="src">
												<div class="thumbnail-creative-overlay"></div>
											</a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
												class="thumbnail-creative" data-lightgallery="item"
												href="https://scontent.cdninstagram.com/vp/9cd98fa49caebb8da53e20a7ea94fbec/5AF6F8BB/t51.2885-15/s640x640/sh0.08/e35/21480314_115609609151834_2653927375292596224_n.jpg"
												data-images-standard_resolution-url="href"><img
													src="/themes/sport/images/Home_files/21480314_115609609151834_2653927375292596224_n.jpg"
													alt="" data-images-thumbnail-url="src">
												<div class="thumbnail-creative-overlay"></div>
											</a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
												class="thumbnail-creative" data-lightgallery="item"
												href="https://scontent.cdninstagram.com/vp/38796f3b1733bc439fa8bcb1a5b53517/5ADFFD77/t51.2885-15/s640x640/sh0.08/e35/21479737_472843706431477_5206562929071095808_n.jpg"
												data-images-standard_resolution-url="href"><img
													src="/themes/sport/images/Home_files/21479737_472843706431477_5206562929071095808_n.jpg"
													alt="" data-images-thumbnail-url="src">
												<div class="thumbnail-creative-overlay"></div>
											</a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
												class="thumbnail-creative" data-lightgallery="item"
												href="https://scontent.cdninstagram.com/vp/47ac13418227d2809c47e9b49faea15a/5AE99F44/t51.2885-15/s640x640/sh0.08/e35/21436119_473048923066578_5148142032291627008_n.jpg"
												data-images-standard_resolution-url="href"><img
													src="/themes/sport/images/Home_files/21436119_473048923066578_5148142032291627008_n.jpg"
													alt="" data-images-thumbnail-url="src">
												<div class="thumbnail-creative-overlay"></div>
											</a>
										</div>
										<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
												class="thumbnail-creative" data-lightgallery="item"
												href="https://scontent.cdninstagram.com/vp/3ed592c6f2e58e5b2a174ad58b8c568b/5AEFAB27/t51.2885-15/s640x640/sh0.08/e35/21569195_772356829603866_7348995180134924288_n.jpg"
												data-images-standard_resolution-url="href"><img
													src="/themes/sport/images/Home_files/21569195_772356829603866_7348995180134924288_n.jpg"
													alt="" data-images-thumbnail-url="src">
												<div class="thumbnail-creative-overlay"></div>
											</a>
										</div>
									</div>
								</article>
							</div>
							<div class="aside-component">
								<!-- Heading Component-->
								<article class="heading-component">
									<div class="heading-component-inner">
										<h5 class="heading-component-title">Team Stats
										</h5>
									</div>
								</article>

								<div class="table-custom-responsive">
									<table class="table-custom table-custom-bordered table-team-statistic">
										<tbody>
										<tr>
											<td>
												<p class="team-statistic-counter">109</p>
												<p class="team-statistic-title">Points Per Game</p>
											</td>
											<td>
												<p class="team-statistic-counter">65</p>
												<p class="team-statistic-title">Rebounds Per Game</p>
											</td>
										</tr>
										<tr>
											<td>
												<p class="team-statistic-counter">23.6</p>
												<p class="team-statistic-title">Assists Per Game</p>
											</td>
											<td>
												<p class="team-statistic-counter">102</p>
												<p class="team-statistic-title">Points Allowed</p>
											</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="aside-component">
								<div class="owl-carousel-outer-navigation">
									<!-- Heading Component-->
									<article class="heading-component">
										<div class="heading-component-inner">
											<h5 class="heading-component-title">Shop
											</h5>
											<div class="owl-carousel-arrows-outline">
												<div class="owl-nav">
													<button class="owl-arrow owl-arrow-prev"></button>
													<button class="owl-arrow owl-arrow-next"></button>
												</div>
											</div>
										</div>
									</article>

									<!-- Owl Carousel-->
									<div class="owl-carousel owl-loaded" data-items="1" data-dots="false"
									     data-nav="true" data-stage-padding="0" data-loop="true" data-margin="30"
									     data-mouse-drag="false" data-nav-custom=".owl-carousel-outer-navigation">


										<div class="owl-stage-outer">
											<div class="owl-stage"
											     style="transform: translate3d(-799px, 0px, 0px); transition: all 0s ease 0s; width: 2800px;">
												<div class="owl-item cloned"
												     style="width: 369.983px; margin-right: 30px;">
													<article class="product">
														<header class="product-header">
															<!-- Badge-->
															<div class="badge badge-shop">new
															</div>
															<div class="product-figure"><img
																	src="/themes/sport/images/Home_files/product-small-2.png"
																	alt=""></div>
															<div class="product-buttons">
																<div
																	class="product-button product-button-share fa fa-share-alt"
																	style="font-size: 22px;">
																	<ul class="product-share">
																		<li class="product-share-item">
																			<span>Share</span></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-facebook"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-instagram"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-twitter" href="#"></a>
																		</li>
																		<li class="product-share-item"><a
																				class="icon fa fa-google-plus"
																				href="#"></a></li>
																	</ul>
																</div>
																<a class="product-button fa fa-shopping-cart"
																   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
																   style="font-size: 26px;"></a><a
																	class="product-button fa fa-search-plus"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-2-original.jpg"
																	data-lightgallery="item"
																	style="font-size: 25px;"></a>
															</div>
														</header>
														<footer class="product-content">
															<h6 class="product-title"><a
																	href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
																	Air Zoom Pegasus</a></h6>
															<div class="product-price"><span
																	class="heading-6 product-price-new">$290</span>
															</div>
															<ul class="product-rating">
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star_half"></span></li>
															</ul>
														</footer>
													</article>
												</div>
												<div class="owl-item cloned"
												     style="width: 369.983px; margin-right: 30px;">
													<article class="product">
														<header class="product-header">
															<div class="product-figure"><img
																	src="/themes/sport/images/Home_files/product-small-3.png"
																	alt=""></div>
															<div class="product-buttons">
																<div
																	class="product-button product-button-share fa fa-share-alt"
																	style="font-size: 22px;">
																	<ul class="product-share">
																		<li class="product-share-item">
																			<span>Share</span></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-facebook"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-instagram"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-twitter" href="#"></a>
																		</li>
																		<li class="product-share-item"><a
																				class="icon fa fa-google-plus"
																				href="#"></a></li>
																	</ul>
																</div>
																<a class="product-button fa fa-shopping-cart"
																   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
																   style="font-size: 26px;"></a><a
																	class="product-button fa fa-search-plus"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-3-original.jpg"
																	data-lightgallery="item"
																	style="font-size: 25px;"></a>
															</div>
														</header>
														<footer class="product-content">
															<h6 class="product-title"><a
																	href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
																	distressed baseball hat</a></h6>
															<div class="product-price"><span
																	class="heading-6 product-price-new">$290</span>
															</div>
															<ul class="product-rating">
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star_half"></span></li>
															</ul>
														</footer>
													</article>
												</div>
												<div class="owl-item active"
												     style="width: 369.983px; margin-right: 30px;">
													<article class="product">
														<header class="product-header">
															<!-- Badge-->
															<div class="badge badge-red">hot<span
																	class="icon fa fa-bolt"></span>
															</div>
															<div class="product-figure"><img
																	src="/themes/sport/images/Home_files/product-small-1.png"
																	alt=""></div>
															<div class="product-buttons">
																<div
																	class="product-button product-button-share fa fa-share-alt"
																	style="font-size: 22px;">
																	<ul class="product-share">
																		<li class="product-share-item">
																			<span>Share</span></li>
																		<li class="product-share-item"><a
																				class="icon fa icon fa-facebook"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-instagram"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-twitter" href="#"></a>
																		</li>
																		<li class="product-share-item"><a
																				class="icon fa fa-google-plus"
																				href="#"></a></li>
																	</ul>
																</div>
																<a class="product-button fa fa-shopping-cart"
																   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
																   style="font-size: 26px;"></a><a
																	class="product-button fa fa-search-plus"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-1-original.jpg"
																	data-lightgallery="item"
																	style="font-size: 25px;"></a>
															</div>
														</header>
														<footer class="product-content">
															<h6 class="product-title"><a
																	href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
																	hoops elite backpack</a></h6>
															<div class="product-price"><span class="product-price-old">$400</span><span
																	class="heading-6 product-price-new">$290</span>
															</div>
															<ul class="product-rating">
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star_half"></span></li>
															</ul>
														</footer>
													</article>
												</div>
												<div class="owl-item" style="width: 369.983px; margin-right: 30px;">
													<article class="product">
														<header class="product-header">
															<!-- Badge-->
															<div class="badge badge-shop">new
															</div>
															<div class="product-figure"><img
																	src="/themes/sport/images/Home_files/product-small-2.png"
																	alt=""></div>
															<div class="product-buttons">
																<div
																	class="product-button product-button-share fa fa-share-alt"
																	style="font-size: 22px;">
																	<ul class="product-share">
																		<li class="product-share-item">
																			<span>Share</span></li>
																		<li class="product-share-item"><a
																				class="icon fa icon fa-facebook"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-instagram"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-twitter" href="#"></a>
																		</li>
																		<li class="product-share-item"><a
																				class="icon fa fa-google-plus"
																				href="#"></a></li>
																	</ul>
																</div>
																<a class="product-button fa fa-shopping-cart"
																   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
																   style="font-size: 26px;"></a><a
																	class="product-button fa fa-search-plus"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-2-original.jpg"
																	data-lightgallery="item"
																	style="font-size: 25px;"></a>
															</div>
														</header>
														<footer class="product-content">
															<h6 class="product-title"><a
																	href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
																	Air Zoom Pegasus</a></h6>
															<div class="product-price"><span
																	class="heading-6 product-price-new">$290</span>
															</div>
															<ul class="product-rating">
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star_half"></span></li>
															</ul>
														</footer>
													</article>
												</div>
												<div class="owl-item" style="width: 369.983px; margin-right: 30px;">
													<article class="product">
														<header class="product-header">
															<div class="product-figure"><img
																	src="/themes/sport/images/Home_files/product-small-3.png"
																	alt=""></div>
															<div class="product-buttons">
																<div
																	class="product-button product-button-share fa fa-share-alt"
																	style="font-size: 22px;">
																	<ul class="product-share">
																		<li class="product-share-item">
																			<span>Share</span></li>
																		<li class="product-share-item"><a
																				class="icon fa icon fa-facebook"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-instagram"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-twitter" href="#"></a>
																		</li>
																		<li class="product-share-item"><a
																				class="icon fa fa-google-plus"
																				href="#"></a></li>
																	</ul>
																</div>
																<a class="product-button fa fa-shopping-cart"
																   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
																   style="font-size: 26px;"></a><a
																	class="product-button fa fa-search-plus"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-3-original.jpg"
																	data-lightgallery="item"
																	style="font-size: 25px;"></a>
															</div>
														</header>
														<footer class="product-content">
															<h6 class="product-title"><a
																	href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
																	distressed baseball hat</a></h6>
															<div class="product-price"><span
																	class="heading-6 product-price-new">$290</span>
															</div>
															<ul class="product-rating">
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star_half"></span></li>
															</ul>
														</footer>
													</article>
												</div>
												<div class="owl-item cloned"
												     style="width: 369.983px; margin-right: 30px;">
													<article class="product">
														<header class="product-header">
															<!-- Badge-->
															<div class="badge badge-red">hot<span
																	class="icon fa fa-bolt"></span>
															</div>
															<div class="product-figure"><img
																	src="/themes/sport/images/Home_files/product-small-1.png"
																	alt=""></div>
															<div class="product-buttons">
																<div
																	class="product-button product-button-share fa fa-share-alt"
																	style="font-size: 22px;">
																	<ul class="product-share">
																		<li class="product-share-item">
																			<span>Share</span></li>
																		<li class="product-share-item"><a
																				class="icon fa icon fa-facebook"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-instagram"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-twitter" href="#"></a>
																		</li>
																		<li class="product-share-item"><a
																				class="icon fa fa-google-plus"
																				href="#"></a></li>
																	</ul>
																</div>
																<a class="product-button fa fa-shopping-cart"
																   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
																   style="font-size: 26px;"></a><a
																	class="product-button fa fa-search-plus"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-1-original.jpg"
																	data-lightgallery="item"
																	style="font-size: 25px;"></a>
															</div>
														</header>
														<footer class="product-content">
															<h6 class="product-title"><a
																	href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
																	hoops elite backpack</a></h6>
															<div class="product-price"><span class="product-price-old">$400</span><span
																	class="heading-6 product-price-new">$290</span>
															</div>
															<ul class="product-rating">
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star_half"></span></li>
															</ul>
														</footer>
													</article>
												</div>
												<div class="owl-item cloned"
												     style="width: 369.983px; margin-right: 30px;">
													<article class="product">
														<header class="product-header">
															<!-- Badge-->
															<div class="badge badge-shop">new
															</div>
															<div class="product-figure"><img
																	src="/themes/sport/images/Home_files/product-small-2.png"
																	alt=""></div>
															<div class="product-buttons">
																<div
																	class="product-button product-button-share fa fa-share-alt"
																	style="font-size: 22px;">
																	<ul class="product-share">
																		<li class="product-share-item">
																			<span>Share</span></li>
																		<li class="product-share-item"><a
																				class="icon fa icon fa-facebook"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-instagram"
																				href="#"></a></li>
																		<li class="product-share-item"><a
																				class="icon fa fa-twitter" href="#"></a>
																		</li>
																		<li class="product-share-item"><a
																				class="icon fa fa-google-plus"
																				href="#"></a></li>
																	</ul>
																</div>
																<a class="product-button fa fa-shopping-cart"
																   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
																   style="font-size: 26px;"></a><a
																	class="product-button fa fa-search-plus"
																	href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-2-original.jpg"
																	data-lightgallery="item"
																	style="font-size: 25px;"></a>
															</div>
														</header>
														<footer class="product-content">
															<h6 class="product-title"><a
																	href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
																	Air Zoom Pegasus</a></h6>
															<div class="product-price"><span
																	class="heading-6 product-price-new">$290</span>
															</div>
															<ul class="product-rating">
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star"></span></li>
																<li><span class="fa fa-star_half"></span></li>
															</ul>
														</footer>
													</article>
												</div>
											</div>
										</div>
										<div class="owl-nav">
											<div class="owl-prev"></div>
											<div class="owl-next"></div>
										</div>
										<div class="owl-dots disabled"></div>
									</div>
								</div>
							</div>
						</aside>
					</div>
				</div>
			</div>
		</section>

		<!-- Page Footer-->
		<footer class="section footer-classic footer-classic-dark context-dark">
			<div class="footer-classic-main">
				<div class="container">
					<p class="heading-7">Subscribe to our Newsletter</p>
					<!-- RD Mailform-->
					<form class="rd-mailform rd-form rd-inline-form-creative" data-form-output="form-output-global"
					      data-form-type="subscribe" method="post" action="bat/rd-mailform.php" novalidate="novalidate">
						<div class="form-wrap">
							<div class="form-input-wrap">
								<input class="form-input form-control-has-validation" id="footer-form-email"
								       name="email" data-constraints="@Required" type="email"><span
									class="form-validation"></span>
								<label class="form-label rd-input-label" for="footer-form-email">Enter your
									E-mail</label>
							</div>
						</div>
						<div class="form-button">
							<button class="button button-primary-outline" type="submit" aria-label="Send"><span
									class="icon fa fa-arrow-right"></span></button>
						</div>
					</form>
					<div class="row row-50">
						<div class="col-lg-5 text-center text-sm-left">
							<article
								class="unit unit-sm-horizontal unit-middle justify-content-center justify-content-sm-start footer-classic-info">
								<div class="unit-left"><a class="brand brand-md link-circle"
								                          href="https://livedemo00.template-help.com/wt_63853/soccer/index.html"><img
											class="brand-logo "
											src="/themes/sport/images/Home_files/logo-soccer-default-129x129.png" alt=""
											width="129" height="129"></a>
								</div>
								<div class="unit-body">
									<p>Atletico website offers you the latest news about our team as well as updates on
										our matches and other events.</p>
								</div>
							</article>
							<ul class="list-inline list-inline-bordered list-inline-bordered-lg">
								<li>
									<div class="unit unit-horizontal unit-middle">
										<div class="unit-left">
											<svg class="svg-color-primary svg-sizing-35" x="0px" y="0px" width="27px"
											     height="27px" viewBox="0 0 27 27" preserveAspectRatio="none">
												<path
													d="M2,26c0,0.553,0.447,1,1,1h5c0.553,0,1-0.447,1-1v-8.185c-0.373-0.132-0.711-0.335-1-0.595V19 H6v-1v-1v-1H5v1v2H3v-9H2v1H1V9V8c0-0.552,0.449-1,1-1h1h1h3h0.184c0.078-0.218,0.173-0.426,0.297-0.617C8.397,5.751,9,4.696,9,3.5 C9,1.567,7.434,0,5.5,0S2,1.567,2,3.5C2,4.48,2.406,5.364,3.056,6H3H2C0.895,6,0,6.895,0,8v7c0,1.104,0.895,2,2,2V26z M8,26H6v-6h2 V26z M5,26H3v-6h2V26z M3,3.5C3,2.121,4.121,1,5.5,1S8,2.121,8,3.5S6.879,6,5.5,6S3,4.879,3,3.5 M1,15v-3h1v4 C1.449,16,1,15.552,1,15"></path>
												<path
													d="M11.056,6H11h-1C8.895,6,8,6.895,8,8v7c0,1.104,0.895,2,2,2v9c0,0.553,0.447,1,1,1h5 c0.553,0,1-0.447,1-1v-9c1.104,0,2-0.896,2-2V8c0-1.105-0.896-2-2-2h-1h-0.056C16.594,5.364,17,4.48,17,3.5 C17,1.567,15.434,0,13.5,0S10,1.567,10,3.5C10,4.48,10.406,5.364,11.056,6 M10,15v1c-0.551,0-1-0.448-1-1v-3h1V15z M11,20h2v6h-2 V20z M16,26h-2v-6h2V26z M17,16v-1v-3h1v3C18,15.552,17.551,16,17,16 M17,7c0.551,0,1,0.448,1,1v1v1v1h-1v-1h-1v5v4h-2v-1v-1v-1h-1 v1v1v1h-2v-4v-5h-1v1H9v-1V9V8c0-0.552,0.449-1,1-1h1h1h3h1H17z M13.5,1C14.879,1,16,2.121,16,3.5C16,4.879,14.879,6,13.5,6 S11,4.879,11,3.5C11,2.121,12.121,1,13.5,1"></path>
												<polygon
													points="15,13 14,13 14,9 13,9 12,9 12,10 13,10 13,13 12,13 12,14 13,14 14,14 15,14 	"></polygon>
												<polygon
													points="7,14 7,13 5,13 5,12 6,12 7,12 7,10 7,9 6,9 4,9 4,10 6,10 6,11 5,11 4,11 4,12 4,13 4,14 5,14"></polygon>
												<polygon
													points="20,10 22,10 22,11 21,11 21,12 22,12 22,13 20,13 20,14 22,14 23,14 23,13 23,12 23,11 23,10 23,9 22,9 20,9 	"></polygon>
												<path
													d="M19.519,6.383C19.643,6.574,19.738,6.782,19.816,7H20h3h1h1c0.551,0,1,0.448,1,1v3h-1v-1h-1v9 h-2v-2v-1h-1v1v2h-2v-1.78c-0.289,0.26-0.627,0.463-1,0.595V26c0,0.553,0.447,1,1,1h5c0.553,0,1-0.447,1-1v-9c1.104,0,2-0.896,2-2 V8c0-1.105-0.896-2-2-2h-1h-0.056C24.594,5.364,25,4.48,25,3.5C25,1.567,23.434,0,21.5,0S18,1.567,18,3.5 c0,0.736,0.229,1.418,0.617,1.981C18.861,5.834,19.166,6.14,19.519,6.383 M19,20h2v6h-2V20z M24,26h-2v-6h2V26z M26,15 c0,0.552-0.449,1-1,1v-4h1V15z M21.5,1C22.879,1,24,2.121,24,3.5C24,4.879,22.879,6,21.5,6C20.121,6,19,4.879,19,3.5 C19,2.121,20.121,1,21.5,1"></path>
											</svg>
										</div>
										<div class="unit-body">
											<h6>Join Our Team</h6><a class="link" href="mailto:#">team@demolink.org</a>
										</div>
									</div>
								</li>
								<li>
									<div class="unit unit-horizontal unit-middle">
										<div class="unit-left">
											<svg class="svg-color-primary svg-sizing-35" x="0px" y="0px" width="72px"
											     height="72px" viewBox="0 0 72 72">
												<path
													d="M36.002,0c-0.41,0-0.701,0.184-0.931,0.332c-0.23,0.149-0.4,0.303-0.4,0.303l-9.251,8.18H11.58 c-1.236,0-1.99,0.702-2.318,1.358c-0.329,0.658-0.326,1.3-0.326,1.3v11.928l-8.962,7.936V66c0,0.015-0.038,1.479,0.694,2.972 C1.402,70.471,3.006,72,5.973,72h30.03h30.022c2.967,0,4.571-1.53,5.306-3.028c0.736-1.499,0.702-2.985,0.702-2.985V31.338 l-8.964-7.936V11.475c0,0,0.004-0.643-0.324-1.3c-0.329-0.658-1.092-1.358-2.328-1.358H46.575l-9.251-8.18 c0,0-0.161-0.154-0.391-0.303C36.703,0.184,36.412,0,36.002,0z M36.002,3.325c0.49,0,0.665,0.184,0.665,0.184l6,5.306h-6.665 h-6.665l6-5.306C35.337,3.51,35.512,3.325,36.002,3.325z M12.081,11.977h23.92H59.92v9.754v2.121v14.816L48.511,48.762 l-10.078-8.911c0,0-0.307-0.279-0.747-0.548s-1.022-0.562-1.684-0.562c-0.662,0-1.245,0.292-1.686,0.562 c-0.439,0.268-0.747,0.548-0.747,0.548l-10.078,8.911L12.082,38.668V23.852v-2.121v-9.754H12.081z M8.934,26.867v9.015 l-5.091-4.507L8.934,26.867z M63.068,26.867l5.091,4.509l-5.091,4.507V26.867z M69.031,34.44v31.559 c0,0.328-0.103,0.52-0.162,0.771L50.685,50.684L69.031,34.44z M2.971,34.448l18.348,16.235L3.133,66.77 c-0.059-0.251-0.162-0.439-0.162-0.769C2.971,66.001,2.971,34.448,2.971,34.448z M36.002,41.956c0.264,0,0.437,0.057,0.546,0.104 c0.108,0.047,0.119,0.059,0.119,0.059l30.147,26.675c-0.3,0.054-0.79,0.207-0.79,0.207H36.002H5.98H5.972 c-0.003,0-0.488-0.154-0.784-0.207l30.149-26.675c0,0,0.002-0.011,0.109-0.059C35.555,42.013,35.738,41.956,36.002,41.956z"></path>
											</svg>
										</div>
										<div class="unit-body">
											<h6>Contact Us</h6><a class="link" href="mailto:#">team@demolink.org</a>
										</div>
									</div>
								</li>
							</ul>
							<div class="group-md group-middle">
								<div class="group-item">
									<ul class="list-inline list-inline-xs">
										<li><a class="icon icon-corporate fa fa-facebook" href="#"></a></li>
										<li><a class="icon icon-corporate fa fa-twitter" href="#"></a></li>
										<li><a class="icon icon-corporate fa fa-google-plus" href="#"></a></li>
										<li><a class="icon icon-corporate fa fa-instagram" href="#"></a></li>
									</ul>
								</div>
								<a class="button button-sm button-gray-outline"
								   href="https://livedemo00.template-help.com/wt_63853/soccer/contact-us.html">Get in
									Touch</a>
							</div>
						</div>
						<div class="col-lg-7">
							<h5>Popular News</h5>
							<div class="divider-small divider-secondary"></div>
							<div class="row row-20">
								<div class="col-sm-6">
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-aside"><a class="post-classic-figure"
										                                   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
													src="/themes/sport/images/Home_files/footer-soccer-post-1-93x87.jpg"
													alt="" width="93" height="87"></a></div>
										<div class="post-classic-main">
											<!-- Badge-->
											<div class="badge badge-secondary">The Team
											</div>
											<p class="post-classic-title"><a
													href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Bundy
													stymies Blue Jays and Orioles hit 2 HRs</a></p>
											<div class="post-classic-time"><span class="icon fa fa-calendar"></span>
												<time datetime="2017">April 15, 2017</time>
											</div>
										</div>
									</article>
								</div>
								<div class="col-sm-6">
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-aside"><a class="post-classic-figure"
										                                   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
													src="/themes/sport/images/Home_files/footer-soccer-post-2-93x87.jpg"
													alt="" width="93" height="87"></a></div>
										<div class="post-classic-main">
											<!-- Badge-->
											<div class="badge badge-red">Hot<span class="icon fa fa-bolt"></span>
											</div>
											<p class="post-classic-title"><a
													href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Good
													vibes back at struggling Schalke</a></p>
											<div class="post-classic-time"><span class="icon fa fa-calendar"></span>
												<time datetime="2017">April 15, 2017</time>
											</div>
										</div>
									</article>
								</div>
								<div class="col-sm-6">
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-aside"><a class="post-classic-figure"
										                                   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
													src="/themes/sport/images/Home_files/footer-soccer-post-3-93x87.jpg"
													alt="" width="93" height="87"></a></div>
										<div class="post-classic-main">
											<!-- Badge-->
											<div class="badge badge-primary">The League
											</div>
											<p class="post-classic-title"><a
													href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Prem
													in 90 seconds: Chelsea's crisis is over!</a></p>
											<div class="post-classic-time"><span class="icon fa fa-calendar"></span>
												<time datetime="2017">April 15, 2017</time>
											</div>
										</div>
									</article>
								</div>
								<div class="col-sm-6">
									<!-- Post Classic-->
									<article class="post-classic">
										<div class="post-classic-aside"><a class="post-classic-figure"
										                                   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html"><img
													src="/themes/sport/images/Home_files/footer-soccer-post-4-93x87.jpg"
													alt="" width="93" height="87"></a></div>
										<div class="post-classic-main">
											<!-- Badge-->
											<div class="badge badge-primary">The League
											</div>
											<p class="post-classic-title"><a
													href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Liverpool
													in desperate need of backup players</a></p>
											<div class="post-classic-time"><span class="icon fa fa-calendar"></span>
												<time datetime="2017">April 15, 2017</time>
											</div>
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-classic-aside footer-classic-darken">
				<div class="container">
					<div class="layout-justify">
						<!-- Rights-->
						<p class="rights"><span>Athletico FC</span><span>&nbsp;©&nbsp;</span><span
								class="copyright-year">2018</span><span>.&nbsp;</span><a class="link-underline"
						                                                                 href="https://livedemo00.template-help.com/wt_63853/soccer/privacy-policy.html">Privacy
								Policy</a></p>
						<nav class="nav-minimal">
							<ul class="nav-minimal-list">
								<li class="active"><a
										href="https://livedemo00.template-help.com/wt_63853/soccer/index.html">Home</a>
								</li>
								<li><a href="#">Features </a></li>
								<li><a href="#">Statistics </a></li>
								<li><a href="https://livedemo00.template-help.com/wt_63853/soccer/roster.html">Team</a>
								</li>
								<li><a href="https://livedemo00.template-help.com/wt_63853/soccer/news-1.html">News</a>
								</li>
								<li>
									<a href="https://livedemo00.template-help.com/wt_63853/soccer/grid-shop.html">Shop</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</footer>
		<!-- Modal Video-->
		<div class="modal modal-video fade" id="modal1" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">×</span></button>
					</div>
					<div class="modal-body">
						<div class="embed-responsive embed-responsive-16by9">
							<!-- <iframe class="embed-responsive-item" src="Home_files/GRlsN4GyPxo.html" width="560" height="315"></iframe> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--PANEL-->
	<div class="layout-panel-wrap">
		<div class="layout-panel">
			<div class="layout-panel-content">
				<h6>Choose your color scheme:</h6>
				<div class="theme-switcher-list">
					<button class="theme-switcher-list-item active-theme" data-theme-name="soccer">Soccer</button>
					<button class="theme-switcher-list-item" data-theme-name="baseball">Baseball</button>
					<button class="theme-switcher-list-item" data-theme-name="basketball">Basketball</button>
					<button class="theme-switcher-list-item" data-theme-name="billiards">Billiards</button>
					<button class="theme-switcher-list-item" data-theme-name="bowling">Bowling</button>
					<button class="theme-switcher-list-item" data-theme-name="rugby">Rugby</button>
				</div>
			</div>
		</div>
	</div>
	<!--END PANEL-->
	<!-- Global Mailform Output-->
	<div class="snackbars" id="form-output-global"></div>

	<?php $this->endBody() ?>
	<!-- Javascript-->
	<script src="/themes/sport/images/Home_files/core.js"></script>
	<script src="/themes/sport/images/Home_files/script.js"></script>
	<span role="status" aria-live="polite" class="select2-hidden-accessible"></span><a href="#" id="ui-to-top"
	                                                                                   class="ui-to-top fa fa-angle-up"></a>
	</body>
	</html>
<?php $this->endPage() ?>