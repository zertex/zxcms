<?php
/**
 * @var $this \yii\web\View
 */

$js = <<<JS
	jQuery(document).ready(function($) {
		if (device.desktop() || device.tablet()) {
			var news_slider = $('#homepage_carousel__1').bxSlider({
				infiniteLoop: true,
				hideControlOnEnd: true,
				minSlides: 3,
				maxSlides: 3,
				moveSlides: 1,
				slideMargin: 0,
				pager: false,
				prevText: '',
				nextText: '',
				controls: false
			});
			
			$('#news-arrow-prev').on('click', function(){
      			news_slider.goToPrevSlide();
      			return false;
    		});
    		
    		$('#news-arrow-next').on('click', function(){
      			news_slider.goToNextSlide();
      			return false;
    		})
		}
	});

JS;
$this->registerJs($js, $this::POS_READY);
?>

<?php $this->beginContent('@frontend/web/themes/sport/layouts/main.php') ?>

<!-- Swiper-->

<?= \frontend\widgets\slider\SliderWidget::widget([
	'tag' => 'section',
	'classes' => 'section swiper-container swiper-slider swiper-classic bg-gray-2 swiper-container-horizontal swiper-container-fade',
	'attrs' => 'data-loop="true" data-autoplay="false" data-simulate-touch="false" data-lazy-loading="true" data-slide-effect="fade"',
]) ?>

<!-- Latest News -->
<section class="section section-md bg-gray-100">
	<div class="container">
		<div class="row row-50">
			<div class="col-md-12">

				<?= \frontend\widgets\Alert::widget() ?>

				<!-- Heading  -->
				<article class="heading-component">
					<div class="heading-component-inner">
						<h5 class="heading-component-title">Новости
						</h5>
						<div class="owl-carousel-arrows-outline">
							<div class="owl-nav">
								<button class="owl-arrow owl-arrow-prev" id="news-arrow-prev"></button>
								<button class="owl-arrow owl-arrow-next" id="news-arrow-next"></button>
							</div>
						</div>
					</div>
				</article>

				<?= \frontend\widgets\post\PostWidget::widget([
					'count' => 15,
					'view' => 'slider',
					'filter' => \frontend\widgets\post\PostWidget::FILTER_LAST,
					'type' => 2,
				]) ?>



			</div>
		</div>
	</div>
</section>

<!-- Upcoming mutch-->
<section class="section section-md bg-gray-100">
	<div class="container">
		<div class="row row-50">

			<div class="col-lg-8">
				<div class="main-component">
					<!-- Heading Component-->
					<article class="heading-component">
						<div class="heading-component-inner">
							<h5 class="heading-component-title">Upcoming Match
							</h5><a class="button button-xs button-gray-outline"
							        href="https://livedemo00.template-help.com/wt_63853/soccer/sport-elements.html">Calendar</a>
						</div>
					</article>

					<!-- Game Result Bug-->
					<article class="game-result">
						<div class="game-info game-info-creative">
							<p class="game-info-subtitle">Real Stadium -
								<time datetime="08:30"> 08:30 PM</time>
							</p>
							<h3 class="game-info-title">Champions league semi-final 2017</h3>
							<div class="game-info-main">
								<div class="game-info-team game-info-team-first">
									<figure><img src="/themes/sport/images/Home_files/team-atletico-100x100.png"
									             alt="" width="100" height="100">
									</figure>
									<div class="game-result-team-name">Atletico</div>
									<div class="game-result-team-country">Italy</div>
								</div>
								<div class="game-info-middle game-info-middle-vertical">
									<time class="time-big" datetime="2017-04-17"><span
											class="heading-3">Fri 19</span> May 2017
									</time>
									<div class="game-result-divider-wrap"><span class="game-info-team-divider">VS</span>
									</div>
									<div class="group-sm">
										<div class="button button-sm button-share-outline">Share
											<ul class="game-info-share">
												<li class="game-info-share-item"><a class="icon fa fa-facebook"
												                                    href="#"></a></li>
												<li class="game-info-share-item"><a class="icon fa fa-twitter"
												                                    href="#"></a></li>
												<li class="game-info-share-item"><a
														class="icon fa fa-google-plus" href="#"></a></li>
												<li class="game-info-share-item"><a class="icon fa fa-instagram"
												                                    href="#"></a></li>
											</ul>
										</div>
										<a class="button button-sm button-primary" href="#">Buy tickets</a>
									</div>
								</div>
								<div class="game-info-team game-info-team-second">
									<figure><img
											src="/themes/sport/images/Home_files/team-bavaria-fc-113x106.png"
											alt="" width="113" height="106">
									</figure>
									<div class="game-result-team-name">Celta Vigo</div>
									<div class="game-result-team-country">Spain</div>
								</div>
							</div>
						</div>
						<div class="game-info-countdown">
							<div class="countdown countdown-bordered is-countdown" data-type="until"
							     data-time="31 Dec 2018 16:00" data-format="dhms" data-style="short"><span
									class="countdown-row countdown-show4"><span class="countdown-section"><span
											class="countdown-amount">347</span><span class="countdown-period">Days</span></span><span
										class="countdown-section"><span class="countdown-amount">16</span><span
											class="countdown-period">Hors</span></span><span
										class="countdown-section"><span class="countdown-amount">8</span><span
											class="countdown-period">Mins</span></span><span
										class="countdown-section"><span class="countdown-amount">7</span><span
											class="countdown-period">Secs</span></span></span></div>
						</div>
					</article>
				</div>
				<div class="main-component">
					<!-- Heading Component-->
					<article class="heading-component">
						<div class="heading-component-inner">
							<h5 class="heading-component-title">Популярные новости
							</h5><a class="button button-xs button-gray-outline"
							        href="https://livedemo00.template-help.com/wt_63853/soccer/news-1.html">Все новости</a>
						</div>
					</article>

					<div class="row row-30">

						<!-- 2 popular news-->
						<?= \frontend\widgets\post\PostWidget::widget([
							'count' => 2,
							'view' => 'plate-vertical',
							'filter' => \frontend\widgets\post\PostWidget::FILTER_POPULAR,
							'type' => 2, // news
						]) ?>

						<div class="col-md-12">
							<!-- Post Gloria-->
							<article class="post-gloria"><img
									src="/themes/sport/images/Home_files/post-gloria-1-769x429.jpg" alt=""
									width="769" height="429">
								<div class="post-gloria-main">
									<h3 class="post-gloria-title"><a
											href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Premier
											League Winners and Losers: a quick look</a></h3>
									<div class="post-gloria-meta">
										<!-- Badge-->
										<div class="badge badge-primary">The League
										</div>
										<div class="post-gloria-time"><span class="icon fa fa-calendar"></span>
											<time datetime="2017">April 15, 2017</time>
										</div>
									</div>
									<div class="post-gloria-text">
										<svg version="1.1" x="0px" y="0px" width="6.888px" height="4.68px"
										     viewBox="0 0 6.888 4.68" enable-background="new 0 0 6.888 4.68"
										     xml:space="preserve">
                            <path d="M1.584,0h1.8L2.112,4.68H0L1.584,0z M5.112,0h1.776L5.64,4.68H3.528L5.112,0z"></path>
                          </svg>
										<p>During this year’s premier league, we are glad to announce that there
											are new players who are...</p>
									</div>
									<a class="button"
									   href="https://livedemo00.template-help.com/wt_63853/soccer/blog-post.html">Read
										more</a>
								</div>
							</article>
						</div>

						<!-- 2 last news-->
						<?= \frontend\widgets\post\PostWidget::widget([
							'count' => 2,
							'view' => 'plate-vertical',
							'filter' => \frontend\widgets\post\PostWidget::FILTER_LAST,
							'type' => 2, // news
						]) ?>

						<!-- 2 last articles-->
						<?= \frontend\widgets\post\PostWidget::widget([
							'count' => 2,
							'view' => 'plate-horizontal',
							'filter' => \frontend\widgets\post\PostWidget::FILTER_LAST,
							'type' => 3, // articles
						]) ?>

					</div>
				</div>
				<div class="main-component">
					<!-- Heading Component-->
					<article class="heading-component">
						<div class="heading-component-inner">
							<h5 class="heading-component-title">Top Players
							</h5><a class="button button-xs button-gray-outline"
							        href="https://livedemo00.template-help.com/wt_63853/soccer/roster.html">See
								all team</a>
						</div>
					</article>

					<div class="row row-30">
						<div class="col-md-6">
							<!-- Player Info Modern-->
							<div class="player-info-modern"><a class="player-info-modern-figure"
							                                   href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html"><img
										src="/themes/sport/images/Home_files/player-1-368x286.png" alt=""
										width="368" height="286"></a>
								<div class="player-info-modern-footer">
									<div class="player-info-modern-number">
										<p>05</p>
									</div>
									<div class="player-info-modern-content">
										<div class="player-info-modern-title">
											<h5>
												<a href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">Jack
													Windsor</a></h5>
											<p>Midfielder</p>
										</div>
										<div class="player-info-modern-progress">
											<!-- Linear progress bar-->
											<article class="progress-linear progress-bar-modern">
												<div class="progress-header">
													<p>Pass Acc</p>
												</div>
												<div class="progress-bar-linear-wrap">
													<div class="progress-bar-linear"></div>
												</div>
												<span class="progress-value">80</span>
											</article>
											<!-- Linear progress bar-->
											<article class="progress-linear progress-bar-modern">
												<div class="progress-header">
													<p>Shots Acc</p>
												</div>
												<div class="progress-bar-linear-wrap">
													<div class="progress-bar-linear"></div>
												</div>
												<span class="progress-value">60</span>
											</article>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!-- Player Info Modern-->
							<div class="player-info-modern"><a class="player-info-modern-figure"
							                                   href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html"><img
										src="/themes/sport/images/Home_files/player-2-368x286.png" alt=""
										width="368" height="286"></a>
								<div class="player-info-modern-footer">
									<div class="player-info-modern-number">
										<p>21</p>
									</div>
									<div class="player-info-modern-content">
										<div class="player-info-modern-title">
											<h5>
												<a href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">Joe
													Perkins</a></h5>
											<p>Midfielder</p>
										</div>
										<div class="player-info-modern-progress">
											<!-- Linear progress bar-->
											<article class="progress-linear progress-bar-modern">
												<div class="progress-header">
													<p>Pass Acc</p>
												</div>
												<div class="progress-bar-linear-wrap">
													<div class="progress-bar-linear"></div>
												</div>
												<span class="progress-value">95</span>
											</article>
											<!-- Linear progress bar-->
											<article class="progress-linear progress-bar-modern">
												<div class="progress-header">
													<p>Shots Acc</p>
												</div>
												<div class="progress-bar-linear-wrap">
													<div class="progress-bar-linear"></div>
												</div>
												<span class="progress-value">70</span>
											</article>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!-- Player Info Modern-->
							<div class="player-info-modern"><a class="player-info-modern-figure"
							                                   href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html"><img
										src="/themes/sport/images/Home_files/player-3-368x286.png" alt=""
										width="368" height="286"></a>
								<div class="player-info-modern-footer">
									<div class="player-info-modern-number">
										<p>21</p>
									</div>
									<div class="player-info-modern-content">
										<div class="player-info-modern-title">
											<h5>
												<a href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">David
													Hawkins</a></h5>
											<p>Defender</p>
										</div>
										<div class="player-info-modern-progress">
											<!-- Linear progress bar-->
											<article class="progress-linear progress-bar-modern">
												<div class="progress-header">
													<p>Pass Acc</p>
												</div>
												<div class="progress-bar-linear-wrap">
													<div class="progress-bar-linear"></div>
												</div>
												<span class="progress-value">90</span>
											</article>
											<!-- Linear progress bar-->
											<article class="progress-linear progress-bar-modern">
												<div class="progress-header">
													<p>Shots Acc</p>
												</div>
												<div class="progress-bar-linear-wrap">
													<div class="progress-bar-linear"></div>
												</div>
												<span class="progress-value">75</span>
											</article>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<!-- Player Info Modern-->
							<div class="player-info-modern"><a class="player-info-modern-figure"
							                                   href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html"><img
										src="/themes/sport/images/Home_files/player-4-368x286.png" alt=""
										width="368" height="286"></a>
								<div class="player-info-modern-footer">
									<div class="player-info-modern-number">
										<p>21</p>
									</div>
									<div class="player-info-modern-content">
										<div class="player-info-modern-title">
											<h5>
												<a href="https://livedemo00.template-help.com/wt_63853/soccer/player-page.html">Harry
													Stevenson</a></h5>
											<p>Goalkeeper</p>
										</div>
										<div class="player-info-modern-progress">
											<!-- Linear progress bar-->
											<article class="progress-linear progress-bar-modern">
												<div class="progress-header">
													<p>Pass Acc</p>
												</div>
												<div class="progress-bar-linear-wrap">
													<div class="progress-bar-linear"></div>
												</div>
												<span class="progress-value">55</span>
											</article>
											<!-- Linear progress bar-->
											<article class="progress-linear progress-bar-modern">
												<div class="progress-header">
													<p>Shots Acc</p>
												</div>
												<div class="progress-bar-linear-wrap">
													<div class="progress-bar-linear"></div>
												</div>
												<span class="progress-value">95</span>
											</article>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Aside Block-->
			<div class="col-lg-4">
				<aside class="aside-components">

					<!-- Right news block-->
					<?= \frontend\widgets\post\PostWidget::widget([
						'type' => 2,
						'filter' => \frontend\widgets\post\PostWidget::FILTER_LAST,
						'view' => 'news',
						'count' => 4
					]) ?>

					<div class="aside-component">
						<!-- Heading Component-->
						<article class="heading-component">
							<div class="heading-component-inner">
								<h5 class="heading-component-title">latest Games results
								</h5>
							</div>
						</article>

						<!-- Game Result Classic-->
						<article class="game-result game-result-classic">
							<div class="game-result-main">
								<div class="game-result-team game-result-team-first">
									<figure class="game-result-team-figure game-result-team-figure-big"><img
											src="/themes/sport/images/Home_files/team-atletico-55x55.png" alt=""
											width="55" height="55">
									</figure>
									<div class="game-result-team-name">Atletico</div>
									<div class="game-result-team-country">USA</div>
								</div>
								<div class="game-result-middle">
									<div class="game-result-score-wrap">
										<div class="game-result-score game-result-team-win">2<span
												class="game-result-team-label game-result-team-label-top">Win</span>
										</div>
										<div class="game-result-score-divider">
											<svg x="0px" y="0px" width="7px" height="21px" viewBox="0 0 7 21"
											     enable-background="new 0 0 7 21" xml:space="preserve">
                              <g>
	                              <circle cx="3.5" cy="3.5" r="3"></circle>
	                              <path
		                              d="M3.5,1C4.879,1,6,2.122,6,3.5S4.879,6,3.5,6S1,4.878,1,3.5S2.122,1,3.5,1 M3.5,0C1.567,0,0,1.567,0,3.5S1.567,7,3.5,7      S7,5.433,7,3.5S5.433,0,3.5,0L3.5,0z"></path>
                              </g>
												<g>
													<circle cx="3.695" cy="17.5" r="3"></circle>
													<path
														d="M3.695,15c1.378,0,2.5,1.122,2.5,2.5S5.073,20,3.695,20s-2.5-1.122-2.5-2.5S2.316,15,3.695,15 M3.695,14      c-1.933,0-3.5,1.567-3.5,3.5s1.567,3.5,3.5,3.5s3.5-1.567,3.5-3.5S5.628,14,3.695,14L3.695,14z"></path>
												</g>
                            </svg>
										</div>
										<div class="game-result-score">1
										</div>
									</div>
									<div class="game-results-status">Home</div>
								</div>
								<div class="game-result-team game-result-team-second">
									<figure class="game-result-team-figure game-result-team-figure-big"><img
											src="/themes/sport/images/Home_files/team-real-madrid-41x59.png"
											alt="" width="41" height="59">
									</figure>
									<div class="game-result-team-name">Real madrid</div>
									<div class="game-result-team-country">Spain</div>
								</div>
							</div>
							<div class="game-result-footer">
								<ul class="game-result-details">
									<li>New Yorkers Stadium</li>
									<li>
										<time datetime="2017-04-14">April 14, 2017</time>
									</li>
								</ul>
							</div>
						</article>
						<!-- Game Result Classic-->
						<article class="game-result game-result-classic">
							<div class="game-result-main">
								<div class="game-result-team game-result-team-first">
									<figure class="game-result-team-figure game-result-team-figure-big"><img
											src="/themes/sport/images/Home_files/team-bavaria-fc-56x52.png"
											alt="" width="56" height="52">
									</figure>
									<div class="game-result-team-name">Bavaria FC</div>
									<div class="game-result-team-country">Germany</div>
								</div>
								<div class="game-result-middle">
									<div class="game-result-score-wrap">
										<div class="game-result-score">2
										</div>
										<div class="game-result-score-divider">
											<svg x="0px" y="0px" width="7px" height="21px" viewBox="0 0 7 21"
											     enable-background="new 0 0 7 21" xml:space="preserve">
                              <g>
	                              <circle cx="3.5" cy="3.5" r="3"></circle>
	                              <path
		                              d="M3.5,1C4.879,1,6,2.122,6,3.5S4.879,6,3.5,6S1,4.878,1,3.5S2.122,1,3.5,1 M3.5,0C1.567,0,0,1.567,0,3.5S1.567,7,3.5,7      S7,5.433,7,3.5S5.433,0,3.5,0L3.5,0z"></path>
                              </g>
												<g>
													<circle cx="3.695" cy="17.5" r="3"></circle>
													<path
														d="M3.695,15c1.378,0,2.5,1.122,2.5,2.5S5.073,20,3.695,20s-2.5-1.122-2.5-2.5S2.316,15,3.695,15 M3.695,14      c-1.933,0-3.5,1.567-3.5,3.5s1.567,3.5,3.5,3.5s3.5-1.567,3.5-3.5S5.628,14,3.695,14L3.695,14z"></path>
												</g>
                            </svg>
										</div>
										<div class="game-result-score game-result-team-win">3<span
												class="game-result-team-label game-result-team-label-top">Win</span>
										</div>
									</div>
									<div class="game-results-status">Away</div>
								</div>
								<div class="game-result-team game-result-team-second">
									<figure class="game-result-team-figure game-result-team-figure-big"><img
											src="/themes/sport/images/Home_files/team-atletico-55x55.png" alt=""
											width="55" height="55">
									</figure>
									<div class="game-result-team-name">Atletico</div>
									<div class="game-result-team-country">USA</div>
								</div>
							</div>
							<div class="game-result-footer">
								<ul class="game-result-details">
									<li>New Yorkers Stadium</li>
									<li>
										<time datetime="2017-04-14">April 14, 2017</time>
									</li>
								</ul>
							</div>
						</article>
						<!-- Game Result Classic-->
						<article class="game-result game-result-classic">
							<div class="game-result-main">
								<div class="game-result-team game-result-team-first">
									<figure class="game-result-team-figure game-result-team-figure-big"><img
											src="/themes/sport/images/Home_files/team-atletico-55x55.png" alt=""
											width="55" height="55">
									</figure>
									<div class="game-result-team-name">Atletico</div>
									<div class="game-result-team-country">USA</div>
								</div>
								<div class="game-result-middle">
									<div class="game-result-score-wrap">
										<div class="game-result-score game-result-team-win">4<span
												class="game-result-team-label game-result-team-label-top">Win</span>
										</div>
										<div class="game-result-score-divider">
											<svg x="0px" y="0px" width="7px" height="21px" viewBox="0 0 7 21"
											     enable-background="new 0 0 7 21" xml:space="preserve">
                              <g>
	                              <circle cx="3.5" cy="3.5" r="3"></circle>
	                              <path
		                              d="M3.5,1C4.879,1,6,2.122,6,3.5S4.879,6,3.5,6S1,4.878,1,3.5S2.122,1,3.5,1 M3.5,0C1.567,0,0,1.567,0,3.5S1.567,7,3.5,7      S7,5.433,7,3.5S5.433,0,3.5,0L3.5,0z"></path>
                              </g>
												<g>
													<circle cx="3.695" cy="17.5" r="3"></circle>
													<path
														d="M3.695,15c1.378,0,2.5,1.122,2.5,2.5S5.073,20,3.695,20s-2.5-1.122-2.5-2.5S2.316,15,3.695,15 M3.695,14      c-1.933,0-3.5,1.567-3.5,3.5s1.567,3.5,3.5,3.5s3.5-1.567,3.5-3.5S5.628,14,3.695,14L3.695,14z"></path>
												</g>
                            </svg>
										</div>
										<div class="game-result-score">1
										</div>
									</div>
									<div class="game-results-status">Home</div>
								</div>
								<div class="game-result-team game-result-team-second">
									<figure class="game-result-team-figure game-result-team-figure-big"><img
											src="/themes/sport/images/Home_files/team-sevilla-57x46.png" alt=""
											width="57" height="46">
									</figure>
									<div class="game-result-team-name">Sevilla</div>
									<div class="game-result-team-country">Spain</div>
								</div>
							</div>
							<div class="game-result-footer">
								<ul class="game-result-details">
									<li>New Yorkers Stadium</li>
									<li>
										<time datetime="2017-04-14">April 14, 2017</time>
									</li>
								</ul>
							</div>
						</article>
					</div>

					<?= \frontend\widgets\general\FollowWidget::widget() ?>

					<div class="aside-component">
						<!-- Heading Component-->
						<article class="heading-component">
							<div class="heading-component-inner">
								<h5 class="heading-component-title">Our Awards
								</h5>
							</div>
						</article>

						<!-- Owl Carousel-->
						<div class="owl-carousel owl-carousel-dots-modern awards-carousel owl-loaded owl-drag"
						     data-items="1" data-dots="true" data-nav="false" data-stage-padding="0"
						     data-loop="true" data-margin="0" data-mouse-drag="true">
							<!-- Awards Item-->

							<!-- Awards Item-->

							<!-- Awards Item-->

							<div class="owl-stage-outer">
								<div class="owl-stage"
								     style="transform: translate3d(-739px, 0px, 0px); transition: all 0s ease 0s; width: 2590px;">
									<div class="owl-item cloned" style="width: 369.983px;">
										<div class="awards-item">
											<div class="awards-item-main">
												<h4 class="awards-item-title"><span
														class="text-accent">Best</span>Forward
												</h4>
												<div class="divider"></div>
												<h5 class="awards-item-time">June 2015</h5>
											</div>
											<div class="awards-item-aside"><img
													src="/themes/sport/images/Home_files/thumbnail-minimal-2-68x126.png"
													alt="" width="68" height="126">
											</div>
										</div>
									</div>
									<div class="owl-item cloned" style="width: 369.983px;">
										<div class="awards-item">
											<div class="awards-item-main">
												<h4 class="awards-item-title"><span
														class="text-accent">Best</span>Coach
												</h4>
												<div class="divider"></div>
												<h5 class="awards-item-time">November 2016</h5>
											</div>
											<div class="awards-item-aside"><img
													src="/themes/sport/images/Home_files/thumbnail-minimal-3-73x135.png"
													alt="" width="73" height="135">
											</div>
										</div>
									</div>
									<div class="owl-item active" style="width: 369.983px;">
										<div class="awards-item">
											<div class="awards-item-main">
												<h4 class="awards-item-title"><span
														class="text-accent">World</span>Champions
												</h4>
												<div class="divider"></div>
												<h5 class="awards-item-time">December 2014</h5>
											</div>
											<div class="awards-item-aside"><img
													src="/themes/sport/images/Home_files/thumbnail-minimal-1-67x147.png"
													alt="" width="67" height="147">
											</div>
										</div>
									</div>
									<div class="owl-item" style="width: 369.983px;">
										<div class="awards-item">
											<div class="awards-item-main">
												<h4 class="awards-item-title"><span
														class="text-accent">Best</span>Forward
												</h4>
												<div class="divider"></div>
												<h5 class="awards-item-time">June 2015</h5>
											</div>
											<div class="awards-item-aside"><img
													src="/themes/sport/images/Home_files/thumbnail-minimal-2-68x126.png"
													alt="" width="68" height="126">
											</div>
										</div>
									</div>
									<div class="owl-item" style="width: 369.983px;">
										<div class="awards-item">
											<div class="awards-item-main">
												<h4 class="awards-item-title"><span
														class="text-accent">Best</span>Coach
												</h4>
												<div class="divider"></div>
												<h5 class="awards-item-time">November 2016</h5>
											</div>
											<div class="awards-item-aside"><img
													src="/themes/sport/images/Home_files/thumbnail-minimal-3-73x135.png"
													alt="" width="73" height="135">
											</div>
										</div>
									</div>
									<div class="owl-item cloned" style="width: 369.983px;">
										<div class="awards-item">
											<div class="awards-item-main">
												<h4 class="awards-item-title"><span
														class="text-accent">World</span>Champions
												</h4>
												<div class="divider"></div>
												<h5 class="awards-item-time">December 2014</h5>
											</div>
											<div class="awards-item-aside"><img
													src="/themes/sport/images/Home_files/thumbnail-minimal-1-67x147.png"
													alt="" width="67" height="147">
											</div>
										</div>
									</div>
									<div class="owl-item cloned" style="width: 369.983px;">
										<div class="awards-item">
											<div class="awards-item-main">
												<h4 class="awards-item-title"><span
														class="text-accent">Best</span>Forward
												</h4>
												<div class="divider"></div>
												<h5 class="awards-item-time">June 2015</h5>
											</div>
											<div class="awards-item-aside"><img
													src="/themes/sport/images/Home_files/thumbnail-minimal-2-68x126.png"
													alt="" width="68" height="126">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="owl-nav disabled">
								<div class="owl-prev"></div>
								<div class="owl-next"></div>
							</div>
							<div class="owl-dots">
								<div class="owl-dot active"><span></span></div>
								<div class="owl-dot"><span></span></div>
								<div class="owl-dot"><span></span></div>
							</div>
						</div>
					</div>
					<div class="aside-component">
						<!-- Heading Component-->
						<article class="heading-component">
							<div class="heading-component-inner">
								<h5 class="heading-component-title">Standings
								</h5><a class="button button-xs button-gray-outline"
								        href="https://livedemo00.template-help.com/wt_63853/soccer/standings.html">Full
									Standings</a>
							</div>
						</article>

						<!-- Table team-->
						<div class="table-custom-responsive">
							<table class="table-custom table-standings table-classic">
								<thead>
								<tr>
									<th colspan="2">Team Position</th>
									<th>W</th>
									<th>L</th>
									<th>PTS</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td><span>1</span></td>
									<td class="team-inline">
										<div class="team-figure"><img
												src="/themes/sport/images/Home_files/team-atletico-37x37.png"
												alt="" width="37" height="37">
										</div>
										<div class="team-title">
											<div class="team-name">Atletico</div>
											<div class="team-country">USA</div>
										</div>
									</td>
									<td>153</td>
									<td>30</td>
									<td>186</td>
								</tr>
								<tr>
									<td><span>2</span></td>
									<td class="team-inline">
										<div class="team-figure"><img
												src="/themes/sport/images/Home_files/team-sevilla-45x35.png"
												alt="" width="45" height="35">
										</div>
										<div class="team-title">
											<div class="team-name">Sevilla</div>
											<div class="team-country">Spain</div>
										</div>
									</td>
									<td>120</td>
									<td>30</td>
									<td>186</td>
								</tr>
								<tr>
									<td><span>3</span></td>
									<td class="team-inline">
										<div class="team-figure"><img
												src="/themes/sport/images/Home_files/team-real-madrid-29x43.png"
												alt="" width="29" height="43">
										</div>
										<div class="team-title">
											<div class="team-name">Real Madrid</div>
											<div class="team-country">Spain</div>
										</div>
									</td>
									<td>100</td>
									<td>30</td>
									<td>186</td>
								</tr>
								<tr>
									<td><span>4</span></td>
									<td class="team-inline">
										<div class="team-figure"><img
												src="/themes/sport/images/Home_files/team-celta-vigo-37x34.png"
												alt="" width="37" height="34">
										</div>
										<div class="team-title">
											<div class="team-name">Celta Vigo</div>
											<div class="team-country">Italy</div>
										</div>
									</td>
									<td>98</td>
									<td>30</td>
									<td>186</td>
								</tr>
								<tr>
									<td><span>5</span></td>
									<td class="team-inline">
										<div class="team-figure"><img
												src="/themes/sport/images/Home_files/team-barcelona-36x31.png"
												alt="" width="36" height="31">
										</div>
										<div class="team-title">
											<div class="team-name">Barcelona</div>
											<div class="team-country">Spain</div>
										</div>
									</td>
									<td>98</td>
									<td>30</td>
									<td>186</td>
								</tr>
								<tr>
									<td><span>6</span></td>
									<td class="team-inline">
										<div class="team-figure"><img
												src="/themes/sport/images/Home_files/team-bavaria-fc-39x37.png"
												alt="" width="39" height="37">
										</div>
										<div class="team-title">
											<div class="team-name">Bavaria FC</div>
											<div class="team-country">Germany</div>
										</div>
									</td>
									<td>98</td>
									<td>30</td>
									<td>186</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="aside-component">
						<!-- Heading Component-->
						<article class="heading-component">
							<div class="heading-component-inner">
								<h5 class="heading-component-title">Gallery
								</h5>
							</div>
						</article>

						<article class="instafeed" data-instafeed-tagname="tm_63853_football"
						         data-instafeed-get="tagged" data-instafeed-sort="least-liked"
						         data-lightgallery="group">
							<div class="row row-10 row-narrow">
								<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
										class="thumbnail-creative" data-lightgallery="item"
										href="https://scontent.cdninstagram.com/vp/0f10e089d45d8d6c8becfc00330900e3/5AEA53D0/t51.2885-15/s640x640/sh0.08/e35/21480085_1618930664797383_1913910776549081088_n.jpg"
										data-images-standard_resolution-url="href"><img
											src="/themes/sport/images/Home_files/21480085_1618930664797383_1913910776549081088_n.jpg"
											alt="" data-images-thumbnail-url="src">
										<div class="thumbnail-creative-overlay"></div>
									</a>
								</div>
								<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
										class="thumbnail-creative" data-lightgallery="item"
										href="https://scontent.cdninstagram.com/vp/93fa1fe65dab2d35141b5fd27728c6b2/5ADD7B6A/t51.2885-15/s640x640/sh0.08/e35/21568673_150711905520712_2006047875271753728_n.jpg"
										data-images-standard_resolution-url="href"><img
											src="/themes/sport/images/Home_files/21568673_150711905520712_2006047875271753728_n.jpg"
											alt="" data-images-thumbnail-url="src">
										<div class="thumbnail-creative-overlay"></div>
									</a>
								</div>
								<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
										class="thumbnail-creative" data-lightgallery="item"
										href="https://scontent.cdninstagram.com/vp/9cd98fa49caebb8da53e20a7ea94fbec/5AF6F8BB/t51.2885-15/s640x640/sh0.08/e35/21480314_115609609151834_2653927375292596224_n.jpg"
										data-images-standard_resolution-url="href"><img
											src="/themes/sport/images/Home_files/21480314_115609609151834_2653927375292596224_n.jpg"
											alt="" data-images-thumbnail-url="src">
										<div class="thumbnail-creative-overlay"></div>
									</a>
								</div>
								<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
										class="thumbnail-creative" data-lightgallery="item"
										href="https://scontent.cdninstagram.com/vp/38796f3b1733bc439fa8bcb1a5b53517/5ADFFD77/t51.2885-15/s640x640/sh0.08/e35/21479737_472843706431477_5206562929071095808_n.jpg"
										data-images-standard_resolution-url="href"><img
											src="/themes/sport/images/Home_files/21479737_472843706431477_5206562929071095808_n.jpg"
											alt="" data-images-thumbnail-url="src">
										<div class="thumbnail-creative-overlay"></div>
									</a>
								</div>
								<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
										class="thumbnail-creative" data-lightgallery="item"
										href="https://scontent.cdninstagram.com/vp/47ac13418227d2809c47e9b49faea15a/5AE99F44/t51.2885-15/s640x640/sh0.08/e35/21436119_473048923066578_5148142032291627008_n.jpg"
										data-images-standard_resolution-url="href"><img
											src="/themes/sport/images/Home_files/21436119_473048923066578_5148142032291627008_n.jpg"
											alt="" data-images-thumbnail-url="src">
										<div class="thumbnail-creative-overlay"></div>
									</a>
								</div>
								<div class="col-6 col-sm-4 col-md-6 col-lg-4" data-instafeed-item=""><a
										class="thumbnail-creative" data-lightgallery="item"
										href="https://scontent.cdninstagram.com/vp/3ed592c6f2e58e5b2a174ad58b8c568b/5AEFAB27/t51.2885-15/s640x640/sh0.08/e35/21569195_772356829603866_7348995180134924288_n.jpg"
										data-images-standard_resolution-url="href"><img
											src="/themes/sport/images/Home_files/21569195_772356829603866_7348995180134924288_n.jpg"
											alt="" data-images-thumbnail-url="src">
										<div class="thumbnail-creative-overlay"></div>
									</a>
								</div>
							</div>
						</article>
					</div>
					<div class="aside-component">
						<!-- Heading Component-->
						<article class="heading-component">
							<div class="heading-component-inner">
								<h5 class="heading-component-title">Team Stats
								</h5>
							</div>
						</article>

						<div class="table-custom-responsive">
							<table class="table-custom table-custom-bordered table-team-statistic">
								<tbody>
								<tr>
									<td>
										<p class="team-statistic-counter">109</p>
										<p class="team-statistic-title">Points Per Game</p>
									</td>
									<td>
										<p class="team-statistic-counter">65</p>
										<p class="team-statistic-title">Rebounds Per Game</p>
									</td>
								</tr>
								<tr>
									<td>
										<p class="team-statistic-counter">23.6</p>
										<p class="team-statistic-title">Assists Per Game</p>
									</td>
									<td>
										<p class="team-statistic-counter">102</p>
										<p class="team-statistic-title">Points Allowed</p>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="aside-component">
						<div class="owl-carousel-outer-navigation">
							<!-- Heading Component-->
							<article class="heading-component">
								<div class="heading-component-inner">
									<h5 class="heading-component-title">Shop
									</h5>
									<div class="owl-carousel-arrows-outline">
										<div class="owl-nav">
											<button class="owl-arrow owl-arrow-prev"></button>
											<button class="owl-arrow owl-arrow-next"></button>
										</div>
									</div>
								</div>
							</article>

							<!-- Owl Carousel-->
							<div class="owl-carousel owl-loaded" data-items="1" data-dots="false"
							     data-nav="true" data-stage-padding="0" data-loop="true" data-margin="30"
							     data-mouse-drag="false" data-nav-custom=".owl-carousel-outer-navigation">


								<div class="owl-stage-outer">
									<div class="owl-stage"
									     style="transform: translate3d(-799px, 0px, 0px); transition: all 0s ease 0s; width: 2800px;">
										<div class="owl-item cloned"
										     style="width: 369.983px; margin-right: 30px;">
											<article class="product">
												<header class="product-header">
													<!-- Badge-->
													<div class="badge badge-shop">new
													</div>
													<div class="product-figure"><img
															src="/themes/sport/images/Home_files/product-small-2.png"
															alt=""></div>
													<div class="product-buttons">
														<div
															class="product-button product-button-share fa fa-share-alt"
															style="font-size: 22px;">
															<ul class="product-share">
																<li class="product-share-item">
																	<span>Share</span></li>
																<li class="product-share-item"><a
																		class="icon fa fa-facebook"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-instagram"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-twitter" href="#"></a>
																</li>
																<li class="product-share-item"><a
																		class="icon fa fa-google-plus"
																		href="#"></a></li>
															</ul>
														</div>
														<a class="product-button fa fa-shopping-cart"
														   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
														   style="font-size: 26px;"></a><a
															class="product-button fa fa-search-plus"
															href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-2-original.jpg"
															data-lightgallery="item"
															style="font-size: 25px;"></a>
													</div>
												</header>
												<footer class="product-content">
													<h6 class="product-title"><a
															href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
															Air Zoom Pegasus</a></h6>
													<div class="product-price"><span
															class="heading-6 product-price-new">$290</span>
													</div>
													<ul class="product-rating">
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star_half"></span></li>
													</ul>
												</footer>
											</article>
										</div>
										<div class="owl-item cloned"
										     style="width: 369.983px; margin-right: 30px;">
											<article class="product">
												<header class="product-header">
													<div class="product-figure"><img
															src="/themes/sport/images/Home_files/product-small-3.png"
															alt=""></div>
													<div class="product-buttons">
														<div
															class="product-button product-button-share fa fa-share-alt"
															style="font-size: 22px;">
															<ul class="product-share">
																<li class="product-share-item">
																	<span>Share</span></li>
																<li class="product-share-item"><a
																		class="icon fa fa-facebook"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-instagram"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-twitter" href="#"></a>
																</li>
																<li class="product-share-item"><a
																		class="icon fa fa-google-plus"
																		href="#"></a></li>
															</ul>
														</div>
														<a class="product-button fa fa-shopping-cart"
														   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
														   style="font-size: 26px;"></a><a
															class="product-button fa fa-search-plus"
															href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-3-original.jpg"
															data-lightgallery="item"
															style="font-size: 25px;"></a>
													</div>
												</header>
												<footer class="product-content">
													<h6 class="product-title"><a
															href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
															distressed baseball hat</a></h6>
													<div class="product-price"><span
															class="heading-6 product-price-new">$290</span>
													</div>
													<ul class="product-rating">
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star_half"></span></li>
													</ul>
												</footer>
											</article>
										</div>
										<div class="owl-item active"
										     style="width: 369.983px; margin-right: 30px;">
											<article class="product">
												<header class="product-header">
													<!-- Badge-->
													<div class="badge badge-red">hot<span
															class="icon fa fa-bolt"></span>
													</div>
													<div class="product-figure"><img
															src="/themes/sport/images/Home_files/product-small-1.png"
															alt=""></div>
													<div class="product-buttons">
														<div
															class="product-button product-button-share fa fa-share-alt"
															style="font-size: 22px;">
															<ul class="product-share">
																<li class="product-share-item">
																	<span>Share</span></li>
																<li class="product-share-item"><a
																		class="icon fa icon fa-facebook"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-instagram"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-twitter" href="#"></a>
																</li>
																<li class="product-share-item"><a
																		class="icon fa fa-google-plus"
																		href="#"></a></li>
															</ul>
														</div>
														<a class="product-button fa fa-shopping-cart"
														   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
														   style="font-size: 26px;"></a><a
															class="product-button fa fa-search-plus"
															href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-1-original.jpg"
															data-lightgallery="item"
															style="font-size: 25px;"></a>
													</div>
												</header>
												<footer class="product-content">
													<h6 class="product-title"><a
															href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
															hoops elite backpack</a></h6>
													<div class="product-price"><span class="product-price-old">$400</span><span
															class="heading-6 product-price-new">$290</span>
													</div>
													<ul class="product-rating">
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star_half"></span></li>
													</ul>
												</footer>
											</article>
										</div>
										<div class="owl-item" style="width: 369.983px; margin-right: 30px;">
											<article class="product">
												<header class="product-header">
													<!-- Badge-->
													<div class="badge badge-shop">new
													</div>
													<div class="product-figure"><img
															src="/themes/sport/images/Home_files/product-small-2.png"
															alt=""></div>
													<div class="product-buttons">
														<div
															class="product-button product-button-share fa fa-share-alt"
															style="font-size: 22px;">
															<ul class="product-share">
																<li class="product-share-item">
																	<span>Share</span></li>
																<li class="product-share-item"><a
																		class="icon fa icon fa-facebook"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-instagram"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-twitter" href="#"></a>
																</li>
																<li class="product-share-item"><a
																		class="icon fa fa-google-plus"
																		href="#"></a></li>
															</ul>
														</div>
														<a class="product-button fa fa-shopping-cart"
														   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
														   style="font-size: 26px;"></a><a
															class="product-button fa fa-search-plus"
															href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-2-original.jpg"
															data-lightgallery="item"
															style="font-size: 25px;"></a>
													</div>
												</header>
												<footer class="product-content">
													<h6 class="product-title"><a
															href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
															Air Zoom Pegasus</a></h6>
													<div class="product-price"><span
															class="heading-6 product-price-new">$290</span>
													</div>
													<ul class="product-rating">
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star_half"></span></li>
													</ul>
												</footer>
											</article>
										</div>
										<div class="owl-item" style="width: 369.983px; margin-right: 30px;">
											<article class="product">
												<header class="product-header">
													<div class="product-figure"><img
															src="/themes/sport/images/Home_files/product-small-3.png"
															alt=""></div>
													<div class="product-buttons">
														<div
															class="product-button product-button-share fa fa-share-alt"
															style="font-size: 22px;">
															<ul class="product-share">
																<li class="product-share-item">
																	<span>Share</span></li>
																<li class="product-share-item"><a
																		class="icon fa icon fa-facebook"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-instagram"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-twitter" href="#"></a>
																</li>
																<li class="product-share-item"><a
																		class="icon fa fa-google-plus"
																		href="#"></a></li>
															</ul>
														</div>
														<a class="product-button fa fa-shopping-cart"
														   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
														   style="font-size: 26px;"></a><a
															class="product-button fa fa-search-plus"
															href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-3-original.jpg"
															data-lightgallery="item"
															style="font-size: 25px;"></a>
													</div>
												</header>
												<footer class="product-content">
													<h6 class="product-title"><a
															href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
															distressed baseball hat</a></h6>
													<div class="product-price"><span
															class="heading-6 product-price-new">$290</span>
													</div>
													<ul class="product-rating">
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star_half"></span></li>
													</ul>
												</footer>
											</article>
										</div>
										<div class="owl-item cloned"
										     style="width: 369.983px; margin-right: 30px;">
											<article class="product">
												<header class="product-header">
													<!-- Badge-->
													<div class="badge badge-red">hot<span
															class="icon fa fa-bolt"></span>
													</div>
													<div class="product-figure"><img
															src="/themes/sport/images/Home_files/product-small-1.png"
															alt=""></div>
													<div class="product-buttons">
														<div
															class="product-button product-button-share fa fa-share-alt"
															style="font-size: 22px;">
															<ul class="product-share">
																<li class="product-share-item">
																	<span>Share</span></li>
																<li class="product-share-item"><a
																		class="icon fa icon fa-facebook"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-instagram"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-twitter" href="#"></a>
																</li>
																<li class="product-share-item"><a
																		class="icon fa fa-google-plus"
																		href="#"></a></li>
															</ul>
														</div>
														<a class="product-button fa fa-shopping-cart"
														   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
														   style="font-size: 26px;"></a><a
															class="product-button fa fa-search-plus"
															href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-1-original.jpg"
															data-lightgallery="item"
															style="font-size: 25px;"></a>
													</div>
												</header>
												<footer class="product-content">
													<h6 class="product-title"><a
															href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
															hoops elite backpack</a></h6>
													<div class="product-price"><span class="product-price-old">$400</span><span
															class="heading-6 product-price-new">$290</span>
													</div>
													<ul class="product-rating">
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star_half"></span></li>
													</ul>
												</footer>
											</article>
										</div>
										<div class="owl-item cloned"
										     style="width: 369.983px; margin-right: 30px;">
											<article class="product">
												<header class="product-header">
													<!-- Badge-->
													<div class="badge badge-shop">new
													</div>
													<div class="product-figure"><img
															src="/themes/sport/images/Home_files/product-small-2.png"
															alt=""></div>
													<div class="product-buttons">
														<div
															class="product-button product-button-share fa fa-share-alt"
															style="font-size: 22px;">
															<ul class="product-share">
																<li class="product-share-item">
																	<span>Share</span></li>
																<li class="product-share-item"><a
																		class="icon fa icon fa-facebook"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-instagram"
																		href="#"></a></li>
																<li class="product-share-item"><a
																		class="icon fa fa-twitter" href="#"></a>
																</li>
																<li class="product-share-item"><a
																		class="icon fa fa-google-plus"
																		href="#"></a></li>
															</ul>
														</div>
														<a class="product-button fa fa-shopping-cart"
														   href="https://livedemo00.template-help.com/wt_63853/soccer/shopping-cart.html"
														   style="font-size: 26px;"></a><a
															class="product-button fa fa-search-plus"
															href="https://livedemo00.template-help.com/wt_63853/soccer/images/shop/product-2-original.jpg"
															data-lightgallery="item"
															style="font-size: 25px;"></a>
													</div>
												</header>
												<footer class="product-content">
													<h6 class="product-title"><a
															href="https://livedemo00.template-help.com/wt_63853/soccer/product-page.html">Nike
															Air Zoom Pegasus</a></h6>
													<div class="product-price"><span
															class="heading-6 product-price-new">$290</span>
													</div>
													<ul class="product-rating">
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star"></span></li>
														<li><span class="fa fa-star_half"></span></li>
													</ul>
												</footer>
											</article>
										</div>
									</div>
								</div>
								<div class="owl-nav">
									<div class="owl-prev"></div>
									<div class="owl-next"></div>
								</div>
								<div class="owl-dots disabled"></div>
							</div>
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>

<?php $this->endContent() ?>
