<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \core\forms\auth\SignupForm */

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = Yii::t('auth', 'Register');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Register-->
<section class="section section-variant-1 bg-gray-100">
	<div class="container">
		<div class="row row-50 justify-content-center">
			<div class="col-md-10 col-lg-8 col-xl-6">


				<div class="row">
					<div class="col-sm-12">

						<div class="card-login-register" id="card-l-r">
							<div class="card-top-panel">
								<div class="card-top-panel-left">
									<h5 class="card-title card-title-login"><?= Yii::t('auth', 'New Customer') ?></h5>
								</div>
								<div class="card-top-panel-right">
				    <span class="card-subtitle">
					    <span class="card-subtitle-login"><?= Yii::t('auth', 'Register') ?></span>
				    </span>
								</div>
							</div>
							<div class="card-form card-form-login">

								<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

								<div class="form-wrap">
									<?= $form->field($model, 'username', [
										'template' => '{input}{error}',
										'errorOptions' => [ 'class' => 'form-validation' ]
									])->textInput([
										'autofocus' => true,
										'class' => 'form-input form-control-has-validation form-control-last-child',
										'placeholder' => Yii::t('auth', 'Username'),
									])->label(false) ?>
								</div>

								<div class="form-wrap">
									<?= $form->field($model, 'email', [
										'template' => '{input}{error}',
										'errorOptions' => [ 'class' => 'form-validation' ]
									])->textInput([
										'class' => 'form-input form-control-has-validation form-control-last-child',
										'placeholder' => Yii::t('auth', 'E-mail'),
									])->label(false) ?>
								</div>

								<div class="form-wrap">
									<?= $form->field($model, 'password', [
										'template' => '{input}{error}',
										'errorOptions' => [ 'class' => 'form-validation' ]
									])->passwordInput([
										'class' => 'form-input form-control-has-validation form-control-last-child',
										'placeholder' => Yii::t('auth', 'Password'),
									])->label(false) ?>
								</div>

								<div>
									<?= Html::submitButton(Yii::t('auth', 'Register now'), [
										'class' => 'button button-lg button-primary button-block',
										'name' => 'signup-button'
									]) ?>
								</div>

								<?php ActiveForm::end(); ?>

							</div>
						</div>

					</div>
				</div>


			</div>
		</div>
	</div>
