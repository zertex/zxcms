<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \core\forms\auth\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\authclient\widgets\AuthChoice;

$this->title = Yii::t('auth', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Login-->
<section class="section section-variant-1 bg-gray-100">
	<div class="container">
		<div class="row row-50 justify-content-center">
			<div class="col-md-12 col-lg-12 col-xl-12">


<div class="row">
    <div class="col-sm-6">
	    <div class="card-login-register" id="card-2-r">
		    <div class="card-top-panel">
			    <div class="card-top-panel-left">
				    <h5 class="card-title card-title-login"><?= Yii::t('auth', 'New Customer') ?></h5>
			    </div>
			    <div class="card-top-panel-right">
				    <span class="card-subtitle">
					    <span class="card-subtitle-login"><?= Yii::t('auth', 'Register') ?></span>
				    </span>
			    </div>
		    </div>
		    <div class="card-form card-form-login">
			    <?= Html::a(Yii::t('auth', 'Register now'), ['/auth/signup/request'], [
				    'class' => 'button button-lg button-primary button-block',
				    'name' => 'register-button'
			    ]) ?>

		    </div>
        </div>

        <div class="card-login-register" id="card-3-r" style="margin-top:20px">
	        <div class="card-top-panel">
		        <div class="card-top-panel-left">
			        <h5 class="card-title card-title-login"><?= Yii::t('auth', 'Social login') ?></h5>
		        </div>
	        </div>
	        <div class="card-form card-form-login">

			        <?php $authAuthChoice = AuthChoice::begin([
				        'baseAuthUrl' => ['/auth/network/auth'],
				        'options' => [
				            'class' => 'group-sm group-sm-justify group-middle'
				        ]
			        ]); ?>

				        <?php foreach ($authAuthChoice->getClients() as $client): ?>
					        <?= $authAuthChoice->clientLink($client, null, [
					            'class' => 'button button-' . $client->getId() . ' button-icon button-icon-left button-round',
					        ]) ?>
				        <?php endforeach; ?>

			        <?php AuthChoice::end(); ?>

	        </div>
        </div>

    </div>
    <div class="col-sm-6">

	    <div class="card-login-register" id="card-l-r">
		    <div class="card-top-panel">
			    <div class="card-top-panel-left">
				    <h5 class="card-title card-title-login"><?= Yii::t('auth', 'Sign in') ?></h5>
			    </div>
			    <div class="card-top-panel-right">
				    <span class="card-subtitle">
					    <span class="card-subtitle-login"><?= Yii::t('auth', 'Returning customer') ?></span>
				    </span>
			    </div>
		    </div>
		    <div class="card-form card-form-login">

	        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

	        <div class="form-wrap">
		        <?= $form->field($model, 'username', [
			        'template' => '{input}{error}',
			        'errorOptions' => [ 'class' => 'form-validation' ]
		        ])->textInput([
		        	'autofocus' => true,
			        'class' => 'form-input form-control-has-validation form-control-last-child',
			        'placeholder' => Yii::t('auth', 'Username'),
		        ])->label(false) ?>
	        </div>

	        <div class="form-wrap">
		        <?= $form->field($model, 'password', [
			        'template' => '{input}{error}',
			        'errorOptions' => [ 'class' => 'form-validation' ]
		        ])->passwordInput([
			        'class' => 'form-input form-control-has-validation form-control-last-child',
			        'placeholder' => Yii::t('auth', 'Password'),
		        ])->label(false) ?>
	        </div>

			<div class="row">
				<div class="col-md-6">
					<?= $form->field($model, 'rememberMe', [
						'labelOptions' => [
							'class' => 'checkbox-inline checkbox-inline-lg checkbox-light',
							'style' => 'font-weight: normal; text-transform: none',
						]
					])->checkbox([
						'class' => 'checkbox-custom',
					])->label(Yii::t('auth', 'Remember Me')) ?>
				</div>
				<div class="col-md-6" style="text-align: right;">
					<div style="color:#999;margin:1em 0">
						<?= Html::a(Yii::t('user', 'Forgot password?'), ['auth/reset/request'], [
							'style' => 'display: block; margin-top:-15px',
						]) ?>
					</div>
				</div>
			</div>





	        <div>
		        <?= Html::submitButton(Yii::t('auth', 'Sign in'), [
			        'class' => 'button button-lg button-primary button-block',
			        'name' => 'login-button'
		        ]) ?>
	        </div>

	        <?php ActiveForm::end(); ?>

		    </div>
	    </div>

    </div>
</div>


			</div>
		</div>
	</div>
</section>