<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \core\forms\auth\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('auth', 'Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Register-->
<section class="section section-variant-1 bg-gray-100">
	<div class="container">
		<div class="row row-50 justify-content-center">
			<div class="col-md-10 col-lg-8 col-xl-6">


				<div class="row">
					<div class="col-sm-12">

						<div class="card-login-register" id="card-l-r">
							<div class="card-top-panel">
								<div class="card-top-panel-left">
									<h5 class="card-title card-title-login"><?= Yii::t('auth', 'Reset password') ?></h5>
								</div>
							</div>
							<div class="card-form card-form-login">

    <p><?= Yii::t('auth', 'Please choose your new password') ?></p>


            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

				<div class="form-wrap">
					<?= $form->field($model, 'password', [
						'template' => '{input}{error}',
						'errorOptions' => [ 'class' => 'form-validation' ]
					])->passwordInput([
						'autofocus' => true,
						'class' => 'form-input form-control-has-validation form-control-last-child',
						'placeholder' => Yii::t('auth', 'New password'),
					])->label(false) ?>
				</div>

                <<div>
					<?= Html::submitButton(Yii::t('auth', 'Reset'), [
						'class' => 'button button-lg button-primary button-block',
						'name' => 'signup-button'
					]) ?>
				</div>

            <?php ActiveForm::end(); ?>

							</div>
						</div>

					</div>
				</div>


			</div>
		</div>
	</div>
