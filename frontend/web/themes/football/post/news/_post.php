<?php

/* @var $this yii\web\View */
/* @var $model \core\entities\post\Post */

use yii\helpers\Html;
use yii\helpers\Url;

$url = Url::to(['post/post', 'id' =>$model->id]);
$url_full = Yii::$app->params['frontendHostInfo'] . Url::to(['post/post', 'id' =>$model->id]);
?>


<div class="col-md-6">
	<article class="post-future">
		<a class="post-future-figure" href="<?= $url ?>">
			<img src="<?= Html::encode($model->getThumbFileUrl('image', '368_287')) ?>" alt="<?= Html::encode($model->title) ?>" />
		</a>
		<div class="post-future-main">
			<h4 class="post-future-title"><a href="<?= $url ?>"><?= Html::encode($model->title) ?></a></h4>
			<div class="post-future-meta">
				<!-- Badge-->
				<div class="badge badge-secondary"><?= $model->category->name ?></div>
				<div class="post-future-time"><span class="icon fa fa-calendar"></span>
					<time datetime="<?= date('Y') ?>"><?= Yii::$app->formatter->asDate($model->published_at, 'php:d F, Y') ?></time>
				</div>
			</div>
			<hr/>
			<div class="post-future-text">
				<p><?= \yii\helpers\StringHelper::truncateWords(Html::encode($model->description), 12, '...') ?></p>
			</div>
			<div class="post-future-footer group-flex group-flex-xs"><a class="button button-gray-outline" href="#"><?= Yii::t('post', 'Read more') ?></a>
				<div class="post-future-share">
					<div class="inline-toggle-parent">
						<div class="inline-toggle icon fa fa-share-alt" title="<?= Yii::t('post', 'Share') ?>"></div>
						<div class="inline-toggle-element">
							<ul class="list-inline">
								<li><span class="fa fa-share-alt"></span></li>
								<li><a target="_blank" class="icon fa fa-facebook" href="https://www.facebook.com/sharer.php?src=sp&u=<?= urlencode($url_full) ?>"></a></li>
								<li><a target="_blank" class="icon fa fa-odnoklassniki" href="https://connect.ok.ru/offer?url=<?= urlencode($url_full) ?>"></a></li>
								<li><a target="_blank" class="icon fa fa-google-plus" href="https://plus.google.com/share?url=<?= urlencode($url_full) ?>"></a></li>
								<li><a target="_blank" class="icon fa fa-vk" href="https://vk.com/share.php?url=<?= urlencode($url_full) ?>"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</div>
