<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $type \core\entities\post\PostType */

use yii\helpers\Html;

$this->title = $type->plural;
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider
]) ?>