<?php

/* @var $this yii\web\View */
/* @var $post \core\entities\post\Post */
/* @var $type \core\entities\post\PostType */

use frontend\widgets\post\CommentsWidget;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $post->getSeoTitle();

$this->registerMetaTag(['name' =>'description', 'content' => $post->meta->description]);
$this->registerMetaTag(['name' =>'keywords', 'content' => $post->meta->keywords]);

$this->params['breadcrumbs'][] = ['label' => $type->plural, 'url' => ['post/index', 'tid' => $type->id]];
$this->params['breadcrumbs'][] = ['label' => $post->category->name, 'url' => ['post/category', 'id' => $post->category->id]];
$this->params['breadcrumbs'][] = $post->title;

$this->params['active_category'] = $post->category;

$tagLinks = [];
foreach ($post->tags as $tag) {
    $tagLinks[] = '<li>' . Html::a(Html::encode($tag->name), ['tag', 'slug' => $tag->slug]) . '</li>';
}

$url = Url::to(['post/post', 'id' =>$post->id]);
$url_full = Yii::$app->params['frontendHostInfo'] . Url::to(['post/post', 'id' =>$post->id]);

$js = <<<JS
	jQuery(document).ready(function($) {
		if (device.desktop() || device.tablet()) {
			var relative_slider = $('#relative_carousel').bxSlider({
				infiniteLoop: true,
				hideControlOnEnd: true,
				minSlides: 2,
				maxSlides: 2,
				moveSlides: 1,
				slideMargin: 0,
				pager: false,
				prevText: '',
				nextText: '',
				controls: false
			});
			
			$('#relative-button-prev').on('click', function(){
      			relative_slider.goToPrevSlide();
      			return false;
    		});
    		
    		$('#relative-button-next').on('click', function(){
      			relative_slider.goToNextSlide();
      			return false;
    		})
		}
	});

JS;
$this->registerJs($js, $this::POS_READY);
?>


<div class="blog-post">
	<!-- Badge-->
	<div class="badge badge-secondary"><?= $post->category->name ?>
	</div>
	<h3 class="blog-post-title"><?= Html::encode($post->title) ?></h3>
	<div class="blog-post-header">
		<div class="blog-post-meta">
			<time class="blog-post-time" datetime="<?= date('Y') ?>"><span class="icon fa fa-calendar"></span><?= Yii::$app->formatter->asDate($post->published_at, 'php:d F, Y') ?></time>
			<div class="blog-post-comment"><span class="icon fa fa-comment"></span>345</div>
			<div class="blog-post-view"><span class="icon fa fa-eye"></span>234</div>
		</div>
	</div>
	<div class="blog-post-author-quote">
		<p><?= $post->description ?></p>
	</div>
	<div class="blog-post-share">
		<p><?= Yii::t('post', 'Share') ?></p>
		<ul class="group">
			<li><a target="_blank" class="icon fa fa-facebook" href="https://www.facebook.com/sharer.php?src=sp&u=<?= urlencode($url_full) ?>"></a></li>
			<li><a target="_blank" class="icon fa fa-odnoklassniki" href="https://connect.ok.ru/offer?url=<?= urlencode($url_full) ?>"></a></li>
			<li><a target="_blank" class="icon fa fa-google-plus" href="https://plus.google.com/share?url=<?= urlencode($url_full) ?>"></a></li>
			<li><a target="_blank" class="icon fa fa-vk" href="https://vk.com/share.php?url=<?= urlencode($url_full) ?>"></a></li>
		</ul>
	</div>
	<div class="blog-post-content">

		<img src="<?= Html::encode($post->getThumbFileUrl('image', '683_407')) ?>" alt="<?= Html::encode($post->title) ?>" width="683" height="407" />

		<?= Yii::$app->formatter->asHtml($post->content, [
			'Attr.AllowedRel' => array('nofollow'),
			'HTML.SafeObject' => true,
			'Output.FlashCompat' => true,
			'HTML.SafeIframe' => true,
			'URI.SafeIframeRegexp'=>'%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
		]) ?>

	</div>
</div>

<?= \frontend\widgets\post\PostByTagsWidget::widget([
	'count' => 10,
	'view' => 'slider-relative',
	'post' => $post,
]) ?>


<ul class="list-tags" style="margin-top:20px"><?= implode(', ', $tagLinks) ?></ul>

<?= CommentsWidget::widget([
    'post' => $post,
]) ?>


