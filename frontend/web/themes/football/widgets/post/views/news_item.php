<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $post \core\entities\post\Post
 */

$url = Url::to(['post/post', 'id' => $post->id]);
?>

<article class="post-classic">
	<div class="post-classic-aside">
		<a class="post-classic-figure" href="<?= $url ?>">
			<img src="<?= Html::encode($post->getThumbFileUrl('image', '94_94')) ?>" alt="<?= Html::encode($post->title) ?>" />
		</a>
	</div>
	<div class="post-classic-main">
		<p class="post-classic-title">
			<?= Html::a(Html::encode($post->title), $url) ?>
		</p>
		<div class="post-classic-time"><span class="icon fa fa-calendar"></span>
			<time datetime="<?= date('Y') ?>"><?= Yii::$app->formatter->asDate($post->published_at, 'php:d F, Y') ?></time>
		</div>
	</div>
</article>
