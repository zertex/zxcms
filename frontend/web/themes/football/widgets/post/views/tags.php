<?php
/**
 * Created by Error202
 * Date: 30.01.2018
 */

/**
 * @var $this \yii\web\View
 * @var $tags array[]
 */
?>

<div class="block-aside-item">
	<!-- Heading Component-->
	<article class="heading-component">
		<div class="heading-component-inner">
			<h5 class="heading-component-title"><?= Yii::t('post', 'Tags') ?>
			</h5>
		</div>
	</article>

	<!-- List Tags-->
	<ul class="list-tags">

		<?php foreach ($tags as $tag): ?>
			<li><a href="#"><?= $tag['name'] ?></a></li>
		<?php endforeach; ?>

	</ul>
</div>
