<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $model \core\forms\SubscribeForm
 */

$url = \yii\helpers\Url::to(['/site/subscribe']);
$successText = Yii::t('user', 'Your e-mail is subscribed!');
$js = <<<JS
 $('#bottom-subscribe-form').on('beforeSubmit', function(){
	 var data = $(this).serialize();
	 $.ajax({
	 url: '{$url}',
	 type: 'POST',
	 data: data,
	 success: function(res){
	 	var subContainer = $('#bottom-subscribe-container');
	 	subContainer.html('{$successText}');
	 	subContainer.css('padding', '20px');
	 },
	 error: function(){
	 	alert('Error: ' + res.message);
	 }
	 });
	 return false;
 });
JS;

$this->registerJs($js);
?>

<div id="bottom-subscribe-container">
<?php $form = ActiveForm::begin([
	'id' => 'bottom-subscribe-form',
	'options' => [
		'class' => 'rd-mailform rd-form rd-inline-form-creative',
	],
	'fieldConfig' => [
		'options' => [
			'tag' => false,
		],
	],
]); ?>
	<div class="form-wrap">

		<div class="form-input-wrap field-subscribeform-email required has-error">
			<?= $form->field($model, 'email', [
				'template' => '{input}{error}',// . '<label class="form-label rd-input-label" for="footer-form-email">Enter your E-mail</label>',
				'errorOptions' => [ 'class' => 'form-validation' ]
			])->textInput([
				'class' => 'form-input form-control-has-validation',
				'placeholder' => Yii::t('user', 'Enter Your E-mail'),
				'type' => 'email',
			])->label(false) ?>
		</div>

	</div>

	<div class="form-button">
		<?= Html::submitButton('<span class="icon fa fa-arrow-right"></span>', [
			'class' => 'button button-primary-outline',
		]) ?>
	</div>

<?php ActiveForm::end(); ?>
</div>
