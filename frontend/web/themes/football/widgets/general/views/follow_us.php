<?php

/**
 * @var $this \yii\web\View
 * @var $url string
 */
?>

<div class="aside-component">
	<!-- Heading Component-->
	<article class="heading-component">
		<div class="heading-component-inner">
			<h5 class="heading-component-title"><?= Yii::t('post', 'Follow us') ?>
			</h5>
		</div>
	</article>

	<!-- Buttons Media-->
	<div class="group-sm group-flex">
		<a class="button-media button-media-facebook" target="_blank" href="https://www.facebook.com/sharer.php?src=sp&u=<?= urlencode($url) ?>">
			<p class="button-media-action">
				<i class="fa fa-thumbs-o-up fa-4x" aria-hidden="true"></i>
			</p>
			<span class="button-media-icon fa fa-facebook"></span>
		</a>

		<a class="button-media button-media-odnoklassniki" target="_blank" href="https://connect.ok.ru/offer?url=<?= urlencode($url) ?>">
			<p class="button-media-action">
				<i class="fa fa-thumbs-o-up fa-4x" aria-hidden="true"></i>
			</p>
			<span class="button-media-icon fa fa-odnoklassniki"></span>
		</a>

		<a class="button-media button-media-google" target="_blank" href="https://plus.google.com/share?url=<?= urlencode($url) ?>">
			<p class="button-media-action">
				<i class="fa fa-thumbs-o-up fa-4x" aria-hidden="true"></i>
			</p>
			<span class="button-media-icon fa fa-google"></span>
		</a>

		<a class="button-media button-media-instagram" target="_blank" href="https://vk.com/share.php?url=<?= urlencode($url) ?>">
			<p class="button-media-action">
				<i class="fa fa-thumbs-o-up fa-4x" aria-hidden="true"></i>
			</p>
			<span class="button-media-icon fa fa-vk"></span>
		</a>
	</div>
</div>