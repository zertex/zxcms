<?php
/**
 * @var $this \yii\web\View
 * @var $selector string
 * @var $options array
 */

$reflection = $options['reflection'] ? 1 : 0;

$js = <<<JS
$(".$selector").jCountdown({
    selector: "{$selector}",
    timeText: "{$options['timeText']}",
    dayTextNumber: {$options['dayTextNumber']},
    style: "{$options['style']}",
    color: "{$options['color']}",
    reflection: {$reflection},
    daysLabel: "{$options['daysLabel']}",
    hoursLabel: "{$options['hoursLabel']}",
    minutesLabel: "{$options['minutesLabel']}",
    secondsLabel: "{$options['secondsLabel']}",
    labelsClass: "{$options['labelsClass']}"
});
JS;
$this->registerJs($js, $this::POS_READY);
?>

<div id="content">
    <div class="page">
        <div class="<?= $selector ?>"></div>
    </div>
</div>
