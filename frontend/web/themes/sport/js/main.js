// SmartMenus mobile menu toggle button
$(function() {
    var $mainMenuState = $('#main-menu-state');
    if ($mainMenuState.length) {
        // animate mobile menu
        $mainMenuState.change(function(e) {
            var $menu = $('#main-menu');
            if (this.checked) {
                $menu.hide().slideDown(250, function() { $menu.css('display', ''); });
            } else {
                $menu.show().slideUp(250, function() { $menu.css('display', ''); });
            }
        });
        // hide mobile menu beforeunload
        $(window).bind('beforeunload unload', function() {
            if ($mainMenuState[0].checked) {
                $mainMenuState[0].click();
            }
        });
    }
});


jQuery(document).ready(function () {

    $(function() {
        $('#main-menu').smartmenus({
            subMenusSubOffsetX: 6,
            subMenusSubOffsetY: -8
        });
    });

        if ( $.cookie('promobanner_toggle') && $.cookie('promobanner_toggle') == 'disabled' ) {
        $('#promobanner').hide();
    }
    else {
        $('#promobanner_close').on('click', function(e) {
            e.preventDefault();
            $.cookie('promobanner_toggle', 'disabled');
            $('#promobanner').slideUp();
        });
    }
    $(".promobanner-countdown").jCountdown({
        selector: "promobanner-countdown",
        timeText: "2018/01/01 09:00:00",
        dayTextNumber: 3,
        style: "flip",
        color: "black",
        reflection: 0,
        daysLabel: "Дни",
        hoursLabel: "Часы",
        minutesLabel: "Минуты",
        secondsLabel: "Секунды",
        labelsClass: "promobanner_countdown_labels"
    });

});

jQuery(function($){
    $('html').removeClass('no-js');

    // MAIN PRODUCT LISTING IMAGE CHANGE
    if ( device.desktop() ) {
        $(document).on({
            mouseenter: function(){
                $(this).find(".img__2").stop().animate({"opacity": 1});
            },
            mouseleave: function(){
                $(this).find(".img__2").stop().animate({"opacity": 0});
            }
        }, '.img_change');
    }

    // BACK TO TOP BUTTON
    $(document).ready(function(){
        $(document.body).append('<a id="back_top" title="Back to top" href="#"></a>');
        $('#back_top').hide();

        $(window).scroll(function(){
            if ( $(this).scrollTop() > 300 ) {
                $('#back_top').fadeIn("slow");
            }
            else {
                $('#back_top').fadeOut("slow");
            }
        });

        $('#back_top').on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({scrollTop : 0},800);
            $('#back_top').fadeOut("slow").stop();
        });
    });

    // SJAX //
    $(document).unbind('ajaxComplete');
    $(document.body).on('click', '.sjax', function(e) {
        e.preventDefault();
        var id = $(this).data('sjax-id');
        var message = $(this).data('sjax-message');
        var type = $(this).data('sjax-type');
        var ids = $(this).data('sjax-ids');
        var url = $(this).data('sjax-url');

        var method = $(this).data('sjax-method') ? $(this).data('sjax-method') : 'get';
        if (id) {
            $.ajax({
                url: $(this).attr('href'),
                type: method
            }).success(function(data) {
                if (url) {
                    document.location.href = url;
                }
                else {
                    $("#" + id).html($(data).find('#' + id).html());
                    if (ids) {
                        jQuery.each(ids, function (index, value) {
                            $("#" + value).html($(data).find('#' + value).html());
                        });
                    }
                    if ($(data).find("#sj_result_message").html())
                    {
                        message = $(data).find("#sj_result_message").html();
                    }

                    if ($(data).find("#sj_result_type").html())
                    {
                        type = $(data).find("#sj_result_type").html();
                    }

                    if (message && type) {
                        showNotify(type, message);
                    }
                }
            });
        }
        return false;
    });

    $(document).on("focusout",".quantity_input",function(){var t=$(this).val();$(this).val(isNaN(parseFloat(t))&&!isFinite(t)||0==parseInt(t)||""==t?1:parseInt(t)<0?parseInt(t)-2*parseInt(t):parseInt(t))}),$(document).on("click",".quantity_up",function(){var t=$(this).parent().find(".quantity_input");t.val(!isNaN(parseFloat(t.val()))&&isFinite(t.val())?parseInt(t.val())+1:1)}),$(document).on("click",".quantity_down",function(){var t=$(this).parent().find(".quantity_input");t.val(!isNaN(parseFloat(t.val()))&&isFinite(t.val())&&t.val()>1?parseInt(t.val())-1:1)});
});


function showNotify(type, message)
{
    $.notify(message ? message : 'Notify',{
        // settings
        delay: 5000,
        timer: 1000,
        element: 'body',
        position: null,
        type: type,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        }
    });
}