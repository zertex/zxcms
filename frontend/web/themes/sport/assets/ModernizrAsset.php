<?php
/**
 * Created by Error202
 * Date: 21.10.2017
 */

namespace frontend\web\themes\sport\assets;

use yii\web\AssetBundle;

class ModernizrAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/themes/sport';
    public $js = [
        'js/modernizr-2.8.3-respond-1.4.2.min.js',
    ];
}
