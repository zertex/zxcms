<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\web\themes\sport\assets;

use yii\web\AssetBundle;

class DeviceJsAsset extends AssetBundle
{
    public $sourcePath = '@bower/devicejs';
    public $js = [
        'lib/device.js',
    ];
}
