<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\web\themes\sport\assets;

use yii\web\AssetBundle;

class TemplateAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $depends = [
        FontAwesomeAsset::class,
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        MigrateAsset::class,
        DeviceJsAsset::class,
        CookieAsset::class,
        TemplateCssAsset::class,
        TemplateJsAsset::class,
    ];
}
