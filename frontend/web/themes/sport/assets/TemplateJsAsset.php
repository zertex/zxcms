<?php
/**
 * Created by Error202
 * Date: 21.08.2017
 */

namespace frontend\web\themes\sport\assets;

use yii\web\AssetBundle;

class TemplateJsAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/themes/sport';
    public $js = [
        'js/main.js',
        'plugins/smartmenus/jquery.smartmenus.js'
        //'plugins/countdown/js/countdown.js',
    ];

    public function __construct($config = [])
    {
        if (YII_ENV_DEV) {
            $this->publishOptions['forceCopy'] = true;
        }
        parent::__construct($config);
    }
}
