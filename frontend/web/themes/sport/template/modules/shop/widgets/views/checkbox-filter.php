<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $searchForm \app\modules\shop\forms\search\SearchForm
 * @var $items array
 * @var $title string
 * @var $attribute string
 * @var $form \yii\widgets\ActiveForm;
 */
?>

<div class="menu-title"><?= Html::encode($title) ?> <button type="submit" class="btn btn-blue btn-sm"></button></div>
<div class="widget_content filter">


        <?= $form->field($searchForm, $attribute, [
            'template' => '<ul class="filter-checkboxlist">{input}</ul>',
            'options' => [
                'tag' => null, // Don't wrap with "form-group" div
            ],
        ])->checkboxList($items, [
            'onclick' => "$(this).val( $('input:checkbox:checked').val()); ",
            'item' => function($index, $label, $name, $checked, $value)
            {
                $disabled = isset($label['disabled']) && $label['disabled']? 'disabled="disabled"' : '';
                $disabledCheckboxClass = isset($label['disabled']) && $label['disabled']? 'class="filter_disabled_checkbox"' : '';
                return '<li><label ' . $disabledCheckboxClass . '><input type="checkbox" ' . ($checked ? 'checked' : '') . ' name="' . $name . '" value="' . $value .'" ' . $disabled . '>' . $label['name'] . '<span class="filter_doc_count">' . $label['doc_count'] .  '</span>' . '</label></li>';
            }
        ]) ?>

</div>