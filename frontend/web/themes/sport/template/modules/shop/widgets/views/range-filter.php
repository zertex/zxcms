<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $searchForm \app\modules\shop\forms\search\SearchForm
 * @var $form \yii\widgets\ActiveForm;
 * @var $items array // [min, max]
 * @var $title string
 *
 * @var $from array; // 'attrubute'=>XX min attribute, 'value'=>XX ex.: searchForm->price_from, id for jQuery selector
 * @var $to array; // 'attrubute'=>XX max attribute, 'value'=>XX ex.: searchForm->price_to, id for jQuery selector
 */
?>

<div class="menu-title"><?= Html::encode($title) ?> <button type="submit" class="btn btn-blue btn-sm"></button></div>
<div class="widget_content filter">

        <?= $form->field($searchForm, $from['attribute'])->hiddenInput(['value' => $from['value'] ?: ''])->label(false) ?>
        <?= $form->field($searchForm, $to['attribute'])->hiddenInput(['value' => $to['value'] ?: ''])->label(false) ?>


        <?=
        \yii2mod\slider\IonSlider::widget([
            'name' => 'slider',
            'type' => \yii2mod\slider\IonSlider::TYPE_DOUBLE,
            'pluginOptions' => [
                //'disable' => true,
                'min' => $items[0],
                'max' => $items[1],
                'from' => $from['value'] ?: $items[0],
                'to' => $to['value'] ?: $items[1],
                'step' => 1,
                'force_edges' => true,
                'onChange' => new \yii\web\JsExpression('
                function(data) {
                     //console.log(data);
                     $("#' . $from['id'] . '").val(data.from);
                     $("#' . $to['id'] . '").val(data.to);
                }
           ')
            ]
        ]);
        ?>

</div>

