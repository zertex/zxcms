<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $searchForm \app\modules\shop\forms\search\SearchForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('shop_public', 'Search');

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-filter">
    <div class="panel-heading">
        <?= Yii::t('shop_public', 'Found in categories') ?>
    </div>
    <div class="panel-body">

        <?php foreach ($dataProvider->categories as $path): ?>
            <?= Breadcrumbs::widget([
                'links' => isset($path) ? $path : [],
            ]) ?>
        <?php endforeach; ?>

    </div>
</div>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider
]) ?>


