<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $brand \app\modules\shop\entities\Brand */

use yii\helpers\Html;

$this->title = $brand->getSeoTitle();

$this->registerMetaTag(['name' =>'description', 'content' => $brand->meta->description]);
$this->registerMetaTag(['name' =>'keywords', 'content' => $brand->meta->keywords]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('shop_public', 'Catalog'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $brand->name;
?>

<h2 class="page_heading"><?= Html::encode($brand->name) ?></h2>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider
]) ?>


