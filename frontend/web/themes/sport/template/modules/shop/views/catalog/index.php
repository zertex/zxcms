<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $category \app\modules\shop\entities\Category */

use yii\helpers\Html;

$this->title = Yii::t('shop_public','Catalog');
$this->params['breadcrumbs'][] = $this->title;
?>

<h2 class="page_heading"><?= Html::encode($this->title) ?></h2>
<br>
<?= $this->render('_subcategories', [
    'category' => $category
]) ?>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider
]) ?>


