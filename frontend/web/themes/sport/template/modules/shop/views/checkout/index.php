<?php

/* @var $this yii\web\View */
/* @var $cart \app\modules\shop\cart\Cart */
/* @var $model \app\modules\shop\forms\order\OrderForm */
/* @var $customerProfiles \app\modules\shop\entities\CustomerProfile[] */
/* @var $deliveryProfiles \app\modules\shop\entities\DeliveryProfile[] */

use app\modules\shop\helpers\PriceHelper;
use app\modules\shop\helpers\WeightHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;

$this->title = Yii::t('shop_public', 'Checkout Form');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop_public', 'Catalog'), 'url' => ['/shop/catalog/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop_public', 'Shopping Cart'), 'url' => ['/shop/cart/index']];
$this->params['breadcrumbs'][] = $this->title;

$profilesC = [];
// Prepare Customer Profiles Json
foreach ($customerProfiles as $customerProfile)
{
    $array = Json::decode($customerProfile->company, true);
    $profilesC[$customerProfile->id] = array_merge([
        'name' => $customerProfile->name,
        'phone' => $customerProfile->phone,
    ], is_array($array) ? $array : []);
}
$jsArrayC = Json::encode($profilesC);

$profilesD = [];
// Prepare Delivery Profiles Json
foreach ($deliveryProfiles as $deliveryProfile)
{
    $profilesD[$deliveryProfile->id] = [
        'method_id' => $deliveryProfile->method_id,
        'post_index' => $deliveryProfile->post_index,
        'post_address' => $deliveryProfile->post_address,
    ];
}
$jsArrayD = Json::encode($profilesD);

$deleteCustomerProfileUrl = Url::to(['/shop/checkout/delete-customer-profile']);
$deleteDeliveryProfileUrl = Url::to(['/shop/checkout/delete-delivery-profile']);
$checkPromoUrl = Url::to(['/shop/checkout/check']);

$js = <<<JS
    var customerProfiles = JSON.parse('{$jsArrayC}');
    var deliveryProfiles = JSON.parse('{$jsArrayD}');

    function checkPromo() {
        var code = $("#orderform-promo").val();
        if (code)
        {
            $.get( "{$checkPromoUrl}", { code: code } )
                .done(function(data) {
                    if (data.result === 'active')
                    {
                        $("#promo_info").show();
                        $("#promo_amount").html(data.amount + ' ' + data.tag);
                        $("#promo_count").html(data.count);
                    }
                    else 
                    {
                        showNotify('danger', data.message);        
                    }
            });
        }
        return false;
    }
    
    function deleteCustomerProfile() {
        var id = $("#customer_profile").val();
        if (id)
        {
            $.get( "{$deleteCustomerProfileUrl}", { id: id } )
                .done(function(data) {
                    if (data.result === 'success')
                    {
                        document.location.reload();
                    }
                    else 
                    {
                        showNotify('danger', data.message);        
                    }
            });
        }
        return false;
    }
    
    function deleteDeliveryProfile() {
        var id = $("#delivery_profile").val();
        if (id)
        {
            $.get( "{$deleteDeliveryProfileUrl}", { id: id } )
                .done(function(data) {
                    if (data.result === 'success')
                    {
                        document.location.reload();
                    }
                    else 
                    {
                        showNotify('danger', data.message);        
                    }
            });
        }
        return false;
    }

    function updateCustomerProfile() {
        var id = $("#customer_profile").val();
        if (id)
        {
            $("#customerform-phone").val(customerProfiles[id]['phone']);
            $("#customerform-name").val(customerProfiles[id]['name']);
            
            if (customerProfiles[id]['company'])
            {
                $("#customerform-is_company").prop('checked', true);
                $("#customer_company").show();
                
                $("#customerform-company").val(customerProfiles[id]['company']);
                $("#customerform-director").val(customerProfiles[id]['director']);
                $("#customerform-finance_director").val(customerProfiles[id]['finance_director']);
                $("#customerform-address").val(customerProfiles[id]['address']);
                $("#customerform-inn").val(customerProfiles[id]['inn']);
                $("#customerform-kpp").val(customerProfiles[id]['kpp']);
                $("#customerform-bank_name").val(customerProfiles[id]['bank_name']);
                $("#customerform-bik").val(customerProfiles[id]['bik']);
                $("#customerform-account_number").val(customerProfiles[id]['account_number']);
                $("#customerform-bank_account_number").val(customerProfiles[id]['bank_account_number']);
            }
            else
            {
               $("#customerform-company").val('');
                $("#customerform-director").val('');
                $("#customerform-finance_director").val('');
                $("#customerform-address").val('');
                $("#customerform-inn").val('');
                $("#customerform-kpp").val('');
                $("#customerform-bank_name").val('');
                $("#customerform-bik").val('');
                $("#customerform-account_number").val('');
                $("#customerform-bank_account_number").val('');
                
                $("#customerform-is_company").prop('checked', false);
                $("#customer_company").hide();
            }
        }
    }
    
    function updateDeliveryProfile() {
        var id = $("#delivery_profile").val();
        if (id)
        {
            $("#deliveryform-method").val(deliveryProfiles[id]['method_id']);
            $("#deliveryform-index").val(deliveryProfiles[id]['post_index']);
            $("#deliveryform-address").val(deliveryProfiles[id]['post_address']);
        }
    }
JS;
$this->registerJs($js, $this::POS_HEAD);

$js2 = <<<JS2
    $("#customer_profile").on('change', function() {
        updateCustomerProfile();
    });
    
    $("#delivery_profile").on('change', function() {
        updateDeliveryProfile();
    });
    
    $("#promo-code-button").on('click', function(){
        checkPromo();
    });
JS2;
$this->registerJs($js2, $this::POS_READY);
$cartTotal = $cart->getCost();
$cartPay = $cart->getCalculatedCost();
$cartDiscount = $cartTotal - $cartPay;
?>
<div class="cabinet-index">
    <h2 class="page_heading"><?= Html::encode($this->title) ?></h2>

    <div class="table-responsive">
        <table class="cart_list">
            <thead>
            <tr>
                <th><?= Yii::t('shop_public', 'Product Name') ?></th>
                <th><?= Yii::t('shop_public', 'Modification') ?></th>
                <th><?= Yii::t('shop_public', 'Quantity') ?></th>
                <th><?= Yii::t('shop_public', 'Price') ?></th>
                <th><?= Yii::t('shop_public', 'Total') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($cart->getItems() as $item): ?>
                <?php
                $product = $item->getProduct();
                $modification = $item->getModification();
                $url = Url::to(['/shop/catalog/product', 'id' => $product->id]);
                ?>
                <tr>
                    <td class="text-left">
                        <a href="<?= $url ?>"><?= Html::encode($product->name) ?></a>
                    </td>
                    <td class="text-left">
                        <?php if ($modification): ?>
                            <?= Html::encode($modification->name) ?>
                        <?php endif; ?>
                    </td>
                    <td class="text-left">
                        <?= $item->getQuantity() ?>
                    </td>
                    <td class="text-right"><?= PriceHelper::format($item->getCalculatedPrice()) ?></td>
                    <td class="text-right"><?= PriceHelper::format($item->getCalculatedCost()) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
    <br />

    <table class="table table-striped">
        <tr>
            <td class="text-right"><strong><?= Yii::t('shop_public', 'Total') ?></strong></td>
            <td class="text-right"><?= PriceHelper::format($cartTotal) ?></td>
        </tr>
        <tr>
            <td class="text-right"><strong><?= Yii::t('shop_public', 'Your discount') ?></strong></td>
            <td class="text-right"><?= PriceHelper::format($cartDiscount) ?></td>
        </tr>
        <tr>
            <td class="text-right"><strong><?= Yii::t('shop_public', 'To Pay') ?></strong></td>
            <td class="text-right"><?= PriceHelper::format($cartPay) ?></td>
        </tr>
        <tr>
            <td class="text-right"><strong><?= Yii::t('shop_public', 'Weight') ?></strong></td>
            <td class="text-right"><?= WeightHelper::format($cart->getWeight()) ?></td>
        </tr>
    </table>

    <?php $form = ActiveForm::begin() ?>

    <div class="table-responsive">
        <table class="cart_list">
            <thead>
            <tr>
                <th><?= Yii::t('shop_public', 'Promo Code') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-left">
                    <div style="width: 300px;">
                    <?= $form->field($model, 'promo',[
                        'template' => '{label}<div class="input-group">{input}<div class="input-group-btn"><button type="button" class="btn btn-blue" id="promo-code-button">' . Yii::t('shop_public', 'Check') . '</button></div></div>',
                    ])->textInput(['maxlength' => true])->label(Yii::t('shop_public', 'Promo Code')) ?>
                    </div>
                    <div id="promo_info" style="display:none;margin-top:15px;">
                        <?= Yii::t('shop_public', 'Discount') ?>: <span id="promo_amount"></span>
                        <br>
                        <?= Yii::t('shop_public', 'Use count') ?>: <span id="promo_count"></span>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="table-responsive">
        <table class="cart_list">
            <thead>
            <tr>
                <th><?= Yii::t('shop_public', 'Customer') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-left">

                    <?php if (!empty($customerProfiles)): ?>
                        <?= Yii::t('shop_public', 'Customer Profile') ?>
                        <?= Html::dropDownList('customer_profile', null, \yii\helpers\ArrayHelper::map($customerProfiles, 'id', 'title'), [
                            'prompt' => Yii::t('shop_public', '--- Select ---'),
                            'id' => 'customer_profile',
                        ]); ?>
                        <?= Html::a(Yii::t('shop_public', 'Delete Profile'), '#', [
                            'class' => 'text-danger',
                            'onclick' => 'return deleteCustomerProfile();'
                        ]) ?>
                    <?php endif; ?>

                    <?= $form->field($model->customer, 'phone')->textInput() ?>
                    <?= $form->field($model->customer, 'name')->textInput() ?>

                    <?= $form->field($model->customer, 'is_company')->checkbox([
                        'style' => 'vertical-align: middle; margin-top:3px; padding:0;',
                        'onclick' => 'if (this.checked) {$("#customer_company").show();} else {$("#customer_company").hide();}',
                    ]) ?>

                    <div id="customer_company" style="display: none">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model->customer, 'company')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model->customer, 'address')->textInput() ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model->customer, 'director')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model->customer, 'finance_director')->textInput() ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?= $form->field($model->customer, 'inn')->textInput() ?>
                            </div>
                            <div class="col-md-3">
                                <?= $form->field($model->customer, 'kpp')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model->customer, 'account_number')->textInput() ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <?= $form->field($model->customer, 'bik')->textInput() ?>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model->customer, 'bank_name')->textInput() ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model->customer, 'bank_account_number')->textInput() ?>
                            </div>
                        </div>

                    </div>

                </td>
            </tbody>
        </table>
    </div>


    <div class="table-responsive">
        <table class="cart_list">
            <thead>
            <tr>
                <th><?= Yii::t('shop_public', 'Delivery') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-left">

                    <?php if (!empty($deliveryProfiles)): ?>
                        <?= Yii::t('shop_public', 'Delivery Profile') ?>
                        <?= Html::dropDownList('delivery_profile', null, \yii\helpers\ArrayHelper::map($deliveryProfiles, 'id', 'title'), [
                            'prompt' => Yii::t('shop_public', '--- Select ---'),
                            'id' => 'delivery_profile',
                        ]); ?>
                        <?= Html::a(Yii::t('shop_public', 'Delete Profile'), '#', [
                            'class' => 'text-danger',
                            'onclick' => 'return deleteDeliveryProfile();'
                        ]) ?>
                    <?php endif; ?>

                    <?= $form->field($model->delivery, 'method')->dropDownList($model->delivery->deliveryMethodsList(), ['prompt' => Yii::t('shop_public', '--- Select ---') ]) ?>
                    <?= $form->field($model->delivery, 'index')->textInput() ?>
                    <?= $form->field($model->delivery, 'address')->textarea(['rows' => 3]) ?>
                </td>
            </tbody>
        </table>
    </div>

    <div class="table-responsive">
        <table class="cart_list">
            <thead>
            <tr>
                <th><?= Yii::t('shop_public', 'Note') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="text-left">
                    <?= $form->field($model, 'note')->textarea(['rows' => 3])->label(false) ?>
                </td>
            </tbody>
        </table>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('shop_public', 'Checkout'), ['class' => 'btn btn-blue order_button']) ?>
    </div>

    <?php ActiveForm::end() ?>

</div>
    
