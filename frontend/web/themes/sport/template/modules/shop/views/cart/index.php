<?php

/* @var $this yii\web\View */
/* @var $cart \app\modules\shop\cart\Cart */

use app\modules\shop\helpers\PriceHelper;
use app\modules\shop\helpers\WeightHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\shop\helpers\DiscountHelper;

$this->title = Yii::t('shop_public', 'Shopping Cart');
$this->params['breadcrumbs'][] = ['label' => Yii::t('shop_public', 'Catalog'), 'url' => ['/shop/catalog/index']];
$this->params['breadcrumbs'][] = $this->title;

$cartTotal = $cart->getCost();
$cartPay = $cart->getCalculatedCost();
$cartDiscount = $cartTotal - $cartPay;
?>
<div class="cabinet-index">
    <h2 class="page_heading"><?= Html::encode($this->title) ?></h2>

        <div id="cart_content">
            <table class="cart_list">
                <thead>
                <tr>
                    <th><?= Yii::t('shop_public', 'Product Name') ?></th>
                    <th><?= Yii::t('shop_public', 'Quantity') ?></th>
                    <th><?= Yii::t('shop_public', 'Price') ?></th>
                    <th><?= Yii::t('shop_public', 'Total') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($cart->getItems() as $item): ?>
                    <?php
                    $product = $item->getProduct();
                    $modification = $item->getModification();
                    $url = Url::to(['/shop/catalog/product', 'id' => $product->id]);
                    ?>
                    <tr class="cart_item">
                        <td class="cell_1">
                            <div class="cart_item__img">
                                <a href="<?= $url ?>"><img src="<?= $product->mainPhoto->getThumbFileUrl('file', 'cart_list') ?>" alt="" class="img-thumbnail" /></a>
                            </div>
                            <div class="cart_item__info">
                                <h3 class="cart_item__name product_name">
                                    <a href="<?= $url ?>">
                                        <?= Html::encode($product->name) ?>
                                    </a>
                                </h3>

                                <p class="cart_item__variant">
                                    <?php if ($modification): ?>
                                        <?= Html::encode($modification->name) ?>
                                    <?php endif; ?>
                                </p>

                                <div class="cart_item__details">
                                    <p class="item_vendor"><span><?= Yii::t('shop_public', 'Vendor') ?>:</span> <a href="<?= Html::encode(Url::to(['/shop/catalog/brand', 'id' => $product->brand->id])) ?>"><?= Html::encode($product->brand->name) ?></a></p>
                                    <p class="item_weight"><span><?= Yii::t('shop_public', 'Weight') ?>:</span> <?= WeightHelper::format($product->weight * $product->quantity) ?></p>
                                </div>

                            </div>
                        </td>

                        <td class="cell_2 cart_price">
                            <span class="money">
                                <?= PriceHelper::format($item->getCalculatedPrice()) ?>
                            </span>
                        </td>

                        <td class="cell_3" style="width: 110px">
                            <?= Html::beginForm(['quantity', 'id' => $item->getId()]); ?>
                            <div class="quantity_box">
                                <input class="quantity_input" name="quantity" value="<?= $item->getQuantity() ?>" type="text">
                                <span class="quantity_modifier quantity_down"><i class="fa fa-minus"></i></span>
                                <span class="quantity_modifier quantity_up"><i class="fa fa-plus"></i></span>
                            </div>
                            <div class="clearfix" style="margin-top: 3px"></div>
                            <div class="btn-group" style="display: flex; flex-direction: row;">
                                    <button style="width:100%" type="submit" title="" class="btn btn-blue" data-original-title="Update"><i class="fa fa-refresh" style="font-size: 16px"></i></button>
                                    <a style="width:100%" title="Remove" class="btn btn-red" href="<?= Url::to(['remove', 'id' => $item->getId()]) ?>" data-method="post"><i class="fa fa-times-circle" style="font-size: 16px"></i></a>
                            </div>
                            <?= Html::endForm() ?>
                        </td>

                        <td class="cell_4 cart_price">
                            <span class="money">
                                <?= PriceHelper::format($item->getCalculatedCost()) ?>
                            </span>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>


                <tfoot>
                <tr class="cart_buttons">
                    <td colspan="5">
                        <a href="<?= Url::to(['/shop/catalog/index']) ?>" class="btn btn-blue btn-alt cart_continue"><?= Yii::t('shop_public', 'Continue Shopping') ?></a></div>
                        <a class="btn btn-red" id="cart_clear" href="<?= Url::to(['/shop/cart/clear']) ?>"><?= Yii::t('shop_public', 'Clear cart') ?></a>


                    </td>
                </tr>

                <tr class="cart_summary">
                    <td colspan="5">
                        <p class="cart_summary__row"><?= Yii::t('shop_public', 'Total') ?> <span class="money"><?= PriceHelper::format($cartTotal) ?></span></p>

                        <?php if ($cartDiscount): ?>
                            <p class="cart_summary__row" style="font-size: 14px"><?= Yii::t('shop_public', 'Your discount') ?> <span class="money"><?= PriceHelper::format($cartDiscount) ?></span></p>
                        <?php endif; ?>

                        <p class="cart_summary__row"><?= Yii::t('shop_public', 'To Pay') ?> <span class="money"><?= PriceHelper::format($cartPay) ?></span></p>
                        <p class="cart_summary__row"><?= Yii::t('shop_public', 'Weight') ?> <span><?= WeightHelper::format($cart->getWeight()) ?></span></p>

                        <div class="cart_summary__checkout">
                            <?php //todo payment methods icons ?>
                            <?php if ($cart->getItems()): ?>
                                <a href="<?= Url::to('/shop/checkout/index') ?>" class="btn btn-blue btn-alt checkout_button"><?= Yii::t('shop_public', 'Checkout') ?></a>

                                <?php if (Yii::$app->user->isGuest): ?>
                                    <a href="<?= Url::to('/shop/checkout/fast') ?>" class="btn btn_blue checkout_button"><?= Yii::t('shop_public', 'Fast checkout') ?></a>
                                <?php endif; ?>

                            <?php endif; ?>
                        </div>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
</div>
    
