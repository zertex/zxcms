<?php

/* @var $this yii\web\View */
/* @var $product \app\modules\shop\entities\product\Product */
/* @var $itemClass string */

use app\modules\shop\helpers\PriceHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use app\modules\shop\helpers\ProductHelper;

$url = Url::to(['product', 'id' =>$product->id]);
$inCompare = in_array($product->id, Yii::$app->session->get('compare', []));
$inWish = Yii::$app->user->isGuest ? false : Yii::$app->user->identity->user->inWishlist($product->id);

$priceNew = $product->getCalculatedNewPrice();
$priceOld = $product->getCalculatedOldPrice();
?>


<div class="product col-sm-4 product_collection <?= $itemClass ?>">

<div class="product_wrapper">

    <?php if ($product->label && $product->label > 0): ?>
        <div class="ribbon ribbon_<?= $product->label ?>"><span><?= ProductHelper::labelName($product->label) ?></span></div>
    <?php endif; ?>

    <?php if ($product->mainPhoto): ?>
    <div class="product_img">
        <a class="img_change" href="<?= Html::encode($url) ?>">
            <img src="<?= Html::encode($product->mainPhoto->getThumbFileUrl('file', 'catalog_list')) ?>" alt="<?= Html::encode($product->name) ?>">

            <?php if (isset($product->photos[1])) { ?>
                <!-- <img class="img__2" src="//cdn.shopify.com/s/files/1/0980/5368/products/ab_wheel_roller_2_large.png?v=1441292702" alt="<?= Html::encode($product->name) ?>" style="opacity: 0;"> -->
                <img class="img__2" src="<?= Html::encode($product->photos[1]->getThumbFileUrl('file', 'catalog_list')) ?>" alt="<?= Html::encode($product->name) ?>" style="opacity: 0;">
            <?php } ?>

            <!--
            < ?php if ($product->label && $product->label > 0): ?>
                <span class="product_badge new">< ?= ProductHelper::labelName($product->label) ?></span>
            < ?php endif; ?>
            -->

            <?php if ($product->getCalculatedOldPrice()): ?>
                <span class="product_badge sale"><?= round(100-($product->getCalculatedNewPrice()/$product->getCalculatedOldPrice())*100) ?>%</span>
            <?php endif; ?>
        </a>
    </div>
    <?php endif; ?>

    <div class="product_info">
        <div class="product_price">
            <span class="money"><?= PriceHelper::format($priceNew) ?></span>
            <?php if ($priceOld): ?>
                <span class="money money_sale"><?= PriceHelper::format($priceOld) ?></span>
            <?php endif; ?>
        </div>

        <div class="product_name">
            <a href="<?= Html::encode($url) ?>"><?= Html::encode($product->name) ?></a>
        </div>

        <div class="product_desc product_desc_short"><?= Html::encode(StringHelper::truncateWords(strip_tags($product->description), 12, '...', 'utf8')) ?></div>
        <div class="product_desc product_desc_long"><?= Html::encode(StringHelper::truncateWords(strip_tags($product->description), 30, '...', 'utf8')) ?></div>

        <div class="product_manage">

        <?php if (!Yii::$app->user->isGuest): ?>
        <span id="sj-favorite-<?= $product->id ?>">
        <?= Html::a('
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
                </span>',
            $inWish ? ['/shop/wishlist/delete', 'id' => $product->id] : ['/shop/wishlist/add', 'id' => $product->id],
            [
                'class' => $inWish ? "link-red" . " sjax" : "link-blue" . " sjax",
                'title' => $inWish ? Yii::t('shop_public', 'Remove from Wish') : Yii::t('shop_public', 'Add to Wish'),
                'data' => [
                    'method' => 'post',
                ],
                'data-sjax-id' => 'sj-favorite-' . $product->id,
                'data-sjax-method' => 'post',
                'data-sjax-type' => 'success',
                'data-sjax-message' => $inWish ? Yii::t('shop_public', 'Product removed from wish list.') : Yii::t('shop_public', 'Product added to wish list.'),
            ]
            )
        ?>
        </span>
        <?php endif; ?>


        <span id="sj-compare-<?= $product->id ?>">
        <?= Html::a('
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
                </span>',
            $inCompare ? ['/shop/compare/delete', 'id' => $product->id] : ['/shop/compare/add', 'id' => $product->id],
            [
                'class' => $inCompare ? "link-red" . " sjax" : "link-blue" . " sjax",
                'title' => $inCompare ? Yii::t('shop_public', 'Remove from compare') : Yii::t('shop_public', 'Add to compare'),
                'data' => [
                    'method' => 'post',
                ],
                'data-sjax-id' => 'sj-compare-' . $product->id,
                'data-sjax-method' => 'post',
                'data-sjax-type' => 'success',
                'data-sjax-ids' => '["sj-compare-indicator"]',
                'data-sjax-message' => $inCompare ? Yii::t('shop_public', 'Product successfully removed from comparison.') : Yii::t('shop_public', 'Product successfully added for comparison.'),
            ]
        )
        ?>
        </span>

        <?php if (!$product->modifications): ?>
            <?= Html::a('
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span>',
                ['/shop/cart/add', 'id' => $product->id],
                [
                    'class' => "link-blue sjax",
                    'title' => Yii::t('shop_public', 'Add to Cart'),
                    'data-sjax-id' => 'sj-cart-widget',
                    'data-sjax-method' => 'post',
                    'data-sjax-type' => 'success',
                    'data-sjax-message' => Yii::t('shop_public', 'Product added to cart.'),
                ]
            )
            ?>
        <?php else: ?>
            <?= Html::a('
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-bars fa-stack-1x fa-inverse"></i>
                </span>',
                ['/shop/catalog/product', 'id' => $product->id],
                [
                    'class' => "link-blue",
                    'title' => Yii::t('shop_public', 'Specify options'),
                ]
            )
            ?>
        <?php endif; ?>
        </div>

    </div>

    <div class="clearfix"></div>
</div>

</div>