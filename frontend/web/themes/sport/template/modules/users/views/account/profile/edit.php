<?php

/* @var $this yii\web\View */
/* @var $model \app\modules\users\forms\manage\user\UserEditForm */
/* @var $user \app\modules\users\entities\User */

use kartik\form\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('users_public', 'Edit Profile');
//$this->params['breadcrumbs'][] = ['label' => 'Profile', 'url' => ['cabinet/default/index']];
$this->params['breadcrumbs'][] = Yii::t('users_public', 'Profile');

$image = new \app\components\MultiUploader\ImageManager();

$js = <<<JS
$('#form-profile').on('beforeValidateAttribute', function (e) {
    var phoneElement = $('#form-profile').yiiActiveForm('find', 'profileeditform-phone');
    var oldValidate = phoneElement.validate;

    phoneElement.validate = function (attribute, value, messages, deferred, form) {
        value = !value.length ? value : value.match(/\d+/g).join('');
        oldValidate(attribute, value, messages, deferred, form);
    }
});
JS;
$this->registerJs($js, $this::POS_READY);
?>
<h2 class="page_heading"><?= $this->title ?></h2>

<div class="user-update">

    <div class="row">
        <div class="col-sm-4">
            <img style="width: 150px; margin-top: 10px;" src="<?= $image->avatar(Yii::$app->user->id) ?>" alt="<?= Yii::$app->user->identity->user->username ?>">
            <p>
                <?= Yii::$app->user->identity->user->username ?>
            </p>
        </div>

        <div class="col-sm-6">

            <?php $form = ActiveForm::begin(['id' => 'form-profile']); ?>

            <?= $form->field($model, 'email')->label(Yii::t('users_public', 'E-mail')) ?>
            <?= $form->field($model, 'phone', [
                'addon' => [
                    'prepend' => ['content'=>'+']
                ]])->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9 (999) 999-9999',
                'clientOptions' => [
                    'removeMaskOnSubmit' => true,
                ],
            ])->label(Yii::t('users_public', 'Phone')) ?>
            <?= $form->field($model, 'userpic')->fileInput() ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('users_public', 'Save'), ['class' => 'btn btn-blue']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
