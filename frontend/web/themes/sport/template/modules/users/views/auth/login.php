<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\users\forms\auth\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<h2 class="page_heading"><?= Yii::t('users_public', 'Login or create an account') ?></h2>

<div id="account_login">
    <div class="account_wrapper">
        <div class="account_left">
            <div class="account_section">
                <h4><?= Yii::t('users_public', 'New here?') ?></h4>

                <p class="note"><?= Yii::t('users_public', 'Registration is free and easy!') ?></p>

                <div class="signup_feature">
                <ul>
                    <li><?= Yii::t('users_public', 'Faster checkout') ?></li>
                    <li><?= Yii::t('users_public', 'Save shipping address') ?></li>
                    <li><?= Yii::t('users_public', 'View and track orders and more') ?></li>
                </ul>
                </div>

                <?= Html::a(Yii::t('users_public', 'Create an account'), ['/users/signup/request'], ['class' => 'btn']) ?>

                <div class="row social_login">
                    <div class="col-md-12">
                        <h2><?= Yii::t('users_public', 'Social Login') ?></h2>
                        <?= yii\authclient\widgets\AuthChoice::widget([
                            'baseAuthUrl' => ['/users/network/auth']
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="account_right">
            <div class="account_section">


                    <h4><?= Yii::t('users_public', 'Already registered?') ?></h4>

                    <div class="row">
                        <div class="col-lg-12">

                            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                            <?= $form->field($model, 'username')->textInput() ?>

                            <?= $form->field($model, 'password')->passwordInput() ?>

                            <?= $form->field($model, 'rememberMe')->checkbox() ?>

                            <div>
                                <?= Html::submitButton(Yii::t('users_public', 'Sign in'), ['class' => 'btn btn-blue', 'name' => 'login-button']) ?>
                                &nbsp;&nbsp;&nbsp;
                                <?= Html::a(Yii::t('users_public', 'Forgot your password?'), ['reset/request']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>
