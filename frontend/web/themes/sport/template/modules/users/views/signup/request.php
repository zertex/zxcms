<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\users\forms\auth\SignupForm */

use yii\helpers\Html;
use kartik\form\ActiveForm;
//use yii\widgets\ActiveForm;

$this->title = Yii::t('users_public', 'Create an account');
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$('#form-signup').on('beforeValidateAttribute', function (e) {
    var phoneElement = $('#form-signup').yiiActiveForm('find', 'signupform-phone');
    var oldValidate = phoneElement.validate;

    phoneElement.validate = function (attribute, value, messages, deferred, form) {
        value = !value.length ? value : value.match(/\d+/g).join('');
        oldValidate(attribute, value, messages, deferred, form);
    }
});
JS;
$this->registerJs($js, $this::POS_READY);

?>
<h2 class="page_heading"><?= Yii::t('users_public', $this->title) ?></h2>

<div id="account_login">
    <div class="account_wrapper">
        <div class="account_section">

    <p class="note"><?= Yii::t('users_public', 'Please fill out the following fields to signup.') ?></p>

    <div class="row">
        <div class="col-lg-6">
            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['autocomplete' => 'off']]); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(Yii::t('users_public', 'Username')) ?>

            <?= $form->field($model, 'email')->label(Yii::t('users_public', 'E-mail')) ?>

            <?= $form->field($model, 'phone', [
                'addon' => [
                    'prepend' => ['content'=>'+']
                ]])->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9 (999) 999-9999',
                'clientOptions' => [
                    'removeMaskOnSubmit' => true,
                ],
            ])->label(Yii::t('users_public', 'Phone')) ?>


            <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('users_public', 'Password')) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('users_public', 'Sign up'), ['class' => 'btn btn-blue', 'name' => 'signup-button']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('users_public', 'Cancel'), ['/users/auth/login']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

        </div>
    </div>
</div>
