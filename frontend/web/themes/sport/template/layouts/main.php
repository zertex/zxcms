<?php

//use app\components\MultiUploader\ImageManager;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\web\themes\sport\widgets\CartWidget;

/* @var $this \yii\web\View */
/* @var $content string */

\kartik\growl\GrowlAsset::register($this);
\kartik\base\AnimateAsset::register($this);
$this->registerAssetBundle(\frontend\web\themes\sport\assets\TemplateAsset::class, $this::POS_END);
$this->registerAssetBundle(\frontend\web\themes\sport\assets\ModernizrAsset::class, $this::POS_HEAD);
//$this->registerAssetBundle(\app\assets\TemplateJsAsset::className(), $this::POS_END);
//\app\assets\TemplateJsAsset::register($this);

$js = <<<JS
// sf-Touchscreen v1.0b
        !function(e){e.fn.sftouchscreen=function(){return this.each(function(){e(this).find("li > ul").closest("li").children("a").each(function(){var c=e(this);c.click(function(e){if(c.hasClass("sf-clicked")){var i=c.attr("href");window.location=i}else e.preventDefault(),c.addClass("sf-clicked")}).closest("li").mouseleave(function(){c.removeClass("sf-clicked")})})})}}(jQuery);
JS;
$this->registerJs($js, $this::POS_READY);
?>
<?php $this->beginPage() ?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Магазин</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <?= Html::csrfMetaTags() ?>
    <!-- PAGE TITLE -->
    <title><?= Yii::t('main', 'TITLE TEMPLATE', [
            'title' => Html::encode($this->title),
            'site_name' => 'SITE NAME',
        ]) ?></title>

    <!-- LINK TAGS -->
    <?= \core\widgets\FaviconWidget::widget() ?>

    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>

    <?php $this->head() ?>
</head>
<body class="template-index">
<?php $this->beginBody() ?>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div id="wrapper">
    <div id="wrapper2">

        <!-- PROMO BANNER -->
        <!-- < ?= \app\modules\banners\widgets\PromobannerWidget::widget() ?> -->

        <!-- HEADER -->
        <header>
            <div class="container">
                <div class="header-top">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="private-links">
                                <a href="#"><i class="fa fa-heart" aria-hidden="true"></i> Избранное</a>

                                <a href="<?= Url::to(['/shop/compare/index']) ?>" id="compare-total" target="_blank"><i class="fa fa-exchange" aria-hidden="true"></i> <?= Yii::t('shop_public','Product Compare') ?> <?= !empty(Yii::$app->session->get('compare', [])) ? '<span id="sj-compare-indicator" class="compare_count_indicator">(' . count(Yii::$app->session->get('compare', [])) . ')</span>' : '<span id="sj-compare-indicator" class="compare_count_indicator"></span>' ?></a>

                                <?= CartWidget::widget() ?>

                                <?php if (Yii::$app->user->isGuest) : ?>
                                    <a href="<?= \yii\helpers\Url::to(['//users/auth/login']) ?>"><i class="fa fa-user" aria-hidden="true"></i> <?= Yii::t('user', 'My Account') ?></a>
                                <?php else : ?>
                                    <a href="<?= \yii\helpers\Url::to(['/users/account/profile/edit']) ?>"><i class="fa fa-user" aria-hidden="true"></i> <?= Yii::$app->user->identity->user->username ?></a>
                                    <a href="<?= \yii\helpers\Url::to(['/users/auth/logout']) ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> <?= Yii::t('user', 'Sign out') ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="social-channels">
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-google" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header-middle">
                    <div class="header-table">

                        <div class="left-box column">
                            LOGO<!--<a href="/"> < ?= Html::img((new ImageManager())->image('logo', 'logo.png')) ?></a>-->
                        </div>

                        <div class="center-box column search-box">
                            <?= \frontend\web\themes\sport\widgets\SearchLineWidget::widget() ?>
                        </div>

                        <div class="right-box column">
                            <div class="phone-number">
                                <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                                <span>+7 495 111-1111</span>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="header-bottom">
                    <nav class="main-nav" role="navigation">

                        <!-- Mobile menu toggle button (hamburger/x icon) -->
                        <input id="main-menu-state" type="checkbox"/>
                        <label class="main-menu-btn" for="main-menu-state">
                            <span class="main-menu-btn-icon"></span> Toggle main menu visibility
                        </label>

                        <!-- Sample menu definition -->
                        <ul id="main-menu" class="sm sm-sport-blue">
                            <li>
                                <a href="<?= \yii\helpers\Url::to(['/shop/catalog/index']) ?>">Каталог</a>
                                <?= \frontend\web\themes\sport\widgets\CategoryMenuWidget::widget() ?>

                            </li>



                            <li><a href="/"><?= Yii::t('main', 'Home') ?></a></li>

                            <!--<li class="has-mega-menu">
                                < ?= Html::a(Yii::t('main', 'Blog'), ['/blog/post/index']) ?>
                                < ?= \app\themes\sport\widgets\MenuLastPostWidget::widget() ?>
                            </li> -->

                            <!--< ?= \app\modules\pages\widgets\PagesMenuWidget::widget() ?> -->

                            <li class="megamenu_item_6">
                                <?= Html::a(Yii::t('main', 'Contacts'), ['/contact/contact/index']) ?>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>

        <main role="main">
            <div class="container">
                <?= $content ?>
            </div>
        </main>

        <!-- FOOTER -->
        <footer>
            <div class="container">
                <div class="footer_row__1">
                    <div class="row">
                        <div class="col-sm-6 footer_block footer_block__1">
                            <h3>О нас</h3>
                            <p>
                                Группа компаний СпортМаг включает одноименный спортивный гипермаркет, 14 спортивных
                                магазинов, крупную дилерскую сеть в стране, велотрек, теннисные корты, тренажерный зал,
                                бассен, пункт проката и сервисный центр.
                                Наши магазины специализируются на 5 видах спорта: велоспорт, плавание, теннис, туризм и
                                фитнес.
                                Нас отличает от других высокий профессионализм продавцов-консультантов, прошедших
                                специальное обучение и специально подготовленных к каждой коллекции нового сезона,
                                возможность приобрести в одном месте полный ассортимент правильно подобранной
                                экипировки.
                            </p>
                        </div>

                        <div class="col-sm-6 footer_block footer_block__2">
                            <h3>Информация</h3>
                            <ul>
                                <li>
                                    <a href="page.html">
                                        <i class="fa fa-truck"></i> <span>Доставка.</span> Наша курьерская служба
                                        доставит товар в люое время и в любое место.
                                    </a>
                                </li>
                                <li>
                                    <a href="page.html">
                                        <i class="fa fa-refresh"></i> <span>Возврат.</span> В случае брака наш курьер
                                        доставит Вам новый товар или вернет деньги.
                                    </a>
                                </li>
                                <li>
                                    <a href="page.html">
                                        <i class="fa fa-thumbs-o-up"></i> <span>Сервис.</span> Мы имеем свой сервисный
                                        центр, в который Вы всегда можете обратиться.
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="footer_row__2">
                    <div class="row">
                        <div class="col-xs-6 col-sm-3 footer_block footer_block__3">
                            <h3>Информация</h3>
                            <ul class="footer_links">

                                <li class="active"><a href="index.html" title="">Главная</a></li>


                                <li class="megamenu_pages_2">
                                    <a href="page.html">О магазине</a>
                                </li>


                                <li class="megamenu_pages_3">
                                    <a href="page.html">Доставка</a>
                                </li>


                                <li><a href="blog.html" title="">Блог</a></li>

                                <li><a href="contacts.html" title="">Контакты</a></li>

                            </ul>
                        </div>

                        <div class="col-xs-6 col-sm-3 footer_block footer_block__4">
                            <h3>Категории</h3>

                            <ul class="footer_links">
                                <li><a href="category.html" title="">Для школы</a></li>
                                <li><a href="category.html" title="">Аксессуары</a></li>
                                <li><a href="category.html" title="">Для женщин</a></li>
                                <li><a href="category.html" title="">Для детей</a></li>
                                <li><a href="category.html" title="">Виды спорта</a></li>
                            </ul>
                        </div>

                        <div class="col-xs-6 col-sm-3 footer_block footer_block__5">
                            <h3>Мой аккаунт</h3>
                            <ul class="footer_links">

                                <li><a href="profile.html" title="">Профиль</a></li>
                                <li><a href="history.html" title="">История заказов</a></li>
                                <li><a href="favorites.html" title="">Избранное</a></li>
                                <li><a href="social.html" title="">Социальные сети</a></li>
                                <li><a href="auth.html" title="">Вход</a></li>
                                <li><a href="reset.html" title="">Забыл пароль</a></li>

                            </ul>
                        </div>


                        <div class="col-xs-6 col-sm-3 footer_block footer_block__6">
                            <h3>Мы в соцсетях</h3>


                            <ul class="footer_social">
                                <li><a href="http://vk.com/test"><i class="fa fa-vk"></i> ВКонтакте</a></li>
                                <li><a href="http://facebook.com/test"><i class="fa fa-facebook"></i> Facebook</a></li>
                            </ul>

                        </div>

                    </div>
                </div>

                <div class="copyright">
                    <p role="contentinfo">&copy; 2017 СпортМаг. Все права защищены.</p>
                </div>
            </div>
        </footer>

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

