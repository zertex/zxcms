<?php
use app\components\widgets\Alert;
use yii\widgets\Breadcrumbs;

/**
 * @var $this \yii\web\View
 * @var $content string
 */

$this->title = Yii::$app->settings->data['system']['meta_title'];
?>

<?php $this->beginContent('@app/themes/sport/template/layouts/main.php') ?>

<div class="row">
    <div class="main-content col-sm-12">
        <!-- BREADCRUMBS -->
        <div class="breadcrumb_wrap">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>

        <?= Alert::widget() ?>

        <?= $content ?>

    </div>
</div>

<?php $this->endContent() ?>


