<?php
use app\components\widgets\Alert;
use app\themes\sport\widgets\CategoryWidget;
use app\themes\sport\widgets\TypeWidget;
use app\modules\shop\widgets\FeaturedWidget;
use app\modules\shop\widgets\SidebarBestsellersWidget;

/**
 * @var $this \yii\web\View
 */

$js = <<<JS
    $('.nivoSlider').nivoSlider({
        effect:'fade',
        animSpeed:500,
        pauseTime:7000,
        startSlide:0,
        pauseOnHover:true,
        directionNav:true,
        directionNavHide:false,
        controlNav:false
    });
JS;
$this->registerJs($js, $this::POS_READY);

\app\assets\NivoSliderAsset::register($this);
\app\assets\BxSliderAsset::register($this);

$js2 = <<<JSS
if ( device.desktop() || device.tablet() ) {
    $('#homepage_carousel__1').bxSlider({
        infiniteLoop: true,
        hideControlOnEnd: true,
        minSlides: 4,
        maxSlides: 4,
        moveSlides: 1,
        slideMargin: 0,
        pager: false,
        prevText: '',
        nextText: '',
    });

    $('#homepage_carousel__2').bxSlider({
        infiniteLoop: true,
        hideControlOnEnd: true,
        minSlides: 4,
        maxSlides: 4,
        moveSlides: 1,
        slideMargin: 0,
        pager: false,
        prevText: '',
        nextText: '',
    });

    $('#homepage_carousel__2').parent().parent().find('.bx-controls').hide();

    var carousel_1_prevBtn = $('#homepage_carousel__1').parent().parent().find('.bx-controls .bx-prev');
    var carousel_1_nextBtn = $('#homepage_carousel__1').parent().parent().find('.bx-controls .bx-next');

    var carousel_2_prevBtn = $('#homepage_carousel__2').parent().parent().find('.bx-controls .bx-prev');
    var carousel_2_nextBtn = $('#homepage_carousel__2').parent().parent().find('.bx-controls .bx-next');


    carousel_1_prevBtn.on('click', function(){
        carousel_2_nextBtn.trigger( "click" );
    });

    carousel_1_nextBtn.on('click', function(){
        carousel_2_prevBtn.trigger( "click" );
    });

};
                    
var titleHeight = $('.featured_products .page_heading').outerHeight();
$('.featured_products .bx-controls').css({ 'top' : -((titleHeight - 20)/2 + 20) });
JSS;
$this->registerJs($js2, $this::POS_READY);

$this->title = Yii::$app->settings->data['system']['meta_title'];
?>

<?php $this->beginContent('@app/themes/sport/template/layouts/main.php') ?>

<div class="row">
    <div class="col-sm-3">
        <aside class="left-box" role="complementary" aria-label="Боковое меню">
            <nav role="navigation" aria-label="Тематические категории">
                <?= CategoryWidget::widget([
                    'active' => $this->params['active_category'] ?? null
                ]) ?>
            </nav>

            <nav role="navigation" aria-label="Типы товаров">
                <div class="menu-title">Типы товаров</div>
                <!--<ul class="types_links">
                    <li><a href="#">Одежда</a></li>
                    <li><a href="#">Обувь</a></li>
                    <li><a href="#">Инвентарь</a></li>
                    <li><a href="#">Электроника</a></li>
                    <li><a href="#">Инструмент</a></li>
                    <li><a href="#">Аксессуары</a></li>
                </ul>-->
                <?= TypeWidget::widget() ?>
            </nav>

            <?= SidebarBestsellersWidget::widget([
                'count' => 3,
            ]) ?>

            <!--<div class="sidebar_widget sidebar_widget__products">
                <h3 class="widget_header">Хиты продаж</h3>
                <div class="widget_content">
                    <ul class="list_products">
                        <li class="product">
                            <div class="product_img">
                                <a href="#">
                                    <img src="http://zxcart.dev/static/cache/products/3/catalog_list_9.jpg" alt="Детский горный велосипед Decathlon 24" />
                                </a>
                            </div>

                            <div class="product_info">
                                <div class="product_price">
                                    <div class="money">5 939 руб.</div>
                                    <span class="money compare-at-price">12 999 руб.</span>
                                </div>

                                <div class="product_name">
                                    <a href="#">Детский горный велосипед Decathlon 24</a>
                                </div>
                            </div>
                        </li>
                        <li class="product">
                            <div class="product_img">
                                <a href="#">
                                    <img src="http://zxcart.dev/static/cache/products/4/catalog_list_15.jpg" alt="Горный велосипед Rockrider 340 26&quot; B&#039;TWIN" />
                                </a>
                            </div>

                            <div class="product_info">
                                <div class="product_price">
                                    <div class="money">9 599 руб.</div>
                                    <span class="money compare-at-price">15 999 руб.</span>
                                </div>

                                <div class="product_name">
                                    <a href="#">Горный велосипед Rockrider 340 26&quot; B&#039;TWIN</a>
                                </div>
                            </div>
                        </li>
                        <li class="product">
                            <div class="product_img">
                                <a href="#">
                                    <img src="http://zxcart.dev/static/cache/products/5/catalog_list_26.jpg" alt="Велосипедный шлем VELO 300 (детский) B&#039;TWIN" />
                                </a>
                            </div>

                            <div class="product_info">
                                <div class="product_price">
                                    <div class="money">419 руб.</div>
                                    <span class="money compare-at-price">599 руб.</span>
                                </div>

                                <div class="product_name">
                                    <a href="#">Велосипедный шлем VELO 300 (детский) B&#039;TWIN</a>
                                </div>
                            </div>
                        </li>
                        <li class="product">
                            <div class="product_img">
                                <a href="#">
                                    <img src="http://zxcart.dev/static/cache/products/6/catalog_list_37.jpg" alt="Мужские кроссовки Asics Gel Ziruss" />
                                </a>
                            </div>

                            <div class="product_info">
                                <div class="product_price">
                                    <div class="money">7 623 руб.</div>
                                    <span class="money compare-at-price">10 890 руб.</span>
                                </div>

                                <div class="product_name">
                                    <a href="#">Мужские кроссовки Asics Gel Ziruss</a>
                                </div>
                            </div>
                        </li>
                        <li class="product">
                            <div class="product_img">
                                <a href="#">
                                    <img src="http://zxcart.dev/static/cache/products/10/catalog_list_74.jpg" alt="Велосипед Hoptown 320 20&quot;" />
                                </a>
                            </div>

                            <div class="product_info">
                                <div class="product_price">
                                    <div class="money">6 720 руб.</div>
                                    <span class="money compare-at-price">11 200 руб.</span>
                                </div>

                                <div class="product_name">
                                    <a href="#">Велосипед Hoptown 320 20&quot;</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>-->

        </aside>
    </div>
    <div class="main-content col-sm-9">
        <?= Alert::widget() ?>

        <?= \app\modules\banners\widgets\SliderWidget::widget() ?>

        <!-- SLIDER
        <div class="slider_wrap">
            <div class="nivoSlider">
                <a href="product.html"><img src="img/frame_1.jpg" alt="Ботинок" title="#htmlcaption-1"></a>
                <a href="product.html"><img src="img/frame_2.jpg" alt="Нет... Ботинок тут" title="#htmlcaption-2"></a>
                <a href="product.html"><img src="img/frame_3.jpg" alt="А вот часы!" title="#htmlcaption-3"></a>
            </div>
        </div>

        <div class="caption_hidden">
            <div id="htmlcaption-1">
                <a href="product.html">
                    <div class="wrap_1">
                        <div class="wrap_2">
                            <h3>Ботинок</h3>
                            <p>Описание ботинка</p>
                            <span class="btn btn-blue">Купить ботинок!</span>
                        </div>
                    </div>
                </a>
            </div>
            <div id="htmlcaption-2">
                <a href="product.html">
                    <div class="wrap_1">
                        <div class="wrap_2">
                            <h3>Нет... Ботинок тут</h3>
                            <p>А там был коврик для иоооги</p>
                            <span class="btn btn-blue">Купи уже!</span>
                        </div>
                    </div>
                </a>
            </div>
            <div id="htmlcaption-3">
                <a href="product.html">
                    <div class="wrap_1">
                        <div class="wrap_2">
                            <h3>А вот часы!</h3>
                            <p>Лучшие умные часы, проверенные нами</p>
                            <span class="btn btn-blue">Забрать сейчас</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>-->

        <!-- SHOWCASE CUSTOM BLOCKS -->
        <div id="showcase">

            <div class="row">
                <div class="col-sm-12 col-lg-6" style="margin-top: 30px;">
                    <?= \app\modules\banners\widgets\BannerWidget::widget([
                        'group_id' => 1,
                    ]) ?>
                </div>

                <div class="col-sm-12 col-lg-6" style="margin-top: 30px;">
                    <?= \app\modules\banners\widgets\BannerWidget::widget([
                        'group_id' => 2,
                    ]) ?>
                </div>
            </div>

        </div>
        <!--
        <div id="showcase">
            <div class="row">
                <div class="col-sm-12 col-lg-6" style="margin-top: 20px;">
                    <a href="product.html" target="_blank"><img src="img/4.png" alt=""></a>
                </div>

                <div class="col-sm-12 col-lg-6" style="margin-top: 20px;">
                    <a href="product.html" target="_self"><img src="img/5.png" alt=""></a>
                </div>
            </div>
        </div>-->

        <!-- FEATURED
        <div class="featured_products">

            <h2 class="page_heading">Рекомендуем</h2>

            <div id="homepage_carousel__1" class="product_listing_main homepage_carousel row">
                <div class="product col-sm-3 product_homepage item_1">
                    <div class="product_wrapper">

                        <div class="product_img">
                            <a class="img_change" href="product.html">
                                <img src="img/catalog_list_9.jpg"
                                     alt="Детский горный велосипед Decathlon 24">

                                <img class="img__2"
                                     src="img/catalog_list_10.jpg"
                                     alt="Детский горный велосипед Decathlon 24" style="opacity: 0;">

                                <span class="product_badge sale">54%</span>
                            </a>
                        </div>

                        <div class="product_info">
                            <div class="product_price">
                                <span class="money">5 939 руб.</span>
                                <span class="money money_sale">12 999 руб.</span>
                            </div>

                            <div class="product_name">
                                <a href="product.html">Детский горный велосипед Decathlon 24</a>
                            </div>

                            <div class="product_desc product_desc_short">Детский велосипед с колесами 24
                                дюйма для начала занятий горным велоспортом. Жесткая...
                            </div>
                            <div class="product_desc product_desc_long">Детский велосипед с колесами 24
                                дюйма для начала занятий горным велоспортом. Жесткая вилка и 6
                                скоростей,
                                переключающихся шифтерной рукояткой, для катания по любому покрытию.

                                Для обучения катанию на горном велосипеде...
                            </div>

                            <div class="product_manage">
                                                    <span id="sj-favorite-3">
                                                        <a class="link-red sjax" href="favorites.html" title="Убрать из избранного"
                                                           data-method="post" data-sjax-id="sj-favorite-3"
                                                           data-sjax-method="post" data-sjax-type="success"
                                                           data-sjax-message="Товар удален из избранного.">
                                                            <span class="fa-stack fa-lg">
                                                                <i class="fa fa-circle fa-stack-2x"></i>
                                                                <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>
                                                    </span>

                                <span id="sj-compare-3">
                                                        <a class="link-blue sjax" href="compare.html" title="Сравнить"
                                                           data-method="post" data-sjax-id="sj-compare-3"
                                                           data-sjax-method="post" data-sjax-type="success"
                                                           data-sjax-ids="[&quot;sj-compare-indicator&quot;]"
                                                           data-sjax-message="Товар добавлен для сравнения.">
                                                            <span class="fa-stack fa-lg">
                                                                <i class="fa fa-circle fa-stack-2x"></i>
                                                                <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>
                                                    </span>

                                <a class="link-blue sjax" href="basket.html" title="В корзину"
                                   data-sjax-id="sj-cart-widget" data-sjax-method="post"
                                   data-sjax-type="success"
                                   data-sjax-message="Товар добавлен в корзину.">
                                                        <span class="fa-stack fa-lg">
                                                            <i class="fa fa-circle fa-stack-2x"></i>
                                                            <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                                                        </span>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="product col-sm-3 product_homepage item_2">
                    <div class="product_wrapper">
                        <div class="product_img">
                            <a class="img_change" href="product.html">
                                <img src="img/catalog_list_15.jpg"
                                     alt="Горный велосипед Rockrider 340 26&quot; B&#039;TWIN">
                                <img class="img__2"
                                     src="img/catalog_list_16.jpg"
                                     alt="Горный велосипед Rockrider 340 26&quot; B&#039;TWIN"
                                     style="opacity: 0;">
                                <span class="product_badge sale">40%</span>
                            </a>
                        </div>

                        <div class="product_info">
                            <div class="product_price">
                                <span class="money">9 599 руб.</span>
                                <span class="money money_sale">15 999 руб.</span>
                            </div>

                            <div class="product_name">
                                <a href="product.html">Горный велосипед Rockrider 340 26&quot; B&#039;TWIN</a>
                            </div>
                            <div class="product_desc product_desc_short">Для нерегулярных занятий
                                спортивным
                                горным велотуризмом. &quot;При заказе онлайн: велосипеды доставляются
                                с...
                            </div>
                            <div class="product_desc product_desc_long">Для нерегулярных занятий
                                спортивным
                                горным велотуризмом. &quot;При заказе онлайн: велосипеды доставляются с
                                незакрепленными педалями и незакрепленным рулем. Полную сборку
                                велосипеда
                                можно осуществить бесплатно в мастерских наших магазинов.&quot;
                                Первый горный велосипед...
                            </div>
                            <div class="product_manage">
                                                    <span id="sj-favorite-4">
                                                        <a class="link-blue sjax" href="favorites.html" title="В избранное"
                                                           data-method="post" data-sjax-id="sj-favorite-4"
                                                           data-sjax-method="post" data-sjax-type="success"
                                                           data-sjax-message="Товар добавлен в избранное.">
                                                            <span class="fa-stack fa-lg">
                                                                <i class="fa fa-circle fa-stack-2x"></i>
                                                                <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>
                                                    </span>

                                <span id="sj-compare-4">
                                                        <a class="link-blue sjax" href="compare.html" title="Сравнить"
                                                           data-method="post" data-sjax-id="sj-compare-4"
                                                           data-sjax-method="post" data-sjax-type="success"
                                                           data-sjax-ids="[&quot;sj-compare-indicator&quot;]"
                                                           data-sjax-message="Товар добавлен для сравнения.">
                                                            <span class="fa-stack fa-lg">
                                                                <i class="fa fa-circle fa-stack-2x"></i>
                                                                <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>
                                                    </span>

                                <a class="link-blue" href="product.html" title="Укажите параметры">
                                                        <span class="fa-stack fa-lg">
                                                            <i class="fa fa-circle fa-stack-2x"></i>
                                                            <i class="fa fa-bars fa-stack-1x fa-inverse"></i>
                                                        </span>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="product col-sm-3 product_homepage item_3">
                    <div class="product_wrapper">
                        <div class="product_img">
                            <a class="img_change" href="product.html">
                                <img src="img/catalog_list_37.jpg"
                                     alt="Мужские кроссовки Asics Gel Ziruss">
                                <img class="img__2"
                                     src="img/catalog_list_38.jpg"
                                     alt="Мужские кроссовки Asics Gel Ziruss" style="opacity: 0;">
                                <span class="product_badge sale">30%</span>
                            </a>
                        </div>

                        <div class="product_info">
                            <div class="product_price">
                                <span class="money">7 623 руб.</span>
                                <span class="money money_sale">10 890 руб.</span>
                            </div>

                            <div class="product_name">
                                <a href="product.html">Мужские кроссовки Asics Gel Ziruss</a>
                            </div>

                            <div class="product_desc product_desc_short">Для занятий бегом на дорогах
                                3-6
                                раз в неделю со скоростью до...
                            </div>
                            <div class="product_desc product_desc_long">Для занятий бегом на дорогах 3-6
                                раз
                                в неделю со скоростью до 10-12 км/ч.

                                Эксклюзивная разработка, впервые представленная с моделью GEL NIMBUS 18,
                                вновь порадует спортсменов с нейтральной пронацией в...
                            </div>

                            <div class="product_manage">

                                                    <span id="sj-favorite-6">
                                                        <a class="link-red sjax" href="favorites.html" title="Убрать из избранного"
                                                           data-method="post" data-sjax-id="sj-favorite-6"
                                                           data-sjax-method="post" data-sjax-type="success"
                                                           data-sjax-message="Товар удален из избранного.">
                                                            <span class="fa-stack fa-lg">
                                                                <i class="fa fa-circle fa-stack-2x"></i>
                                                                <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>
                                                    </span>

                                <span id="sj-compare-6">
                                                        <a class="link-blue sjax" href="compare.html"
                                                           title="Сравнить" data-method="post"
                                                           data-sjax-id="sj-compare-6" data-sjax-method="post"
                                                           data-sjax-type="success"
                                                           data-sjax-ids="[&quot;sj-compare-indicator&quot;]"
                                                           data-sjax-message="Товар добавлен для сравнения.">
                                                            <span class="fa-stack fa-lg">
                                                                <i class="fa fa-circle fa-stack-2x"></i>
                                                                <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>
                                                    </span>

                                <a class="link-blue sjax" href="basket.html" title="В корзину"
                                   data-sjax-id="sj-cart-widget" data-sjax-method="post"
                                   data-sjax-type="success"
                                   data-sjax-message="Товар добавлен в корзину.">
                                                        <span class="fa-stack fa-lg">
                                                            <i class="fa fa-circle fa-stack-2x"></i>
                                                            <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                                                        </span>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div id="homepage_carousel__2" class="product_listing_main homepage_carousel row">
                <div class="product col-sm-3 product_homepage item_1">
                    <div class="product_wrapper">
                        <div class="product_img">
                            <a class="img_change" href="product.html">
                                <img src="img/catalog_list_45.jpg"
                                     alt="Полуботинки Arpenaz 50 (детские) QUECHUA">

                                <img class="img__2"
                                     src="img/catalog_list_46.jpg"
                                     alt="Полуботинки Arpenaz 50 (детские) QUECHUA" style="opacity: 0;">

                                <span class="product_badge sale">30%</span>
                            </a>
                        </div>

                        <div class="product_info">
                            <div class="product_price">
                                <span class="money">559 руб.</span>
                                <span class="money money_sale">799 руб.</span>
                            </div>

                            <div class="product_name">
                                <a href="product.html">Полуботинки Arpenaz 50 (детские)
                                    QUECHUA</a>
                            </div>

                            <div class="product_desc product_desc_short">Для непродолжительных (полдня)
                                походов в сухую погоду по легким маршрутам.

                                Для нерегулярного...
                            </div>
                            <div class="product_desc product_desc_long">Для непродолжительных (полдня)
                                походов в сухую погоду по легким маршрутам.

                                Для нерегулярного использования.Прочная и легкая обувь по невысокой
                                цене.
                                Подошва из резины: долгий срок службы обуви.
                            </div>

                            <div class="product_manage">

                <span id="sj-favorite-7">
        <a class="link-blue sjax" href="favorites.html" title="В избранное" data-method="post"
           data-sjax-id="sj-favorite-7" data-sjax-method="post" data-sjax-type="success"
           data-sjax-message="Товар добавлен в избранное.">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
                </span></a>        </span>


                                <span id="sj-compare-7">
        <a class="link-blue sjax" href="compare.html" title="Сравнить" data-method="post"
           data-sjax-id="sj-compare-7" data-sjax-method="post" data-sjax-type="success"
           data-sjax-ids="[&quot;sj-compare-indicator&quot;]" data-sjax-message="Товар добавлен для сравнения.">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
                </span></a>        </span>

                                <a class="link-blue sjax" href="basket.html" title="В корзину"
                                   data-sjax-id="sj-cart-widget" data-sjax-method="post"
                                   data-sjax-type="success"
                                   data-sjax-message="Товар добавлен в корзину.">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span></a></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div>

                <div class="product col-sm-3 product_homepage item_2">

                    <div class="product_wrapper">

                        <div class="product_img">
                            <a class="img_change" href="product.html">
                                <img src="img/catalog_list_56.jpg"
                                     alt="Женская утепленная куртка белого цвета для катания на лыжах Nordic 50 Queshua">

                                <img class="img__2"
                                     src="img/catalog_list_57.jpg"
                                     alt="Женская утепленная куртка белого цвета для катания на лыжах Nordic 50 Queshua"
                                     style="opacity: 0;">

                                <span class="product_badge sale">30%</span>
                            </a>
                        </div>

                        <div class="product_info">
                            <div class="product_price">
                                <span class="money">2 449 руб.</span>
                                <span class="money money_sale">3 499 руб.</span>
                            </div>

                            <div class="product_name">
                                <a href="product.html">Женская утепленная куртка белого
                                    цвета для катания на лыжах Nordic 50 Queshua</a>
                            </div>

                            <div class="product_desc product_desc_short">Для защиты от холода и снега во
                                время прогулочного катания на беговых...
                            </div>
                            <div class="product_desc product_desc_long">Для защиты от холода и снега во
                                время прогулочного катания на беговых лыжах.

                                Утеплитель куртки способствует сохранению тепла тела, обеспечивая при
                                этом
                                отвод влагоиспарений.
                            </div>

                            <div class="product_manage">

                <span id="sj-favorite-8">
        <a class="link-blue sjax" href="favorites.html" title="В избранное" data-method="post"
           data-sjax-id="sj-favorite-8" data-sjax-method="post" data-sjax-type="success"
           data-sjax-message="Товар добавлен в избранное.">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
                </span></a>        </span>


                                <span id="sj-compare-8">
        <a class="link-blue sjax" href="compare.html" title="Сравнить" data-method="post"
           data-sjax-id="sj-compare-8" data-sjax-method="post" data-sjax-type="success"
           data-sjax-ids="[&quot;sj-compare-indicator&quot;]" data-sjax-message="Товар добавлен для сравнения.">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
                </span></a>        </span>

                                <a class="link-blue sjax" href="basket.html" title="В корзину"
                                   data-sjax-id="sj-cart-widget" data-sjax-method="post"
                                   data-sjax-type="success"
                                   data-sjax-message="Товар добавлен в корзину.">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span></a></div>

                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div>

                <div class="product col-sm-3 product_homepage item_3">

                    <div class="product_wrapper">

                        <div class="product_img">
                            <a class="img_change" href="product.html">
                                <img src="img/catalog_list_141.jpg"
                                     alt="Круг надувной, детский">

                                <img class="img__2"
                                     src="img/catalog_list_142.jpg"
                                     alt="Круг надувной, детский" style="opacity: 0;">

                                <span class="product_badge sale">48%</span>
                            </a>
                        </div>

                        <div class="product_info">
                            <div class="product_price">
                                <span class="money">209 руб.</span>
                                <span class="money money_sale">399 руб.</span>
                            </div>

                            <div class="product_name">
                                <a href="product.html">Круг надувной, детский</a>
                            </div>

                            <div class="product_desc product_desc_short">Круг надувной
                            </div>
                            <div class="product_desc product_desc_long">Круг надувной
                            </div>

                            <div class="product_manage">

                                            <span id="sj-favorite-18">
                                                <a class="link-red sjax" href="favorites.html" title="Убрать из избранного"
                                                   data-method="post"
                                                   data-sjax-id="sj-favorite-18" data-sjax-method="post"
                                                   data-sjax-type="success"
                                                   data-sjax-message="Товар удален из избранного.">
                                                        <span class="fa-stack fa-lg">
                                                            <i class="fa fa-circle fa-stack-2x"></i>
                                                            <i class="fa fa-bookmark fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                </a>
                                            </span>


                                <span id="sj-compare-18">
                                                <a class="link-blue sjax" href="compare.html" title="Сравнить" data-method="post"
                                                   data-sjax-id="sj-compare-18" data-sjax-method="post"
                                                   data-sjax-type="success"
                                                   data-sjax-ids="[&quot;sj-compare-indicator&quot;]"
                                                   data-sjax-message="Товар добавлен для сравнения.">
                                                        <span class="fa-stack fa-lg">
                                                            <i class="fa fa-circle fa-stack-2x"></i>
                                                            <i class="fa fa-exchange fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                </a>
                                            </span>

                                <a class="link-blue sjax" href="basket.html" title="В корзину"
                                   data-sjax-id="sj-cart-widget" data-sjax-method="post"
                                   data-sjax-type="success"
                                   data-sjax-message="Товар добавлен в корзину.">
                                                <span class="fa-stack fa-lg">
                                                    <i class="fa fa-circle fa-stack-2x"></i>
                                                    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                                                </span>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div> -->

        <div class="featured_products">

            <h2 class="page_heading"><?= Yii::t('shop_public', 'Featured Products') ?></h2>

            <?= FeaturedWidget::widget([
                'count' => 12,
            ]) ?>

        </div>


    </div>
</div>

<?php $this->endContent() ?>


