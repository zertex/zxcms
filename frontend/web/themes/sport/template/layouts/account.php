<?php
use app\components\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/**
 * @var $this \yii\web\View
 * @var $content string
 */

$this->title = Yii::$app->settings->data['system']['meta_title'];
?>

<?php $this->beginContent('@app/themes/sport/template/layouts/main.php') ?>

<div class="row">
    <div class="col-sm-3">
        <aside class="left-box" role="complementary" aria-label="Боковое меню">
            <nav role="navigation" aria-label="Тематические категории">
                <ul class="category_links">

                    <li>
                        <?= Html::a(Yii::t('users_public', 'Profile'), ['/users/account/profile/edit']) ?>
                    </li>

                    <li>
                        <?= Html::a(Yii::t('shop_public', 'Orders History'), ['/shop/order/index']) ?>
                    </li>

                    <li>
                        <?= Html::a(Yii::t('users_public', 'Wish list'), ['/shop/wishlist/index']) ?>
                    </li>

                    <li>
                        <?= Html::a(Yii::t('users_public', 'Social networks'), ['/users/account/profile/social']) ?>
                    </li>

                    <li>
                        <?= Html::a(Yii::t('users_public', 'Sign out'), ['/users/auth/logout']) ?>
                    </li>

                </ul>
            </nav>

        </aside>
    </div>
    <div class="main-content col-sm-9">
        <!-- BREADCRUMBS -->
        <div class="breadcrumb_wrap">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>

        <?= Alert::widget() ?>

        <?= $content ?>

    </div>
</div>

<?php $this->endContent() ?>


