<?php

namespace app\themes\sport\widgets;

use app\modules\shop\entities\ProductType;
use app\modules\shop\repositories\read\views\TypeView;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\widgets\Menu;

class TypeWidget extends Widget
{
    public function run(): string
    {
        $types = ProductType::find()->all();
        $menuArray = array_map(function(ProductType $type){
            return [
                'label' => $type->name,
                'url' => ['/shop/catalog/product-type', 'id' => $type->id],
            ];
        }, $types);

        return Menu::widget([
            'items' => $menuArray,
            'encodeLabels' => false,
            'options' => [
                'class' => 'types_links',
            ],
        ]);
    }
}