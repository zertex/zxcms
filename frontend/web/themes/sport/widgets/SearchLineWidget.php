<?php
/**
 * Created by Error202
 * Date: 21.10.2017
 */

namespace frontend\web\themes\sport\widgets;

use common\modules\shop\forms\search\ShopSearchForm;
use yii\base\Widget;

class SearchLineWidget extends Widget
{
    public function run(): string
    {
        $searchForm = new ShopSearchForm();
        $searchForm->load(\Yii::$app->request->queryParams);
        $searchForm->validate();

        return $this->render('search-line', [
            'searchForm' => $searchForm,
        ]);
    }
}
