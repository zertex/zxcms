<?php
/**
 * Created by Error202
 * Date: 24.10.2017
 */

namespace frontend\web\themes\sport\widgets;

use yii\base\Widget;
use common\modules\shop\cart\ShopCart;

class CartWidget extends Widget
{
    private $_cart;

    public function __construct(ShopCart $cart, $config = [])
    {
        parent::__construct($config);
        $this->_cart = $cart;
    }

    public function run(): string
    {
        return $this->render('cart', [
            'cart' => $this->_cart,
        ]);
    }
}
