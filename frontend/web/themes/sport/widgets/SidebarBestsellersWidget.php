<?php
/**
 * Created by Error202
 * Date: 18.09.2017
 */

namespace app\themes\sport\widgets;


use app\modules\shop\entities\product\Product;
use yii\base\Widget;
use Yii;
use yii\helpers\Json;
use app\modules\shop\repositories\read\ProductReadRepository;

class SidebarBestsellersWidget extends Widget
{
    public $count;

    private $products;

    public function __construct(ProductReadRepository $products, $config = [])
    {
        parent::__construct($config);
        $this->products = $products;
    }

    public function init(){
        parent::init();
        $this->count = $this->count != null ? $this->count : 3;
    }

    public function run(): string
    {
        if (Yii::$app->settings->data['shop']['bestsellers_type'] == 'random')
        {
            $products = Product::find()->active()->bestseller()->limit($this->count)->all();
        }
        else
        {
            $ids = Json::decode(Yii::$app->settings->data['shop']['bestsellers_products']);
            $products = $this->products->findMany($ids);
        }

        return $this->render('sidebar-bestsellers', [
            'products' => $products,
        ]);
    }
}