<?php
/**
 * Created by Error202
 * Date: 22.10.2017
 */

namespace frontend\web\themes\sport\widgets;

use common\modules\shop\entities\ShopCategory;
use common\modules\shop\repositories\read\ShopCategoryReadRepository;
use yii\helpers\Html;
use yii\base\Widget;

class CategoryMenuWidget extends Widget
{
    /** @var ShopCategory|null */
    private $_categories;

    public function __construct(ShopCategoryReadRepository $categories, $config = [])
    {
        parent::__construct($config);
        $this->_categories = $categories;
    }

    public function run(): string
    {
        $menu = '';
        //$categories = $this->_categories->getAllTree(5);
        $categories = $this->_categories->getAllTree();
        $depth=0;
        foreach ($categories as $n => $category) {
            if ($category->depth == $depth) {
                $menu .= Html::endTag('li') . "\n";
            } elseif ($category->depth > $depth) {
                $menu .= Html::beginTag('ul') . "\n";
            } else {
                $menu .= Html::endTag('li') . "\n";

                for ($i = $depth - $category->depth; $i; $i--) {
                    $menu .= Html::endTag('ul') . "\n";
                    $menu .= Html::endTag('li') . "\n";
                }
            }
            $menu .= Html::beginTag('li');
            $menu .= Html::a(Html::encode($category->translation->name), '#');
            $depth = $category->depth;
        }
        for ($i = $depth; $i; $i--) {
            $menu .= Html::endTag('li') . "\n";
            $menu .= Html::endTag('ul') . "\n";
        }
        return $menu;
    }
}
