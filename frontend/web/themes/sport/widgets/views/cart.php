<?php

/* @var $cart \common\modules\shop\cart\ShopCart */

use common\modules\shop\helpers\PriceHelper;
use yii\helpers\Url;
?>

    <span id="sj-cart-widget">
        <a href="<?= Url::to(['/shop/cart']) ?>"><i class="fa fa-shopping-cart"></i> <?= Yii::t('shop_public', 'Cart ({count})', ['count' => $cart->getSumAmount()]) ?></a>
    </span>