<?php

use yii\widgets\ActiveForm;

/**
 * @var $this \yii\web\View
 * @var $searchForm \common\modules\shop\forms\search\ShopSearchForm
 */
?>

    <?php $form = ActiveForm::begin([
        'action' => ['/shop/catalog/search'],
        'method' => 'get',
    ]) ?>

    <div class="input-group">
        <?= $form->field($searchForm, 'text', [
            'template' => '{input}',
            'options' => [
                'tag' => null, // Don't wrap with "form-group" div
            ],
        ])->textInput([
            'class' => 'form-control',
            'id' => 'search-field',
        ])->label(false) ?>
        <span class="input-group-btn">
            <button class="btn btn-blue"><?= Yii::t('shop_public', 'Search') ?></button>>
        </span>
    </div>


    <?php ActiveForm::end() ?>

