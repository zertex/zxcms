<?php

/** @var $posts \app\modules\blog\entities\post\Post[] */

use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
?>


<ul class="mega-menu">
    <li>
        <?php foreach ($posts as $post): ?>
            <?php $url = Url::to(['/blog/post/post', 'id' =>$post->id]); ?>
        <div class="column-1-3">
            <p class="blog_date"><?= Yii::$app->formatter->asDateTime($post->created_at, 'php:l, d F, Y'); ?></p>
            <?php if ($post->photo): ?>
                <div class="blog_menu_image" style="background-image: url('<?= Html::encode($post->getThumbFileUrl('photo', 'origin')) ?>')"></div>
            <?php endif; ?>
            <h3 class="blog_title"><a href="<?= Html::encode($url) ?>"><?= Html::encode(StringHelper::truncateWords(strip_tags($post->title), 6)) ?></a></h3>
            <p class="blog_content">
                <?= Html::encode(StringHelper::truncateWords(strip_tags($post->description), 20)) ?>
            </p>
        </div>
        <?php endforeach; ?>
    </li>
</ul>
