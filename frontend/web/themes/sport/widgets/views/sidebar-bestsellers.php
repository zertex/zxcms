<?php
/**
 * Created by Error202
 * Date: 18.09.2017
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\shop\helpers\PriceHelper;

/**
 * @var $this \yii\web\View
 * @var $products \app\modules\shop\entities\product\Product[]
 */
?>

<div class="sidebar_widget sidebar_widget__products">
    <h3 class="widget_header"><?= Yii::t('shop_public', 'Bestsellers') ?></h3>
    <div class="widget_content">
        <ul class="list_products">
            <?php foreach ($products as $product): ?>
            <li class="product">
                <?php if ($product->mainPhoto): ?>
                <div class="product_img">
                    <a href="<?= Html::encode(Url::to(['/shop/catalog/product', 'id' =>$product->id])) ?>">
                        <img src="<?= Html::encode($product->mainPhoto->getThumbFileUrl('file', 'catalog_list')) ?>" alt="<?= Html::encode($product->name) ?>" />
                    </a>
                </div>
                <?php endif; ?>

                <div class="product_info">
                    <div class="product_price">
                        <div class="money"><?= PriceHelper::format($product->getCalculatedNewPrice()) ?></div>
                        <?php if ($product->getCalculatedOldPrice()): ?>
                            <span class="money compare-at-price"><?= PriceHelper::format($product->getCalculatedOldPrice()) ?></span>
                        <?php endif; ?>
                    </div>

                    <div class="product_name">
                        <a href="<?= Html::encode(Url::to(['/shop/catalog/product', 'id' =>$product->id])) ?>"><?= Html::encode(\yii\helpers\StringHelper::truncateWords($product->name, 7)) ?></a>
                    </div>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
