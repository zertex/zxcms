<?php

use common\auth\Identity;
use filsh\yii2\oauth2server\filters\auth\CompositeAuth;
use filsh\yii2\oauth2server\filters\ErrorToExceptionFilter;
use filsh\yii2\oauth2server\Module;
use filsh\yii2\oauth2server\Request;
use filsh\yii2\oauth2server\Response;
use OAuth2\GrantType\RefreshToken;
use OAuth2\GrantType\UserCredentials;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\log\FileTarget;
use yii\web\JsonParser;
use yii\web\JsonResponseFormatter;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-api',
    'language'            => 'ru',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => [
        'log',
        'common\bootstrap\SetUp',
        'api\bootstrap\SetUp',
        [
            'class' => ContentNegotiator::class,
            'formats' => [
                'application/json' => 'json',
                'application/xml' => 'xml',
            ]
        ]
    ],
    'aliases'             => [
        '@staticRoot' => $params['staticPath'],
        '@static'     => $params['staticHostInfo'],
    ],
    'controllerNamespace' => 'api\controllers',
    'modules' => [
        'oauth2' => [
            'class' => Module::class,
            'tokenParamName' => 'accessToken',
            'tokenAccessLifetime' => 3600 * 24,
            'storageMap' => [
                'user_credentials' => Identity::class,
            ],
            'components' => [
                'request' => function () {
                    return Request::createFromGlobals();
                },
                'response' => [
                    'class' => Response::class,
                ],
            ],
            'grantTypes' => [
                'user_credentials' => [
                    'class' => UserCredentials::class,
                ],
                'refresh_token' => [
                    'class' => RefreshToken::class,
                    'always_issue_new_refresh_token' => true
                ],
            ],
        ],
    ],
    'components'          => [
        'request'            => [
            'baseUrl' => '',
            'cookieValidationKey' => $params['cookieValidationKey'],
            'parsers' => [
                'application/json' => JsonParser::class,
            ],
        ],
        'response' => [
            'formatters' => [
                'json' => [
                    'class' => JsonResponseFormatter::class,
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
        ],
        'user'               => [
            'identityClass' => Identity::class,
            'enableAutoLogin' => false,
            'enableSession' => false,
            /*'identityClass'   => 'common\auth\Identity',
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity', 'httpOnly' => true, 'domain' => $params['cookieDomain']],
            'loginUrl'        => ['auth/auth/login'],*/
        ],
        /*'session'            => [
            'name'          => '_session',
            'class'         => 'yii\web\DbSession',
            'writeCallback' => function ($session) {
                return [
                    'user_id' => Yii::$app->user->id
                ];
            },
            'cookieParams'  => [
                'domain'   => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],*/
        'log'                => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'backendUrlManager'  => require __DIR__ . '/../../backend/config/urlManager.php',
        'frontendUrlManager' => require __DIR__ . '/../../frontend/config/urlManager.php',
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ''                              => 'site/index',
                'GET offer'                     => 'site/offer',

                'GET profile'                   => 'user/profile/index',
                'PUT profile'                   => 'user/profile/update',
                'GET profile/avatar'            => 'user/profile/avatar-get',
                'POST profile/avatar'           => 'user/profile/avatar-set',

                'POST oauth2/<action:\w+>'      => 'oauth2/rest/<action>',

                'POST auth/signup'              => 'auth/signup/request',
                'POST auth/reset/request'       => 'auth/reset/request',
                'POST auth/reset/check'         => 'auth/reset/check',
                'POST auth/reset/confirm'       => 'auth/reset/confirm',
            ],
        ],
        'i18n' => [
            'translations' => [
                'modules/oauth2/*' => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@api/messages',
                ],
                'user'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'auth'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'main'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
            ],
        ],
        /*'errorHandler'       => [
            'errorAction' => 'site/error',
        ],
        'backendUrlManager'  => require __DIR__ . '/../../backend/config/urlManager.php',
        'frontendUrlManager' => require __DIR__ . '/urlManager.php',
        'urlManager'         => function () {
            return Yii::$app->get('frontendUrlManager');
        },*/
        /*'i18n'               => [
            'translations' => [
                'post'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'slider' => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'auth'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'user'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
                'main'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                ],
            ],
        ],*
        'view'               => [
            'theme' => [
                'basePath' => '@webroot/themes/sport',
                'baseUrl'  => '@web/themes/sport',
                'pathMap'  => [
                    '@frontend/views'   => '@webroot/themes/sport',
                    '@frontend/widgets' => '@webroot/themes/sport/widgets',
                ],
            ],
        ],
        'assetManager'       => [
            'bundles' => [
                'yii\web\JqueryAsset'                => [
                    'sourcePath' => '@frontend/assets/libs/jquery321',   // do not publish the bundle
                    'js'         => [
                        YII_ENV_DEV ? 'jquery-3.2.1.js' : 'jquery-3.2.1.min.js'
                    ],
                ],
                'yii\bootstrap\BootstrapAsset'       => [
                    'sourcePath' => '@frontend/assets/libs/bootstrap4/css',   // do not publish the bundle
                    'css'        => [
                        YII_ENV_DEV ? 'bootstrap.css' : 'bootstrap.min.css'
                    ],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => '@frontend/assets/libs/bootstrap4/js',   // do not publish the bundle
                    'js'         => [
                        YII_ENV_DEV ? 'bootstrap.js' : 'bootstrap.min.js'
                    ],
                    'depends'    => [
                        'yii\web\JqueryAsset',
                        'yii\bootstrap\BootstrapAsset',
                    ],
                ],
            ],
        ],*/
    ],
    'as authenticator' => [
        'class' => CompositeAuth::class,
        'except' => [
            'site/index',
            'site/offer',
            'oauth2/rest/token',
            'auth/reset/check',
            'auth/reset/confirm',
            'auth/reset/request',
            'auth/signup/request'
        ],
        'authMethods' => [
            ['class' => 'yii\filters\auth\HttpBearerAuth'],
            ['class' => 'yii\filters\auth\QueryParamAuth', 'tokenParam' => 'accessToken'],
        ]
    ],
    'as access' => [
        'class' => AccessControl::class,
        'except' => [
            'site/index',
            'site/offer',
            'oauth2/rest/token',
            'auth/reset/check',
            'auth/reset/confirm',
            'auth/reset/request',
            'auth/signup/request'
        ],
        'rules' => [
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
    'as exceptionFilter' => [
        'class' => ErrorToExceptionFilter::class,
    ],
    'params'              => $params,
];
