<?php
/**
 * Created by Error202
 * Date: 19.07.2018
 */

use core\entities\menu\Menu;
use yii\web\View;

/**
 * @var $this View
 * @var $menu Menu
 */
?>

<ul class="navbar-nav ml-auto">
    <?php if (isset($menu->items)): ?>
        <?php foreach ($menu->items as $item): ?>
            <li class="<?= $item->isActive() ? 'active' : '' ?> nav-item<?= $item->hasChildren() ? ' dropdown' : '' ?>">
                <?php if ($item->hasChildren() && $item->parent_id == 0): ?>
                    <a <?= $item->target ? 'target="' . $item->target . '"' : '' ?> href="<?= $item->getUrl() ?>"
                                                                                    class="nav-link dropdown-toggle"
                                                                                    id="navbarDropdownMenuLink"
                                                                                    data-toggle="dropdown"
                                                                                    aria-haspopup="true"
                                                                                    aria-expanded="false"><?= $item->translation->name ?></a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <?php $children = $item->children; ?>
                        <?php foreach ($children as $child): ?>
                            <a <?= $child->target ? 'target="' . $child->target . '"' : '' ?> class="dropdown-item"
                                                                                              href="<?= $child->getUrl() ?>"><?= $child->translation->name ?></a>
                        <?php endforeach; ?>
                    </div>
                <?php elseif ($item->parent_id == 0): ?>
                    <a <?= $item->target ? 'target="' . $item->target . '"' : '' ?> href="<?= $item->getUrl() ?>"
                                                                                    class="nav-link"><?= $item->translation->name ?></a>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    <?php endif; ?>
</ul>