<?php
/**
 * Created by Error202
 * Date: 19.07.2018
 */

namespace core\widgets\menu;

use core\entities\menu\Menu;
use yii\base\Widget;

class MenuWidget extends Widget
{
	public int $menu_id;

    /**
     * @return string
     */
	public function run(): string
    {
		$menu = Menu::findOne($this->menu_id);

		return $this->render('menu', [
			'menu' => $menu,
		]);
	}
}
