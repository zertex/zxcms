<?php
/**
 * Created by Error202
 * Date: 06.07.2018
 */

namespace core\widgets;

use yii\base\Widget;
use core\helpers\VideoHelper;

class VideoWidget extends Widget
{
	public string $src;

	public int $width = 640;
	public int $height = 360;
	public string $style = "width: 100%";

	public function run() {
		$player = '';
		switch (VideoHelper::identityProvider($this->src)) {
			case 'youtube':
				$id = VideoHelper::parseYoutubeUrl($this->src);
				$player = '<iframe style="'.$this->style.'" width="'.$this->width.'" height="'.$this->height.'" src="https://www.youtube.com/embed/'.$id.'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				break;
			case 'vimeo':
				$id = VideoHelper::parseVimeoUrl($this->src);
				$player = '<iframe style="'.$this->style.'" src="https://player.vimeo.com/video/'.$id.'" width="'.$this->width.'" height="'.$this->height.'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
				break;
		}
		return $player;
	}
}
