<?php

namespace core;

interface AggregateRoot
{
    /**
     * @return array
     */
    public function releaseEvents(): array;
}
