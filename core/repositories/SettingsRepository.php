<?php

namespace core\repositories;

use core\entities\Settings;
use RuntimeException;
use yii\db\StaleObjectException;

class SettingsRepository
{
    public function get($section, $key): Settings
    {
        if (!$settings = Settings::find()->andWhere(['section' => $section])->andWhere(['key' => $key])->one()) {
            throw new NotFoundException('Setting is not found.');
        }
        return $settings;
    }

    public function save(Settings $settings): void
    {
        if (!$settings->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param Settings $settings
     * @throws StaleObjectException
     */
    public function remove(Settings $settings): void
    {
        if (!$settings->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
