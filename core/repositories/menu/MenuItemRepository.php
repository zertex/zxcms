<?php

namespace core\repositories\menu;

use core\entities\menu\MenuItem;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class MenuItemRepository
{
    public function get($id): MenuItem
    {
	    if (!$item = MenuItem::find()->with('translations')->andWhere(['id' => $id])->one()) {
            throw new NotFoundException('Menu is not found.');
        }
        return $item;
    }

    public function save(MenuItem $item): void
    {
        if (!$item->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param MenuItem $item
     * @throws StaleObjectException
     */
    public function remove(MenuItem $item): void
    {
        if (!$item->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}