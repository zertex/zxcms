<?php

namespace core\repositories\menu;

use core\entities\menu\Menu;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class MenuRepository
{
    public function get($id): Menu
    {
	    if (!$menu = Menu::find()->with('translations')->andWhere(['id' => $id])->one()) {
            throw new NotFoundException('Menu is not found.');
        }
        return $menu;
    }

    public function save(Menu $menu): void
    {
        if (!$menu->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param Menu $menu
     * @throws StaleObjectException
     */
    public function remove(Menu $menu): void
    {
        if (!$menu->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}