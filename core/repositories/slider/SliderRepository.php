<?php

namespace core\repositories\slider;

use core\entities\Slider;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class SliderRepository
{
    public function get($id): Slider
    {
        if (!$slide = Slider::findOne($id)) {
            throw new NotFoundException('Slide is not found.');
        }
        return $slide;
    }

    public function save(Slider $slider): void
    {
        if (!$slider->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param Slider $slider
     * @throws StaleObjectException
     */
    public function remove(Slider $slider): void
    {
        if (!$slider->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}