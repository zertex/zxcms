<?php

namespace core\repositories\post;

use core\entities\post\Post;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class PostRepository
{
    public function get($id): Post
    {
        if (!$post = Post::findOne($id)) {
            throw new NotFoundException('Post is not found.');
        }
        return $post;
    }

    public function existsByCategory($id): bool
    {
        return Post::find()->andWhere(['category_id' => $id])->exists();
    }

    public function save(Post $post): void
    {
        if (!$post->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param Post $post
     * @throws StaleObjectException
     */
    public function remove(Post $post): void
    {
        if (!$post->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
