<?php

namespace core\repositories\post;

use core\entities\post\PostType;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class PostTypeRepository
{
    public function get($id): PostType
    {
        if (!$type = PostType::findOne($id)) {
            throw new NotFoundException('Type is not found.');
        }
        return $type;
    }

    public function findByName($name): ?PostType
    {
        return PostType::findOne(['name' => $name]);
    }

    public function save(PostType $type): void
    {
        if (!$type->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param PostType $type
     * @throws StaleObjectException
     */
    public function remove(PostType $type): void
    {
        if (!$type->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
