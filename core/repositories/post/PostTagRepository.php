<?php

namespace core\repositories\post;

use core\entities\post\PostTag;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class PostTagRepository
{
    public function get($id): PostTag
    {
        if (!$tag = PostTag::findOne($id)) {
            throw new NotFoundException('Tag is not found.');
        }
        return $tag;
    }

    public function findByName($name, $type_id): ?PostTag
    {
        return PostTag::findOne(['name' => $name, 'type_id' => $type_id]);
    }

    public function save(PostTag $tag): void
    {
        if (!$tag->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param PostTag $tag
     * @throws StaleObjectException
     */
    public function remove(PostTag $tag): void
    {
        if (!$tag->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
