<?php

namespace core\repositories\post;

use core\entities\post\PostCategory;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class PostCategoryRepository
{
    public function get($id): PostCategory
    {
        if (!$category = PostCategory::findOne($id)) {
            throw new NotFoundException('Category is not found.');
        }
        return $category;
    }

    public function save(PostCategory $category): void
    {
        if (!$category->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param PostCategory $category
     * @throws StaleObjectException
     */
    public function remove(PostCategory $category): void
    {
        if (!$category->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}