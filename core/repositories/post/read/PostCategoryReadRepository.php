<?php

namespace core\repositories\post\read;

use core\entities\post\PostCategory;

class PostCategoryReadRepository
{
    public function getAll(): array
    {
        return PostCategory::find()->orderBy('sort')->all();
    }

    public function find($id): ?PostCategory
    {
        return PostCategory::findOne($id);
    }

    public function findBySlug($slug): ?PostCategory
    {
        return PostCategory::find()->andWhere(['slug' => $slug])->one();
    }
}
