<?php

namespace core\repositories\post\read;

use core\entities\post\PostTag;

class PostTagReadRepository
{
    public function find($id): ?PostTag
    {
        return PostTag::findOne($id);
    }

    public function findBySlug($slug): ?PostTag
    {
        return PostTag::find()->andWhere(['slug' => $slug])->one();
    }
}
