<?php

namespace core\repositories\post\read;

use core\entities\post\PostCategory;
use core\entities\post\Post;
use core\entities\post\PostTag;
use core\entities\post\PostType;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

class PostTypeReadRepository
{
    public function getLast($limit): array
    {
        return Post::find()->with('category')->orderBy(['id' => SORT_DESC])->limit($limit)->all();
    }

    public function getPopular($limit): array
    {
        return Post::find()->with('category')->orderBy(['comments_count' => SORT_DESC])->limit($limit)->all();
    }

    public function find($id): ?PostType
    {
        return PostType::find()->andWhere(['id' => $id])->one();
    }

	public function findByName($name): ?PostType
	{
		return PostType::find()->andWhere(['name' => $name])->one();
	}
}
