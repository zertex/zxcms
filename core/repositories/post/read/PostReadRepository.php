<?php

namespace core\repositories\post\read;

use core\entities\post\PostCategory;
use core\entities\post\Post;
use core\entities\post\PostTag;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

class PostReadRepository
{
    public function count(): int
    {
        return Post::find()->active()->count();
    }

    public function getAllByRange($offset, $limit): array
    {
        return Post::find()->active()->orderBy(['id' => SORT_ASC])->limit($limit)->offset($offset)->all();
    }

    public function getAll($tid): DataProviderInterface
    {
        $query = Post::find()->active()->andWhere(['type_id' => $tid])->with('category');
        return $this->getProvider($query);
    }

    public function getAllByCategory(PostCategory $category): DataProviderInterface
    {
        $query = Post::find()->active()->andWhere(['category_id' => $category->id])->with('category');
        return $this->getProvider($query);
    }

    public function findNext(int $id): ?Post
    {
        return Post::find()->active()->andWhere(['>', 'id', $id])->one();
    }

    public function findPrev(int $id): ?Post
    {
        return Post::find()->active()->andWhere(['<', 'id', $id])->orderBy(['id' => SORT_DESC])->one();
    }


    public function getAllByTag(PostTag $tag): DataProviderInterface
    {
        $query = Post::find()->alias('p')->active('p')->with('category');
        $query->joinWith(['postTagAssignments ta'], false);
        $query->andWhere(['ta.tag_id' => $tag->id]);
        $query->groupBy('p.id');
        return $this->getProvider($query);
    }

	public function getByTagsId(Post $post, array $tag_ids, int $limit = 15): DataProviderInterface
	{
		$query = Post::find()->alias('p')->active('p')->with('category');
		$query->joinWith(['postTagAssignments ta'], false);
		$query->andWhere(['ta.tag_id' => $tag_ids]);
		$query->andWhere(['p.type_id' => $post->type_id]);
		$query->andWhere(['!=', 'p.id', $post->id]);
		$query->groupBy('p.id');
		$query->limit($limit);
		return $this->getProvider($query);
	}

    public function getLast($limit): array
    {
        return Post::find()->with('category')->orderBy(['id' => SORT_DESC])->limit($limit)->all();
    }

    public function getPopular($limit): array
    {
        return Post::find()->with('category')->orderBy(['comments_count' => SORT_DESC])->limit($limit)->all();
    }

    public function find($id): ?Post
    {
        return Post::find()->active()->andWhere(['id' => $id])->one();
    }

    private function getProvider(ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
        ]);
    }

	public function findBySlug($slug): ?Post
	{
		return Post::find()->andWhere(['slug' => $slug])->one();
	}
}
