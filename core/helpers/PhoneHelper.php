<?php
/**
 * Created by Error202
 * Date: 23.09.2018
 */

namespace core\helpers;

class PhoneHelper
{
    public static function normalizePhone(string $phone): string
    {
        return preg_replace('/[^0-9]/', '', $phone);
    }

    public static function isCorrect(string $phone): bool
    {
        $string = PhoneHelper::normalizePhone($phone);
        return strlen($string) == 11;
    }
}
