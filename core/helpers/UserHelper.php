<?php

namespace core\helpers;

use core\entities\user\User;
use Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use Yii;

class UserHelper
{
    public static function statusList(): array
    {
        return [
            User::STATUS_WAIT => Yii::t('user', 'Wait'),
            User::STATUS_ACTIVE => Yii::t('user', 'Active'),
        ];
    }

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
    public static function statusLabel($status): string
    {
        $class = match ($status) {
            User::STATUS_WAIT => 'badge badge-default',
            User::STATUS_ACTIVE => 'badge badge-success',
            default => 'badge badge-default',
        };

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }

    public static function getSetting($key, $default = null)
    {
        $settings = Json::decode(Yii::$app->user->identity->user->settings, true);
        return $settings[$key] ?? $default;
    }

    public static function setSetting($key, $value)
    {
        $settings = Json::decode(Yii::$app->user->identity->user->settings, true);
        $settings[$key] = $value;

        $user = User::findOne(Yii::$app->user->id);

        if ($user) {
            $user->settings = Json::encode($settings);
            $user->save();
        }
    }
}
