<?php

namespace core\helpers;

use core\entities\post\Post;
use Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

class PostHelper
{
    public static function statusList(): array
    {
        return [
            Post::STATUS_DRAFT => Yii::t('post', 'Draft'),
            Post::STATUS_ACTIVE => Yii::t('post', 'Active'),
        ];
    }

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
    public static function statusLabel($status): string
    {
        $class = match ($status) {
            Post::STATUS_DRAFT => 'label label-default',
            Post::STATUS_ACTIVE => 'label label-success',
            default => 'label label-default',
        };

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }

	public static function parseYoutubeUrl($url)
	{
		$urls = parse_url($url);
		//url is http://youtu.be/xxxx
		if($urls['host'] == 'youtu.be'){
			$id = ltrim($urls['path'],'/');
		}
		//url is http://www.youtube.com/embed/xxxx
		else if(strpos($urls['path'],'embed') == 1){
            $parts = explode('/',$urls['path']);
			$id = end($parts);
		}
		//url is xxxx only
		else if (!str_contains($url, '/')) {
			$id = $url;
		}
		//http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI
		//url is http://www.youtube.com/watch?v=xxxx
		else {
			parse_str($urls['query']);
            /* @var $v string */
			$id = $v;
			if(!empty($feature)){
                $parts = explode('v=',$urls['query']);
				$id = end($parts);
			}
		}
		return $id;
	}
}