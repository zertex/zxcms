<?php
/**
 * Created by Error202
 * Date: 06.07.2018
 */

namespace core\helpers;

class VideoHelper
{
	public static function identityProvider($url): ?string
    {
		if (preg_match('%youtube|youtu\.be%i', $url)) {
			return 'youtube';
		}
		elseif (preg_match('%vimeo%i', $url)) {
			return 'vimeo';
		}
		return null;
	}

	public static function getThumb($url): string
    {
		$src = '';
		switch (self::identityProvider($url)) {
			case 'youtube':
				$id = VideoHelper::parseYoutubeUrl($url);
				$src = 'https://i.ytimg.com/vi/' . $id . '/maxresdefault.jpg';
				break;
			case 'vimeo':
				$id = VideoHelper::parseVimeoUrl($url);
				$data = file_get_contents("http://vimeo.com/api/v2/video/$id.json");
				$data = json_decode($data);
				$src = $data[0]->thumbnail_large;
				break;
		}
		return $src;
	}

	public static function parseVimeoUrl($url): bool|string
    {
		$urls = parse_url($url);
		// https://vimeo.com/274720181
		if($urls['host'] == 'vimeo.com'){
			$id = ltrim($urls['path'],'/');
		}
		// https://player.vimeo.com/video/274720181
		else {
			$parts = explode('/',$urls['path']);
			$id = end($parts);
		}

		return $id;
	}

	public static function parseYoutubeUrl($url)
	{
		$urls = parse_url($url);
		//url is http://youtu.be/xxxx
		if($urls['host'] == 'youtu.be'){
			$id = ltrim($urls['path'],'/');
		}
		//url is http://www.youtube.com/embed/xxxx
		else if(strpos($urls['path'],'embed') == 1){
			$parts = explode('/',$urls['path']);
			$id = end($parts);
		}
		//url is xxxx only
		else if (!str_contains($url, '/')) {
			$id = $url;
		}
		//http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI
		//url is http://www.youtube.com/watch?v=xxxx
		else{
			parse_str($urls['query']);
			/* @var $v */
			$id = $v;
			if(!empty($feature)){
                $parts = explode('v=',$urls['query']);
				$id = end($parts);
			}
		}
		return $id;
	}
}
