<?php
/**
 * Created by Error202
 * Date: 18.08.2018
 */

namespace core\helpers;

use yii\helpers\ArrayHelper;
use Yii;

class ModuleHelper
{
	// add module translation without load module
	public static function addModuleAdminTranslation($module_name)
	{
		Yii::$app->getI18n()->translations = ArrayHelper::merge(Yii::$app->getI18n()->translations, [
			$module_name => [
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@common/modules/' . $module_name . '/messages',
			],
		]);
	}

}