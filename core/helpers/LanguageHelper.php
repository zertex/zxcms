<?php
/**
 * Created by Error202
 * Date: 23.08.2018
 */

namespace core\helpers;

use Yii;

class LanguageHelper
{
	public static function enabled(): bool
    {
		return count(Yii::$app->params['translatedLanguages']) > 1;
	}

	public static function suffixList(): array
    {
		$list = array();
		$enabled = self::enabled();

		foreach (Yii::$app->params['translatedLanguages'] as $lang => $name) {
			if ($lang === Yii::$app->params['defaultLanguage']) {
				$suffix = '';
				$list[$suffix] = $enabled ? $name : '';
			} else {
				$suffix = '_' . $lang;
				$list[$suffix] = $name;
			}
		}

		return $list;
	}

	public static function isLangExists($url): bool
	{
		$index = self::_getLangIndex();
		$domains = explode('/', ltrim($url, '/'));
		return in_array($domains[$index], array_keys(Yii::$app->params['translatedLanguages']));
	}

	public static function setLanguage($url)
	{
		$index = self::_getLangIndex();
		$domains = explode('/', ltrim($url, '/'));
		$isLangExists = in_array($domains[$index], array_keys(Yii::$app->params['translatedLanguages']));
		if ($isLangExists) {
			Yii::$app->language = $domains[$index];
		}
		else {
			Yii::$app->language = Yii::$app->params['defaultLanguage'];
		}
	}

	public static function processLangInUrl($url): string
	{
		if (self::enabled()) {
			$index = self::_getLangIndex();
			$domains = explode('/', ltrim($url, '/'));
			$isLangExists = in_array($domains[$index], array_keys(Yii::$app->params['translatedLanguages']));
			$isDefaultLang = $domains[$index] == Yii::$app->params['defaultLanguage'];

			if ($isLangExists && !$isDefaultLang) {
				array_splice($domains, $index, 1);
			}
			$url = '/' . implode('/', $domains);
		}
		return $url;
	}

	public static function addLangToUrl($url, $language = null): string
	{
		if (self::enabled()) {
			$index = self::_getLangIndex();
			$domains = explode('/', ltrim($url, '/'));
			$isHasLang = in_array($language ?: $domains[$index], array_keys(Yii::$app->params['translatedLanguages']));
			$isDefaultLang = $language ? $language == Yii::$app->params['defaultLanguage'] : Yii::$app->language == Yii::$app->params['defaultLanguage'];

			if ($isHasLang && $isDefaultLang) {
				array_splice($domains, $index, 1);
			}

			if (!$isHasLang && !$isDefaultLang) {
				array_splice($domains, $index, 0, Yii::$app->language);
			}

			$domains = array_filter($domains);
			$url = '/' . implode('/', $domains);
		}
		return $url;
	}

	public static function getName($language)
	{
		return Yii::$app->params['translatedLanguages'][$language] ?? $language;
	}

	public static function getBackendName($language)
	{
		return Yii::$app->params['backendTranslatedLanguages'][$language] ?? Yii::$app->params['backendTranslatedLanguages'][Yii::$app->params['backendDefaultLanguage']];
	}

	private static function _getLangIndex(): int
	{
		$index = 0;
		$baseUrl = ltrim(Yii::$app->request->baseUrl, '/');

		if (strlen($baseUrl)) {
			$baseUrlChunks = explode('/', $baseUrl);
			if (count($baseUrlChunks) > 0) {
				$index = count( $baseUrlChunks );
			}
		}
		return $index;
	}
}
