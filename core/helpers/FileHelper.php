<?php
/**
 * Created by Error202
 * Date: 13.09.2018
 */

namespace core\helpers;

class FileHelper extends \yii\helpers\FileHelper
{
    public static function downloadFile($url, $path): bool
    {
        $new_file = null;
        $new_name = $path;
        $file     = fopen($url, 'rb');
        if ($file) {
            $new_file = fopen($new_name, 'wb');
            if ($new_file) {
                while (!feof($file)) {
                    fwrite($new_file, fread($file, 1024 * 8), 1024 * 8);
                }
            }
        }
        if ($file) {
            fclose($file);
        }
        if ($new_file) {
            fclose($new_file);
        }
        return (bool)$new_file;
    }
}
