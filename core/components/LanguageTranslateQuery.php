<?php
/**
 * Created by Error202
 * Date: 26.08.2018
 */

namespace core\components;

use yii\db\ActiveQuery;

class LanguageTranslateQuery extends ActiveQuery
{
	use LanguageTranslateTrait;
}
