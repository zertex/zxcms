<?php

namespace core\components\bootstrap4;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class Bootstrap4Asset extends AssetBundle
{
    public $sourcePath = '@core/components/bootstrap4';
    public $js = [
        'js/bootstrap.bundle.js',
    ];
    public $css = [
        'css/bootstrap.css',
    ];
    public $depends = [
        JqueryAsset::class,
    ];
}
