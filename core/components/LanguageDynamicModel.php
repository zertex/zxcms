<?php
/**
 * Created by Error202
 * Date: 24.08.2018
 */

namespace core\components;

use yii\base\DynamicModel;
use Yii;
use yii\db\ActiveRecord;

class LanguageDynamicModel extends DynamicModel
{
    private array $_new_labels = [];
    private array $_new_hints = [];
    private array $_new_rules = [];

    public function __construct(ActiveRecord $entity = null, array $attributes = [], array $config = [])
    {
        $used_attributes = $this->getStringAttributes();
        parent::__construct(array_merge($this->getPublicAttributes(), $this->prepareLanguageAttributes()), $config);

        if ($entity) {
            foreach ($used_attributes as $used_attribute) {
                foreach ($entity->translations as $translate) {
                    if (!in_array($translate->language, $entity->translatedLanguages)) {
                        continue;
                    }

                    $defaultLanguage = basename(\Yii::$app->getBasePath()) === 'backend'
                        ?
                        Yii::$app->params['backendDefaultLanguage']
                        :
                        Yii::$app->params['defaultLanguage'];

                    $languageAttribute = $used_attribute . '_' . $translate->language;

                    if ($translate->language === $defaultLanguage && isset($translate->{$used_attribute})) {
                        $this->{$used_attribute} = $translate->{$used_attribute};
                    } elseif (isset($translate->{$used_attribute})) {
                        $this->{$languageAttribute} = $translate->{$used_attribute};
                    }
                }
            }
        }
    }

    private function getStringAttributes(): array
    {
        $string_attributes = [];
        foreach ($this->rules() as $rule) {
            $attributes = is_array($rule[0]) ? $rule[0] : [$rule[0]];
            $type       = $rule[1];
            if ($type == 'string') {
                foreach ($attributes as $attribute) {
                    $string_attributes[] = $attribute;
                }
            }
        }

        return $string_attributes;
    }

    private function prepareLanguageAttributes(): array
    {
        $language_attributes = [];
        $labels              = $this->attributeLabels();
        $hints               = $this->attributeHints();
        foreach ($this->rules() as $rule) {
            $attributes = is_array($rule[0]) ? $rule[0] : [$rule[0]];
            $type       = $rule[1];
            if ($type == 'string') {
                foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
                    $rule_attributes = [];
                    foreach ($attributes as $attribute) {
                        // add attribute
                        $language_attributes[]                          = $attribute . '_' . $language;
                        $this->_new_labels[$attribute . '_' . $language] = $labels[$attribute] ?? null;
                        $this->_new_hints[$attribute . '_' . $language]  = $hints[$attribute] ?? null;
                        $rule_attributes[]                              = $attribute . '_' . $language;
                    }
                    // add rule
                    if (!empty($rule_attributes)) {
                        $this->_new_rules[] = [$rule_attributes, $rule[1]];
                    }
                }
            }
        }

        return $language_attributes;
    }

    public function attributeLabels(): array
    {
        return $this->_new_labels;
    }

    public function rules(): array
    {
        return $this->_new_rules;
    }

    public function getPublicAttributes()
    {
        return call_user_func('get_object_vars', $this);
    }
}
