<?php
/**
 * Created by Error202
 * Date: 05.06.2018
 */

namespace core\components\modules;

use yii\web\Application;

interface ModuleInterface {

	/**
	 * Initializes the module.
	 */
	public function init();
	/**
	 * Bootstrap method to be called during application bootstrap stage.
	 * @param Application $app the application currently running
	 */
	public function bootstrap(Application $app);

}
