<?php
/**
 * Created by Error202
 * Date: 02.02.2020
 */

namespace core\components\adminlte3\widgets;

use yii\base\Widget;
use lavrentiev\widgets\toastr\Notification;
use Yii;

class ToastrNotification extends Widget
{
	public $alertTypes = [
		'error'   => 'danger',
		'danger'  => 'danger',
		'success' => 'success',
		'info'    => 'info',
		'warning' => 'warning'
	];

	public function init()
	{
		parent::init();

		$session = Yii::$app->session;
		$flashes = $session->getAllFlashes();

		foreach ($flashes as $type => $data) {
			if (isset($this->alertTypes[$type])) {
				$data = (array) $data;
				foreach ($data as $i => $message) {
					echo Notification::widget([
						'type' => $this->alertTypes[$type],
						'title' => '',
						'message' => $message
					]);
				}

				$session->removeFlash($type);
			}
		}
	}
}
