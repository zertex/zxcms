<?php
/**
 * Created by Error202
 * Date: 02.02.2020
 */

namespace core\components\adminlte3\widgets;

use yii\widgets\Breadcrumbs as BaseBreadcrumbs;

class Breadcrumbs extends BaseBreadcrumbs
{
	public $tag = 'ol';

	public $options = ['class' => 'breadcrumb float-sm-right'];

	/**
	 * @inheritdoc
	 */
	public $itemTemplate = "<li class=\"breadcrumb-item\">{link}</li>\n";
	/**
	 * @inheritdoc
	 */
	public $activeItemTemplate = "<li class=\"breadcrumb-item active\">{link}</li>\n";
}
