<?php
/**
 * Created by Error202
 * Date: 02.02.2020
 */

namespace core\components\adminlte3;

use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;

class AdminLteAsset extends AssetBundle
{
	public $sourcePath = '@core/components/adminlte3/assets';

	public $css = [
		'css/adminlte.css',
	];

	public $js = [
		'js/adminlte.min.js'
	];

	public $depends = [
		'yii\web\YiiAsset',
        BootstrapAsset::class,
        BootstrapPluginAsset::class
	];
}
