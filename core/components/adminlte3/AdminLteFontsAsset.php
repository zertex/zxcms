<?php
/**
 * Created by Error202
 * Date: 02.02.2020
 */

namespace core\components\adminlte3;

use yii\web\AssetBundle;

class AdminLteFontsAsset extends AssetBundle
{
	public $sourcePath = '@core/components/adminlte3/plugins/fontawesome-free';

	public $css = [
		'css/all.min.css',
	];
}
