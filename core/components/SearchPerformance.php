<?php
/**
 * Created by Error202
 * Date: 05.07.2018
 */

namespace core\components;

use Yii;
use yii\db\Exception;

class SearchPerformance
{
    /**
     * @throws Exception
     */
    public static function init()
    {
        if (isset(Yii::$app->params['search_rules']) && is_array(Yii::$app->params['search_rules'])) {
            $connection = Yii::$app->getDb();
            $command    = $connection->createCommand('CREATE OR REPLACE VIEW view_search AS ' . implode(' UNION ', Yii::$app->params['search_rules']));
            $command->execute();
        }
    }
}
