<?php

namespace core\events;

trait EventTrait
{
    private $_events = [];

    public function recordEvent($event): void
    {
        $this->_events[] = $event;
    }

    public function releaseEvents(): array
    {
        $events = $this->_events;
        $this->_events = [];
        return $events;
    }
}
