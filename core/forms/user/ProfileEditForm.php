<?php
/**
 * Created by Error202
 * Date: 22.08.2017
 */

namespace core\forms\user;

use core\entities\user\User;
use yii\base\Model;
use Yii;
use yii\web\UploadedFile;

class ProfileEditForm extends Model
{
    public ?string $email = null;
    public ?string $username = null;
    public ?string $password = null;
    public string|UploadedFile|null $user_pic = null;
    public ?string $backend_language = null;

    public $_user;

    public function __construct(User $user, $config = [])
    {
        $this->email = $user->email;
        $this->username = $user->username;
        $this->_user = $user;
        $this->user_pic = $user->user_pic;
        $this->backend_language = $user->backend_language ?: Yii::$app->language;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['email', 'username'], 'required'],
	        [['email', 'username'], 'trim'],
            ['email', 'email'],
            [['email', 'password', 'backend_language'], 'string', 'max' => 255],
	        [['username', 'email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->_user->id]],
	        ['username', 'string', 'min' => 2, 'max' => 255],
	        ['user_pic', 'image', 'extensions' => 'png, jpg, jpeg, gif',
                 'skipOnEmpty' => true,
                 'minWidth' => 100,
                 'minHeight' => 100,
	        ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('user', 'E-mail'),
            'username' => Yii::t('user', 'Username'),
	        'password' => Yii::t('user', 'Password'),
	        'user_pic' => Yii::t('user', 'User Picture'),
            'backend_language' => Yii::t('user', 'Dashboard Language'),
        ];
    }
}
