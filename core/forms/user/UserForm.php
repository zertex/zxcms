<?php

namespace core\forms\user;

use core\entities\user\User;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use Yii;

class UserForm extends Model
{
    public ?string $username = null;
    public ?string $email = null;
    public ?string $password = null;
    public ?string $role = null;

    public User $user;

	public function __construct(User $user = null, $config = [])
	{
		if ($user) {
			$this->username = $user->username;
			$this->email    = $user->email;
			$roles          = Yii::$app->authManager->getRolesByUser( $user->id );
			$this->role     = $roles ? reset( $roles )->name : null;
			$this->user    = $user;
		}
		parent::__construct($config);
	}

	public function rules(): array
	{
		return [
			[['username', 'email', 'role'], 'required'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			//[['username', 'email'], 'unique', 'targetClass' => User::class, 'filter' => ['<>', 'id', $this->_user->id]],
			[['username', 'email'], 'unique', 'targetClass' => User::class, 'filter' => isset($this->user->id) ? ['<>', 'id', $this->user->id] : null ],
			['password', 'string', 'min' => 6],
		];
	}

	public function attributeLabels(): array
    {
		return [
			'id' => Yii::t('user', 'ID'),
			'username' => Yii::t('user', 'Username'),
			'password' => Yii::t('user', 'Password'),
			'email' => Yii::t('user', 'E-mail'),
			'created_at' => Yii::t('user', 'Created At'),
			'role' => Yii::t('user', 'Role'),
		];
	}

    public function rolesList(): array
    {
        return ArrayHelper::map(\Yii::$app->authManager->getRoles(), 'name', 'description');
    }
}
