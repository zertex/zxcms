<?php
/**
 * Created by Error202
 * Date: 10.02.2018
 */

namespace core\forms;

use yii\base\Model;

class SubscribeForm extends Model
{
	public ?string $email = null;

	public function rules(): array
    {
		return [
			['email', 'required'],
			['email', 'email'],
		];
	}

	public function attributeLabels(): array
    {
		return [
			'email' => 'E-mail',
		];
	}
}
