<?php

namespace core\forms\post;

use core\entities\post\PostComment;
use yii\base\Model;
use Yii;

class PostCommentEditForm extends Model
{
    public int $parentId;
    public string $text;

    public function __construct(PostComment $comment, $config = [])
    {
        $this->parentId = $comment->parent_id;
        $this->text = $comment->text;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['text'], 'required'],
            ['text', 'string'],
            ['parentId', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'parentId' => Yii::t('post', 'Parent Comment ID'),
            'text' => Yii::t('post', 'Comment'),
        ];
    }
}
