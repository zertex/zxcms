<?php

namespace core\forms\post;

use core\entities\post\PostCategory;
use core\entities\post\Post;
use core\forms\CompositeForm;
use core\forms\MetaForm;
use core\validators\SlugValidator;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use Yii;

/**
 * @property MetaForm $meta
 * @property PostTagForm $tags
 */
class PostForm extends CompositeForm
{
    public int $category_id;
    public string $title;
    public string $description;
    public string $content;
    public $image;
    public string $video;
    public int $type_id;
    public int $published_at;
    public string $slug;
    public $reset_image;

	public Post $post;

    public function __construct(Post $post = null, $config = [])
    {
        if ($post) {
            $this->category_id = $post->category_id;
            $this->title = $post->title;
            $this->description = $post->description;
            $this->content = $post->content;
            $this->video = $post->video;
            $this->type_id = $post->type_id;
            $this->published_at = $post->published_at;
            $this->slug = $post->slug;
            $this->meta = new MetaForm($post->meta);
            $this->tags = new PostTagForm($post);
            $this->post = $post;
        } else {
            $this->meta = new MetaForm();
            $this->tags = new PostTagForm();
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['category_id', 'title'], 'required'],
            [['title', 'video'], 'string', 'max' => 255],
            [['category_id', 'type_id'], 'integer'],
            [['description', 'content'], 'string'],
            [['image'], 'image'],
	        ['reset_image', 'boolean'],
	        ['published_at', 'safe'],
	        ['slug', SlugValidator::class],
	        [['slug'], 'unique', 'targetClass' => Post::class, 'filter' => $this->post ? ['<>', 'id', $this->post->id] : null],
        ];
    }

    public function attributeLabels()
    {
	    return [
		    'id' => Yii::t('post', 'ID'),
		    'category_id' => Yii::t('post', 'Category'),
		    'published_at' => Yii::t('post', 'Published At'),
		    'created_at' => Yii::t('post', 'Created At'),
		    'updated_at' => Yii::t('post', 'Updated At'),
		    'title' => Yii::t('post', 'Title'),
		    'description' => Yii::t('post', 'Description'),
		    'content' => Yii::t('post', 'Content'),
		    'image' => Yii::t('post', 'Image'),
		    'video' => Yii::t('post', 'Video'),
		    'status' => Yii::t('post', 'Status'),
		    'meta_json' => Yii::t('post', 'Meta Json'),
		    'comments_count' => Yii::t('post', 'Comments Count'),
		    'type_id' => Yii::t('post', 'Type'),
		    'views' => Yii::t('post', 'Views'),
		    'slug' => Yii::t('post', 'Slug'),
		    'reset_image' => Yii::t('post', 'Reset Image'),
	    ];
    }

    public function categoriesList(int $type_id): array
    {
        return ArrayHelper::map(PostCategory::find()->andWhere(['type_id' => $type_id])->orderBy('sort')->asArray()->all(), 'id', 'name');
    }

    protected function internalForms(): array
    {
        return ['meta', 'tags'];
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {
	        $this->image = UploadedFile::getInstance($this, 'image');
            $this->published_at = strtotime($this->published_at);
            return true;
        }
        return false;
    }
}
