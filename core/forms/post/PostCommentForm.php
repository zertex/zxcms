<?php

namespace core\forms\post;

use yii\base\Model;

class PostCommentForm extends Model
{
    public int $parentId;
    public string $text;

    public function rules(): array
    {
        return [
            [['text'], 'required'],
            ['text', 'string'],
            ['parentId', 'integer'],
        ];
    }
}
