<?php

namespace core\forms\post;

use core\entities\post\PostType;
use yii\base\Model;
use Yii;

class PostTypeForm extends Model
{
    public string $name;
    public string $singular;
    public string $plural;

	private $type;

    public function __construct(PostType $type = null, $config = [])
    {
        if ($type) {
            $this->name = $type->name;
            $this->singular = $type->singular;
            $this->plural = $type->plural;
            $this->type = $type;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name', 'plural', 'singular'], 'required'],
            [['name', 'plural', 'singular'], 'string', 'max' => 255],
	        ['name', 'match', 'pattern' => '/^[a-z]+$/'],
	        [['name'], 'unique', 'targetClass' => PostType::class, 'filter' => $this->type ? ['<>', 'id', $this->type->id] : null],
        ];
    }

    public function attributeLabels()
    {
	    return [
		    'id' => Yii::t('post', 'ID'),
		    'name' => Yii::t('post', 'Name'),
		    'singular' => Yii::t('post', 'Singular'),
		    'plural' => Yii::t('post', 'Plural'),
	    ];
    }
}
