<?php

namespace core\forms\post\search;

use core\entities\post\PostType;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PostTypeSearch extends Model
{
    public $id;
    public $name;
    public $singular;
    public $plural;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['name', 'singular', 'plural'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = PostType::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
	        ->andFilterWhere(['like', 'singular', $this->singular])
	        ->andFilterWhere(['like', 'plural', $this->plural]);

        return $dataProvider;
    }
}
