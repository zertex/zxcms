<?php

namespace core\forms\post\search;

use core\entities\post\PostCategory;
use core\helpers\PostHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use core\entities\post\Post;
use yii\helpers\ArrayHelper;

class PostSearch extends Model
{
    public $id;
    public $title;
    public $status;
    public $category_id;
    public $tid;

    public function rules(): array
    {
        return [
            [['id', 'status', 'category_id', 'tid'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
	    $this->tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT);
        $query = Post::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type_id' => $this->tid,
            'category_id' => $this->category_id,
        ]);

        $query
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

    public function categoriesList(): array
    {
	    $this->tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT);
        return ArrayHelper::map(PostCategory::find()->where(['type_id' => $this->tid])->orderBy('sort')->asArray()->all(), 'id', 'title');
    }

    public function statusList(): array
    {
        return PostHelper::statusList();
    }
}
