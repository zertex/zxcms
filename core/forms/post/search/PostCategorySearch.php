<?php

namespace core\forms\post\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use core\entities\post\PostCategory;

class PostCategorySearch extends Model
{
    public $id;
    public $name;
    public $slug;
    public $title;
    public $tid;

    public function rules(): array
    {
        return [
            [['id', 'tid'], 'integer'],
            [['name', 'slug', 'title'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
    	$this->tid = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT);
        $query = PostCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['sort' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type_id' => $this->tid,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
