<?php
/**
 * Created by Error202
 * Date: 11.09.2018
 */

namespace core\forms;

use yii\base\Model;
use Yii;
use yii\web\UploadedFile;

class FaviconForm extends Model
{
    public ?UploadedFile $image = null;

    public function rules()
    {
        return [
            ['image', 'required'],
            [['image'], 'image', 'extensions' => 'png', 'minWidth' => 200, 'minHeight' => 200],
        ];
    }

    public function attributeLabels()
    {
        return [
            'image' => Yii::t('main', 'Image'),
        ];
    }

    public function attributeHints()
    {
        return [
            'image' => Yii::t('main', 'Only png files allowed. Minimum size: 200x200. Form: square'),
        ];
    }
}
