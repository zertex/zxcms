<?php
/**
 * Created by Error202
 * Date: 10.02.2018
 */

namespace core\forms;

use yii\base\Model;

class SearchForm extends Model
{
	public ?string $query = null;

	public function rules(): array
    {
		return [
			['query', 'required'],
			['query', 'string'],
		];
	}
}
