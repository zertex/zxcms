<?php

namespace core\forms;

use Yii;
use yii\base\Model;

class ContactForm extends Model
{
    public ?string $name = null;
    public ?string $email = null;
    public ?string $subject = null;
    public ?string $body = null;
    public ?string $verifyCode = null;

    public function rules(): array
    {
        return [
            [['name', 'email', 'subject', 'body'], 'required'],
            ['email', 'email'],
            ['verifyCode', 'captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'verifyCode' => Yii::t('main', 'Verification Code'),
	        'name' => Yii::t('main', 'Your name'),
	        'subject' => Yii::t('main', 'Subject'),
	        'message' => Yii::t('main', 'Message'),
	        'email' => Yii::t('main', 'E-mail'),
        ];
    }
}
