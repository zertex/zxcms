<?php
namespace core\forms\auth;

use Yii;
use yii\base\Model;
use core\entities\user\User;

class PasswordResetRequestForm extends Model
{
    public ?string $email = null;

    public function rules(): array
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => User::class,
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => Yii::t('auth', 'There is no user with this email address.')
            ],
        ];
    }
}
