<?php
namespace core\forms\auth;

use yii\base\Model;

class ResetPasswordForm extends Model
{
    public ?string $password = null;

    public function rules(): array
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels() {
	    return [
	        'password' => \Yii::t('auth', 'Password'),
	    ];
    }
}
