<?php
namespace core\forms\auth;

use Yii;
use yii\base\Model;

class LoginForm extends Model
{
    public ?string $username = null;
    public ?string $password = null;
    public bool $rememberMe = true;

    public function rules(): array
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
        ];
    }

    public function attributeLabels() {
	    return [
	        'username' => Yii::t('user', 'Username'),
	        'password' => Yii::t('user', 'Password'),
		    'rememberMe' => Yii::t('user', 'Remember Me'),
	    ];
    }
}
