<?php

namespace core\forms\menu;

use core\components\LanguageDynamicModel;
use core\entities\menu\MenuItem;
use Yii;

class MenuItemForm extends LanguageDynamicModel
{
    public ?int $menu_id = null;
    public ?int $parent_id = null;
    public ?string $name = null;
    public ?string $title_attr = null;
    public ?string $target = null;
    public ?string $css = null;
    public ?string $style = null;
    public ?string $module = null;
    public ?string $url = null;
    public ?string $url_params = null;

    private MenuItem $menu;

    public function __construct(MenuItem $menu = null, array $attributes = [], $config = [])
    {
        if ($menu) {
            $this->menu_id    = $menu->menu_id;
            $this->parent_id  = $menu->parent_id;
            $this->target     = $menu->target;
            $this->css        = $menu->css;
            $this->style      = $menu->style;
            $this->module     = $menu->module;
            $this->url        = $menu->url;
            $this->url_params = $menu->url_params;

            $this->menu = $menu;
        }
        parent::__construct($menu, $attributes, $config);
        if ($menu) {
            foreach ($menu->translations as $translate) {
                if (!in_array($translate->language, $menu->translatedLanguages)) {
                    continue;
                }

                if ($translate->language == Yii::$app->params['backendDefaultLanguage']) {
                    $this->name       = $translate->name;
                    $this->title_attr = $translate->title_attr;
                } else {
                    $this->{'name' . '_' . $translate->language}       = $translate->name;
                    $this->{'title_attr' . '_' . $translate->language} = $translate->title_attr;
                }
            };
        }
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['name', 'menu_id'], 'required'],
                [['name', 'title_attr', 'css', 'style', 'module', 'url'], 'string', 'max' => 255],
                [['target'], 'string', 'max' => 20],
                ['url_params', 'string'],
                [['parent_id', 'menu_id'], 'integer'],
            ]
        );
    }

    public function attributeLabels(): array
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'menu_id'    => Yii::t('menu', 'Menu'),
                'parent id'  => Yii::t('menu', 'Parent menu item'),
                'name'       => Yii::t('menu', 'Name'),
                'title_attr' => Yii::t('menu', 'Title attribute'),
                'target'     => Yii::t('menu', 'Target'),
                'css'        => Yii::t('menu', 'CSS Classes'),
                'style'      => Yii::t('menu', 'CSS Style'),
                'module'     => Yii::t('menu', 'Module'),
                'url'        => Yii::t('menu', 'Url'),
                'url_params' => Yii::t('menu', 'Url Params'),
            ]
        );
    }
}
