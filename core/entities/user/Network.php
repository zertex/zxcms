<?php

namespace core\entities\User;

use Webmozart\Assert\Assert;
use yii\db\ActiveRecord;

/**
 * @property int $id [bigint]
 * @property int $user_id [bigint]
 * @property string $identity [varchar(255)]
 * @property string $network [varchar(16)]
 */
class Network extends ActiveRecord
{
    public static function create($network, $identity): self
    {
        Assert::notEmpty($network);
        Assert::notEmpty($identity);

        $item = new static();
        $item->network = $network;
        $item->identity = $identity;
        return $item;
    }

    public function isFor($network, $identity): bool
    {
        return $this->network === $network && $this->identity === $identity;
    }

    public static function tableName()
    {
        return '{{%user_networks}}';
    }
}
