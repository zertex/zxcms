<?php

namespace core\entities;

use yii\helpers\Json;

class Meta
{
    public string $title;
    public string $description;
    public string $keywords;

    public function __construct($title, $description, $keywords)
    {
        $this->title = $title;
        $this->description = $description;
        $this->keywords = $keywords;
    }

    public static function createMeta($json): Meta
    {
    	$meta = new Meta(null, null, null);
		$meta_data = Json::decode($json);
		$meta->title = $meta_data->title ?? '';
	    $meta->description = $meta_data->description ?? '';
	    $meta->keywords = $meta_data->keywords ?? '';
	    return $meta;
    }
}