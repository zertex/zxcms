<?php
/**
 * Created by Error202
 * Date: 04.06.2018
 */

namespace core\entities;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use core\behaviors\LanguageBehavior;

/**
 * @property string $type
 * @property string $section
 * @property string $key
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 */

class Settings extends ActiveRecord
{
    public Settings $_form;

    public static function tableName(): string
    {
        return '{{%settings}}';
    }

    public function attributeLabels()
    {
        return [
            'type'       => Yii::t('main', 'Type'),
            'section'    => Yii::t('main', 'Section'),
            'key'        => Yii::t('main', 'Key'),
            'value'      => Yii::t('main', 'Value'),
            'active'     => Yii::t('main', 'Active'),
            'created_at' => Yii::t('main', 'Created At'),
            'updated_at' => Yii::t('main', 'Updated At'),
        ];
    }

    public static function create($form, $type, $section, $key, $active): self
    {
        $settings          = new static();
        $settings->type    = $type;
        $settings->section = $section;
        $settings->key     = $key;
        $settings->active  = $active;

        $settings->_form   = $form;

        return $settings;
    }

    public function edit($form, $type, $section, $key, $active): void
    {
        $this->type    = $type;
        $this->section = $section;
        $this->key     = $key;
        $this->active  = $active;

        $this->_form   = $form;
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'SettingsVirtualTranslate',
                'translatedLanguages' => Yii::$app->params['translatedLanguages'],
                'relativeField'       => ['section', 'key'],
                'tableName'           => '{{%settings_lng}}',
                'attributes'          => ['value'],
                'defaultLanguage'     => Yii::$app->params['defaultLanguage'],
            ],
        ];
    }
}
