<?php

namespace core\entities\queries;

use yii\db\ActiveQuery;

class ModuleRecordQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere([
            'active' => 1,
        ]);
    }
}