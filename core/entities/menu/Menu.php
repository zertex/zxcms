<?php
/**
 * Created by Error202
 * Date: 09.07.2018
 */

namespace core\entities\menu;

use core\behaviors\LanguageBehavior;
use yii\db\ActiveRecord;

/**
 * Class Menu
 * @package core\entities
 *
 * @property integer $id
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] translations
 * @property ActiveRecord[] translation
 *
 * @property MenuItem[] $items
 */

class Menu extends ActiveRecord
{
	public $_form;

	public static function create($form): self
	{
		$menu = new static();
		$menu->_form = $form;
		return $menu;
	}

	public function edit($form): void
	{
		$this->_form = $form;
	}

	public static function tableName(): string
	{
		return '{{%menu}}';
	}

	public function getItems()
	{
		return $this->hasMany(MenuItem::class, ['menu_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
	}

	public function behaviors()
	{
		return [
			[
				'class' => LanguageBehavior::class,
				'virtualClassName' => 'MenuVirtualTranslate',
				'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
				'relativeField' => 'menu_id',
				'tableName' => "{{%menu_lng}}",
				'attributes' => ['name'],
				'defaultLanguage' => \Yii::$app->params['defaultLanguage'],
			],
		];
	}

	/*public static function find()
	{
		return new LanguageTranslateQuery(get_called_class());
	}*/
}
