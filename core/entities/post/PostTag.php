<?php

namespace core\entities\post;

use yii\db\ActiveRecord;
use yii\caching\TagDependency;
use Yii;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $type_id
 */
class PostTag extends ActiveRecord
{
    public static function create($name, $slug, $type_id): self
    {
        $tag = new static();
        $tag->name = $name;
        $tag->slug = $slug;
        $tag->type_id = $type_id;
        return $tag;
    }

    public function edit($name, $slug, $type_id): void
    {
        $this->name = $name;
        if ($slug != $this->slug)
        {
            TagDependency::invalidate(\Yii::$app->cache, 'post_tags');
        }
        $this->slug = $slug;
	    $this->type_id = $type_id;
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('post', 'Tag Name'),
            'slug' => Yii::t('post', 'SEO link'),
        ];
    }

    public static function tableName(): string
    {
        return '{{%post_tags}}';
    }
}