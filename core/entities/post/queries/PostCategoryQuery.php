<?php

namespace core\entities\post\queries;

use yii\db\ActiveQuery;

class PostCategoryQuery extends ActiveQuery
{

    public function byType($type)
    {
    	return $this->andWhere(['type_id' => $type]);
    }
}