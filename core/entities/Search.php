<?php
/**
 * Created by Error202
 * Date: 05.07.2018
 */

namespace core\entities;

use yii\db\ActiveRecord;

/**
 * Class Search
 * @package core\entities
 *
 * @property string $title
 * @property string $content
 * @property string $url
 */

class Search extends ActiveRecord
{
	public static function tableName(): string
	{
		return '{{view_search}}';
	}
}