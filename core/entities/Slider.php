<?php

namespace core\entities;

use yiidreamteam\upload\ImageUploadBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use Yii;

/**
 * @property integer $id
 * @property string $title
 * @property string $tagline
 * @property string $image
 * @property string $url
 * @property integer $sort
 *
 * @mixin ImageUploadBehavior
 */
class Slider extends ActiveRecord
{

	const SCENARIO_UPDATE = 'update';
	const SCENARIO_CREATE = 'create';

    public static function create($title, $tagline, $url, $sort): self
    {
        $slider = new static();
        $slider->title = $title;
        $slider->tagline = $tagline;
        $slider->url = $url;
        $slider->sort = $sort;
        return $slider;
    }

	public function setImage(UploadedFile $image): void
	{
		$this->image = $image;
	}

    public function edit($title, $tagline, $url, $sort): void
    {
        $this->title = $title;
        $this->tagline = $tagline;
        $this->url = $url;
        $this->sort = $sort;
    }

    public static function tableName(): string
    {
        return '{{%slider}}';
    }

    public function attributeLabels() {
	    return [
	    	'title' => Yii::t('slider', 'Title'),
		    'tagline' => Yii::t('slider', 'Tagline'),
		    'image' => Yii::t('slider', 'Image'),
		    'url' => Yii::t('slider', 'URL'),
		    'sort' => Yii::t('slider', 'Sort'),
	    ];
    }

	public function behaviors(): array
	{
		return [
			[
				'class' => ImageUploadBehavior::class,
				'attribute' => 'image',
				'createThumbsOnRequest' => true,
				'filePath' => '@staticRoot/origin/slider/[[id]].[[extension]]',
				'fileUrl' => '@static/origin/slider/[[id]].[[extension]]',
				'thumbPath' => '@staticRoot/cache/slider/[[profile]]_[[id]].[[extension]]',
				'thumbUrl' => '@static/cache/slider/[[profile]]_[[id]].[[extension]]',
				'thumbs' => [
					'admin' => ['width' => 150, 'height' => 60],
					'thumb' => ['width' => 400, 'height' => 150],
					'1920_671' => ['width' => 1920, 'height' => 671],
					//'origin' => ['processor' => [new WaterMarker(1024, 768, '@frontend/web/image/logo.png'), 'process']],
					'origin' => ['width' => 1920, 'height' => 1080],
				],
			],
		];
	}
}