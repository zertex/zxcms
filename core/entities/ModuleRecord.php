<?php

namespace core\entities;

use core\entities\queries\ModuleRecordQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "modules".
 *
 * @property int $id
 * @property string $name
 * @property string $class
 * @property string $type
 * @property int $active
 * @property int $system
 */
class ModuleRecord extends ActiveRecord
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    const SYSTEM_YES = 1;
    const SYSTEM_NO = 0;

    public string $description;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{%modules}}';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['name', 'class', 'type'], 'required'],
            [['name', 'class', 'type'], 'string', 'max' => 255],
            ['active', 'integer'],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id'    => 'ID',
            'name'  => 'Name',
            'class' => 'Class',
            'type'  => 'Type',
        ];
    }

    public function isEnabled(): bool
    {
        return $this->active == $this::STATUS_ENABLED;
    }

    public function isDisabled(): bool
    {
        return $this->active == $this::STATUS_DISABLED;
    }

    public function isSystem(): bool
    {
        return $this->system == $this::SYSTEM_YES;
    }

    public static function find(): ModuleRecordQuery
    {
        return new ModuleRecordQuery(static::class);
    }
}
