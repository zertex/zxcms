<?php

namespace core\services;

use core\entities\Slider;
use core\forms\SliderForm;
use core\repositories\slider\SliderRepository;

class SliderService
{
    private SliderRepository $sliders;

    public function __construct(SliderRepository $sliders)
    {
        $this->sliders = $sliders;
    }

    public function create(SliderForm $form): Slider
    {
	    $slider = Slider::create(
		    $form->title,
		    $form->tagline,
		    $form->url,
		    $form->sort
	    );

	    if ($form->image) {
		    $slider->setImage($form->image);
	    }

	    $this->sliders->save($slider);
	    return $slider;
    }

    public function edit($id, SliderForm $form): void
    {
	    $slider = $this->sliders->get($id);

	    $slider->edit(
		    $form->title,
		    $form->tagline,
		    $form->url,
		    $form->sort
	    );

	    if ($form->image) {
		    $slider->setImage($form->image);
	    }

	    $this->sliders->save($slider);
    }

	public function remove($id): void
	{
		$tag = $this->sliders->get($id);
		$this->sliders->remove($tag);
	}
}
