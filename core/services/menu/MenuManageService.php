<?php

namespace core\services\menu;

use core\entities\menu\Menu;
use core\forms\menu\MenuForm;
use core\repositories\menu\MenuRepository;

class MenuManageService
{
    private MenuRepository $repository;

    public function __construct(MenuRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(MenuForm $form): Menu
    {
        $menu = Menu::create(
        	$form
        );
        $this->repository->save($menu);
        return $menu;
    }

    public function edit($id, MenuForm $form): void
    {
        $menu = $this->repository->get($id);
        $menu->edit(
        	$form
        );
        $this->repository->save($menu);
    }

    public function remove($id): void
    {
        $menu = $this->repository->get($id);
        $this->repository->remove($menu);
    }
}
