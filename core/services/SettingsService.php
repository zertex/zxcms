<?php

namespace core\services;

use core\components\favicon\FaviconGenerator;
use core\entities\Settings;
use core\forms\SettingsForm;
use core\repositories\SettingsRepository;

class SettingsService
{
    private SettingsRepository $settings_repository;

    public function __construct(SettingsRepository $settings_repository)
    {
        $this->settings_repository = $settings_repository;
    }

    public function create(SettingsForm $form): Settings
    {
        $settings = Settings::create(
            $form,
            $form->type,
            $form->section,
            $form->key,
            $form->active
        );
        $this->settings_repository->save($settings);

        return $settings;
    }

    public function edit($section, $key, SettingsForm $form): void
    {
        $settings = $this->settings_repository->get($section, $key);
        $settings->edit(
            $form,
            $form->type,
            $form->section,
            $form->key,
            $form->active
        );
        $this->settings_repository->save($settings);
    }

    public function remove($section, $key): void
    {
        $settings = $this->settings_repository->get($section, $key);
        $this->settings_repository->remove($settings);
    }

    public function newFavicon(): void
    {
        $fg = new FaviconGenerator();
        $fg->generateIcons();
    }
}
