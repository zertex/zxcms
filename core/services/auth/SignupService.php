<?php

namespace core\services\auth;

use core\access\Rbac;
use core\entities\user\User;
use core\forms\auth\SignupForm;
use core\repositories\user\UserRepository;
use core\services\RoleManager;
use core\services\TransactionManager;
use DomainException;
use yii\base\Exception;

class SignupService
{
    private UserRepository $users;
    private RoleManager $roles;
    private TransactionManager $transaction;

    public function __construct(
        UserRepository $users,
        RoleManager $roles,
        TransactionManager $transaction
    )
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->transaction = $transaction;
    }

    /**
     * @param SignupForm $form
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function signup(SignupForm $form): void
    {
        $user = User::requestSignup(
            $form->username,
            $form->email,
            $form->password
        );
        $this->transaction->wrap(function () use ($user) {
            $this->users->save($user);
            $this->roles->assign($user->id, Rbac::ROLE_USER);
        });
    }

    public function confirm($token): void
    {
        if (empty($token)) {
            throw new DomainException('Empty confirm token.');
        }
        $user = $this->users->getByEmailConfirmToken($token);
        $user->confirmSignup();
        $this->users->save($user);
    }
}