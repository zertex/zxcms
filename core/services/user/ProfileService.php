<?php
/**
 * Created by Error202
 * Date: 22.08.2017
 */

namespace core\services\user;

use core\repositories\user\UserRepository;
use core\forms\user\ProfileEditForm;
use yii\base\Exception;

class ProfileService
{
    private UserRepository $users;

    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * @param $id
     * @param ProfileEditForm $form
     * @throws Exception
     */
    public function edit($id, ProfileEditForm $form): void
    {
        $user = $this->users->get($id);
        $user->editProfile($form->email, $form->username, $form->password, $form->user_pic, $form->backend_language);
        $this->users->save($user);
    }
}
