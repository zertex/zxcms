<?php

namespace core\services\post;

use core\entities\post\PostComment;
use core\forms\post\PostCommentForm;
use core\repositories\post\PostRepository;
use core\repositories\user\UserRepository;

class PostCommentService
{
    private PostRepository $posts;
    private UserRepository $users;

    public function __construct(PostRepository $posts, UserRepository $users)
    {
        $this->posts = $posts;
        $this->users = $users;
    }

    public function create($postId, $userId, PostCommentForm $form): PostComment
    {
        $post = $this->posts->get($postId);
        $user = $this->users->get($userId);
        $comment = $post->addComment($user->id, $form->parentId, $form->text);
        $this->posts->save($post);

        return $comment;
    }
}
