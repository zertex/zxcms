<?php

namespace core\services\post;

use core\entities\post\PostTag;
use core\forms\post\PostTagSingleForm;
use core\repositories\post\PostTagRepository;

class PostTagManageService
{
    private PostTagRepository $tags;

    public function __construct(PostTagRepository $tags)
    {
        $this->tags = $tags;
    }

    public function create(PostTagSingleForm $form): PostTag
    {
        $tag = PostTag::create(
            $form->name,
            $form->slug,
            $form->type_id
        );
        $this->tags->save($tag);
        return $tag;
    }

    public function edit($id, PostTagSingleForm $form): void
    {
        $tag = $this->tags->get($id);
        $tag->edit(
            $form->name,
            $form->slug,
            $form->type_id
        );
        $this->tags->save($tag);
    }

    public function remove($id): void
    {
        $tag = $this->tags->get($id);
        $this->tags->remove($tag);
    }
}
