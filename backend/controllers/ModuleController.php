<?php
/**
 * Created by Error202
 * Date: 17.08.2018
 */

namespace backend\controllers;

use core\entities\ModuleRecord;
use core\services\ModuleService;
use Yii;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Modules Management
 * Class ModuleController
 * @package backend\controllers
 */
class ModuleController extends Controller
{
    /**
     * @var ModuleService Modules management service
     */
    private $service;

    public function __construct(string $id, $module, ModuleService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['list', 'disable', 'enable', 'delete', 'search'],
                        'allow'   => true,
                        'roles'   => ['ModuleManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete'  => ['POST'],
                    'disable' => ['POST'],
                    'enable'  => ['POST'],
                    'install' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * List of modules
     * @return string
     */
    public function actionList(): string
    {
        $modules = Yii::$app->moduleManager->getModules();

        return $this->render('list', [
            'modules' => $modules,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws ErrorException
     * @throws Exception
     */
    public function actionDelete($id): Response
    {
        $module = $this->findModel($id);
        $this->service->delete($module);

        return $this->redirect(['module/list']);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDisable($id): Response
    {
        $module = $this->findModel($id);
        $this->service->disable($module);

        return $this->redirect(['module/list']);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionEnable($id): Response
    {
        $module = $this->findModel($id);
        $this->service->enable($module);

        return $this->redirect(['module/list']);
    }

    public function actionSearch(): string
    {
        $message = null;
        try {
            $list    = file_get_contents('http://zertex.ru/zxcms/modules.txt');
            $links   = array_filter(explode('|', $list));
            $modules = [];
            foreach ($links as $link) {
                $manifest = file_get_contents($link);
                $manifest = eval(str_replace('<?php', '', $manifest));

                if (isset($manifest['enabled']) && $manifest['enabled'] == false) {
                    continue;
                }

                $languageFile = str_replace('manifest.php', 'messages/' . Yii::$app->language . '/' . $manifest['name'] . '.php', $link);
                try {
                    $language = file_get_contents($languageFile);
                    $language = eval(str_replace('<?php', '', $language));
                    $manifest['locale_name'] = $language[$manifest['name']] ?? $manifest['name'];
                    $manifest['locale_description'] = $language[$manifest['description']] ?? $manifest['description'];
                } catch (\Exception) {
                    $manifest['locale_name'] = $manifest['name'];
                    $manifest['locale_description'] = $manifest['description'];
                }

                $modules[] = $manifest;
            }
        } catch (\Exception) {
            $modules = [];
            $message = Yii::t('main', 'Available modules not found');
        }

        return $this->render('remote-list', [
            'modules' => $modules,
            'message' => $message,
        ]);
    }

    /**
     * @param $id
     * @return ModuleRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($id): ModuleRecord
    {
        if (($model = ModuleRecord::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested module does not exist.');
    }
}
