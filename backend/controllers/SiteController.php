<?php

namespace backend\controllers;

use core\entities\Search;
use core\forms\SearchForm;
use core\services\user\UserManageService;
use DomainException;
use RuntimeException;
use Yii;
use yii\base\Action;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use core\helpers\UserHelper;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    private UserManageService $service;

    public function __construct(string $id, $module, UserManageService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['error'],
                        'allow'   => true,
                    ],
                    [
                        'allow'   => true,
                        'roles'   => ['Dashboard'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'logout'   => ['post'],
                    'language' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }

    public function actionSearch(): string
    {
        $form = new SearchForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $query = Search::find()
                               ->andWhere(['LIKE', 'title', $form->query])
                               ->orWhere(['LIKE', 'content', $form->query]);

                $provider = new ActiveDataProvider([
                    'query'      => $query,
                    'pagination' => [
                        'pageSize' => 10,
                    ],
                    'sort'       => [],
                ]);

                return $this->render('search', [
                    'provider' => $provider,
                    'form'     => $form
                ]);
            } catch (DomainException) {
            }
        }

        return '';
    }

    public function actionLanguage($language): Response
    {
        if ($language && in_array($language, array_keys(Yii::$app->params['backendTranslatedLanguages']))) {
            $this->service->setBackendLanguage($language);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param Action $action
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        if ($action->id === 'error') {
            $this->layout = 'error';
        }

        return parent::beforeAction($action);
    }

    public function actionGetWidgetsList(): string
    {
        return $this->renderAjax('widgets-list');
    }

    public function actionAddWidget($itemIdx, $color = ''): string
    {
        if (!Yii::$app->request->isAjax) {
            throw new RuntimeException(Yii::t('main', 'The requested page does not exist.'));
        }

        $widgets = UserHelper::getSetting('widgetsLayout', []);
        $item = Yii::$app->params['dashboard_widgets'][$itemIdx];  //require($itemFile);
        $resizable = $item['resizable'] ?? 0;
        $newWidget = [
            'w' => $item['size']['width'] ?: 0,
            'h' => $item['size']['height'] ?: 0,
            'x' => 0,
            'y' => 0,
            'c' => $color,
            'resize' => $resizable,
            'auto' => 1,
            'method' => $item['method'],
            'title' => $item['title'],
            'name' => $item['name'],
            'icon' => $item['icon'],
            'widget' => $item['widget'],
        ];
        array_push($widgets, $newWidget);
        UserHelper::setSetting('widgetsLayout', $widgets);
        return 'ok';
    }

    public function actionRemoveWidget($idx)
    {
        if (!Yii::$app->request->isAjax) {
            throw new RuntimeException(Yii::t('main', 'The requested page does not exist.'));
        }
        $widgets = UserHelper::getSetting('widgetsLayout', []);
        array_splice($widgets, $idx, 1);
        UserHelper::setSetting('widgetsLayout', $widgets);
    }

    public function actionSaveWidgets()
    {
        $widgetsJson = Yii::$app->request->post('widgets');
        $widgets = json_decode($widgetsJson, true);
        if ($widgets) {
            UserHelper::setSetting('widgetsLayout', $widgets);
        }
    }

    public function actionGetSelectedWidgetsList(): string
    {
        return $this->renderAjax( 'widgets-selected-list');
    }
}
