<?php
/**
 * Created by Error202
 * Date: 15.08.2017
 */

namespace backend\controllers;

use backend\forms\rbac\RbacEditRoleForm;
use backend\forms\rbac\RbacUpdateChildren;
use core\services\PermissionManager;
use DomainException;
use Exception;
use yii\behaviors\TimestampBehavior;
use backend\forms\rbac\RbacCreateRoleForm;
use core\services\RoleManager;
use yii\data\ArrayDataProvider;
use yii\rbac\Role;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Yii;
use yii\web\Response;

class RoleController extends Controller
{
    private RoleManager $role;
    private PermissionManager $permission;

    public function __construct($id, $module, RoleManager $role, PermissionManager $permission, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->role       = $role;
        $this->permission = $permission;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['create', 'view', 'index', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['UserManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex(): string
    {
        $data = $this->role->getRolesListArray();

        $dataProvider = new ArrayDataProvider([
            'allModels'  => $data,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort'       => [
                'attributes' => ['name', 'description'],
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    /**
     * @return Response|string
     * @throws Exception
     */
    public function actionCreate(): Response|string
    {
        $form = new RbacCreateRoleForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->role->create($form->name, $form->description, $form->rule_name, $form->data);

                return $this->redirect(['view', 'id' => $form->name]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws Exception
     */
    public function actionUpdate($id): Response|string
    {
        $role = $this->findModel($id);

        $form = new RbacEditRoleForm($role);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->role->update($role->name, $form->name, $form->description, $form->rule_name, $form->data);

                return $this->redirect(['view', 'id' => $form->name]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    public function actionDelete($id): Response
    {
        try {
            $this->role->delete($id);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionView($id): string
    {
        $currentRole      = $this->role->getRole($id);
        $rolesSelectArray = array_diff_assoc($this->role->getRolesSelectArray(), [$currentRole->name => $currentRole->description]);

        $itemsForm = new RbacUpdateChildren();
        if ($itemsForm->load(Yii::$app->request->post()) && $itemsForm->validate()) {
            $this->role->saveChildren($id, $itemsForm->roles, $itemsForm->permissions);
            Yii::$app->session->setFlash('success', Yii::t('user', 'Children roles and permissions for "{role}" is updated.', ['role' => $currentRole->description]));
        }

        $rolesSelected = $this->role->getRolesSelectArrayByRole($id);

        $permissionsSelectArray = $this->permission->getPermissionsSelectArray();
        $permissionsSelected    = $this->permission->getPermissionsSelectArrayByRole($id);

        $itemsForm->roles       = $rolesSelected;
        $itemsForm->permissions = $permissionsSelected;

        return $this->render('view', [
            'model'               => $this->findModel($id),
            'roles'               => $rolesSelectArray,
            'permissions'         => $permissionsSelectArray,
            'itemsForm'           => $itemsForm,
        ]);
    }

    /**
     * @param $id
     * @return Role|null
     */
    protected function findModel($id): ?Role
    {
        return $this->role->getRole($id);
    }
}
