<?php
/**
 * Created by Error202
 * Date: 15.08.2017
 */

namespace backend\controllers;

use backend\forms\rbac\RbacCreatePermissionForm;
use backend\forms\rbac\RbacEditPermissionForm;
use core\services\PermissionManager;
use DomainException;
use Exception;
use yii\behaviors\TimestampBehavior;
use yii\data\ArrayDataProvider;
use yii\rbac\Permission;
use yii\web\Controller;
use yii\filters\VerbFilter;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

class PermissionController extends Controller
{
    private PermissionManager $permission;

    public function __construct($id, $module, PermissionManager $permission, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->permission = $permission;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['create', 'view', 'index', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['UserManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex(): string
    {
        $data = array_map(function (Permission $permission) {
            return [
                'name'        => $permission->name,
                'description' => $permission->description,
            ];
        }, $this->permission->getPermissions());

        $dataProvider = new ArrayDataProvider([
            'allModels'  => $data,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort'       => [
                'attributes' => ['name', 'description'],
            ],
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionCreate(): Response|string
    {
        $form = new RbacCreatePermissionForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->permission->create($form->name, $form->description, $form->rule_name, $form->data);

                return $this->redirect(['view', 'id' => $form->name]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * @param $id
     * @return Response|string
     * @throws Exception
     */
    public function actionUpdate($id): Response|string
    {
        $permission = $this->findModel($id);

        $form = new RbacEditPermissionForm($permission);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->permission->update($permission->name, $form->name, $form->description, $form->rule_name, $form->data);

                return $this->redirect(['view', 'id' => $form->name]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
        ]);
    }

    public function actionDelete($id): Response
    {
        $this->permission->delete($id);

        return $this->redirect(['index']);
    }

    public function actionView($id): string
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id): ?Permission
    {
        return $this->permission->getPermission($id);
    }
}
