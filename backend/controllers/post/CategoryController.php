<?php

namespace backend\controllers\post;

use core\entities\post\PostType;
use core\forms\post\PostCategoryForm;
use core\services\post\PostCategoryManageService;
use DomainException;
use Yii;
use core\entities\post\PostCategory;
use core\forms\post\search\PostCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

class CategoryController extends Controller
{
    private PostCategoryManageService $service;

    public function __construct($id, $module, PostCategoryManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['PostManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	/**
	 * @param $tid
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionIndex($tid): string
    {
	    $type = $this->findType($tid);

        $searchModel = new PostCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	        'type' => $type,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionView($id): string
    {
    	$category = $this->findModel($id);
    	$type = $this->findType($category->type_id);
        return $this->render('view', [
            'category' => $category,
	        'type' => $type,
        ]);
    }

	/**
	 * @param $tid
	 *
	 * @return string|Response
	 * @throws NotFoundHttpException
	 */
    public function actionCreate($tid): Response|string
    {
	    $type = $this->findType($tid);
        $form = new PostCategoryForm();
        $form->type_id = $type->id;
        $form->updateSort();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $category = $this->service->create($form);
                return $this->redirect(['view', 'id' => $category->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $form,
	        'type' => $type,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string|Response
	 * @throws NotFoundHttpException
	 */
    public function actionUpdate($id): Response|string
    {
        $category = $this->findModel($id);
	    $type = $this->findType($category->type_id);
        $form = new PostCategoryForm($category);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($category->id, $form);
                return $this->redirect(['view', 'id' => $category->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'category' => $category,
	        'type' => $type,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return Response
	 * @throws NotFoundHttpException
	 */
    public function actionDelete($id): Response
    {
	    $category = $this->findModel($id);
	    $type = $this->findType($category->type_id);
        try {
            $this->service->remove($id);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index', 'tid' => $type->id]);
    }


    /**
     * @param integer $id
     * @return PostCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): PostCategory
    {
        if (($model = PostCategory::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param integer $id
     * @return PostType
     * @throws NotFoundHttpException
     */
    protected function findType(int $id): PostType
    {
    	if (($type = PostType::findOne($id)) !== null) {
    		return $type;
	    }
	    throw new NotFoundHttpException('The requested page does not exist.');
    }
}
