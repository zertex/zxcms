<?php

namespace backend\controllers\post;

use core\entities\post\PostType;
use core\forms\post\PostTypeForm;
use core\forms\post\search\PostTypeSearch;
use core\services\post\PostTypeService;
use DomainException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;
use yii\web\Response;

class TypeController extends Controller
{
    private $service;

    public function __construct($id, $module, PostTypeService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['PostManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	/**
	 * @return string
	 */
    public function actionIndex(): string
    {
        $searchModel = new PostTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionView($id): string
    {
    	$type = $this->findModel($id);
        return $this->render('view', [
            'type' => $type,
        ]);
    }

	/**
	 * @return string|Response
	 */
    public function actionCreate(): Response|string
    {
        $form = new PostTypeForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $type = $this->service->create($form);
                return $this->redirect(['view', 'id' => $type->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $form,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string|Response
	 * @throws NotFoundHttpException
	 */
    public function actionUpdate($id): Response|string
    {
	    $type = $this->findModel($id);

        $form = new PostTypeForm($type);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($type->id, $form);
                return $this->redirect(['view', 'id' => $type->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'type' => $type,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return Response
	 * @throws NotFoundHttpException
	 */
    public function actionDelete($id)
    {
	    $type = $this->findModel($id);

        try {
            $this->service->remove($id);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

	/**
	 * @param $id
	 *
	 * @return PostType
	 * @throws NotFoundHttpException
	 */
	protected function findModel($id): PostType
	{
		if (($type = PostType::findOne($id)) !== null) {
			return $type;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
