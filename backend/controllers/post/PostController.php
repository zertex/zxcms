<?php

namespace backend\controllers\post;

use core\entities\post\PostTag;
use core\entities\post\PostType;
use core\forms\post\PostForm;
use core\services\post\PostManageService;
use DomainException;
use Yii;
use core\entities\post\Post;
use core\forms\post\search\PostSearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

class PostController extends Controller
{
    private PostManageService $service;

    public function __construct($id, $module, PostManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['PostManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'activate' => ['POST'],
                    'draft' => ['POST'],
                    'delete-photo' => ['POST'],
                    'move-photo-up' => ['POST'],
                    'move-photo-down' => ['POST'],
                ],
            ],
        ];
    }

	/**
	 * @param $tid
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionIndex($tid): string
    {
    	$type = $this->findType($tid);

        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render( '/post/' . $type->name . '/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
	        'type' => $type,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionView($id): string
    {
    	$post = $this->findModel($id);
	    $type = $this->findType($post->type_id);
        return $this->render('/post/' . $type->name . '/view', [
            'post' => $post,
            'type' => $type,
        ]);
    }

    /**
     * @param $tid
     * @return Response|string
     * @throws NotFoundHttpException
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function actionCreate($tid): Response|string
    {
	    $type = $this->findType($tid);
        $form = new PostForm();
	    $form->type_id = $type->id;
	    $form->published_at = date('d.m.Y H:i:s');
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $post = $this->service->create($form);
                return $this->redirect(['view', 'id' => $post->id, 'tid' => $post->type_id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('/post/' . $type->name . '/create', [
            'model' => $form,
            'type' => $type,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $post = $this->findModel($id);
		$type = $this->findType($post->type_id);
        $form = new PostForm($post);
        $form->published_at = date('d.m.Y H:i:s', $form->published_at);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($post->id, $form);
                return $this->redirect(['view', 'id' => $post->id, 'tid' => $post->type_id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('/post/' . $type->name . '/update', [
            'model' => $form,
            'post' => $post,
	        'type' => $type,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return Response
	 * @throws NotFoundHttpException
	 */
    public function actionDelete($id): Response
    {
    	$post = $this->findModel($id);
	    $type = $this->findType($post->type_id);
        try {
            $this->service->remove($id);
        } catch (DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index', 'tid' => $type->id]);
    }

    /**
     * @param integer $id
     * @return Response
     */
    public function actionActivate(int $id): Response
    {
        try {
            $this->service->activate($id);
        } catch (DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param integer $id
     * @return Response
     */
    public function actionDraft(int $id): Response
    {
        try {
            $this->service->draft($id);
        } catch (DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionTagSearch($type_id, $q = null, $id = null)
    {
	    \Yii::$app->response->format = Response::FORMAT_JSON;
	    $out = ['results' => ['id' => '', 'text' => '']];
	    if (!is_null($q)) {
		    $data = PostTag::find()->select('name as id, name as text')->andWhere(['type_id' => $type_id])->andWhere(['like', 'name', $q])->orderBy('name')->limit(20)->asArray()->all();
		    $out['results'] = array_values($data);
	    }
	    elseif ($id > 0) {
	    	$tag_name = PostTag::findOne($id)->name;
		    $out['results'] = ['id' => $tag_name, 'text' => $tag_name];
	    }
	    return $out;
    }

    /**
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): Post
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return PostType
     * @throws NotFoundHttpException
     */
	protected function findType($id): PostType
	{
		if (($type = PostType::findOne($id)) !== null) {
			return $type;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}
