<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-backend',
    'language'            => 'ru',
    'basePath'            => dirname(__DIR__),
    'aliases'             => [
        '@staticRoot' => $params['staticPath'],
        '@static'     => $params['staticHostInfo'],
    ],
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => [
        'log',
        'common\bootstrap\SetUp',
        'backend\bootstrap\SetUp',
    ],
    'modules'             => [
        'elfinder' => [
            'class' => 'core\components\elfinder\ElFinderModule',
        ],
    ],
    /*'controllerMap' => [
	    'elfinder' => [
		    'class' => 'mihaildev\elfinder\Controller',
		    'access' => ['@'],
		    'plugin' => [
			    [
				    'class'=>'\mihaildev\elfinder\plugin\Sluggable',
				    'lowercase' => true,
				    'replacement' => '-'
			    ]
		    ],
		    'roots' => [
			    [
				    'baseUrl'=>'@static',
				    'basePath'=>'@staticRoot',
				    'path' => 'files',
				    'name' => 'Global'
			    ],
		    ],
	    ],
    ],*/
    'controllerMap'       => [
        'elfinder' => [
            'class'  => 'zertex\elfinder\Controller',
            'access' => ['@'],
            'plugin' => [
                [
                    'class'       => '\zertex\elfinder\plugin\Sluggable',
                    'lowercase'   => true,
                    'replacement' => '-'
                ]
            ],
            'roots'  => [
                [
                    'baseUrl'  => '@static',
                    'basePath' => '@staticRoot',
                    'path'     => 'files',
                    'name'     => 'Global'
                ],
            ],
        ],
    ],
    'components'          => [
        'request'            => [
            'baseUrl'             => '',
            'csrfParam'           => '_csrf-backend',
            'cookieValidationKey' => $params['cookieValidationKey'],
        ],
        'user'               => [
            'identityClass'   => 'common\auth\Identity',
            'enableAutoLogin' => true,
            'identityCookie'  => [
                'name'     => '_identity',
                'httpOnly' => true,
                'domain'   => $params['cookieDomain'],
            ],
            'loginUrl'        => ['auth/login'],
        ],
        'session'            => [
            'name'          => '_session',
            'class'         => 'yii\web\DbSession',
            'writeCallback' => function ($session) {
                return [
                    'user_id' => Yii::$app->user->id
                ];
            },
            'cookieParams'  => [
                'domain'   => $params['cookieDomain'],
                'httpOnly' => true,
            ],
        ],
        'log'                => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler'       => [
            'errorAction' => 'site/error',
        ],
        'backendUrlManager'  => require __DIR__ . '/urlManager.php',
        'frontendUrlManager' => require __DIR__ . '/../../frontend/config/urlManager.php',
        'urlManager'         => function () {
            return Yii::$app->get('backendUrlManager');
        },
        'i18n'               => [
            'translations' => [
                'main'      => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                ],
                'dashboard' => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                ],
                'menu'      => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                ],
                'page'      => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                ],
                'user'      => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                ],
                'post'      => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                ],
                'buttons'   => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                ],
                'slider'    => [
                    'class'    => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                ],
            ],
        ],
        'assetManager'       => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset'       => [
                    'sourcePath' => '@core/components/bootstrap4',   // do not publish the bundle
                    'css'        => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css'
                    ],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => '@core/components/bootstrap4',
                    'js'         => [
                        YII_ENV_DEV ? 'js/bootstrap.bundle.js' : 'js/bootstrap.bundle.min.js'
                    ],
                    'depends'    => [
                        'yii\web\JqueryAsset',
                        'yii\bootstrap\BootstrapAsset',
                    ],
                ],
                /*'yii\web\JqueryAsset'                => [
                    'sourcePath' => '@cp/assets/libs/jquery321',   // do not publish the bundle
                    'js'         => [
                        YII_ENV_DEV ? 'jquery-3.2.1.js' : 'jquery-3.2.1.min.js'
                    ],
                ],
                'yii\bootstrap\BootstrapAsset'       => [
                    'sourcePath' => '@cp/assets/libs/bootstrap4/css',   // do not publish the bundle
                    'css'        => [
                        YII_ENV_DEV ? 'bootstrap.css' : 'bootstrap.min.css'
                    ],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => '@cp/assets/libs/bootstrap4/js',   // do not publish the bundle
                    'js'         => [
                        YII_ENV_DEV ? 'bootstrap.js' : 'bootstrap.min.js'
                    ],
                    'depends'    => [
                        'yii\web\JqueryAsset',
                        'yii\bootstrap\BootstrapAsset',
                    ],
                ],*/
            ],
        ],
    ],
    'params'              => $params,
];
