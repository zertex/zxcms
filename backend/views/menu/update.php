<?php

/* @var $this yii\web\View */
/* @var $menu Menu */
/* @var $model MenuForm */

use core\entities\menu\Menu;
use core\forms\menu\MenuForm;

$this->title = Yii::t('menu', 'Update Menu: {name}', ['name' => isset($menu->translation) ? $menu->translation->name : Yii::t('menu', 'Not set')]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('menu', 'Menu'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => isset($menu->translation) ? $menu->translation->name : Yii::t('menu', 'Not set'), 'url' => ['index', 'id' => $menu->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="menu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
