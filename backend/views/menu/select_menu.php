<?php
/**
 * Created by Error202
 * Date: 10.07.2018
 */

use core\entities\menu\Menu;
use core\forms\menu\MenuSelectForm;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model MenuSelectForm
 * @var $menus Menu[]
 */

$this->title = Yii::t('menu', 'Menu');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="menu">
	<div class="box box-default">
		<div class="box-header with-border"><?= Yii::t('menu', 'Current Menu') ?></div>
		<div class="box-body">

			<div class="row">
				<div class="col-md-6">
					<?php $form = ActiveForm::begin([
						'enableClientValidation' => false,
						'method' => 'get',
						'id' => 'select_menu',
					]); ?>

					<?= $form->field($model, 'id')->dropDownList($menus, [
						'prompt' => Yii::t('menu', 'Select menu...'),
						'onchange' => 'this.form.submit()',
					])->label(false) ?>

					<?php ActiveForm::end(); ?>
				</div>
				<div class="col-md-2">
					<?= Html::a(Yii::t('menu', 'Create Menu'), ['menu/create'], [
						'class' => 'btn btn-success',
					]) ?>
				</div>
			</div>

		</div>
	</div>
</div>

