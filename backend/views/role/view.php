<?php

use backend\forms\rbac\RbacUpdateChildren;
use yii\helpers\Html;
use yii\rbac\Permission;
use yii\rbac\Role;
use yii\widgets\DetailView;
use kartik\select2\Select2;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Role */
/* @var $roles Role[] */
/* @var $permissions Permission[] */
/* @var $rolesSelected array */
/* @var $permissionsSelected array */
/* @var $itemsForm RbacUpdateChildren */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-rbac-role-view">

    <p>
        <?= Html::a(Yii::t('user', 'Roles'), ['index'], ['class' => 'btn btn-outline-primary btn-sm']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $model->name], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $model->name], [
            'class' => 'btn btn-danger btn-sm',
            'data'  => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <div class="card">
        <div class="card-body">
            <?= DetailView::widget([
                'model'      => $model,
                'attributes' => [
                    [
                        'attribute' => 'name',
                        'label'     => Yii::t('user', 'Role Name'),
                    ],
                    [
                        'attribute' => 'description',
                        'label'     => Yii::t('user', 'Role Description'),
                    ],
                    [
                        'attribute' => 'ruleName',
                        'label'     => Yii::t('user', 'Rule Name'),
                    ],
                ],
            ]) ?>
        </div>
    </div>


    <?php if ($model->name !== 'admin') : ?>

    <div class="card">
        <div class="card-body">
            <h4><?= Yii::t('user', 'Contains roles') ?></h4>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($itemsForm, 'roles')->widget(
                Select2::class,
                [
                    'name'          => 'childrenRoles',
                    'data'          => $roles,
                    'size'          => Select2::SMALL,
                    'options'       => [
                        'placeholder' => Yii::t('user', 'Select role...'),
                        'multiple'    => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]
            )->label(false);
            ?>

            <h4><?= Yii::t('user', 'Permissions') ?></h4>

            <?= $form->field($itemsForm, 'permissions')->widget(
                Select2::class,
                [
                    'name'          => 'childrenPermissions',
                    'data'          => $permissions,
                    'size'          => Select2::SMALL,
                    'options'       => [
                        'placeholder' => Yii::t('user', 'Select permission...'),
                        'multiple'    => true
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]
            )->label(false);
            ?>
        </div>
        <div class="card-footer text-right">
            <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

        <?php ActiveForm::end(); ?>

    <?php else : ?>
        <div class="callout callout-warning">
            <?= Yii::t('user', 'Administrator has full access rules') ?>
        </div>
    <?php endif; ?>
</div>
