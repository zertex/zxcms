<?php

use backend\forms\rbac\RbacCreateRoleForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model RbacCreateRoleForm */

$this->title = Yii::t('user', 'Create Role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-rbac-role-create">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card">
        <div class="card-body">
            <?= $form->field($model, 'name')->textInput(['maxLength' => true]) ?>
            <?= $form->field($model, 'description')->textarea() ?>
            <?= $form->field($model, 'rule_name')->textInput(['maxLength' => true]) ?>
            <?= $form->field($model, 'data')->textarea() ?>
        </div>
        <div class="card-footer text-right">
            <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
