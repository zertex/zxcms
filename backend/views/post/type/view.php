<?php

use core\entities\post\PostType;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $type PostType */

$this->title = $type->plural;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title
?>
<div class="type-view">

    <p>
        <?= Html::a(Yii::t('post', 'Types'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $type->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $type->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $type,
                'attributes' => [
                    'id',
                    'name',
                    'singular',
                    'plural',
                ],
            ]) ?>
        </div>
    </div>

</div>
