<?php

use core\entities\post\PostType;
use core\forms\post\PostTagSingleForm;

/* @var $this yii\web\View */
/* @var $model PostTagSingleForm */
/* @var $type PostType */

$title = Yii::t('post', 'Create Tag');
$this->title = $type->plural . ' > ' . $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
