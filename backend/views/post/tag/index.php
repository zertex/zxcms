<?php

use core\entities\post\PostTag;
use core\entities\post\PostType;
use core\forms\post\search\PostTagSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel PostTagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $type PostType */

$title = Yii::t('post', 'Tags');
$this->title = $type->plural . ' > ' . $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="blog-tags-index">

    <p>
        <?= Html::a(Yii::t('post', 'Create Tag'), ['create', 'tid' => $type->id], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'value' => function (PostTag $model) {
                            return Html::a(Html::encode($model->name), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'slug',
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
