<?php

use core\entities\post\PostTag;
use core\entities\post\PostType;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $tag PostTag */
/* @var $type PostType */

$title = $tag->name;
$this->title = $type->plural . ' > ' . $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
?>
<div class="user-view">

    <p>
        <?= Html::a(Yii::t('post', 'Tags'), ['index', 'tid' => $type->id], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $tag->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $tag->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $tag,
                'attributes' => [
                    'name',
                    'slug',
                ],
            ]) ?>
        </div>
    </div>
</div>
