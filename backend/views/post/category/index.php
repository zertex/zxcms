<?php

use core\entities\post\PostCategory;
use core\entities\post\PostType;
use core\forms\post\search\PostCategorySearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel PostCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $type PostType */

$title = Yii::t('post', 'Categories');
$this->title = $type->plural .' > '. $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="user-index">

    <p>
        <?= Html::a(Yii::t('post', 'Create Category'), ['create', 'tid' => $type->id], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'sort',
                        'options' => ['style' => 'width: 100px;'],
                    ],
                    [
                        'attribute' => 'name',
                        'value' => function (PostCategory $model) {
                            return Html::a(Html::encode($model->name), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'slug',
                    ],
                    [
                        'attribute' => 'title',
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
