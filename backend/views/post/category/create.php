<?php

use core\entities\post\PostType;
use core\forms\post\PostCategoryForm;

/* @var $this yii\web\View */
/* @var $model PostCategoryForm */
/* @var $type PostType */

$title = Yii::t('post', 'Create Category');
$this->title = $type->plural . ' > ' . $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
?>
<div class="category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
