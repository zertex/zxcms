<?php

use core\forms\post\PostCategoryForm;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model PostCategoryForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-header with-border"><?= Yii::t('post', 'Common') ?></div>
        <div class="box-body">

            <div class="row">
                <div class="col-md-2">
	                <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-5">
	                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-5">
	                <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                </div>
            </div>


            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->widget(CKEditor::class) ?>

        </div>
    </div>

    <div class="box box-default">
        <div class="box-header with-border"><?= Yii::t('post', 'SEO') ?></div>
        <div class="box-body">
            <?= $form->field($model->meta, 'title')->textInput() ?>
            <?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
            <?= $form->field($model->meta, 'keywords')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('buttons','Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
