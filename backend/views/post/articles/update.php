<?php

use core\entities\post\Post;
use core\entities\post\PostType;
use core\forms\post\PostForm;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $post Post */
/* @var $model PostForm */
/* @var $type PostType */

$title = Yii::t('post', 'Update Post: {name}', ['name' => StringHelper::truncateWords($post->title, 6, '...')]);
$this->title = $type->plural . ' > ' . $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', $type->plural), 'url' => ['index', 'tid' => $type->id]];
$this->params['breadcrumbs'][] = ['label' => $type->singular, 'url' => ['view', 'id' => $post->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="post-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
