<?php

use core\entities\post\Post;
use core\entities\post\PostType;
use core\forms\post\search\PostSearch;
use core\helpers\PostHelper;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $type PostType */

$title = Yii::t('post', 'All Posts');
$this->title = $type->plural .' > '. $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="blog-post-index">

    <p>
        <?= Html::a(Yii::t('post', 'Create Post'), ['create', 'tid' => $type->id], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    /*[
                        'value' => function (Post $model) {
                            return $model->image ? Html::img($model->getThumbFileUrl('image', 'admin')) : null;
                        },
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'width: 100px'],
                    ],*/
                    [
                        'attribute' => 'id',
                        'options' => ['style' => 'width: 40px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                    [
                        'attribute' => 'title',
                        'value' => function (Post $model) {
                            return Html::a(Html::encode($model->title), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'category_id',
                        'filter' => $searchModel->categoriesList(),
                        'value' => 'category.name',
                    ],
	                [
		                'attribute' => 'published_at',
		                'format' => ['datetime', 'php:d.m.Y H:i'],
		                'options' => ['style' => 'width: 60px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
                    [
                        'attribute' => 'status',
                        'filter' => $searchModel->statusList(),
                        'value' => function (Post $model) {
                            return PostHelper::statusLabel($model->status);
                        },
                        'format' => 'raw',
                        'options' => ['style' => 'width: 120px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
	                [
		                'class' => ActionColumn::class,
		                'urlCreator' => function($action, Post $model, $key, $index, $column) {
			                $params = is_array($key) ? $key : ['id' => (string) $key, 'tid' => $model->type_id];
			                $params[0] = $column->controller ? $column->controller . '/' . $action : $action;
			                return Url::toRoute($params);
		                },
		                'options' => ['style' => 'width: 100px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
                ],
            ]); ?>
        </div>
    </div>
</div>
