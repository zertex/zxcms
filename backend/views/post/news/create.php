<?php

use core\entities\post\PostType;
use core\forms\post\PostForm;

/* @var $this yii\web\View */
/* @var $model PostForm */
/* @var $type PostType */

$title = Yii::t('post', 'Create Post');
$this->title = $type->plural . ' > ' . $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', $type->plural), 'url' => ['index', 'tid' => $type->id]];
$this->params['breadcrumbs'][] = $title;
?>
<div class="post-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
