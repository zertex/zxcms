<?php

use core\forms\post\PostForm;
use kartik\file\FileInput;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model PostForm */
/* @var $form yii\widgets\ActiveForm */


$fetchUrl = Url::to( [ 'tag-search' ] );

if (isset($model->_post)) {
	$tagsJson = Json::encode( Json::encode( ArrayHelper::map( $model->_post->tags, 'id', 'name' ) ) );

	$js = <<<JS
	var arr = {$tagsJson};
	$.each(JSON.parse(arr), function( key, value ) {
  		$("#posttagform-new_tags").append("<option value='"+value+"' selected>"+value+"</option>");
	});
	$('#posttagform-new_tags').trigger('change');
JS;
	$this->registerJs( $js, $this::POS_READY );
}
?>

<div class="post-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype'=>'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border"><?= Yii::t('post', 'Common') ?></div>
                <div class="box-body">
                    <?= $form->field($model, 'category_id')->dropDownList($model->categoriesList($model->type_id), ['prompt' => '']) ?>
                    <?= $form->field($model, 'published_at')->widget(DateTimePicker::class, [
	                    'options' => [],
	                    'pluginOptions' => [
		                    'autoclose' => true,
		                    'format' => 'dd.mm.yyyy hh:ii:ss',
	                    ]
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-default">
                <div class="box-header with-border"><?= Yii::t('post', 'Tags') ?></div>
                <div class="box-body">
	                <?= $form->field($model->tags, 'new_tags')->widget(Select2::class, [
		                'options' => [
		                	'placeholder' => Yii::t('post','Set tags...'),
			                'multiple' => true,
		                ],
		                'pluginOptions' => [
			                'tags' => true,
			                'tokenSeparators' => [',', ' '],
			                'maximumInputLength' => 12,
			                'ajax' => [
				                'url' => $fetchUrl,
				                'dataType' => 'json',
				                'data' => new JsExpression('function(params) { return {q:params.term, type_id:' . $model->type_id . '}; }')
			                ],
			                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
			                'templateResult' => new JsExpression('function(tag) { return tag.text; }'),
			                'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
		                ],
	                ])->label(false); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="box box-default">
        <div class="box-body">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 5]) ?>
            <?= $form->field($model, 'content')->widget(CKEditor::class) ?>
        </div>
    </div>

	<div class="box box-default">
		<div class="box-body">

			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#field_image"><?= Yii::t('post', 'Image') ?></a></li>
				<li><a data-toggle="tab" href="#field_video"><?= Yii::t('post', 'Video') ?></a></li>
			</ul>

			<div class="tab-content">
				<div id="field_image" class="tab-pane fade in active" style="padding-top: 20px;">
					<?= $form->field($model, 'image')->label(false)->widget(FileInput::class, [
						'options' => [
							'accept' => 'image/*',
						],
						'pluginOptions' => [
							'showUpload' => false,
						],
					]) ?>

					<?php if (isset($model->_post) && $model->_post->image): ?>
						<?= Html::img($model->_post->getThumbFileUrl('image', 'thumb_gallery_view'), [
							'class' => 'thumbnail',
							'width' => 300,
						]) ?>

						<?= $form->field($model, 'reset_image')->checkbox() ?>
					<?php endif; ?>
				</div>
				<div id="field_video" class="tab-pane fade" style="padding-top: 20px;">
					<?= $form->field($model, 'video')->textInput()->label(Yii::t('post', 'YouTube URL'))->hint(Yii::t('post', 'If a video link is specified, the image will be used as a preview image')) ?>
				</div>
			</div>

		</div>
	</div>

    <div class="box box-default">
        <div class="box-header with-border"><?= Yii::t('post', 'SEO') ?></div>
        <div class="box-body">
            <?= $form->field($model->meta, 'title')->textInput() ?>
            <?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
            <?= $form->field($model->meta, 'keywords')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
