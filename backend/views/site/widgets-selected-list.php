<?php

/**
 * @var $this \yii\web\View
 */

use core\helpers\UserHelper;

$widgets = UserHelper::getSetting('widgetsLayout', []);
$i       = 0;
?>

<?php foreach ($widgets as $widget) : ?>

    <a href="#" onClick="removeWidget(<?= $i ?>)"><?= $widget['name'] ?></a><br>

    <?php
    $i++;
endforeach;
?>
