<?php

use core\entities\Search;
use core\forms\SearchForm;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $provider yii\data\ActiveDataProvider */
/* @var $form SearchForm */

$title = Yii::t('main', 'Search results');
$this->title = $title . ' > ' . Html::encode($form->query);
$this->params['breadcrumbs'][] = $title;
?>
<div class="blog-post-index">

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $provider,
                'showHeader' => false,
                'options' => [
                	'style' => 'word-wrap:break-word;',
                ],
                'tableOptions' => [
					'class' => 'table',
				],
                'columns' => [
                    [
                        'attribute' => 'title',
                        'value' => function (Search $model) {
                            return Html::a(Html::tag('h3', $model->title), [$model->url]) . StringHelper::truncateWords(strip_tags($model->content), 60, '...');
                        },
                        'format' => 'raw',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
