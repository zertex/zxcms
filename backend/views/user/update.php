<?php

/* @var $this yii\web\View */
/* @var $model UserForm */
/* @var $user User */


use core\entities\user\User;
use core\forms\user\UserForm;

$this->title = Yii::t('user', 'Update User: {user}', ['user' => $user->username]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->username, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="user-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
