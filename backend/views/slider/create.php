<?php

use core\forms\SliderForm;

/* @var $this yii\web\View */
/* @var $model SliderForm */

$this->title = Yii::t('slider', 'Create Slide');
$this->params['breadcrumbs'][] = ['label' => Yii::t('slider', 'Slider'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-create">

	<?= $this->render('_form', [
		'model' => $model,
		'slider' => null,
	]) ?>

</div>
