<?php

use core\entities\Slider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model Slider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('slider', 'Slider'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">

    <p>
	    <?= Html::a(Yii::t('slider', 'Slider'), ['index'], ['class' => 'btn btn-outline-primary btn-sm']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>


	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<?php if ($model->image): ?>
						<div class="thumbnail">
							<?= Html::img($model->getThumbFileUrl('image', 'thumb')) ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="card">
				<div class="card-body">
					<?= DetailView::widget([
						'model' => $model,
						'attributes' => [
							'id',
							'title',
							'tagline',
							'url',
						],
					]) ?>
				</div>
			</div>
		</div>
	</div>
</div>
