<?php

use core\components\adminlte3\widgets\Menu;
use yii\helpers\ArrayHelper;
use yii\web\View;

//use app\modules\customers\entities\CustomerBase;

/**
 * @var $this View
 */

//$image = Yii::$app->imager;
$css = <<<CSS
    .brand-link .brand-image {
        float: none !important;
    }

    .sidebar-dark-primary .nav-sidebar.nav-legacy > .nav-item > .nav-link.active, .sidebar-light-primary .nav-sidebar.nav-legacy > .nav-item > .nav-link.active {
        border-color: #9ed6db;
    }
CSS;
$this->registerCss($css);
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<!-- Brand Logo -->
	<a href="<?= Yii::$app->homeUrl ?>" class="brand-link">
        <img src="<?= Yii::$app->params['staticHostInfo'] ?>/images/ego_white.svg" class="brand-image " style="opacity: .8">
	</a>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user panel (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<!-- <img src="< ?= $image->avatar(Yii::$app->user->id) ?>" class="img-circle elevation-2" alt="User Image"> -->
                <img src="<?= Yii::$app->avatar->show(Yii::$app->user->identity->user->username . '_' . Yii::$app->user->id, null, null, Yii::$app->user->identity->user->user_pic) ?>?<?= rand(10, 5000) ?>" class="img-circle elevation-2" alt="<?= Yii::$app->user->identity->user->username ?>">
			</div>
			<div class="info">
				<a href="#" class="d-block"><?= Yii::$app->user->identity->user->username ?></a>
			</div>
		</div>

		<!-- Sidebar Menu -->
		<nav class="mt-2">
            <?php
            /*$basesMenu = [
                'label'   => Yii::t('main', 'Customers'),
                'icon'    => 'fas fa-star',
                'url'     => ['/customers/customer/index'],
                'active'  => Yii::$app->controller->getUniqueId() == 'customers/customer',
                'visible' => Yii::$app->user->can('admin') ||
                             Yii::$app->user->can('CustomerManagement')
            ];
            $bases     = CustomerBase::find()->all();
            if (count($bases) > 1) {
                $baseItems = array_map(function ($base) {
                    return [
                        'label'  => $base->name,
                        'icon'   => 'fas fa-caret-right',
                        'url'    => ['/customers/customer/index', 'base_id' => $base->id],
                        'active' => Yii::$app->controller->getUniqueId() == 'customers/customer' &&
                                    Yii::$app->getRequest()->get('base_id') == $base->id
                    ];
                }, $bases);
                $basesMenu = [
                    'label'   => Yii::t('main', 'Customers'),
                    'icon'    => 'fas fa-star',
                    'items'   => $baseItems,
                    'visible' => Yii::$app->user->can('admin') ||
                                 Yii::$app->user->can('CustomerManagement')
                ];
            }*/
            ?>

			<?= Menu::widget([
                'items'   => ArrayHelper::merge([
                    //['label' => Yii::t('main', 'Menu'), 'options' => ['class' => 'header']],
                    [
                        'label'  => Yii::t('dashboard', 'Dashboard'),
                        'icon'   => 'fas fa-tachometer-alt',
                        'url'    => ['/site/index'],
                        'active' => $this->context->id == '/site/index'
                    ],
                    [
                        'label'   => Yii::t('user', 'Users'),
                        'icon'    => 'fas fa-user',
                        'url'     => ['/user/index'],
                        'active'  => $this->context->id == 'user',
                        'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('UserManagement'),
                    ],
                    [
                        'label'   => Yii::t('user', 'Access Rules'),
                        'icon'    => 'fas fa-lock',
                        'items'   => [
                            [
                                'label'  => Yii::t('user', 'Roles'),
                                'icon'   => 'fas fa-caret-right',
                                'url'    => ['/role/index'],
                                'active' => Yii::$app->controller->getUniqueId() == 'role'
                            ],
                            [
                                'label'  => Yii::t('user', 'Permissions'),
                                'icon'   => 'fas fa-caret-right',
                                'url'    => ['/permission/index'],
                                'active' => Yii::$app->controller->getUniqueId() == 'permission'
                            ],
                        ],
                        'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('UserManagement')
                    ],
                    [
                        'label'   => Yii::t('menu', 'Menu'),
                        'icon'    => 'fas fa-bars',
                        'url'     => ['/menu/index'],
                        'active'  => $this->context->id == 'menu',
                        'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('MenuManagement'),
                    ],
                    [
                        'label'   => Yii::t('main', 'Modules'),
                        'icon'    => 'fas fa-cubes',
                        'items'   => [
                            [
                                'label'  => Yii::t('main', 'Modules'),
                                'icon'   => 'fas fa-caret-right',
                                'url'    => ['/module/list'],
                                'active' => Yii::$app->controller->action->getUniqueId() == 'module/list'
                            ],
                            [
                                'label'  => Yii::t('main', 'Find modules'),
                                'icon'   => 'fas fa-caret-right',
                                'url'    => ['/module/search'],
                                'active' => Yii::$app->controller->action->getUniqueId() == 'module/search'
                            ],
                        ],
                        'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('ModuleManagement')
                    ],
                    [
                        'label'   => Yii::t('slider', 'Slider'),
                        'icon'    => 'fa fa-exchange-alt',
                        'url'     => ['/slider/index'],
                        'active'  => $this->context->id == 'slider',
                        'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('SliderManagement'),
                    ],
                    [
                        'label'   => Yii::t('main', 'Settings'),
                        'icon'    => 'fas fa-cog',
                        'items'   => [
                            [
                                'label'  => Yii::t('main', 'Settings List'),
                                'icon'   => 'fas fa-caret-right',
                                'url'    => ['/settings/list/index'],
                                'active' => Yii::$app->controller->getUniqueId() == 'settings/list'
                            ],
                            //['label' => Yii::t('main', 'Find modules'), 'icon' => 'caret-right', 'url' => ['/module/search'], 'active' => \Yii::$app->controller->action->getUniqueId() == 'module/search'],
                        ],
                        'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('SettingsManagement')
                    ],






                    //[
                    //    'label' => Yii::t('builder', 'Databases'),
                    //    'icon'  => 'fas fa-database',
                    //    'items' => \app\modules\base_builder\helpers\BuilderHelper::getBasesMenu()
                    //],

                    //$basesMenu,
                    /*[
                        'label'   => Yii::t('customers', 'Customer bases'),
                        'icon'    => 'fas fa-users',
                        'url'     => ['/customers/customer-base/index'],
                        'active'  => Yii::$app->controller->getUniqueId() == 'customers/customer-base',
                        'visible' => Yii::$app->user->can('admin') ||
                                     Yii::$app->user->can('CustomerManagement')
                    ],*/

                    /*[
                        'label'   => Yii::t('deals', 'Deals'),
                        'icon'    => 'fas fa-plug',
                        'url'     => ['/deals/deal/index'],
                        'active'  => Yii::$app->controller->getUniqueId() == 'deals/deal',
                        'visible' => Yii::$app->user->can('admin') ||
                                     Yii::$app->user->can('DealManagement')
                    ],*/

                    /*[
                        'label'   => Yii::t('calendar', 'Calendar'),
                        'icon'    => 'fas fa-calendar-alt',
                        'url'     => ['/calendar/events'],
                        'active'  => Yii::$app->controller->getUniqueId() == 'calendar/events',
                        'visible' => Yii::$app->user->can('admin') ||
                                     Yii::$app->user->can('CalendarManagement')
                    ],*/

                    /*[
                        'label'   => Yii::t('board', 'ToDo'),
                        'icon'    => 'fas fa-check-square',
                        'url'     => ['/board/cards'],
                        'active'  => Yii::$app->controller->getUniqueId() == 'board/cards',
                        'visible' => Yii::$app->user->can('admin') ||
                                     Yii::$app->user->can('BoardManagement')
                    ],*/

                    /*[
                        'label' => Yii::t('subs', 'Mail Subs'),
                        'icon'  => 'fas fa-envelope',
                        'items' => [
                            [
                                'label'   => Yii::t('subs', 'Subs'),
                                'icon'    => 'fas fa-caret-right',
                                'url'     => ['/sub/index'],
                                'active'  => Yii::$app->controller->getUniqueId() == 'sub',
                                'visible' => Yii::$app->user->can('admin') ||
                                             Yii::$app->user->can('SubManagement')
                            ],
                            [
                                'label'   => Yii::t('mail-template', 'Mail Templates'),
                                'icon'    => 'fas fa-caret-right',
                                'url'     => ['/mail-template/index'],
                                'active'  => Yii::$app->controller->getUniqueId() == 'mail-template',
                                'visible' => Yii::$app->user->can('admin') ||
                                             Yii::$app->user->can('MailTemplateManagement')
                            ],
                            [
                                'label'   => Yii::t('subs', 'Signs'),
                                'icon'    => 'fas fa-caret-right',
                                'url'     => ['/sign/index'],
                                'active'  => Yii::$app->controller->getUniqueId() == 'sign',
                                'visible' => Yii::$app->user->can('admin') ||
                                             Yii::$app->user->can('SignManagement')
                            ],
                            [
                                'label'   => Yii::t('subs', 'DKIM'),
                                'icon'    => 'fas fa-caret-right',
                                'url'     => ['/dkim/index'],
                                'active'  => Yii::$app->controller->getUniqueId() == 'dkim',
                                'visible' => Yii::$app->user->can('admin') ||
                                             Yii::$app->user->can('DkimManagement')
                            ],
                        ]
                    ],*/

                    /*[
                        'label'   => Yii::t('sms', 'SMS Subs'),
                        'icon'    => 'fas fa-mobile',
                        'url'     => ['/sms/sms/index'],
                        'active'  => Yii::$app->controller->getUniqueId() == 'sms/sms',
                        'visible' => Yii::$app->user->can('admin') ||
                                     Yii::$app->user->can('SmsManagement')
                    ],*/

                    /*[
                        'label' => Yii::t('stat', 'Statistics'),
                        'icon'  => 'fas fa-chart-bar',
                        'items' => [
                            [
                                'label'   => Yii::t('stat', 'Deals'),
                                'icon'    => 'fas fa-caret-right',
                                'url'     => ['/stat/stat/deals'],
                                'active'  => Yii::$app->controller->action->getUniqueId() == 'stat/stat/deals',
                                'visible' => Yii::$app->user->can('admin') ||
                                             Yii::$app->user->can('StatDealsStatistics')
                            ],
                            [
                                'label'   => Yii::t('stat', 'My Deals'),
                                'icon'    => 'fas fa-caret-right',
                                'url'     => ['/stat/stat/my-deals'],
                                'active'  => Yii::$app->controller->action->getUniqueId() == 'stat/stat/my-deals',
                                'visible' => Yii::$app->user->can('admin') ||
                                             Yii::$app->user->can('StatMyDealsStatistics')
                            ],
                        ]
                    ],*/

                    /*[
                        'label'   => 'API',
                        'icon'    => 'fas fa-cogs',
                        'url'     => ['/site/api'],
                        'active'  => Yii::$app->controller->action->getUniqueId() == 'site/api',
                        'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('Franchise')
                    ],*/
                ], (isset(Yii::$app->params['adminMenu']) && is_array(Yii::$app->params['adminMenu']) ? Yii::$app->params['adminMenu'] : [])),
			]) ?>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>