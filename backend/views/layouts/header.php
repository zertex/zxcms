<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use kartik\select2\Select2;

/* @var $this View */
/* @var $content string */
?>

<nav class="main-header navbar navbar-expand navbar-white navbar-light">
	<!-- Left navbar links -->
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
		</li>
		<!-- <li class="nav-item d-none d-sm-inline-block">
			<?= Html::a(Yii::t('links', 'Plans'), ['/links/link/plans'], [
				'class' => 'nav-link',
			]) ?>
		</li> -->
	</ul>

	<!-- Right navbar links -->
	<ul class="navbar-nav ml-auto">

		<!-- < ?= NotifyContactWidget::widget() ?>
		< ?= NotifyOrderWidget::widget() ?>
		< ?= NotifyReviewWidget::widget() ?> -->

		<!-- User Menu -->
		<li class="nav-item dropdown">
			<a class="nav-link" data-toggle="dropdown" href="#">
				<i class="far fa-user"></i>
			</a>
			<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

				<?= Html::a(
					'<i class="fas fa-user fa-fw"></i> ' . Yii::t('user', 'Profile'),
					['/user/profile'],
					['class' => 'dropdown-item']
				) ?>
				<div class="dropdown-divider"></div>
				<?= Html::a(
					'<i class="fas fa-sign-out-alt fa-fw"></i> ' . Yii::t('user', 'Sign out'),
					['/auth/auth/logout'],
					['data-method' => 'post', 'class' => 'dropdown-item']
				) ?>

			</div>
		</li>

	</ul>
</nav>
