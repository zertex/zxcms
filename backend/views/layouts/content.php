<?php

use core\components\adminlte3\widgets\Breadcrumbs;
use core\components\adminlte3\widgets\ToastrNotification;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Inflector;

/**
 * @var $this View
 * @var $content string
 */

?>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

					<?php if (isset($this->blocks['content-header'])) : ?>
						<h4 class="m-0 text-dark"><?= $this->blocks['content-header'] ?></h4>
					<?php else: ?>
						<h4 class="m-0 text-dark">
							<?php
							if ($this->title !== null) {
								echo Html::encode($this->title);
							} else {
								echo Inflector::camel2words(
									Inflector::id2camel($this->context->module->id)
								);
								echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
							} ?>
						</h4>
					<?php endif; ?>

				</div><!-- /.col -->
				<div class="col-sm-6">

					<?=
					Breadcrumbs::widget([
							'links' => $this->params['breadcrumbs'] ?? [],
					]) ?>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<div class="content">
		<div class="container-fluid">

			<section class="content">
				<?= ToastrNotification::widget() ?>
				<?= $content ?>
			</section>

		</div>
		<!-- /.container-fluid -->
	</div>
	<!-- /.content -->
</div>
