<?php

use core\components\adminlte3\AdminLteAsset;
use core\components\adminlte3\AdminLteICheckAsset;
use core\components\adminlte3\widgets\ToastrNotification;
use cp\assets\AdminLteDarkAsset;
use cp\assets\AdminLteLightAsset;
use cp\assets\AppAsset;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $content string */

AppAsset::register($this);
AdminLteAsset::register($this);

//if (Yii::$app->params['theme'] == \app\forms\SettingsForm::THEME_BLACK) {
    //AdminLteDarkAsset::register($this);
//}
//else {
    AdminLteLightAsset::register($this);
//}
//AdminLteICheckAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

    <?= ToastrNotification::widget() ?>
    <div class="text-center mt-4">
        <img style="height: 70px;" src="<?= Yii::$app->params['staticHostInfo'] ?>/images/ego_black.png">
        <!-- <h4 style="color: #888;">International franchise program</h4> -->
    </div>
    <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
