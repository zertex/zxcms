<?php
/**
 * Created by Error202
 * Date: 17.08.2018
 */

use yii\helpers\Html;
use kartik\dialog\Dialog;
use yii\web\View;

/**
 * @var $this View
 * @var $modules array
 * @var $message string
 */

$this->title                   = Yii::t('main', 'Available Modules');
$this->params['breadcrumbs'][] = $this->title;

$messageInstall = Yii::t('main', 'To install the module, use the following command in the SSH console:');
$js = <<<JS
    $(".install-button").on('click', function () {
        var name = $(this).data('name');
        krajeeDialog.alert('{$messageInstall}' + "\\n" + '<pre><code>php yii module/install ' + name + '</code></pre>'); 
    });
JS;
$this->registerJs($js, $this::POS_READY);
?>

<?php if ($message) : ?>
    <div><?= $message ?></div>
<?php endif; ?>

<?=  Dialog::widget([
        'options' => [],
    ]);
?>

<div class="row">

    <?php foreach ($modules as $module) : ?>

        <?php
        $color = '#00aced';

        $module_name        = $module['locale_name'];
        $module_description = $module['locale_description'];
        ?>

        <div class="col-md-4">

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= $module_name ?></h3>
                </div>
                <div class="box-body">
                    <?= $module_description ?>
                </div>
                <div class="box-footer" style="text-align: right">
                    <?php if (Yii::$app->moduleManager->isExists($module['name'])) : ?>
                        <?= Html::button(Yii::t('main', 'Installed'), [
                            'class' => 'btn btn-default btn-sm disabled',
                        ]) ?>
                    <?php else : ?>
                        <?= Html::button(Yii::t('main', 'Install'), [
                            'class' => 'btn btn-success btn-sm install-button',
                            'data-name' => $module['name'],
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
