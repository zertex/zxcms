<?php
/**
 * Created by Error202
 * Date: 17.08.2018
 */

use core\entities\ModuleRecord;
use yii\helpers\Html;
use core\helpers\ModuleHelper;
use kartik\dialog\Dialog;
use yii\web\View;

/**
 * @var $this View
 * @var $modules ModuleRecord[]
 */

$this->title                   = Yii::t('main', 'Modules');
$this->params['breadcrumbs'][] = $this->title;

$message = Yii::t('main', 'To remove the module, use the following command in the SSH console:');
$js = <<<JS
    $(".delete-button").on('click', function () {
        var name = $(this).data('name');
        krajeeDialog.alert('{$message}' + "\\n" + '<pre><code>php yii module/remove ' + name + '</code></pre>'); 
    });
JS;
$this->registerJs($js, $this::POS_READY);
?>

<?=  Dialog::widget([
    'options' => [],
]);
?>

<div class="row">

    <?php foreach ($modules as $module) : ?>

        <?php
        $color = $module->isEnabled() ? '#00aced' : '#cccccc';
        if ($module->isDisabled()) {
            ModuleHelper::addModuleAdminTranslation($module->name);
        }

        $module_name        = in_array($module->name, array_keys(Yii::$app->getI18n()->translations)) ? Yii::t($module->name, $module->name) : $module->name;
        $module_description = in_array($module->name, array_keys(Yii::$app->getI18n()->translations)) ? Yii::t($module->name, $module->description) : $module->description;
        ?>

        <div class="col-md-4">

            <div class="card">
                <div class="card-header">
                    <h3 class="box-title"><i class="fa fa-power-off" aria-hidden="true"
                                             style="color: <?= $color ?>"></i> <?= $module_name ?></h3>
                </div>
                <div class="card-body">
                    <?= $module_description ?>
                </div>
                <div class="card-footer" style="text-align: right">
                    <?php if (!$module->isSystem()) : ?>
                        <?php if ($module->isEnabled()) : ?>
                            <?= Html::a(Yii::t('main', 'Disable'), ['module/disable', 'id' => $module->id], [
                                'class' => 'btn btn-default btn-sm',
                                'data'  => [
                                    'method' => 'post',
                                ],
                            ]) ?>
                        <?php else : ?>
                            <?= Html::a(Yii::t('main', 'Enable'), ['module/enable', 'id' => $module->id], [
                                'class' => 'btn btn-default btn-sm',
                                'data'  => [
                                    'method' => 'post',
                                ],
                            ]) ?>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if (!$module->isSystem()) : ?>
                        <?= Html::button(Yii::t('buttons', 'Delete'), [
                            'class' => 'btn btn-danger btn-sm delete-button',
                            'data-name' => $module->name,
                        ]) ?>
                    <?php else : ?>
                        <?= Html::button(Yii::t('buttons', 'Delete'), [
                            'class' => 'btn btn-danger btn-sm disabled',
                            'data-name' => $module->name,
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
