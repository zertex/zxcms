<?php

use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('user', 'Permissions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-rbac-permission-index">
    <div class="card">
        <div class="card-header">
            <?= Html::a(Yii::t('user', 'Create Permission'), ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('user', 'Permission Name'),
                    ],
                    [
                        'attribute' => 'description',
                        'label' => Yii::t('user', 'Permission Description'),
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
