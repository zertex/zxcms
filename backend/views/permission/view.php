<?php

use yii\helpers\Html;
use yii\rbac\Role;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model Role */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-rbac-permission-view">

    <p>
        <?= Html::a(Yii::t('user', 'Permissions'), ['index'], ['class' => 'btn btn-outline-primary btn-sm']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $model->name], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $model->name], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="card">
        <div class="card-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('user', 'Role Name'),
                    ],
                    [
                        'attribute' => 'description',
                        'label' => Yii::t('user', 'Role Description'),
                    ],
                    [
                        'attribute' => 'ruleName',
                        'label' => Yii::t('user', 'Rule Name'),
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
