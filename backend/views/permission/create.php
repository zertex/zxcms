<?php

/* @var $this yii\web\View */
/* @var $model RbacCreatePermissionForm */

use backend\forms\rbac\RbacCreatePermissionForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('user', 'Create Permission');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-rbac-permission-create">

    <?php $form = ActiveForm::begin(); ?>


    <div class="card">
        <div class="card-body">
            <?= $form->field($model, 'name')->textInput(['maxLength' => true]) ?>
            <?= $form->field($model, 'description')->textarea() ?>
            <?= $form->field($model, 'rule_name')->textInput(['maxLength' => true]) ?>
            <?= $form->field($model, 'data')->textarea() ?>
        </div>
        <card class="card-footer text-right">
            <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
        </card>
    </div>

    <?php ActiveForm::end(); ?>

</div>