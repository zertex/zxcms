<?php

use core\forms\SettingsForm;

/**
 * @var yii\web\View $this
 * @var SettingsForm $model
 */

$this->title                   = Yii::t('main', 'Create Setting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-create">

    <?=
    $this->render(
        '_form',
        [
            'model' => $model,
        ]
    ) ?>

</div>