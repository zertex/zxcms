<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\web\View;
use yii\widgets\DetailView;
use core\entities\Settings;

/**
 * @var $this View
 * @var $setting Settings
 * @var $language string
 */

echo DetailView::widget([
    'model'      => $setting,
    'attributes' => [
        [
            'label' => Yii::t('main', 'Value'),
            'value' => function (Settings $entity) use ($language) {
                return $entity->findTranslation($language)->value;
            }
        ],
    ],
]);
