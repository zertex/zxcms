<?php

use core\forms\SettingsForm;
use core\entities\Settings;

/**
 * @var yii\web\View $this
 * @var SettingsForm $model
 * @var Settings $settings
 */

$this->title = Yii::t('main', 'Updating Setting') . ' ' . $model->section . '.' . $model->key;
$this->params['breadcrumbs'][] = ['label' => Yii::t('main', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => $model->section . '.' . $model->key,
    'url'   => ['view', 'section' => $settings->section, 'key' => $settings->key]
];
$this->params['breadcrumbs'][] = Yii::t('main', 'Editing');
?>
<div class="setting-update">

    <?=
    $this->render(
        '_form',
        [
            'model' => $model,
        ]
    ) ?>

</div>