<?php
use common\widgets\Alert;
use core\forms\auth\LoginForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model LoginForm */

$this->title = Yii::t('main', 'Sign in');

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-logo">
        <?php if (file_exists(Yii::getAlias('@staticRoot/cache/favicon/favicon-96x96.png'))) : ?>
            <img style="filter: drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.2));" src="<?= Yii::getAlias('@static/cache/favicon/favicon-96x96.png') ?>" alt="Zertex CMS">
            <br>
        <?php endif; ?>
        <a style="filter: drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.2));" href="#"><b>Zertex</b>CMS</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body" style="filter: drop-shadow(0px 0px 5px rgba(0, 0, 0, 0.2));">
        <p class="login-box-msg"><?= Yii::t('main', 'Sign in to start your session') ?></p>

        <?= Alert::widget() ?>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableClientValidation' => false
        ]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton(Yii::t('buttons', 'Sign in'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
