<?php
/**
 * Created by Error202
 * Date: 26.10.2017
 */

namespace backend\assets;

use yii\web\AssetBundle;

class AdminLteSkinAsset extends AssetBundle
{
    public $css = [
        'css/skin.css',
    ];
}
