<?php
/**
 * Created by Error202
 * Date: 04.06.2018
 */

namespace backend\forms;

use core\entities\Settings;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SettingsSearch extends Settings
{
    public ?int $id = null;
    public ?int $type = null;
    public ?string $section = null;
    public ?string $key = null;
    public ?string $value = null;
    public ?int $active = null;

    public function rules(): array
    {
        return [
            [['id'], 'integer'],
            [['active'], 'boolean'],
            [['type', 'section', 'key', 'value'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios(): array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query        = Settings::find();
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );
        if (!($this->load($params) && $this->validate())) {
            $query->andFilterWhere(
                [
                    'section' => $this->section,
                ]
            );
            return $dataProvider;
        }
        $query->andFilterWhere(
            [
                'id'      => $this->id,
                'active'  => $this->active,
                'section' => $this->section,
            ]
        );
        $query->andFilterWhere(['like', 'key', $this->key])
              ->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}
