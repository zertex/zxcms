<?php
/**
 * Created by Error202
 * Date: 17.08.2017
 */

namespace backend\forms\rbac;

use yii\base\Model;

class RbacUpdateChildren extends Model
{
    public ?array $roles = null;
    public ?array $permissions = null;

    public function rules(): array
    {
        return [
            [['roles', 'permissions'], 'each', 'rule' => ['string']],
        ];
    }
}
