<?php
/**
 * Created by Error202
 * Date: 15.08.2017
 */

namespace backend\forms\rbac;

use yii\rbac\Role;
use yii\base\Model;
use Yii;

class RbacEditRoleForm extends Model
{
    public ?string $name        = null;
    public ?string $description = null;
    public ?string $rule_name   = null;
    public ?string $data        = null;
    public ?int $created_at     = null;
    public ?int $updated_at     = null;

    public function __construct(Role $role, $config = [])
    {
        $this->name = $role->name;
        $this->description = $role->description;
        $this->rule_name = $role->ruleName;
        $this->data = $role->data;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name', 'description', 'rule_name'], 'trim'],

            [['description', 'data'], 'string'],
            [['rule_name'], 'string', 'max' => 64],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('user', 'Role Name'),
            'description' => Yii::t('user', 'Role Description'),
            'rule_name' => Yii::t('user', 'Rule Name'),
            'data' => Yii::t('user', 'Role Data'),
        ];
    }
}
