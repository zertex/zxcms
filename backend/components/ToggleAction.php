<?php
/**
 * Created by Error202
 * Date: 04.06.2018
 */

namespace backend\components;

use yii\base\Action;
use yii\base\ExitException;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\base\InvalidConfigException;
use yii\web\MethodNotAllowedHttpException;
use Yii;
use yii\web\Response;

class ToggleAction extends Action
{
    /**
     * @var string name of the model
     */
    public string $modelClass;
    /**
     * @var string model attribute
     */
    public string $attribute = 'active';
    /**
     * @var string scenario model
     */
    public ?string $scenario = null;
    /**
     * @var string|array additional condition for loading the model
     */
    public string|array $andWhere;
    /**
     * @var string|int|boolean|Expression what to set active models to
     */
    public string|int|bool|Expression $onValue = 1;
    /**
     * @var string|int|boolean what to set inactive models to
     */
    public string|int|bool $offValue = 0;
    /**
     * @var bool whether to set flash messages or not
     */
    public bool $setFlash = false;
    /**
     * @var string flash message on success
     */
    public string $flashSuccess = 'Model saved';
    /**
     * @var string flash message on error
     */
    public string $flashError = 'Error saving Model';
    /**
     * @var string|array URL to redirect to
     */
    public string|array $redirect;
    /**
     * @var string pk field name
     */
    public string $primaryKey = 'id';

    /**
     * @param $id
     * @return Response
     * @throws InvalidConfigException
     * @throws MethodNotAllowedHttpException
     * @throws ExitException
     */
    public function run($id)
    {
        if (!Yii::$app->request->getIsPost()) {
            throw new MethodNotAllowedHttpException();
        }
        $id     = (int)$id;
        $result = null;
        if (empty($this->modelClass) || !class_exists($this->modelClass)) {
            throw new InvalidConfigException("Model class doesn't exist");
        }
        /* @var $modelClass ActiveRecord */
        $modelClass = $this->modelClass;
        $attribute  = $this->attribute;
        $model      = $modelClass::find()->where([$this->primaryKey => $id]);
        if (!empty($this->andWhere)) {
            $model->andWhere($this->andWhere);
        }
        $model = $model->one();
        if (!is_null($this->scenario)) {
            $model->scenario = $this->scenario;
        }
        if (!$model->hasAttribute($this->attribute)) {
            throw new InvalidConfigException("Attribute doesn't exist");
        }
        if ($model->$attribute == $this->onValue) {
            $model->$attribute = $this->offValue;
        } elseif ($this->onValue instanceof Expression && $model->$attribute != $this->offValue) {
            $model->$attribute = $this->offValue;
        } else {
            $model->$attribute = $this->onValue;
        }
        if ($model->save()) {
            if ($this->setFlash) {
                Yii::$app->session->setFlash('success', $this->flashSuccess);
            }
        } else {
            if ($this->setFlash) {
                Yii::$app->session->setFlash('error', $this->flashError);
            }
        }
        if (Yii::$app->request->getIsAjax()) {
            Yii::$app->end();
        }
        /* @var $controller \yii\web\Controller */
        $controller = $this->controller;
        if (!empty($this->redirect)) {
            return $controller->redirect($this->redirect);
        }

        return $controller->redirect(Yii::$app->request->getReferrer());
    }
}
