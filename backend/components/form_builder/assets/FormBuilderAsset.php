<?php
/**
 * Created by Error202
 * Date: 11.07.2018
 */

namespace backend\components\form_builder\assets;

use yii\jui\JuiAsset;
use yii\web\AssetBundle;

class FormBuilderAsset extends AssetBundle
{
    public $sourcePath = '@backend/components/form_builder';

    public $js = [
        'js/form-builder.min.js',
        'js/form-render.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        JuiAsset::class,
    ];

    public $publishOptions = [
        'forceCopy' => YII_ENV_DEV ? true : false,
    ];
}
