<?php
/**
 * Created by Error202
 * Date: 11.07.2018
 */

namespace backend\components\menu\widgets;

use core\entities\menu\Menu;
use core\entities\menu\MenuItem;
use yii\base\Widget;

class MenuEditorWidget extends Widget
{
    public int $menu_id;

    public function run(): string
    {
        $menu  = Menu::findOne($this->menu_id);
        $items = $menu->items;

        return $this->render('menu', [
            'items' => $this->getMenu($items),
            'menu'  => $menu,
        ]);
    }

    /**
     * @param array $items
     * @param null $paren_id
     * @return array
     */
    private function getMenu(array $items, $paren_id = null): array
    {
        $array = [];
        foreach ($items as $item) {
            /* @var $item MenuItem */
            if ($item->parent_id != $paren_id) {
                continue;
            }
            $array[$item->id]['item'] = $item;
            if ($children = $item->children) {
                $array[$item->id]['children'] = $this->getMenu($children, $item->id);
            }
        }

        return $array;
    }
}
