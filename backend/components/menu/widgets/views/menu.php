<?php
/**
 * Created by Error202
 * Date: 11.07.2018
 */

use backend\components\menu\assets\MenuAsset;
use core\entities\menu\Menu;
use core\forms\menu\MenuItemForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var $this View
 * @var $items array
 * @var $menu Menu
 */

MenuAsset::register($this);

function menu_generate($items): string
{
	$html = '<ol class="dd-list">';
	foreach ($items as $item) {
		$menuItemForm = new MenuItemForm($item['item']);
		$html.=Yii::$app->getView()->render( '_item', [
			'item' => $item,
			'model' => $menuItemForm,
		] );
	}
	return $html . '</ol>';
}

$name_empty_error = Yii::t('menu', 'Name must be specified');
$item_save_url = ''; // delete this
$item_delete_url = Url::to(['menu/delete-menu-item']);
$confirm_delete_message = Yii::t('buttons', 'Are you sure you want to delete this item?');
$current_url = Url::to(['menu/index', 'id' => $menu->id]);
$js = <<<JS
	$(".item-delete-button").on('click', function(e) {
	  	e.preventDefault();
	  	if (confirm('{$confirm_delete_message}')) {
	  	    var id = $(this).data('id');
		    $.ajax({
	            method: "POST",
	            url: "{$item_delete_url}",
	            data: { id: id }
			})
	        .done(function( data ) {
	            if (data.result === 'success') {
	                document.location.href = '{$current_url}';
	            }
	        });
	  	}
	});
JS;
$this->registerJs($js, $this::POS_READY);

$url = Url::to(['/menu/save-menu-items']);
$redirect = Url::to(['menu/index', 'id' => $menu->id]);
?>

<div class="dd" id="nestable3">
	<?= menu_generate($items) ?>
</div>

	<div style="clear: both;"></div>
	<hr>

<?= Html::button(Yii::t('buttons', 'Save'), [
	'class' => 'btn btn-success float-right',
	'onclick' => new JsExpression('sendTree('.$menu->id.', "'.$url.'", "'.$redirect.'")'),
]) ?>

