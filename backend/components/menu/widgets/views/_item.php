<?php
/**
 * Created by Error202
 * Date: 11.07.2018
 */

use core\components\bootstrap4\widgets\Tab4;
use core\entities\menu\MenuItem;
use core\forms\menu\MenuItemForm;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\View;

/**
 * @var $this View
 * @var $item array
 * @var $model MenuItemForm
 */

/* @var $menu_item MenuItem */
$menu_item = $item['item'];
?>

<li class="dd-item dd3-item" data-id="<?= $menu_item->id ?>" xmlns="http://www.w3.org/1999/html">
	<div class="dd3-content">
		<div class="card-group" id="accordion">
			<div class="card m-0">
				<div class="card-header" style="border: 0 !important; padding-left: 40px;">
					<h4 class="card-title">
						<div class="dd-handle dd3-handle"> </div>
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $menu_item->id ?>">
                            <?= $menu_item->translation->name ?>
                            <div style="position: absolute; right: 10px; top: 10px;">
                                <span style="font-size: 14px; color: #999"><?= Yii::t($menu_item->module, ucfirst($menu_item->module)) ?></span>
                                <i class="fas fa-angle-down" aria-hidden="true"></i>
                            </div>
                        </a>
					</h4>
                    <div>

                    </div>
				</div>
				<div id="collapse<?= $menu_item->id ?>" class="panel-collapse collapse">
					<div class="panel-body p-2">

						<?php $form = ActiveForm::begin([
							'action' => ['/menu/save-item', 'id' => $menu_item->id],
						]); ?>

						<?php
						$items = [];
						foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
							$items[] = [
								'label' => $language_name,
								'content' => $this->render('_item_tab', [
									'form' => $form,
									'model' => $model,
									'language' => $language,
								]),
							];
						}
						?>

						<div class="nav-tabs-custom">
							<?= Tab4::widget([
								'items' => $items
							]) ?>
						</div>

						<div class="row">
							<div class="col-md-6">
								<?= $form->field($model, 'target')->dropDownList([
									'' => Yii::t('menu', 'Self Window'),
									'_blank' => Yii::t('menu', 'Blank Window'),
								], [
                                    'class' => 'form-control form-control-sm'
                                ]) ?>
								<?= $form->field($model, 'style')->textInput(['maxlength' => true, 'class' => 'form-control form-control-sm'],) ?>
							</div>
							<div class="col-md-6">
								<?= $form->field($model, 'css')->textInput(['maxlength' => true, 'class' => 'form-control form-control-sm']) ?>
								<?php if (!$menu_item->url_params): ?>
									<?= $form->field($model, 'url')->textInput(['maxlength' => true, 'class' => 'form-control form-control-sm']) ?>
								<?php endif; ?>
							</div>
						</div>

						<div class="float-right">
							<?= Html::a(Yii::t('buttons', 'Delete'), '#', [
								'class' => 'item-delete-button btn btn-sm btn-danger',
								'data-id' => $menu_item->id,
							]) ?>
							<?= Html::submitButton(Yii::t('buttons', 'Save'), [
								'class' => 'btn btn-success btn-sm'
							]) ?>
						</div>
						<div style="clear: both"></div>

						<?php ActiveForm::end(); ?>

					</div>
				</div>
			</div>
		</div>
	</div>

	<?php if (isset($item['children'])): ?>
	<?= menu_generate($item['children']) ?>
	<?php endif; ?>

</li>
