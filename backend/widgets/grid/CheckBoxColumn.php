<?php
/**
 * Created by Error202
 * Date: 20.11.2017
 */

namespace backend\widgets\grid;

class CheckBoxColumn extends \yii\grid\CheckboxColumn
{
    public $headerOptions  = ['class' => 'text-center', 'style' => 'width: 3em'];
    public $contentOptions = [
        'class' => 'text-center grid_checkbox',
    ];
    public $name = 'gridSelection';
}