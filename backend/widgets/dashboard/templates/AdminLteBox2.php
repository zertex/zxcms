<?php

namespace backend\widgets\dashboard\templates;

use yii\bootstrap\Widget;

class AdminLteBox2 extends Widget
{
    public $title;
    public $cssclass;
    public $color;
    public $bottomtext;
    public $bottomlink;
    public $body;


    public function init()
    {
        parent::init();

        if ($this->cssclass === null) {
            $this->cssclass = 'col-md-6 col-sm-12 col-xs-12';
        }

        if ($this->color === null) {
            $this->color = 'box-info';
        }
        $this->color = $this->color === null ? 'box-info' : 'box-' . $this->color;

        if ($this->title === null) {
            $this->title = 'Box Title';
        }
    }

    public function run()
    {
        $header = '<div class="box-header with-border">
            <h3 class="box-title">' . $this->title . '</h3>
            <div class="box-tools pull-right">
                <i class="fa fa-arrows moveHandle moveButton"></i>
            </div>
        </div>';

        $body = $this->body;


        $footer = '
            <div class="box-footer text-center">
                <a href="' . $this->bottomlink . '" class="uppercase">' . $this->bottomtext . '</a>
            </div>';

        return '<div class="box ' . $this->color . ' ' . $this->cssclass . '">' .
               $header .
               $body .
               $footer .
               '</div>';
    }
}
