<?php
/**
 * Created by Error202
 * Date: 19.12.2017
 */

namespace backend\widgets\dashboard\helpers;

use core\entities\user\User;
use Yii;
use yii\web\View;
use backend\widgets\dashboard\templates\AdminLteBox2;
use yii\helpers\Url;

class UsersLastInfo
{
    public static function widgetLastUsers($color = 'green')
    {
        $users = User::find()->orderBy(['id' => SORT_DESC])->limit(8)->all();
        $body  = (new View())->render('@backend/widgets/dashboard/views/usersLastInfo-body', [
            'users' => $users,
        ]);

        return AdminLteBox2::widget([
            'color'      => $color,
            'cssclass'   => 'grid-stack-item-content',
            'title'      => Yii::t('user', 'Users'),
            'bottomtext' => Yii::t('user', 'All users'),
            'bottomlink' => Url::toRoute(['/user-manage/index']),
            'body'       => $body,
        ]);
    }
}
