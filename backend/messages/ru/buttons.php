<?php
return [
	'Are you sure you want to delete this item?' => 'Вы уверены, что хотите удалить эту запись',
	'Update' => 'Обновить',
	'Save' => 'Сохранить',
	'Create' => 'Создать',
	'Delete' => 'Удалить',
	'Edit' => 'Изменить',
	'Editing' => 'Редактирование',
	'All Settings' => 'Все настройки',
	'Create Setting' => 'Новый параметр',
	'Go' => 'Перейти',
	'Add to menu' => 'Добавить в меню',
	'Sign in' => 'Вход',
];