<?php

return [
	'Title' => 'Заголовок',
	'Tagline' => 'Слоган',
	'Image' => 'Изображение',
	'URL' => 'Ссылка',
	'Sort' => 'Сортировка',
	'Slider' => 'Карусель',
	'Slide' => 'Слайд',
	'Create Slide' => 'Новый слайд',
	'Update Slide' => 'Редактирование слайда',
	'ID' => '№',
	'Background Image' => 'Фоновое изображение',
];