Zertex CMS
==========
## Site management system based on Yii2 framework

### Installation

To install Zertex CMS, go to the web (web, www, public_html, etc.) folder of your site in the console and type
```
composer create-project zertex/zxcms --keep-vcs
```

Dev version
```
composer create-project zertex/zxcms --keep-vcs --stability=dev
```

After creating the project, type in the console
```
cd zxcms
php setup.php
```
After completing the setup, the system will be available at the following urls
+ http://domain.com - Public website
+ http://admin.domain.com - Control Panel
+ http://static.domain.com - Image storage


Documentation is at [docs/guide/README.md](docs/guide/README.md).

Developer russian documentation is at [docs/developer/README.md](docs/developer/README.md).