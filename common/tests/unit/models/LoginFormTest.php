<?php

namespace common\tests\unit\models;

use common\auth\Identity;
use core\entities\user\User;
use core\repositories\user\UserRepository;
use core\services\auth\AuthService;
use core\dispatchers\SimpleEventDispatcher;
use Yii;
use core\forms\auth\LoginForm;
use common\fixtures\UserFixture;
use yii\helpers\VarDumper;

/**
 * Login form test
 */
class LoginFormTest extends \Codeception\Test\Unit
{

    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;

	public function __construct( ?string $name = null, array $data = [], string $dataName = '') {
		parent::__construct( $name, $data, $dataName );
	}

	protected function _after() {
		//Yii::$app->user->logout();
		//Yii::$app->session->close();
	}

	/**
     * @return array
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::class,
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ];
    }

    public function testLoginNoUser()
    {
    	$form = new LoginForm();
	    $form->username = 'not_existing_username';
	    $form->password = 'not_existing_password';

	    $model = User::find()->andWhere(['or', ['username' => $form->username], ['email' => $form->username]])->one();

	    expect('user not exists', !$model)->true();
        expect('model should not login user', $model && Yii::$app->user->login(new Identity($model)))->false();
        expect('user should not be logged in', Yii::$app->user->isGuest)->true();
    }

    public function testLoginWrongPassword()
    {
	    $form = new LoginForm();
	    $form->username = 'Error202';
	    $form->password = 'wrong_password';

	    $model = User::find()->andWhere(['or', ['username' => $form->username], ['email' => $form->username]])->one();

	    //VarDumper::dump($form);

	    expect('model should not login user', $model && Yii::$app->user->login(new Identity($model)))->false();
        //expect('error message should be set', $form->errors)->hasKey('password');
        expect('user should not be logged in', Yii::$app->user->isGuest)->true();
    }

    public function testLoginCorrect()
    {
	    $form = new LoginForm();
	    $form->username = 'Error202';
	    $form->password = '1111111';

	    //$user->validatePassword($form->password)
	    $model = User::find()->andWhere(['or', ['username' => $form->username], ['email' => $form->username]])->one();

        //expect('model should login user', $model && $model->validatePassword($form->password))->true();
	    expect('model should login user', $model && Yii::$app->user->login(new Identity($model)))->true();
        //expect('error message should not be set', $model->errors)->hasntKey('password');
        expect('user should be logged in', Yii::$app->user->isGuest)->false();
    }

    public function testValidPassword()
    {
	    $form = new LoginForm();
	    $form->username = 'Error202';
	    $form->password = '1111111';

	    $model = User::find()->andWhere(['or', ['username' => $form->username], ['email' => $form->username]])->one();

	    expect('user password validation correct', $model && $model->validatePassword($form->password))->true();
    }

	public function testNotValidPassword()
	{
		$form = new LoginForm();
		$form->username = 'Error202';
		$form->password = 'wrong_password';

		$model = User::find()->andWhere(['or', ['username' => $form->username], ['email' => $form->username]])->one();

		expect('user password not valid', $model && $model->validatePassword($form->password))->false();
	}

	public function testFindByUsername()
	{
		$model = User::findByUsername('Error202');
		expect('find user by username correct', $model && $model->id == 1)->true();
		expect('find user by username failed', !$model || $model->id != 1)->false();
	}
}
