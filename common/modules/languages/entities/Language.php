<?php

namespace common\modules\languages\entities;

use common\modules\languages\entities\queries\LanguageQuery;
use DomainException;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "languages".
 *
 * @property int $id
 * @property string $title
 * @property string $name
 * @property int $default
 * @property int $status
 *
 */
class Language extends ActiveRecord
{
	const STATUS_DRAFT = 0;
	const STATUS_ACTIVE = 1;

	const DEFAULT_TRUE = 1;
	const DEFAULT_FALSE = 0;

	public static function create($name, $title, $status): self
	{
		$language = new static();
		$language->name = $name;
		$language->title = $title;
		$language->status = $status;
		return $language;
	}

	public function edit($name, $title, $status): void
	{
		$this->name = $name;
		$this->title = $title;
		$this->status = $status;
	}

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'languages';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('languages', 'ID'),
            'name' => Yii::t('languages', 'Name'),
            'title' => Yii::t('languages', 'Title'),
            'status' => Yii::t('languages', 'Status'),
            'default' => Yii::t('languages', 'Default'),
        ];
    }

	public function activate(): void
	{
		if ($this->isActive()) {
			throw new DomainException('Language is already active.');
		}
		$this->status = self::STATUS_ACTIVE;
	}

	public function draft(): void
	{
		if ($this->isDraft()) {
			throw new DomainException('Language is already draft.');
		}
		$this->status = self::STATUS_DRAFT;
	}

	public function isActive(): bool
	{
		return $this->status == self::STATUS_ACTIVE;
	}

	public function isDraft(): bool
	{
		return $this->status == self::STATUS_DRAFT;
	}

	public static function find(): LanguageQuery
	{
		return new LanguageQuery(static::class);
	}
}
