<?php

namespace common\modules\languages\entities\queries;

use common\modules\languages\entities\Language;
use yii\db\ActiveQuery;

class LanguageQuery extends ActiveQuery
{
	/**
	 * @return LanguageQuery
	 */
    public function active()
    {
        return $this->andWhere(['status' => Language::STATUS_ACTIVE]);
    }

    public function default()
    {
    	return $this->andWhere(['default' => Language::DEFAULT_TRUE]);
    }
}
