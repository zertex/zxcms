<?php

return [
    'version'     => '1.0.1',
    'name'        => 'languages',
    'description' => 'Web site languages management',
    'module'      => 'LanguagesModule',
    'permissions' => [
        'LanguagesManagement' => 'Languages management',
    ],
];
