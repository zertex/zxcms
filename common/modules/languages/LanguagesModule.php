<?php

namespace common\modules\languages;

use core\components\modules\ModuleInterface;
use yii\helpers\ArrayHelper;
use yii\web\Application;


/**
 * languages module definition class
 */
class LanguagesModule extends \yii\base\Module implements ModuleInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\languages\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

	public function bootstrap(Application $app)
	{
		// add migration path
		$app->controllerMap['migrate']['migrationPath'][] = '@common/modules/languages/migrations';

		$app->getUrlManager()->addRules([
			'languages/manage/language/view/<id:\d+>' => 'languages/manage/language/view',
		]);

		// add languages
		$app->getI18n()->translations = ArrayHelper::merge($app->getI18n()->translations, [
			'languages' => [
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@common/modules/languages/messages',
			],
		]);

		// add menu items
		if (basename($app->getBasePath()) === 'backend') {
			$app->params['adminMenu'][] = [
				'label'   => \Yii::t( 'languages', 'Site Languages' ),
				'icon'    => 'fas fa-globe-americas',
				'url'   => [ '/languages/manage/language/index' ],
				'visible' => \Yii::$app->user->can( 'admin' ) || \Yii::$app->user->can( 'LanguagesManagement' )
			];
		}
	}
}
