<?php

return [
	'languages' => 'Языки',
	'Languages' => 'Языки',
	'Site Languages' => 'Языки веб-сайта',
	'Create Language' => 'Новый язык',
	'Update Language: {name}' => 'Редактирование языка: {name}',
	'Title' => 'Заголовок',
	'Name' => 'Название',
	'Status' => 'Статус',
	'Default' => 'По умолчанию',
	'Active' => 'Включен',
	'Draft' => 'Отключен',
	'Publish' => 'Публикация',
	'Are you sure you want to set this language as default?' => 'Вы действительно хотите сделать этот язык основным?',
	'Status for default language is active always' => 'Статус для основного языка: всегда включен',
];