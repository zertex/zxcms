<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\languages\entities\Language;
use common\modules\languages\helpers\LanguageHelper;

/* @var $this yii\web\View */
/* @var $language Language */

$this->title = $language->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('languages', 'Languages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-view">

	<div class="row">
		<div class="col-md-12">

		    <div class="card">
                <div class="card-header">
                    <?= Html::a(Yii::t('languages','Languages'), ['index'], ['class' => 'btn btn-outline-primary btn-sm']) ?>
                    <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $language->id], ['class' => 'btn btn-primary btn-sm']) ?>
                    <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $language->id], [
                        'class' => 'btn btn-danger btn-sm',
                        'data' => [
                            'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
		        <div class="card-body">
		            <?= DetailView::widget([
		                'model' => $language,
		                'attributes' => [
		                    'id',
		                    'title',
			                'name',
			                [
			                	'attribute' => 'status',
				                'format' => 'raw',
				                'value' => function(Language $language) {
		            	            return LanguageHelper::statusLabel($language->status);
				                },
			                ],
			                'default:boolean'
		                ],
		            ]) ?>
		        </div>
		    </div>

		</div>

	</div>

</div>
