<?php

use common\modules\languages\entities\Language;
use common\modules\languages\forms\LanguageForm;

/* @var $this yii\web\View */
/* @var $language Language */
/* @var $model LanguageForm */

$this->title = Yii::t('languages', 'Update Language: {name}', ['name' => $language->title]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('languages', 'Languages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $language->title, 'url' => ['view', 'id' => $language->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="language-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
