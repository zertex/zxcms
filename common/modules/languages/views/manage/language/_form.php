<?php

use common\modules\languages\forms\LanguageForm;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use common\modules\languages\helpers\LanguageHelper;

/* @var $this yii\web\View */
/* @var $model LanguageForm */
/* @var $form yii\widgets\ActiveForm */

$js2 = '
$(".hint-block").each(function () {
    var $hint = $(this);
    var label = $hint.parent().find("label");
    label.html(label.html() + \' <i style="color:#3c8dbc" class="fa fa-question-circle" aria-hidden="true"></i>\');
    label.addClass("help").popover({
        html: true,
        trigger: "hover",
        placement: "bottom",
        content: $hint.html()
    });
    $(this).hide();
});
';
$this->registerJs($js2);
?>

<div class="language-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-md-10">

		    <div class="card">
		        <div class="card-body">

			        <div class="row">
				        <div class="col-md-2">
					        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
				        </div>
				        <div class="col-md-10">
					        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
				        </div>
			        </div>

		        </div>
		    </div>

		    <div class="form-group">
		        <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
		    </div>

		</div>

		<div class="col-md-2">
			<div class="card">
				<div class="card-header with-border"><?= Yii::t('languages', 'Publish') ?></div>
				<div class="card-body">

					<?php if (isset($model->_language->default) && $model->_language->default): ?>
						<div class="callout callout-danger">
							<p><?= Yii::t('languages', 'Status for default language is active always') ?></p>
						</div>
					<?php else: ?>
						<?= $form->field($model, 'status')->radioList(LanguageHelper::statusList()) ?>
					<?php endif; ?>
				</div>
			</div>
		</div>

	</div>

    <?php ActiveForm::end(); ?>

</div>
