<?php

use common\modules\languages\forms\LanguageForm;

/* @var $this yii\web\View */
/* @var $model LanguageForm */

$this->title = Yii::t('languages', 'Create Language');
$this->params['breadcrumbs'][] = ['label' => Yii::t('languages', 'Languages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
