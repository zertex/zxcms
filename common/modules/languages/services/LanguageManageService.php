<?php

namespace common\modules\languages\services;

use common\modules\languages\entities\Language;
use common\modules\languages\forms\LanguageForm;
use common\modules\languages\repositories\LanguageRepository;
use yii\db\StaleObjectException;

class LanguageManageService
{
    private LanguageRepository $repository;

    public function __construct(LanguageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(LanguageForm $form): Language
    {
        $language = Language::create(
            $form->name,
	        $form->title,
            $form->status
        );
        $this->repository->save($language);
        return $language;
    }

    public function edit($id, LanguageForm $form): void
    {
        $language = $this->repository->get($id);

        $language->edit(
	        $form->name,
	        $form->title,
	        $form->status
        );
        $this->repository->save($language);
    }

    /**
     * @param $id
     * @throws StaleObjectException
     */
    public function remove($id): void
    {
        $language = $this->repository->get($id);
        $this->repository->remove($language);
    }

    public function setDefault(Language $language)
    {
    	$this->repository->clearDefaults();
    	$language->default = Language::DEFAULT_TRUE;
    	$language->status = Language::STATUS_ACTIVE;
    	$this->repository->save($language);
    }
}
