<?php

use yii\db\Migration;
use common\modules\languages\entities\Language;

/**
 * Class m180905_063026_init_first_languages
 */
class m180905_063026_init_first_languages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $languages = Language::find()->all();

        if (!$languages) {
            $this->batchInsert('{{%languages}}', ['name', 'title', 'status', 'default'], [
                ['ru', 'Русский', Language::STATUS_ACTIVE, Language::DEFAULT_TRUE],
                ['en', 'English', Language::STATUS_ACTIVE, Language::DEFAULT_FALSE],
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%languages}}', ['in', 'name', ['ru', 'en']]);
    }
}
