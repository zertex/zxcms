<?php

namespace common\modules\languages\forms\search;

use common\modules\languages\entities\Language;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class LanguageSearch extends Model
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $title = null;
    public ?int $status = null;

    public function rules(): array
    {
        return [
            [['id', 'status'], 'integer'],
            [['title', 'name'], 'safe'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Language::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
	        'status' => $this->status,
        ]);

        $query
            ->andFilterWhere(['like', 'title', $this->title])
	        ->andFilterWhere(['like', 'name', $this->name]);
        return $dataProvider;
    }
}
