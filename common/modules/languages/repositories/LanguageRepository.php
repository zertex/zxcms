<?php

namespace common\modules\languages\repositories;

use common\modules\languages\entities\Language;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class LanguageRepository
{
    public function get($id): Language
    {
        if (!$language = Language::findOne($id)) {
            throw new NotFoundException('Language is not found.');
        }
        return $language;
    }

    public function save(Language $language): void
    {
        if (!$language->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param Language $language
     * @throws StaleObjectException
     */
    public function remove(Language $language): void
    {
        if (!$language->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }

    public function clearDefaults()
    {
    	Language::updateAll(['default' => 0]);
    }
}
