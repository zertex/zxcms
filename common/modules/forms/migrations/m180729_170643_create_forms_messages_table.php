<?php

use yii\db\Migration;

/**
 * Handles the creation of table `forms_messages`.
 */
class m180729_170643_create_forms_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%forms_messages}}', [
            'id' => $this->primaryKey(),
	        'form_id' => $this->integer()->notNull(),
	        'data' => 'LONGTEXT',
            'new' => $this->integer(1)->defaultValue(1),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
        ], $tableOptions);

        $this->createIndex('idx_forms_messages_form_id', '{{%forms_messages}}', 'form_id');
        $this->addForeignKey('frg_forms_messages_form_id_forms_id', '{{%forms_messages}}', 'form_id', '{{%forms}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropForeignKey('frg_forms_messages_form_id_forms_id', '{{%forms_messages}}');
    	$this->dropIndex('idx_forms_messages_form_id', '{{%forms_messages}}');

        $this->dropTable('{{%forms_messages}}');
    }
}
