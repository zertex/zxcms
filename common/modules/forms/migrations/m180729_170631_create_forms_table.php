<?php

use yii\db\Migration;

/**
 * Handles the creation of table `forms`.
 */
class m180729_170631_create_forms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%forms}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'data' => 'LONGTEXT',
            'from' => $this->string()->notNull(),
            'reply' => $this->string(),
            'return' => $this->string(),
            'subject' => $this->string(),
            'complete_text' => 'LONGTEXT',
            'complete_page_id' => $this->string(),
            'status' => $this->integer(1),
            'captcha' => $this->integer(1)->defaultValue(0),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
        ], $tableOptions);

	    $this->createIndex('{{%idx-forms-status}}', '{{%forms}}', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%forms}}');
    }
}
