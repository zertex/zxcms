<?php
/**
 * Created by Error202
 * Date: 01.08.2018
 */

use yii\web\View;

/**
 * @var $this View
 * @var $items array
 */
?>

<h3><?= Yii::t('forms', 'Message from site') ?></h3>

<?php foreach ($items as $item): ?>
<?= $item['key'] ?>: <?= $item['value'] ?><br>
<?php endforeach; ?>
