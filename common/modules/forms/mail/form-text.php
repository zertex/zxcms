<?php
/**
 * Created by Error202
 * Date: 01.08.2018
 */

use yii\web\View;

/**
 * @var $this View
 * @var $items array
 */
?>

<?= Yii::t('forms', 'Message from site') ?>
-------

<?php foreach ($items as $item): ?>
	<?= $item['key'] ?>: <?= $item['value'] ?><?php echo PHP_EOL?>
<?php endforeach; ?>
