<?php
/**
 * Created by Error202
 * Date: 29.07.2018
 */

use common\modules\forms\forms\FormForm;
use kartik\widgets\Select2;
use yii\web\View;
use yii\widgets\ActiveForm;
use zertex\ckeditor\CKEditor;
use yii\web\JsExpression;
use yii\helpers\Url;

/**
 * @var $this View
 * @var $form ActiveForm
 * @var $model FormForm
 */

$fetchUrl = Url::to( [ '/forms/manage/form/page-search' ] );
?>

<div class="forms-form">

	<div class="row">
		<div class="col-md-6"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
		<div class="col-md-6"><?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?></div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<?= $form->field($model, 'return')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'from')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-4">
			<?= $form->field($model, 'reply')->textInput(['maxlength' => true]) ?>
		</div>
	</div>

	<?= $form->field($model, 'complete_text')->widget(CKEditor::class) ?>

	<?= $form->field($model, 'complete_page_id')->widget(Select2::class, [
		'options' => [
			'placeholder' => Yii::t('forms', 'Select page...'),
			'id' => 'page_select',
		],
		'pluginOptions' => [
			'allowClear' => true,
			'ajax' => [
				'url' => $fetchUrl,
				'dataType' => 'json',
				'data' => new JsExpression('function(params) { return {q:params.term}; }')
			],
			'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
			'templateResult' => new JsExpression('function(tag) { return tag.text; }'),
			'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
		],
	]) ?>

	<?= $form->field($model, 'data')->hiddenInput()->label(false) ?>

</div>
