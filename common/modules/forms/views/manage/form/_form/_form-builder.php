<?php
/**
 * Created by Error202
 * Date: 29.07.2018
 */

use backend\components\form_builder\assets\FormBuilderAsset;
use common\modules\forms\forms\FormForm;
use \yii\helpers\Json;
use yii\web\View;

/**
 * @var $this View
 * @var $model FormForm
 */
$bundle = FormBuilderAsset::register($this);

$lng = str_replace('_', '-', Yii::$app->language);
$lng_file = $bundle->baseUrl . '/js/languages/';

$data = $model->data ? Json::encode($model->data, JSON_UNESCAPED_UNICODE) : '[]';

$js   = <<<JS
	var options = {
      	controlOrder: [
        	'text',
        	'textarea',
        	'button'
      	],
      	disableFields: [
      	    'number', 'autocomplete', 'date', 'file'
      	],
      	i18n: {
    		locale: '{$lng}',
    		location: '{$lng_file}'
  		},
  		showActionButtons: false,
  		formData: {$data},
  		fields: [
  		    {
	            label: 'E-mail',
				type: 'text',
				subtype: "email",
				placeholder: 'name@email.com',
				icon: '✉'
			},
			{
			    label: 'Кнопка отправки',
				type: 'button',
				subtype: "submit",
				icon: '▶'
			}
		]
    };

	var editor = document.getElementById('fb-editor');
  	var fb = $(editor).formBuilder(options);
  	//document.addEventListener('fieldAdded', function(e) { alert(JSON.stringify( fb.actions.getData('json'))); });
  	
  	$('#form_constructor').on('beforeValidate', function (e) {
  	    //var json = JSON.stringify( fb.actions.getData('json'));
  	    var json = fb.actions.getData('json');
  	    $("#formform-data").val(json);
  	    //alert(json);
	    return true;
	});
  	
  	/*var init_data = '';
  	if (init_data) {
  	    fb.actions.setData(init_data);
  	}*/
JS;
$this->registerJs($js, $this::POS_READY);

$css = <<<CSS
	.stage-wrap .frmb {
		height: 200px;
	}
CSS;
$this->registerCss($css);
?>

<div class="form-builder-container" style="padding: 20px; overflow: auto">
    <div id="fb-editor"></div>
</div>

