<?php

use common\modules\forms\forms\FormForm;
use core\components\bootstrap4\widgets\Tab4;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use common\modules\forms\helpers\FormHelper;

/* @var $this yii\web\View */
/* @var $model FormForm */
/* @var $form yii\widgets\ActiveForm */

$js2 = '
$(".hint-block").each(function () {
    var $hint = $(this);
    var label = $hint.parent().find("label");
    label.html(label.html() + \' <i style="color:#3c8dbc" class="fa fa-question-circle" aria-hidden="true"></i>\');
    label.addClass("help").popover({
        html: true,
        trigger: "hover",
        placement: "bottom",
        content: $hint.html()
    });
    $(this).hide();
});
';
$this->registerJs($js2);
?>

<div class="form-form">

    <?php $form = ActiveForm::begin([
    	'id' => 'form_constructor'
    ]); ?>

	<div class="row">
		<div class="col-md-10">

		    <div class="card">
		        <div class="card-header with-border"><?= Yii::t('forms', 'Common') ?></div>
		        <div class="card-body">

			        <?= Tab4::widget([
			        	'items' => [
					        [
						        'label'     =>  Yii::t('forms', 'Common'),
						        'content'   =>  $this->render('_form/_form-items', ['model' => $model, 'form' => $form]),
						        'active'    =>  true,
					        ],
					        [
						        'label'     => Yii::t('forms', 'Form'),
						        'content'   =>  $this->render('_form/_form-builder', ['model' => $model]),
					        ]
					    ],
			        ]) ?>

		        </div>
		    </div>

		    <div class="form-group">
		        <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
		    </div>

		</div>

		<div class="col-md-2">
			<div class="box box-default">
				<div class="box-header with-border"><?= Yii::t('forms', 'Publish') ?></div>
				<div class="box-body">

					<?= $form->field($model, 'status')->radioList(FormHelper::statusList()) ?>

				</div>
			</div>
		</div>

	</div>

    <?php ActiveForm::end(); ?>

</div>
