<?php

use common\modules\forms\forms\FormForm;

/* @var $this yii\web\View */
/* @var $model FormForm */

$this->title = Yii::t('forms', 'Create Form');
$this->params['breadcrumbs'][] = ['label' => Yii::t('forms', 'Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
