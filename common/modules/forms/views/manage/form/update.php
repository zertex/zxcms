<?php

use common\modules\forms\entities\Form;
use common\modules\forms\forms\FormForm;

/* @var $this yii\web\View */
/* @var $form_model Form */
/* @var $model FormForm */

$this->title = Yii::t('forms', 'Update Form: {name}', ['name' => $form_model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('pages', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $form_model->name, 'url' => ['view', 'id' => $form_model->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="form-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
