<?php

namespace common\modules\forms\services;

use common\modules\forms\entities\FormMessage;
use common\modules\forms\repositories\FormMessageRepository;
use yii\db\StaleObjectException;

class FormMessageManageService
{
    private FormMessageRepository $repository;

    public function __construct(FormMessageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create($form_id, $data): FormMessage
    {
        $message = FormMessage::create(
            $form_id,
            $data
        );
        $this->repository->save($message);
        return $message;
    }

    public function edit($id, $form_id, $data): void
    {
        $message = $this->repository->get($id);
        $message->edit(
	        $form_id,
	        $data
        );
        $this->repository->save($message);
    }

    public function setRead($id)
    {
	    $message = $this->repository->get($id);
	    $message->new = FormMessage::STATUS_OLD;
	    $this->repository->save($message);
    }

	public function setUnread($id)
	{
		$message = $this->repository->get($id);
		$message->new = FormMessage::STATUS_NEW;
		$this->repository->save($message);
	}

    /**
     * @param $id
     * @throws StaleObjectException
     */
    public function remove($id): void
    {
        $message = $this->repository->get($id);
        $this->repository->remove($message);
    }
}
