<?php
/**
 * Created by Error202
 * Date: 29.07.2018
 */

namespace common\modules\forms\helpers;

use common\modules\forms\entities\Form;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class FormHelper {
	public static function statusList(): array
	{
		return [
			Form::STATUS_DRAFT => Yii::t('forms', 'Draft'),
			Form::STATUS_ACTIVE => Yii::t('forms', 'Active'),
		];
	}

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
	public static function statusName($status): string
	{
		return ArrayHelper::getValue(self::statusList(), $status);
	}

    /**
     * @param $status
     * @return string
     * @throws Exception
     */
	public static function statusLabel($status): string
	{
        $class = match ($status) {
            Form::STATUS_DRAFT => 'label label-default',
            Form::STATUS_ACTIVE => 'label label-success',
            default => 'label label-default',
        };

		return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
			'class' => $class,
		]);
	}
}
