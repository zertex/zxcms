<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

?>
<?php $form = ActiveForm::begin([
	'action' => ['/forms/form/send', 'id' => $form_entity->id],
]); ?>

{$fields}

<?php ActiveForm::end(); ?>
