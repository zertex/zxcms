<?php
namespace common\modules\forms\runtime;

use yii\base\Model;

class DynaForm1 extends Model
{
	public $text_1533113814416;
	public $text_1533058533530;
	public $textarea_1533057466900;

	public function rules(): array
	{
		return [
			['text_1533113814416', 'required'],
			['text_1533113814416', 'string'],
			['text_1533058533530', 'required'],
			['text_1533058533530', 'email'],
			['textarea_1533057466900', 'required'],
			['textarea_1533057466900', 'string'],
		];
	}

	public function attributeLabels() {
		return [
			'text_1533113814416' => 'Ваше имя',
			'text_1533058533530' => 'E-mail',
			'textarea_1533057466900' => 'Сообщение',
		];
	}
}