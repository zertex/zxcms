<?php
/**
 * Created by Error202
 * Date: 02.08.2018
 */

namespace common\modules\forms\widgets;

class FormClassField
{
	public function text(array $settings): string
	{
		if ($settings['subtype'] == 'email') {
			return "['{$settings['name']}', 'email'],";
		}
		return "['{$settings['name']}', 'string'],";
	}

	public function textarea(array $settings): string
	{
		return "['{$settings['name']}', 'string'],";
	}

	public function hidden(array $settings): string
	{
		return "['{$settings['name']}', 'string'],";
	}

	public function checkboxGroup(array $settings): string
	{
		return "['{$settings['name']}', 'each', 'rule' => ['string']],";
	}

	public function radioGroup(array $settings): string
	{
		return "['{$settings['name']}', 'string'],";
	}

	public function select(array $settings): string
	{
		if (isset($settings['multiple']) && $settings['multiple'] == true) {
			return "['{$settings['name']}', 'each', 'rule' => ['string']],";
		}
		return "['{$settings['name']}', 'string'],";
	}
}
