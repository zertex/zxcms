<?php
/**
 * Created by Error202
 * Date: 31.07.2018
 */

namespace common\modules\forms\widgets;

use Yii;

class FormGenerator
{
	public static function generateFormView($id, $json)
	{
		$viewName = 'DynaView' . $id;
		$fields = [];
		$fieldRender = new FormViewField();

		foreach ($json as $item) {
			if ( isset( $item['name'] ) ) {
				$item['name'] = str_replace( '-', '_', $item['name'] );
			}
            $fields[] = match ($item['type']) {
                'text' => $fieldRender->text($item),
                'header' => $fieldRender->header($item),
                'paragraph' => $fieldRender->paragraph($item),
                'hidden' => $fieldRender->hidden($item),
                'radio-group' => $fieldRender->radioGroup($item),
                'checkbox-group' => $fieldRender->checkboxGroup($item),
                'select' => $fieldRender->select($item),
                'button' => $fieldRender->button($item),
                'textarea' => $fieldRender->textArea($item),
            };
		}
		$fields = implode("\n", $fields);
		$tpl = file_get_contents(Yii::getAlias('@common/modules/forms/widgets/templates/DynaView.tpl'));

		$tpl = preg_replace([
			'/\{\$fields\}/'
		], [
			$fields
		], $tpl);

		file_put_contents(Yii::getAlias('@common/modules/forms/runtime/' . $viewName . '.php'), $tpl);
	}


	public static function generateFormView2($id, $json)
	{
		$viewName = 'DynaView' . $id;

		$fields = [];

		foreach ($json as $item) {
			if (isset($item['name'])) {
				$item['name'] = str_replace( '-', '_', $item['name'] );
			}

			if ($item['type'] == 'text') {
				$options = [];
				$options[] = isset($item['placeholder']) ? "'placeholder' => '{$item['placeholder']}'" : "";
				$options[] = isset($item['value']) ? "'value' => '{$item['value']}'" : "";
				$options[] = isset($item['maxlength']) ? "'maxlength' => true" : "";
				$options = implode(',', array_filter($options));
				$description = isset($item['description']) ? $item['description'] : '';
				$fields[] = "<?= \$form->field(\$model, '{$item['name']}')->textInput([{$options}])->hint('{$description}') ?>";
			}

			elseif ($item['type'] == 'header' || $item['type'] == 'paragraph') {
				$fields[] = "<{$item['subtype']}>{$item['label']}</{$item['subtype']}>";
			}

			elseif ($item['type'] == 'textarea') {
				$options = [];
				$options[] = isset($item['rows']) ? "'rows' => {$item['rows']}" : "";
				$options[] = isset($item['placeholder']) ? "'placeholder' => '{$item['placeholder']}'" : "";
				$options[] = isset($item['value']) ? "'value' => '{$item['value']}'" : "";
				$options = implode(',', array_filter($options));
				$description = isset($item['description']) ? $item['description'] : '';
				$fields[] = "<?= \$form->field(\$model, '{$item['name']}')->textarea([{$options}])->hint('{$description}') ?>";
			}

			elseif ($item['type'] == 'hidden') {
				$options = [];
				$options[] = isset($item['value']) ? "'value' => '{$item['value']}'" : "";
				$options = implode(',', array_filter($options));
				$fields[] = "<?= \$form->field(\$model, '{$item['name']}')->hiddenInput([{$options}])->label(false) ?>";
			}

			elseif ($item['type'] == 'radio-group') {
				$values = [];
				$selected = [];
				foreach ($item['values'] as $value) {
					$values[] = "'{$value['value']}' => '{$value['label']}'";
					if (isset($value['selected']) && $value['selected'] == true) {
						$selected[] = "'{$value['value']}'";
					}
				}
				$values = implode(',', $values);
				$selected = implode(',', $selected);

				if ($selected) {
					$fields[] = "<?php \$model->{$item['name']} = [{$selected}] ?>";
				}

				$options = [];
				//$options[] = $selected ? "'value' => [{$selected}]" : "";
				$options = implode(',', array_filter($options));
				$description = $item['description'] ?? '';
				$fields[] = "<?= \$form->field(\$model, '{$item['name']}')
                    ->radioList([
				        {$values}
                ], [{$options}])->hint('{$description}'); ?>";
			}

			elseif ($item['type'] == 'checkbox-group') {
				$values = [];
				$selected = [];
				foreach ($item['values'] as $value) {
					$values[] = "'{$value['value']}' => '{$value['label']}'";
					if (isset($value['selected']) && $value['selected'] == true) {
						$selected[] = "'{$value['value']}'";
					}
				}
				$values = implode(',', $values);
				$selected = implode(',', $selected);

				if ($selected) {
					$fields[] = "<?php \$model->{$item['name']} = [{$selected}] ?>";
				}

				$options = [];
				//$options[] = $selected ? "'value' => [{$selected}]" : "";
				$options = implode(',', array_filter($options));
				$description = $item['description'] ?? '';
				$fields[] = "<?= \$form->field(\$model, '{$item['name']}')
                    ->checkboxList([
				        {$values}
                ], [{$options}])->hint('{$description}'); ?>";
			}

			elseif ($item['type'] == 'select') {
				$values = [];
				$selected = [];
				foreach ($item['values'] as $value) {
					$values[] = "'{$value['value']}' => '{$value['label']}'";
					if (isset($value['selected']) && $value['selected'] == true) {
						$selected[] = "'{$value['value']}'";
					}
				}
				$values = implode(',', $values);
				$selected = implode(',', $selected);

				$options = [];
				$options[] = $selected ? "'value' => [{$selected}]" : "";
				$options[] = isset($item['placeholder']) ? "'prompt' => '{$item['placeholder']}'" : "";
				$options[] = isset($item['multiple']) && $item['multiple'] == true ? "'multiple' => 'multiple'" : "";
				$options = implode(',', array_filter($options));
				$description = $item['description'] ?? '';
				$fields[] = "<?= \$form->field(\$model, '{$item['name']}')->dropDownList([{$values}], [{$options}])->hint('{$description}') ?>";
			}

			elseif ($item['type'] == 'button' && $item['subtype'] == 'submit') {
				$fields[] = <<<BUTTON
<div class="form-group">
	<?= Html::submitButton('{$item['label']}', [
		        	'class' => 'btn btn-success',
	]) ?>
</div>
BUTTON;

			}
		}

		// prepare
		$fields = implode("\n", $fields);

		$view = <<<VIEW
<?php

use kartik\\form\\ActiveForm;
use yii\\helpers\\Html;

?>
<?php \$form = ActiveForm::begin([
	'action' => ['/forms/form/send', 'id' => \$form_entity->id],
]); ?>

{$fields}

<?php ActiveForm::end(); ?>

VIEW;
		file_put_contents(Yii::getAlias('@common/modules/forms/runtime/' . $viewName . '.php'), $view);
	}

	public static function generateFormClass2($id, $json)
	{
		$className = 'DynaForm' . $id;
		$publicVars = [];
		$rule = [];
		$labels = [];

		$fieldClass = new FormClassField();

		foreach ($json as $item) {
			if (isset($item['name'])) {
				$item['name'] = str_replace( '-', '_', $item['name'] );
			}

			if ($item['type'] == 'button') {continue;}
			if ($item['type'] == 'paragraph') {continue;}
			if ($item['type'] == 'header') {continue;}
			// public
			$publicVars[] = 'public $' . $item['name'] . ';';

			// required
			if (isset($item['required']) && $item['required'] == 1) {
				$rule[] = "['{$item['name']}', 'required'],";
			}

			// rules
			switch ($item['type']) {
				case 'text':
					$rule[] = $fieldClass->text($item);
					break;
				case 'textarea':
					$rule[] = $fieldClass->textarea($item);
					break;
				case 'hidden':
					$rule[] = $fieldClass->hidden($item);
					break;
				case 'select':
					$rule[] = $fieldClass->select($item);
					break;
				case 'checkbox-group':
					$rule[] = $fieldClass->checkboxGroup($item);
					break;
				case 'radio-group':
					$rule[] = $fieldClass->radioGroup($item);
					break;
			}


			if (isset($item['label'])) {
				$labels[] = "'{$item['name']}' => '{$item['label']}',";
			}
		}

		// prepare data
		$publicVars = implode("\n", $publicVars);
		$rule = implode("\n", $rule);
		$labels = implode("\n", $labels);

		$classData = <<<CLASS
<?php		
namespace common\\modules\\forms\\runtime;		

use yii\\base\\Model;		
		
class {$className} extends Model
{
	{$publicVars}
	
	public function rules(): array
    {
        return [
            {$rule}
        ];
    }
    
    public function attributeLabels() {
		return [
			{$labels}
		];
	}
}
CLASS;

		file_put_contents(Yii::getAlias('@common/modules/forms/runtime/' . $className . '.php'), $classData);
	}

	public static function generateFormClass($id, $json)
	{
		$className = 'DynaForm' . $id;

		$publicVars = [];
		$rule = [];
		$labels = [];

		foreach ($json as $item) {
			if (isset($item['name'])) {
				$item['name'] = str_replace( '-', '_', $item['name'] );
			}

			if ($item['type'] == 'button') {continue;}
			if ($item['type'] == 'paragraph') {continue;}
			if ($item['type'] == 'header') {continue;}
			// public
			$publicVars[] = 'public $' . $item['name'] . ';';
			// rule
			if (isset($item['required']) && $item['required'] == 1)
			{
				$rule[] = "['{$item['name']}', 'required'],";
			}

			if (
				($item['type'] == 'text' && $item['subtype'] == 'text') ||
				($item['type'] == 'textarea' && $item['subtype'] == 'textarea') ||
				($item['type'] == 'hidden')
			)
			{
				$rule[] = "['{$item['name']}', 'string'],";
			}
			if ($item['type'] == 'text' && $item['subtype'] == 'email') {
				$rule[] = "['{$item['name']}', 'email'],";
			}
			if ($item['type'] == 'checkbox-group') {
				$rule[] = "['{$item['name']}', 'each', 'rule' => ['string']],";
			}
			if ($item['type'] == 'radio-group') {
				$rule[] = "['{$item['name']}', 'string'],";
			}
			if ($item['type'] == 'select' && isset($item['multiple']) && $item['multiple'] == true) {
				$rule[] = "['{$item['name']}', 'each', 'rule' => ['string']],";
			}
			if ($item['type'] == 'select' && !isset($item['multiple'])) {
				$rule[] = "['{$item['name']}', 'string'],";
			}

			if (isset($item['label'])) {
				$labels[] = "'{$item['name']}' => '{$item['label']}',";
			}
		}

		// prepare data
		$publicVars = implode("\n", $publicVars);
		$rule = implode("\n", $rule);
		$labels = implode("\n", $labels);

		$classData = <<<CLASS
<?php		
namespace common\\modules\\forms\\runtime;		

use yii\\base\\Model;		
		
class {$className} extends Model
{
	{$publicVars}
	
	public function rules(): array
    {
        return [
            {$rule}
        ];
    }
    
    public function attributeLabels() {
		return [
			{$labels}
		];
	}
}
CLASS;

		file_put_contents(Yii::getAlias('@common/modules/forms/runtime/' . $className . '.php'), $classData);
	}
}
