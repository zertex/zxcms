<?php

namespace common\modules\forms\forms;

use common\modules\forms\entities\FormMessage;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class FormMessageSearch extends Model
{
    public ?int $id = null;
    public ?int $form_id = null;

    public function rules(): array
    {
        return [
            [['id', 'form_id'], 'integer'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = FormMessage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
	        'form_id' => $this->form_id
        ]);

        return $dataProvider;
    }
}
