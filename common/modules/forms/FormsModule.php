<?php

namespace common\modules\forms;

use common\modules\forms\entities\FormMessage;
use common\modules\forms\urls\FormsUrlRule;
use core\components\modules\ModuleInterface;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Application;

/**
 * blog module definition class
 */
class FormsModule extends \yii\base\Module implements ModuleInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\forms\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap(Application $app)
    {
        // add migration path
        $app->controllerMap['migrate']['migrationPath'][] = '@common/modules/forms/migrations';

        // prepare rules
        //$app->getUrlManager()->addRules([
        //    'forms/manage/form/view/<id:\d+>' => 'forms/manage/form/view',
        //]);

        $app->getUrlManager()->addRules([
            ['class' => FormsUrlRule::class],
        ]);

        // add languages
        $app->getI18n()->translations = ArrayHelper::merge($app->getI18n()->translations, [
            'forms' => [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/forms/messages',
            ],
        ]);

        // add menu items
        if (basename($app->getBasePath()) === 'backend') {
            $app->params['adminMenu'][] = [
                'label'   => Yii::t('forms', 'Forms'),
                'icon'    => 'far fa-address-card',
                'items'   => [
                    [
                        'label' => Yii::t('forms', 'Forms'),
                        'icon'  => 'fas fa-caret-right',
                        'url'   => ['/forms/manage/form/index']
                    ],
                    [
                        'label' => Yii::t('forms', 'Messages'),
                        'icon'  => 'fas fa-caret-right',
                        'url'   => ['/forms/manage/form-message/index'],
                    ],
                ],
                'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('FormsManagement'),
            ];
        }

        // prepare notifications
        $new_messages_count = Yii::$app->moduleManager->isTableExist('{{%forms}}') ? FormMessage::find()->unread()->count() : 0;
        if ($new_messages_count > 0) {
            $app->params['notifications'][] = [
                'icon'         => 'address-card-o',
                'color'        => 'yellow',
                'message'      => 'New forms messages: {count}',
                'message_file' => 'forms',
                'url'          => '/forms/manage/form-message/index',
                'count'        => $new_messages_count,
            ];
        }
    }
}
