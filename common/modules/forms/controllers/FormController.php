<?php
/**
 * Created by Error202
 * Date: 01.08.2018
 */

namespace common\modules\forms\controllers;

use common\modules\forms\entities\Form;
use common\modules\forms\services\FormMessageManageService;
use frontend\components\FrontendController;
use RuntimeException;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\mail\MailerInterface;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;

class FormController extends FrontendController
{
    public MailerInterface $mailer;
    public FormMessageManageService $message_service;

    public function __construct(
        string $id,
        $module,
        MailerInterface $mailer,
        FormMessageManageService $message_service,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->mailer          = $mailer;
        $this->message_service = $message_service;
    }

    /**
     * @param $id
     * @return Response|string
     * @throws NotFoundHttpException
     */
    public function actionSend($id): Response|string
    {
        $form = $this->findModel($id);

        $className = 'DynaForm' . $id;
        $classPath = '\\common\\modules\\forms\\runtime\\' . $className;

        /* @var $dynaForm Model */
        $dynaForm = new $classPath();

        if ($dynaForm->load(Yii::$app->request->post()) && $dynaForm->validate()) {
            // send message
            $this->sendMessage($form->data, $form, $dynaForm);

            if ($form->complete_page_id) {
                return $this->redirect(['/pages/page/view', 'id' => $form->complete_page_id]);
            }
            return $this->render('view', ['text' => $form->complete_text]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    private function sendMessage($json_string, Form $form, $dynaForm)
    {
        $messageItems = $this->prepareItems($json_string, $dynaForm);
        // save message
        $this->message_service->create($form->id, Json::encode($messageItems, JSON_UNESCAPED_UNICODE));
        //prepare e-mail message
        $to    = isset($form->return) && $form->return ? explode(',', $form->return) : [Yii::$app->params['adminEmail']];
        $from  = isset($form->from) && $form->from ? explode(',', $form->from) : [Yii::$app->params['adminEmail']];
        $reply = isset($form->reply) && $form->reply ? explode(',', $form->reply) : $from;

        $sent = $this->mailer->compose(
            ['html' => '@common/modules/forms/mail/form-html', 'text' => '@common/modules/forms/mail/form-text'],
            ['items' => $messageItems]
        )
                             ->setTo($to)
                             ->setFrom($from)
                             ->setReplyTo($reply)
                             ->setSubject($form->subject)
                             ->send();
        if (!$sent) {
            throw new RuntimeException('Sending error.');
        }
    }

    private function prepareItems($json_string, $dynaForm): array
    {
        $items = [];
        $json  = Json::decode($json_string, true);
        foreach ($json as $item) {
            if ($item['type'] == 'button') {
                continue;
            }
            if ($item['type'] == 'paragraph') {
                continue;
            }
            if ($item['type'] == 'header') {
                continue;
            }

            $item['name'] = str_replace('-', '_', $item['name']);
            $items[]      = [
                'key'   => $item['label'] ?? '',
                'value' => is_array($dynaForm->{$item['name']}) ? implode(', ', array_map(function ($el) {
                    return Html::encode($el);
                }, $dynaForm->{$item['name']})) : Html::encode($dynaForm->{$item['name']}),
            ];
        }

        return $items;
    }

    /**
     * @param $id
     * @return Form
     * @throws NotFoundHttpException
     */
    protected function findModel($id): Form
    {
        if (($model = Form::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested form does not exist.');
    }
}
