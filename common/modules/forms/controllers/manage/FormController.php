<?php

namespace common\modules\forms\controllers\manage;

use common\modules\forms\services\FormManageService;
use common\modules\forms\forms\FormSearch;
use common\modules\forms\entities\Form;
use common\modules\pages\entities\Page;
use common\modules\forms\forms\FormForm;
use DomainException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use Yii;

class FormController extends Controller
{
    private FormManageService $service;

    public function __construct($id, $module, FormManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['FormsManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel  = new FormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'form' => $this->findModel($id),
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate(): string|Response
    {
        $form = new FormForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $form = $this->service->create($form);

                return $this->redirect(['view', 'id' => $form->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id): Response|string
    {
        $form_model = $this->findModel($id);

        $form = new FormForm($form_model);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($form_model->id, $form);

                return $this->redirect(['view', 'id' => $form_model->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model'      => $form,
            'form_model' => $form_model,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return Response
     */
    public function actionDelete(int $id): Response
    {
        try {
            $this->service->remove($id);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    public function actionPageSearch($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out                         = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = [];
            $pages = Page::find()
                             ->with('translation')
                             ->leftJoin('{{%pages_lng}}', '`pages_lng`.`page_id` = `pages`.`id`')
                             ->andWhere(['like', 'pages_lng.title', $q])
                             ->limit(20)
                             ->all();
            foreach ($pages as $page) {
                $data[] = [
                    'id' => $page->id,
                    'text' => isset($page->translation) ? $page->translation->title : null,
                ];
            }
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $tag_name       = Page::findOne($id)->translation->title;
            $out['results'] = ['id' => $tag_name, 'text' => $tag_name];
        } else {
            $data = [];
            $pages = Page::find()->typePublic()->orderBy(['id' => SORT_DESC])->limit(20)->all();
            foreach ($pages as $page) {
                $data[] = [
                    'id' => $page->id,
                    'text' => isset($page->translation) ? $page->translation->title : null,
                ];
            }
            $out['results'] = array_values($data);
        }

        return $out;
    }

    /**
     * @param integer $id
     *
     * @return Form the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): Form
    {
        if (($model = Form::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested form does not exist.');
    }
}
