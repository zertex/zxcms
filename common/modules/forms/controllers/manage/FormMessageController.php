<?php

namespace common\modules\forms\controllers\manage;

use common\modules\forms\entities\FormMessage;
use common\modules\forms\forms\FormMessageSearch;
use common\modules\forms\services\FormMessageManageService;
use DomainException;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;
use yii\web\Response;

class FormMessageController extends Controller
{
    private FormMessageManageService $service;

    public function __construct($id, $module, FormMessageManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
	        'access' => [
		        'class' => AccessControl::class,
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['FormsManagement'],
			        ],
			        [    // all the action are accessible to admin
				        'allow' => true,
				        'roles' => ['admin'],
			        ],
		        ],
	        ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new FormMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionView($id): string
    {
    	$this->service->setRead($id);
        return $this->render('view', [
            'message' => $this->findModel($id),
        ]);
    }

    /**
     * @param integer $id
     * @return Response
     */
    public function actionDelete(int $id): Response
    {
        try {
            $this->service->remove($id);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

	public function actionDeleteSelected()
	{
		$post = Yii::$app->request->post();
		$ids = $post['ids'] ?? null;
		Yii::$app->response->format = Response::FORMAT_JSON;
		if (!$ids) {
			return [
				'result' => 'error',
			];
		}
		$idsArray = Json::decode($ids, true);

		foreach ($idsArray as $id)
		{
			try {
				$this->service->remove($id);
			} catch (DomainException $e) {
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
				return [
					'result' => 'failed',
				];
			}
		}
		Yii::$app->session->setFlash('success', Yii::t('forms', 'Selected messages deleted'));
		return [
			'result' => 'success',
		];
	}

	public function actionReadSelected(): array
	{
		$post = Yii::$app->request->post();
		$ids = $post['ids'] ?? null;
		Yii::$app->response->format = Response::FORMAT_JSON;
		if (!$ids) {
			return [
				'result' => 'error',
			];
		}
		$idsArray = Json::decode($ids, true);

		foreach ($idsArray as $id)
		{
			try {
				$this->service->setRead($id);
			} catch (DomainException $e) {
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
				return [
					'result' => 'failed',
				];
			}
		}
		return [
			'result' => 'success',
		];
	}

	public function actionUnreadSelected(): array
	{
		$post = Yii::$app->request->post();
		$ids = $post['ids'] ?? null;
		Yii::$app->response->format = Response::FORMAT_JSON;
		if (!$ids) {
			return [
				'result' => 'error',
			];
		}
		$idsArray = Json::decode($ids, true);

		foreach ($idsArray as $id)
		{
			try {
				$this->service->setUnread($id);
			} catch (DomainException $e) {
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
				return [
					'result' => 'failed',
				];
			}
		}
		return [
			'result' => 'success',
		];
	}

	/**
	 * @param $id
	 *
	 * @return FormMessage
	 * @throws NotFoundHttpException
	 */
    protected function findModel($id): FormMessage
    {
        if (($model = FormMessage::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The message does not exist.');
    }
}
