<?php

namespace common\modules\forms\urls;

use common\modules\forms\repositories\FormRepository;
use core\helpers\LanguageHelper;
use yii\base\BaseObject;
use yii\web\UrlRuleInterface;
use yii;

class FormsUrlRule extends BaseObject implements UrlRuleInterface
{
    public string $prefix = 'forms/form/send';

    private FormRepository $repository;

    public function __construct(FormRepository $repository, array $config = [])
    {
        parent::__construct($config);
        $this->repository = $repository;
    }

    public function parseRequest($manager, $request): bool|array
    {
        $uri = ltrim(LanguageHelper::processLangInUrl($request->pathInfo), '/');
        if ($uri == $this->prefix) {
            $id = Yii::$app->request->get('id');
            $form = $this->repository->get($id);

            if ($form) {
                return [$uri, ['id' => $id]];
            }
        }
        return false;
    }

    public function createUrl($manager, $route, $params): bool
    {
        return false;
    }
}
