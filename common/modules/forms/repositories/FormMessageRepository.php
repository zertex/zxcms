<?php

namespace common\modules\forms\repositories;

use common\modules\forms\entities\FormMessage;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class FormMessageRepository
{
    public function get($id): FormMessage
    {
        if (!$message = FormMessage::findOne($id)) {
            throw new NotFoundException('Message is not found.');
        }
        return $message;
    }

    public function save(FormMessage $message): void
    {
        if (!$message->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param FormMessage $message
     * @throws StaleObjectException
     */
    public function remove(FormMessage $message): void
    {
        if (!$message->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
