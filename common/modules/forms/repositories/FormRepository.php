<?php

namespace common\modules\forms\repositories;

use common\modules\forms\entities\Form;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class FormRepository
{
    public function get($id): Form
    {
        if (!$form = Form::findOne($id)) {
            throw new NotFoundException('Form is not found.');
        }
        return $form;
    }

    public function save(Form $form): void
    {
        if (!$form->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param Form $form
     * @throws StaleObjectException
     */
    public function remove(Form $form): void
    {
        if (!$form->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
