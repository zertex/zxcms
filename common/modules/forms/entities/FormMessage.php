<?php

namespace common\modules\forms\entities;

use common\modules\forms\entities\queries\FormMessageQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * @property int $id
 * @property int $form_id
 * @property string $data
 * @property int $new
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Form $form
 *
 */
class FormMessage extends ActiveRecord
{
	const STATUS_OLD = 0;
	const STATUS_NEW = 1;

    public static function create(
    	$form_id,
	    $data
	): self
    {
        $message = new static();
        $message->form_id = $form_id;
        $message->data = $data;
        return $message;
    }

    public function edit(
	    $form_id,
	    $data
    ): void
    {
	    $this->form_id = $form_id;
	    $this->data = $data;
    }

    public static function tableName(): string
    {
        return '{{%forms_messages}}';
    }

    public function behaviors(): array
    {
        return [
	        TimestampBehavior::class,
        ];
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function attributeLabels(): array
    {
	    return [
	    	'form_id' => Yii::t('forms', 'Form'),
		    'data' => Yii::t('forms', 'Form Data'),
	    ];
    }

    public function getForm(): ActiveQuery
    {
    	return $this->hasOne(Form::class, ['id' => 'form_id']);
    }

	public static function find(): FormMessageQuery
	{
		return new FormMessageQuery(static::class);
	}
}
