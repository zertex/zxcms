<?php

namespace common\modules\forms\entities;

use common\modules\forms\entities\queries\FormQuery;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * @property int $id
 * @property string $name
 * @property int $complete_page_id
 * @property string $complete_text
 * @property int $created_at
 * @property int $updated_at
 * @property string $subject
 * @property string $from
 * @property string $reply
 * @property string $return
 * @property int $status
 * @property string $data
 * @property int $captcha
 *
 * @property FormMessage[] $messages
 *
 */
class Form extends ActiveRecord
{
	const STATUS_ACTIVE = 1;
	const STATUS_DRAFT = 0;

    public static function create(
    	$name,
	    $data,
	    $subject,
		$from, $reply, $return,
		$complete_text, $complete_page_id,
		$status,
		$captcha
	): self
    {
        $form = new static();
        $form->name = $name;
        $form->data = $data;
        $form->subject = $subject;
        $form->from = $from;
        $form->reply = $reply;
        $form->return = $return;
        $form->complete_text = $complete_text;
        $form->complete_page_id = $complete_page_id;
        $form->status = $status;
        $form->captcha = $captcha;
        return $form;
    }

    public function edit(
	    $name,
	    $data,
	    $subject,
	    $from, $reply, $return,
	    $complete_text, $complete_page_id,
	    $status,
		$captcha
    ): void
    {
	    $this->name = $name;
	    $this->data = $data;
	    $this->subject = $subject;
	    $this->from = $from;
	    $this->reply = $reply;
	    $this->return = $return;
	    $this->complete_text = $complete_text;
	    $this->complete_page_id = $complete_page_id;
	    $this->status = $status;
	    $this->captcha = $captcha;
    }

    public static function tableName(): string
    {
        return '{{%forms}}';
    }

    public function behaviors(): array
    {
        return [
	        TimestampBehavior::class,
        ];
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function attributeLabels(): array
    {
	    return [
	    	'name' => Yii::t('forms', 'Name'),
		    'data' => Yii::t('forms', 'Form'),
		    'subject' => Yii::t('forms', 'Subject'),
		    'from' => Yii::t('forms', 'From E-mail'),
		    'reply' => Yii::t('forms', 'Reply E-mail'),
		    'return' => Yii::t('forms', 'Return E-mail'),
		    'complete_text' => Yii::t('forms', 'Complete Text'),
		    'complete_page_id' => Yii::t('forms', 'Complete Page'),
		    'status' => Yii::t('forms', 'Status'),
		    'captcha' => Yii::t('forms', 'Use Captcha'),
	    ];
    }

    public function getMessages(): ActiveQuery
    {
    	return $this->hasMany(FormMessage::class, ['form_id' => 'id']);
    }

	public static function find(): FormQuery
	{
		return new FormQuery(static::class);
	}
}
