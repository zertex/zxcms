<?php

namespace common\modules\banners\repositories;

use common\modules\banners\entities\Banner;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class BannerRepository
{
    public function get($id): Banner
    {
        if (!$banner = Banner::findOne($id)) {
            throw new NotFoundException('Banner is not found.');
        }
        return $banner;
    }

    public function save(Banner $banner): void
    {
        if (!$banner->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param Banner $banner
     * @throws StaleObjectException
     */
    public function remove(Banner $banner): void
    {
        if (!$banner->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
