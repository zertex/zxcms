<?php

namespace common\modules\banners\repositories;

use common\modules\banners\entities\BannerPlace;
use core\repositories\NotFoundException;
use RuntimeException;
use yii\db\StaleObjectException;

class BannerPlaceRepository
{
    public function get($id): BannerPlace
    {
        if (!$place = BannerPlace::findOne($id)) {
            throw new NotFoundException('Banner place is not found.');
        }
        return $place;
    }

    public function save(BannerPlace $place): void
    {
        if (!$place->save()) {
            throw new RuntimeException('Saving error.');
        }
    }

    /**
     * @param BannerPlace $place
     * @throws StaleObjectException
     */
    public function remove(BannerPlace $place): void
    {
        if (!$place->delete()) {
            throw new RuntimeException('Removing error.');
        }
    }
}
