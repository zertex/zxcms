<?php

use common\modules\banners\entities\Banner;
use common\modules\banners\forms\search\BannerSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\banners\helpers\BannerHelper;
use yii\helpers\ArrayHelper;
use common\modules\banners\entities\BannerPlace;

/* @var $this yii\web\View */
/* @var $searchModel BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('banners', 'Banners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <p>
        <?= Html::a(Yii::t('banners','Create Banner'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'title',
                        'value' => function (Banner $model) {
                            return Html::a(Html::encode($model->title), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    'url:url',
	                [
	                	'attribute' => 'start_at',
		                'value' => function(Banner $banner) {
            	            return date('d.m.Y H:i', $banner->start_at);
		                },
		                'options' => ['style' => 'width: 180px;'],
	                ],
	                [
		                'attribute' => 'end_at',
		                'value' => function(Banner $banner) {
			                return date('d.m.Y H:i', $banner->end_at);
		                },
		                'options' => ['style' => 'width: 180px;'],
	                ],
	                [
	                	'attribute' => 'place_id',
		                'filter' => ArrayHelper::map(BannerPlace::find()->all(), 'id', 'title'),
		                'value' => 'place.title',
		                'label' => Yii::t('banners', 'Place'),
	                ],
	                [
		                'attribute' => 'target',
		                'filter' => BannerHelper::targetList(),
		                'value' => function(Banner $banner) {
			                return BannerHelper::targetName($banner->target);
		                },
		                'options' => ['style' => 'width: 150px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
	                [
		                'attribute' => 'active',
		                'filter' => BannerHelper::statusList(),
		                'format' => 'raw',
		                'value' => function(Banner $banner) {
			                return BannerHelper::statusLabel($banner->active);
		                },
		                'options' => ['style' => 'width: 150px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
