<?php

use common\modules\banners\entities\Banner;
use common\modules\banners\forms\BannerForm;

/* @var $this yii\web\View */
/* @var $banner Banner */
/* @var $model BannerForm */

$this->title = Yii::t('banners', 'Update Banner: {name}', ['name' => $banner->title]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('banners', 'Banners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $banner->title, 'url' => ['view', 'id' => $banner->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="banner-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
