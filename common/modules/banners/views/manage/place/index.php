<?php

use common\modules\banners\entities\BannerPlace;
use common\modules\banners\forms\search\BannerSearch;
use common\modules\banners\helpers\BannerHelper;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('banners', 'Places');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="place-index">
    <div class="card">
        <div class="card-header">
            <?= Html::a(Yii::t('banners','Create Place'), ['create'], ['class' => 'btn btn-sm btn-success']) ?>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'title',
                        'value' => function (BannerPlace $model) {
                            return Html::a(Html::encode($model->title), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    'width',
	                'height',
	                [
	                	'attribute' => 'active',
		                'filter' => BannerHelper::statusList(),
		                'format' => 'raw',
		                'value' => function(BannerPlace $place) {
            	            return BannerHelper::statusLabel($place->active);
		                },
		                'options' => ['style' => 'width: 150px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
