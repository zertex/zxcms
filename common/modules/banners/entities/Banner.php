<?php

namespace common\modules\banners\entities;

use common\modules\banners\entities\queries\BannerQuery;
use DomainException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "banners".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $url
 * @property string $target
 * @property int $active
 * @property int $start_at
 * @property int $end_at
 * @property int $created_at
 * @property int $updated_at
 * @property string $include_urls
 * @property string $exclude_urls
 * @property int $views
 * @property int $clicks
 * @property int $place_id
 *
 * @property BannerPlace $place
 */
class Banner extends ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    const TARGET_BLANK = '_blank';
    const TARGET_SELF = '_self';

    const FILE_ORIGINAL_PATH = '@staticRoot/origin/banners';

    public static function create(
        $title,
        $image,
        $url,
        $target,
        $start_at,
        $end_at,
        $include_urls,
        $exclude_urls,
        $active,
        $place_id
    ): self {
        $banner               = new static();
        $banner->title        = $title;
        $banner->image        = $image;
        $banner->url          = $url;
        $banner->target       = $target;
        $banner->start_at     = $start_at;
        $banner->end_at       = $end_at;
        $banner->include_urls = $include_urls;
        $banner->exclude_urls = $exclude_urls;
        $banner->active       = $active;
        $banner->place_id     = $place_id;

        return $banner;
    }

    public function setImage(UploadedFile $image): void
    {
        $this->image = $image;
    }


    public function edit(
        $title,
        $image,
        $url,
        $target,
        $start_at,
        $end_at,
        $include_urls,
        $exclude_urls,
        $active,
        $place_id
    ): void {
        $this->title        = $title;
        $this->image        = $image;
        $this->url          = $url;
        $this->target       = $target;
        $this->start_at     = $start_at;
        $this->end_at       = $end_at;
        $this->include_urls = $include_urls;
        $this->exclude_urls = $exclude_urls;
        $this->active       = $active;
        $this->place_id     = $place_id;
    }

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'banners';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels(): array
    {
        return [
            'id'           => Yii::t('banners', 'ID'),
            'title'        => Yii::t('banners', 'Title'),
            'url'          => Yii::t('banners', 'URL'),
            'image'        => Yii::t('banners', 'Image'),
            'target'       => Yii::t('banners', 'Target'),
            'active'       => Yii::t('banners', 'Status'),
            'start_at'     => Yii::t('banners', 'Start At'),
            'end_at'       => Yii::t('banners', 'End At'),
            'created_at'   => Yii::t('banners', 'Created At'),
            'updated_at'   => Yii::t('banners', 'Updated At'),
            'include_urls' => Yii::t('banners', 'Show only on URLs'),
            'exclude_urls' => Yii::t('banners', 'Not show on URLs'),
            'views'        => Yii::t('banners', 'Views'),
            'clicks'       => Yii::t('banners', 'Visits'),
            'place_id'     => Yii::t('banners', 'Place'),
        ];
    }

    public function activate(): void
    {
        if ($this->isActive()) {
            throw new DomainException('Banner is already active.');
        }
        $this->active = self::STATUS_ACTIVE;
    }

    public function draft(): void
    {
        if ($this->isDraft()) {
            throw new DomainException('Banner is already draft.');
        }
        $this->active = self::STATUS_DRAFT;
    }

    public function isActive(): bool
    {
        return $this->active == self::STATUS_ACTIVE;
    }


    public function isDraft(): bool
    {
        return $this->active == self::STATUS_DRAFT;
    }

    public function getPlace(): ActiveQuery
    {
        return $this->hasOne(BannerPlace::class, ['id' => 'place_id']);
    }

    ######################################

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public static function find(): BannerQuery
    {
        return new BannerQuery(static::class);
    }
}
