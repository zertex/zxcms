<?php

namespace common\modules\banners\entities\queries;

use common\modules\banners\entities\BannerPlace;
use yii\db\ActiveQuery;

/**
 * Class BannerPlaceQuery
 *
 * @see BannerPlace
 */
class BannerPlaceQuery extends ActiveQuery
{
    /**
     * @param null $alias
     * @return $this
     */
    public function active($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'active' => BannerPlace::STATUS_ACTIVE,
        ]);
    }
}
