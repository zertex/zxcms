<?php

namespace common\modules\banners\entities\queries;

use common\modules\banners\entities\Banner;
use yii\db\ActiveQuery;

/**
 * Class BannerQuery
 *
 * @see Banner
 */
class BannerQuery extends ActiveQuery
{
    /**
     * @return BannerQuery
     */
    public function active()
    {
        return $this->andWhere(['active' => Banner::STATUS_ACTIVE]);
    }

    /**
     * @return BannerQuery
     */
    public function showTime()
    {
        return $this->andWhere(['<', 'start_at', time()])->andWhere(['>', 'end_at', time()]);
    }

    /*public function excludeFree()
    {
	    $current = Url::current([]);
	    return $this->andWhere(['not rlike', 'exclude_urls', '^'.$current.'$']);
    }*/
}
