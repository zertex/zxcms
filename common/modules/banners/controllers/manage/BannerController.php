<?php

namespace common\modules\banners\controllers\manage;

use common\modules\banners\entities\Banner;
use common\modules\banners\forms\BannerForm;
use common\modules\banners\forms\search\BannerSearch;
use common\modules\banners\services\BannerManageService;
use DomainException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;
use yii\web\Response;

class BannerController extends Controller
{
    private BannerManageService $service;

    public function __construct($id, $module, BannerManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['BannersManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel  = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'banner' => $this->findModel($id),
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate(): string|Response
    {
        $form = new BannerForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $form = $this->service->create($form);

                return $this->redirect(['view', 'id' => $form->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        } else {
            $form->active = $form->active ?: Banner::STATUS_ACTIVE;
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id): Response|string
    {
        $banner         = $this->findModel($id);
        $form           = new BannerForm($banner);
        $form->start_at = date('d.m.Y H:i:s', $form->start_at);
        $form->end_at   = date('d.m.Y H:i:s', $form->end_at);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($banner->id, $form);

                return $this->redirect(['view', 'id' => $banner->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model'  => $form,
            'banner' => $banner,
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function actionDelete(int $id): Response
    {
        try {
            $this->service->remove($id);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     *
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): Banner
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested banner does not exist.');
    }
}
