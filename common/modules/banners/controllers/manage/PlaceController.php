<?php

namespace common\modules\banners\controllers\manage;

use common\modules\banners\entities\BannerPlace;
use common\modules\banners\forms\BannerPlaceForm;
use common\modules\banners\forms\search\PlaceSearch;
use common\modules\banners\services\BannerPlaceManageService;
use DomainException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;
use yii\web\Response;

class PlaceController extends Controller
{
    private BannerPlaceManageService $service;

    public function __construct($id, $module, BannerPlaceManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['BannersManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel  = new PlaceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'place' => $this->findModel($id),
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate(): string|Response
    {
        $form = new BannerPlaceForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $form = $this->service->create($form);

                return $this->redirect(['view', 'id' => $form->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        } else {
            $form->active = $form->active ?: BannerPlace::STATUS_ACTIVE;
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id): Response|string
    {
        $place = $this->findModel($id);

        $form = new BannerPlaceForm($place);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($place->id, $form);

                return $this->redirect(['view', 'id' => $place->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
            'place' => $place,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return Response
     */
    public function actionDelete(int $id): Response
    {
        try {
            $this->service->remove($id);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     *
     * @return BannerPlace the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): BannerPlace
    {
        if (($model = BannerPlace::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested place does not exist.');
    }
}
