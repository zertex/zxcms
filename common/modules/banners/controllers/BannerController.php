<?php
/**
 * Created by Error202
 * Date: 22.08.2018
 */

namespace common\modules\banners\controllers;

use common\modules\banners\entities\Banner;
use common\modules\banners\services\BannerManageService;
use frontend\components\FrontendController;
use Yii;
use yii\web\NotFoundHttpException;

class BannerController extends FrontendController
{
    private BannerManageService $service;

    public function __construct(string $id, $module, BannerManageService $service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionView()
    {
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        if (Yii::$app->request->isAjax && $id) {
            $banner = $this->findModel($id);
            $this->service->addView($banner);
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionClick()
    {
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        if (Yii::$app->request->isAjax && $id) {
            $banner = $this->findModel($id);
            $this->service->addClick($banner);
        }
    }

    /**
     * @param $id
     * @return Banner
     * @throws NotFoundHttpException
     */
    protected function findModel($id): Banner
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested banner does not exist.');
    }
}
