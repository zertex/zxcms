<?php

use yii\db\Migration;

/**
 * Class m180821_100959_add_banners_place_id_field
 */
class m180821_100959_add_banners_place_id_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%banners}}', 'place_id', $this->integer());

        $this->createIndex('idx_banners_place_id', '{{%banners}}', 'place_id');
        $this->addForeignKey('frg_banners_place_id_banners_places_id', '{{%banners}}', 'place_id', 'banners_places', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('frg_banners_place_id_banners_places_id', '{{%banners}}');
        $this->dropIndex('idx_banners_place_id', '{{%banners}}');

        $this->dropColumn('{{%banners}}', 'place_id');
    }
}
