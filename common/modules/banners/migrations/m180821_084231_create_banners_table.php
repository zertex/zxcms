<?php

use yii\db\Migration;

/**
 * Handles the creation of table `banners`.
 */
class m180821_084231_create_banners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%banners}}', [
            'id'           => $this->primaryKey(),
            'title'        => $this->string(255)->notNull(),
            'image'        => $this->string(255)->notNull(),
            'url'          => $this->string(255),
            'target'       => $this->string(15)->defaultValue('_blank'),
            'active'       => $this->integer(1)->defaultValue(1),
            'start_at'     => $this->integer()->unsigned(),
            'end_at'       => $this->integer()->unsigned(),
            'created_at'   => $this->integer()->unsigned(),
            'updated_at'   => $this->integer()->unsigned(),
            'include_urls' => 'LONGTEXT',
            'exclude_urls' => 'LONGTEXT',
            'views'        => $this->integer()->unsigned()->defaultValue(0),
            'clicks'       => $this->integer()->unsigned()->defaultValue(0),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%banners}}');
    }
}
