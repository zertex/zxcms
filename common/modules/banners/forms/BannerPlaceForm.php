<?php

namespace common\modules\banners\forms;

use common\modules\banners\entities\BannerPlace;
use yii\base\Model;
use Yii;

class BannerPlaceForm extends Model
{
    public ?string $title = null;
    public ?int $width = null;
    public ?int $height = null;
    public ?int $active = null;

    public BannerPlace $place;

    public function __construct(BannerPlace $place = null, $config = [])
    {
        if ($place) {
            $this->title  = $place->title;
            $this->width  = $place->width;
            $this->height = $place->height;
            $this->active = $place->active;
            $this->place = $place;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['title'], 'required'],
            [['width', 'height', 'active'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'     => Yii::t('banners', 'ID'),
            'title'  => Yii::t('banners', 'Title'),
            'width'  => Yii::t('banners', 'Width'),
            'height' => Yii::t('banners', 'Height'),
            'active' => Yii::t('banners', 'Status'),
        ];
    }
}
