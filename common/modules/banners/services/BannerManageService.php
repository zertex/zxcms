<?php

namespace common\modules\banners\services;

use common\modules\banners\entities\Banner;
use common\modules\banners\forms\BannerForm;
use common\modules\banners\repositories\BannerRepository;
use Yii;
use yii\base\Exception;
use yii\base\Security;
use yii\db\StaleObjectException;

class BannerManageService
{
    private BannerRepository $repository;

    public function __construct(BannerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param BannerForm $form
     * @return Banner
     * @throws Exception
     */
    public function create(BannerForm $form): Banner
    {
        if ($form->image) {
            $filename = $form->image->baseName . '_' . (new Security())->generateRandomString(5) . '.' . $form->image->extension;
            $path     = Yii::getAlias(Banner::FILE_ORIGINAL_PATH);
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $form->image->saveAs($path . '/' . $filename);
            $form->image = $filename;
        }

        $banner = Banner::create(
            $form->title,
            $form->image,
            $form->url,
            $form->target,
            $form->start_at,
            $form->end_at,
            $form->include_urls,
            $form->exclude_urls,
            $form->active,
            $form->place_id
        );
        $this->repository->save($banner);

        return $banner;
    }

    /**
     * @param $id
     * @param BannerForm $form
     * @throws Exception
     */
    public function edit($id, BannerForm $form): void
    {
        $banner = $this->repository->get($id);

        if ($form->image) {
            $filename = $form->image->baseName . '_' . (new Security())->generateRandomString(5) . '.' . $form->image->extension;
            $path     = Yii::getAlias(Banner::FILE_ORIGINAL_PATH);
            $form->image->saveAs($path . '/' . $filename);
            $form->image = $filename;
        } else {
            $form->image = $banner->image;
        }

        $banner->edit(
            $form->title,
            $form->image,
            $form->url,
            $form->target,
            $form->start_at,
            $form->end_at,
            $form->include_urls,
            $form->exclude_urls,
            $form->active,
            $form->place_id
        );
        $this->repository->save($banner);
    }

    /**
     * @param $id
     * @throws StaleObjectException
     */
    public function remove($id): void
    {
        $banner = $this->repository->get($id);
        $this->repository->remove($banner);
    }

    public function addView(Banner $banner)
    {
        $banner->views++;
        $this->repository->save($banner);
    }

    public function addClick(Banner $banner)
    {
        $banner->clicks++;
        $this->repository->save($banner);
    }
}
