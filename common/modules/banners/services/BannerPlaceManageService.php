<?php

namespace common\modules\banners\services;

use common\modules\banners\entities\BannerPlace;
use common\modules\banners\forms\BannerPlaceForm;
use common\modules\banners\repositories\BannerPlaceRepository;
use yii\db\StaleObjectException;

class BannerPlaceManageService
{
    private BannerPlaceRepository $repository;

    public function __construct(BannerPlaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create(BannerPlaceForm $form): BannerPlace
    {
        $place = BannerPlace::create(
            $form->title,
            $form->width,
            $form->height,
            $form->active
        );
        $this->repository->save($place);

        return $place;
    }

    public function edit($id, BannerPlaceForm $form): void
    {
        $place = $this->repository->get($id);
        $place->edit(
            $form->title,
            $form->width,
            $form->height,
            $form->active
        );
        $this->repository->save($place);
    }

    /**
     * @param $id
     * @throws StaleObjectException
     */
    public function remove($id): void
    {
        $place = $this->repository->get($id);
        $this->repository->remove($place);
    }
}
