<?php

namespace common\modules\banners;

use core\components\modules\ModuleInterface;
use Yii;
use yii\base\Module;
use yii\helpers\ArrayHelper;
use yii\web\Application;

/**
 * blog module definition class
 */
class BannersModule extends Module implements ModuleInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\banners\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap(Application $app)
    {
        // add migration path
        $app->controllerMap['migrate']['migrationPath'][] = '@common/modules/banners/migrations';

        // add search rules
        $app->params['search_rules'][] = "SELECT title, title as content, CONCAT('/banners/manage/banner/view/', id) AS url FROM {{banners}}";

        $app->getUrlManager()->addRules([
            'banner/view'  => 'banners/banner/view',
            'banner/click' => 'banners/banner/click',
        ]);

        $app->getUrlManager()->addRules([
            'banners/manage/banner/view/<id:\d+>' => 'banners/manage/banner/view',
        ]);

        // add languages
        $app->getI18n()->translations = ArrayHelper::merge($app->getI18n()->translations, [
            'banners' => [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/banners/messages',
            ],
        ]);

        // add menu items
        if (basename(Yii::$app->getBasePath()) === 'backend') {
            $app->params['adminMenu'][] = [
                'label'   => Yii::t('banners', 'Banners'),
                'icon'    => 'fas fa-flag',
                'items'   => [
                    [
                        'label' => Yii::t('banners', 'Banners'),
                        'icon'  => 'fas fa-caret-right',
                        'url'   => ['/banners/manage/banner/index'],
                        //'active' => \Yii::$app->controller->getUniqueId() == 'banners/manage/banner'
                    ],
                    [
                        'label' => Yii::t('banners', 'Places'),
                        'icon'  => 'fas fa-caret-right',
                        'url'   => ['/banners/manage/place/index'],
                        //'active' => \Yii::$app->controller->getUniqueId() == 'banners/manage/place'
                    ],
                ],
                'visible' => $app->user->can('admin') || Yii::$app->user->can('BannersManagement'),
            ];
        }
    }
}
