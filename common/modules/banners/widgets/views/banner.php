<?php
/**
 * Created by Error202
 * Date: 22.08.2018
 */

use common\modules\banners\entities\Banner;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $banner Banner;
 */

$banner_id = 'banner_' . $banner->id;
$view_url = Url::to(['/banner/view']);
$click_url = Url::to(['/banner/click']);
$js = <<<JS
	$("#{$banner_id}").on('click', function(){
	   	$.ajax({
		  method: "POST",
		  url: "{$click_url}",
		  data: { id: "{$banner->id}" }
		}); 
	});

	$.ajax({
	  method: "POST",
	  url: "{$view_url}",
	  data: { id: "{$banner->id}" }
	});
JS;
$this->registerJs($js, $this::POS_READY);
?>

<?= Html::a(Html::img('@static/origin/banners/' . $banner->image), $banner->url, [
	'target' => $banner->target,
	'id' => $banner_id,
]) ?>
