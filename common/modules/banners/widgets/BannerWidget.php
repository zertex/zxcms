<?php
/**
 * Created by Error202
 * Date: 31.07.2018
 */

namespace common\modules\banners\widgets;

use common\modules\banners\entities\Banner;
use common\modules\banners\entities\BannerPlace;
use yii\base\Widget;

class BannerWidget extends Widget
{
    public int $id;

    public function run(): string
    {
        $place = BannerPlace::findOne($this->id);
        if (!$place) {
            return 'Place is not found';
        }

        $banners = Banner::find()
                         ->active()
                         ->showTime()
                         ->andWhere(['place_id' => $place->id])
                         ->all();

        if (!$banners || $place->isDraft()) {
            return '';
        }

        /* @var $banner Banner */
        $banner = $banners[array_rand($banners)];

        return $this->render('banner', [
            'banner' => $banner,
        ]);
    }
}
