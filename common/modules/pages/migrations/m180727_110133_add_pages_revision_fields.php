<?php

use yii\db\Migration;

/**
 * Class m180727_110133_add_pages_revision_fields
 */
class m180727_110133_add_pages_revision_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->addColumn('{{%pages}}', 'type', $this->integer(2)->defaultValue(0)); // 0 - public, 1 - revision, 2 - preview
		$this->addColumn('{{%pages}}', 'revision_at', $this->integer()->unsigned());
		$this->addColumn('{{%pages}}', 'revision_id', $this->integer());

		$this->dropIndex('{{%idx-pages-slug}}', '{{%pages}}');
		$this->createIndex('{{%idx-pages-slug}}', '{{%pages}}', 'slug');

		$this->createIndex('idx_pages_revision_id', '{{%pages}}', 'revision_id');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropIndex('idx_pages_revision_id', '{{%pages}}');
		$this->dropColumn('{{%pages}}', 'type');
		$this->dropColumn('{{%pages}}', 'revision_at');
		$this->dropColumn('{{%pages}}', 'revision_id');
	}
}
