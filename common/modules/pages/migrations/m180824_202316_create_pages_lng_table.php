<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages_lng`.
 */
class m180824_202316_create_pages_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = null;
	    if ($this->db->driverName === 'mysql') {
		    $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ENGINE=InnoDB';
	    }

        $this->createTable('{{%pages_lng}}', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer()->notNull(),
            'language' => $this->string(6)->notNull(),
            'title' => $this->string(255),
            'content' => 'MEDIUMTEXT',
        ], $tableOptions);

	    $this->createIndex('idx_pages_lng_language', '{{%pages_lng}}', 'language');
	    $this->createIndex('idx_pages_lng_page_id', '{{%pages_lng}}', 'page_id');
	    $this->addForeignKey('frg_pages_lng_pages_page_id_id', '{{%pages_lng}}', 'page_id', '{{%pages}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropForeignKey('frg_pages_lng_pages_page_id_id', '{{%pages_lng}}');
	    $this->dropIndex('idx_pages_lng_page_id', '{{%pages_lng}}');
	    $this->dropIndex('idx_pages_lng_language', '{{%pages_lng}}');
        $this->dropTable('{{%pages_lng}}');
    }
}
