<?php

use yii\db\Migration;

/**
 * Class m180827_195748_set_pages_unicode_collate
 */
class m180827_195748_set_pages_unicode_collate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tables = ['pages_lng'];
	    $db = Yii::$app->getDb();
	    $db->createCommand('SET FOREIGN_KEY_CHECKS=0;')->execute();

	    foreach ($tables as $table) {
		    $db->createCommand( "ALTER TABLE `$table` CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci" )->execute();
	    }

	    $db->createCommand('SET FOREIGN_KEY_CHECKS=1;')->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }
}
