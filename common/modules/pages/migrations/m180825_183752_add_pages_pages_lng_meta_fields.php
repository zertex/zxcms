<?php

use yii\db\Migration;

/**
 * Class m180825_183752_add_pages_pages_lng_meta_fields
 */
class m180825_183752_add_pages_pages_lng_meta_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->addColumn('{{%pages_lng}}', 'meta_title', $this->string());
	    $this->addColumn('{{%pages_lng}}', 'meta_description', $this->text());
	    $this->addColumn('{{%pages_lng}}', 'meta_keywords', $this->string());

	    $this->dropColumn('{{%pages}}', 'meta_json');
	    $this->dropColumn('{{%pages_lng}}', 'meta_json');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%pages_lng}}', 'meta_title');
	    $this->dropColumn('{{%pages_lng}}', 'meta_description');
	    $this->dropColumn('{{%pages_lng}}', 'meta_keywords');

	    $this->addColumn('{{%pages}}', 'meta_json', $this->text());
	    $this->addColumn('{{%pages_lng}}', 'meta_json', $this->text());
    }
}
