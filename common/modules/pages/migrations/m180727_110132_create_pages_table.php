<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages`.
 */
class m180727_110132_create_pages_table extends Migration
{
	public function up()
	{
		$tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

		$this->createTable('{{%pages}}', [
			'id' => $this->primaryKey(),
			'title' => $this->string()->notNull(),
			'slug' => $this->string()->notNull(),
			'content' => 'MEDIUMTEXT',
			'created_at' => $this->integer()->unsigned(),
			'updated_at' => $this->integer()->unsigned(),
			'meta_json' => $this->text()->notNull(),
			'tree' => $this->integer(),
			'lft' => $this->integer()->notNull(),
			'rgt' => $this->integer()->notNull(),
			'depth' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createIndex('{{%idx-pages-slug}}', '{{%pages}}', 'slug', true);
		$this->createIndex('lft', '{{%pages}}', ['tree', 'lft', 'rgt']);
		$this->createIndex('rgt', '{{%pages}}', ['tree', 'rgt']);

		$this->insert('{{%pages}}', [
			'id' => 1,
			'title' => '',
			'slug' => 'root',
			'content' => null,
			'meta_json' => '{}',
			'tree' => 1,
			'lft' => 1,
			'rgt' => 2,
			'depth' => 0,
		]);

		$this->insert('{{%pages}}', [
			'id' => 2,
			'title' => '',
			'slug' => 'temp',
			'content' => null,
			'meta_json' => '{}',
			'tree' => 2,
			'lft' => 1,
			'rgt' => 2,
			'depth' => 0,
		]);
	}

	public function down()
	{
		$this->dropTable('{{%pages}}');
	}
}
