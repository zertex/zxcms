<?php
/**
 * Created by Error202
 * Date: 10.07.2018
 */

namespace common\modules\pages\widgets;

use core\forms\menu\MenuItemForm;
use yii\base\Widget;

class MenuItemCreatorWidget extends Widget
{
	public int $menu_id;

	public function run(): string
    {
		$form = new MenuItemForm();
		$form->module = 'pages';
		$form->name = 'pages';
		$form->title_attr = 'pages';
		$form->menu_id = $this->menu_id;
		$form->url = '/pages/page/index';

		return $this->render('menu-item/creator', [
			'model' => $form,
		]);
	}
}