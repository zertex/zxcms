<?php

namespace common\modules\pages\entities;

use common\modules\pages\entities\queries\PageQuery;
use core\behaviors\LanguageBehavior;
use core\behaviors\SluggableRelationBehavior;
use paulzi\nestedsets\NestedSetsBehavior;
use yii\behaviors\TimestampBehavior;
use common\behaviors\WidgetContentBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * @property int $id
 * @property string $slug
 * @property int $created_at
 * @property int $updated_at
 * @property int $tree
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property int $type
 * @property int $revision_at
 * @property int $revision_id
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord[] $translation
 *
 * @property Page $parent
 * @property Page[] $parents
 * @property Page[] $children
 * @property Page $prev
 * @property Page $next
 * @mixin NestedSetsBehavior
 */
class Page extends ActiveRecord
{
    const TYPE_PUBLIC = 0;
    const TYPE_REVISION = 1;
    const TYPE_PREVIEW = 2;

    public $_form;

    public static function create($form, $slug, $type = Page::TYPE_PUBLIC): self
    {
        $page       = new static();
        $page->slug = $slug;
        $page->type = $type;

        $page->_form = $form;

        return $page;
    }

    public function edit($form, $slug, $type = Page::TYPE_PUBLIC): void
    {
        $this->slug = $slug;
        $this->type = $type;

        $this->_form = $form;
    }

    public function getSeoTitle(): string
    {
        return $this->translation->meta_title ?: $this->translation->title;
    }

    public static function tableName(): string
    {
        return '{{%pages}}';
    }

    public function behaviors(): array
    {
        return [
            [
                'class'         => NestedSetsBehavior::class,
                'treeAttribute' => 'tree',
            ],
            TimestampBehavior::class,
            WidgetContentBehavior::class,
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'PagesVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'page_id',
                'tableName'           => '{{%pages_lng}}',
                'attributes'          => ['title', 'content', 'meta_title', 'meta_description', 'meta_keywords'],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
                //'defaultLanguage' => basename(Yii::$app->getBasePath()) === 'backend' ? Yii::$app->language : Yii::$app->params['defaultLanguage'],
            ],
            [
                //'class' => SluggableBehavior::class,
                'class'     => SluggableRelationBehavior::class,
                'attribute' => 'title',
                'relation'  => 'translation',
                //'ensureUnique' => true,
                //'preserveNonEmptyValues' => true,
            ],
        ];
    }

    public function transactions(): array
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function attributeLabels()
    {
        return [
            'title'   => Yii::t('pages', 'Title'),
            'slug'    => Yii::t('pages', 'Slug'),
            'id'      => Yii::t('pages', 'ID'),
            'content' => Yii::t('pages', 'Content'),
        ];
    }

    public static function find(): PageQuery
    {
        return new PageQuery(static::class);
    }
}
