<?php

namespace common\modules\pages\controllers;

use common\modules\pages\repositories\read\PageReadRepository;
use frontend\components\FrontendController;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

class PageController extends FrontendController
{
    private PageReadRepository $pages;

    public $layout = 'page';

    public function __construct($id, $module, PageReadRepository $pages, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->pages = $pages;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow'   => true,
                        //'roles'   => ['Pages'],
                    ],
                    [
                        'actions' => ['preview'],
                        'allow'   => true,
                        'roles'   => ['PagesManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     * @internal param string $slug
     */
    public function actionView($id): string
    {
        if (!$page = $this->pages->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('view', [
            'page' => $page,
        ]);
    }

    /**
     * @param $id
     * @param string $language
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPreview($id, string $language = ''): string
    {
        if (!$page = $this->pages->findPreview($id, $language)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('view', [
            'page' => $page,
        ]);
    }
}
