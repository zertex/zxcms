<?php

namespace common\modules\pages\controllers\manage;

use common\modules\pages\forms\PageForm;
use common\modules\pages\services\PageManageService;
use common\modules\pages\entities\Page;
use common\modules\pages\forms\PageSearch;
use DomainException;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Yii;
use yii\web\Response;

class PageController extends Controller
{
    private PageManageService $service;

    public function __construct($id, $module, PageManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function behaviors(): array
    {
        return [
	        'access' => [
		        'class' => AccessControl::class,
		        'rules' => [
			        [
				        'allow' => true,
				        'roles' => ['PagesManagement'],
			        ],
			        [    // all the action are accessible to admin
				        'allow' => true,
				        'roles' => ['admin'],
			        ],
		        ],
	        ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'restore-history' => ['POST'],
                    'clear-history' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionView($id): string
    {
	    $history = Page::find()
	                       ->andWhere(['OR', ['revision_id' => $id], ['id' => $id]])
	                       ->orderBy(['revision_at' => SORT_DESC])
	                       ->limit(20)
	                       ->all();

        return $this->render('view', [
            'page' => $this->findModel($id),
	        'history' => $history,
        ]);
    }

    /**
     * @param string $language
     * @return Response|string
     * @throws InvalidConfigException
     */
	public function actionCreatePreview(string $language = ''): Response|string
    {
		$this->service->removePreviews();

		$form = new PageForm();
		$form->type = Page::TYPE_PREVIEW;
		if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			try {
				$page = $this->service->create($form, Page::TYPE_PREVIEW);
				return $this->redirect(Url::to(Yii::$app->get('frontendUrlManager')->createAbsoluteUrl(['/pages/page/preview', 'id' => $page->id, 'language' => $language])));
			} catch (DomainException $e) {
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}
		}
		return $this->render('create', [
			'model' => $form,
		]);
	}

    /**
     * @return string|Response
     */
    public function actionCreate(): string|Response
    {
        $form = new PageForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $page = $this->service->create($form);
                return $this->redirect(['view', 'id' => $page->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('create', [
            'model' => $form,
        ]);
    }

	/**
	 * @param $id
	 *
	 * @return string|Response
	 * @throws NotFoundHttpException
	 */
    public function actionUpdate($id): Response|string
    {
        $page = $this->findModel($id);

        $form = new PageForm($page);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->service->edit($page->id, $form);
                return $this->redirect(['view', 'id' => $page->id]);
            } catch (DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('update', [
            'model' => $form,
            'page' => $page,
        ]);
    }

    /**
     * @param integer $id
     * @return Response
     */
    public function actionDelete(int $id): Response
    {
        try {
            $this->service->remove($id);
        } catch (DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return Response
     */
    public function actionMoveUp(int $id): Response
    {
        $this->service->moveUp($id);
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return Response
     */
    public function actionMoveDown(int $id): Response
    {
        $this->service->moveDown($id);
        return $this->redirect(['index']);
    }

	public function actionPageSearch($q = null, $id = null)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$out = ['results' => ['id' => '', 'text' => '']];
		if (!is_null($q)) {
			$data = [];
			$pages = Page::find()
			             ->with('translation')
			             ->leftJoin('{{%pages_lng}}', '`pages_lng`.`page_id` = `pages`.`id`')
			             ->andWhere(['tree' => 1])
			             ->andWhere(['like', 'pages_lng.title', $q])
			             ->limit(20)
			             ->all();

			foreach ($pages as $page) {
				$data[] = [
					'id' => $page->id,
					'text' => isset($page->translation) ? $page->translation->title : null,
				];
			}
			$out['results'] = array_values($data);
		}
		elseif ($id > 0) {
			$tag_name = Page::findOne($id)->translate->title;
			$out['results'] = ['id' => $tag_name, 'text' => $tag_name];
		}
		else {
			$data = [];
			$pages = Page::find()->andWhere(['tree' => 1])->orderBy(['id' => SORT_DESC])->limit(20)->all();

			foreach ($pages as $page) {
				$data[] = [
					'id' => $page->id,
					'text' => isset($page->translation) ? $page->translation->title : null,
				];
			}
			$out['results'] = array_values($data);
		}
		return $out;
	}

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
	public function actionRestoreHistory($id): Response
    {
		$page = $this->findModel($id);
		if ($page_id = $page->revision_id) {
			$this->service->restoreHistory($id, $page->revision_id);
			return $this->redirect(['/pages/manage/page/view', 'id' => $page_id]);
		}
		return $this->redirect(['/pages/manage/page/index']);
	}

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
	public function actionClearHistory($id): Response
    {
		$page = $this->findModel($id);
		$this->service->clearHistory($page);
		return $this->redirect(['/pages/manage/page/view', 'id' => $page->id]);
	}

    /**
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id): Page
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
