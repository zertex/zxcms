<?php

return [
    'version'     => '1.0.1',
    'name'        => 'pages',
    'description' => 'Custom pages on site with slug',
    'module'      => 'PagesModule',
    'permissions' => [
        'PagesManagement' => 'Manage website pages',
    ],
];
