<?php

use common\modules\pages\entities\Page;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $page Page */

$this->title = $page->getSeoTitle();

$this->registerMetaTag(['name' => 'description', 'content' => $page->translation->meta_description]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $page->translation->meta_keywords]);

foreach ($page->parents as $parent) {
    if (!$parent->isRoot()) {
        $this->params['breadcrumbs'][] = ['label' => $parent->title, 'url' => ['view', 'id' => $parent->id]];
    }
}
$this->params['breadcrumbs'][] = $page->translation->title;
?>
<article class="page-view">

    <h1><?= Html::encode($page->translation->title) ?></h1>

    <?= $page->content(Yii::$app->formatter->asHtml($page->translation->content, [
        'Attr.AllowedRel'      => ['nofollow'],
        'HTML.SafeObject'      => true,
        'Output.FlashCompat'   => true,
        'HTML.SafeIframe'      => true,
        'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
    ])) ?>

</article>
