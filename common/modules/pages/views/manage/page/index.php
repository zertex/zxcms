<?php

use common\modules\pages\entities\Page;
use common\modules\pages\forms\PageSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('pages', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <div class="card">
        <div class="card-header">
            <?= Html::a(Yii::t('pages','Create Page'), ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <div class="card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'title',
                        'value' => function (Page $model) {
                            $indent = ($model->depth > 1 ? str_repeat('&nbsp;&nbsp;', $model->depth - 1) . ' ' : '');
                            $title = isset($model->findTranslation(Yii::$app->language)->title) ? $model->findTranslation(Yii::$app->language)->title : $model->findTranslation(Yii::$app->params['defaultLanguage'])->title;
                            return $indent . Html::a(Html::encode($title), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'value' => function (Page $model) {
                            return
                                Html::a('<i class="fas fa-arrow-alt-circle-up"></i>', ['move-up', 'id' => $model->id]) .
                                Html::a('<i class="fas fa-arrow-alt-circle-down"></i>', ['move-down', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['style' => 'text-align: center'],
                    ],
                    'slug',
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
