<?php

use common\modules\pages\forms\PageForm;

/* @var $this yii\web\View */
/* @var $model PageForm */

$this->title = Yii::t('pages', 'Create Page');
$this->params['breadcrumbs'][] = ['label' => Yii::t('pages', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
