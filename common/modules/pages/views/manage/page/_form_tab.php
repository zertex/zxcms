<?php
/**
 * Created by Error202
 * Date: 24.08.2018
 */

use common\modules\pages\forms\PageForm;
use yii\web\View;
use yii\widgets\ActiveForm;
use zertex\ckeditor\CKEditor;

/**
 * @var $this View
 * @var $form ActiveForm
 * @var $model PageForm
 * @var $language string
 */

$postfix = $language == Yii::$app->params['defaultLanguage'] ? '' : '_' . $language;
?>

<div class="mt-4"></div>
<?= $form->field($model, 'title' . $postfix)->textInput(['maxlength' => true]) ?>
<?= $form->field($model, 'content' . $postfix)->widget(CKEditor::class) ?>

<div class="card">
	<div class="card-header with-border"><?= Yii::t('pages', 'SEO') ?></div>
	<div class="card-body">
		<?= $form->field($model, 'meta_title' . $postfix)->textInput() ?>
		<?= $form->field($model, 'meta_description' . $postfix)->textarea(['rows' => 2]) ?>
		<?= $form->field($model, 'meta_keywords' . $postfix)->textInput() ?>
	</div>
</div>
