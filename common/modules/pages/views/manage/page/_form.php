<?php

use common\modules\pages\forms\PageForm;
use core\components\bootstrap4\widgets\Tab4;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model PageForm */
/* @var $form yii\widgets\ActiveForm */

$js2 = '
$(".hint-block").each(function () {
    var $hint = $(this);
    var label = $hint.parent().find("label");
    label.html(label.html() + \' <i style="color:#3c8dbc" class="fa fa-question-circle" aria-hidden="true"></i>\');
    label.addClass("help").popover({
        html: true,
        trigger: "hover",
        placement: "bottom",
        content: $hint.html()
    });
    $(this).hide();
});
';
$this->registerJs($js2);
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-10">

            <div class="card">
                <div class="card-header with-border"><?= Yii::t('pages', 'Common') ?></div>
                <div class="card-body">
                    <?= $form->field($model, 'parentId')->dropDownList($model->parentsList()) ?>
                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <?php
            $items = [];
            foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
                $items[] = [
                    'label'   => $language_name,
                    'content' => $this->render('_form_tab', [
                        'form'     => $form,
                        'model'    => $model,
                        'language' => $language,
                    ]),
                ];
            }
            ?>

            <div class="card">
                <div class="card-body">
                    <div class="nav-tabs-custom">
                        <?= Tab4::widget([
                            'items' => $items
                        ]) ?>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>



        </div>

        <div class="col-md-2">
            <div class="card">
                <div class="card-header with-border"><?= Yii::t('pages', 'Publish') ?></div>
                <div class="card-body">

                    <div class="btn-group">
                        <button type="button" class="btn btn-info btn-sm"><?= Yii::t('pages', 'Preview on site') ?></button>
                        <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <?php foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) : ?>
                                <li>
                                    <?= Html::submitButton($language_name, [
                                        'class'      => 'btn btn-block btn-flat bg-white',
                                        'value'      => 'preview',
                                        'name'       => 'submit_preview',
                                        'formaction' => Url::to([
                                            '/pages/manage/page/create-preview',
                                            'language' => $language == Yii::$app->params['defaultLanguage'] ? '' : $language
                                        ]),
                                        'formtarget' => '_blank',
                                        'style'      => 'border:0; background-color:#ffffff;',
                                    ]) ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
