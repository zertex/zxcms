<?php

use common\modules\pages\entities\Page;
use common\modules\pages\forms\PageForm;

/* @var $this yii\web\View */
/* @var $page Page */
/* @var $model PageForm */

$this->title = Yii::t('pages', 'Update Page: {name}', ['name' => $page->translation->title]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('pages', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $page->translation->title, 'url' => ['view', 'id' => $page->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="page-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
