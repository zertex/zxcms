<?php

use common\modules\pages\entities\Page;
use core\components\bootstrap4\widgets\Tab4;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $page Page */
/* @var $history Page[] */

$this->title                   = $page->translation->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('pages', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.detail-view th {
	width: 25%;
}
CSS;
$this->registerCss($css);
?>
<div class="page-view">

    <p>
        <?= Html::a(Yii::t('pages', 'Pages'), ['index'], ['class' => 'btn btn-outline-primary btn-sm']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $page->id], ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $page->id], [
            'class' => 'btn btn-danger btn-sm',
            'data'  => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-9">

            <div class="card">
                <div class="card-header">
                    <?= Yii::t('pages', 'Common') ?>
                </div>
                <div class="card-body">
                    <?= DetailView::widget([
                        'model'      => $page,
                        'attributes' => [
                            'id',
                            'slug',
                        ],
                    ]) ?>
                </div>
            </div>

            <?php
            $items = [];
            foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
                $items[] = [
                    'label'   => $language_name,
                    'content' => $this->render('_view_tab', [
                        'page'     => $page,
                        'language' => $language,
                    ]),
                ];
            }
            ?>

                <div class="nav-tabs-custom">
                    <?= Tab4::widget([
                        'items' => $items,
                        'template' => '<div class="card"><div class="card-header">{headers}</div><div class="card-body">{panes}</div></div>'
                    ]) ?>
                </div>


        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-header"><?= Yii::t('pages', 'History') ?></div>
                <div class="card-body">

                    <?php if ($history): ?>
                        <ul style="padding: 0; font-size: 0.9em;">
                            <?php foreach ($history as $item): ?>
                                <li>
                                    <?php if ($item->revision_id): ?>
                                        <?= date('d.m.Y H:i', $item->revision_at) ?>
                                        →

                                        <?= Html::a(Yii::t('pages', 'View'),
                                            Url::to(Yii::$app->get('frontendUrlManager')->createAbsoluteUrl([
                                                '/pages/page/preview',
                                                'id' => $item->id
                                            ])), [
                                                'style'  => 'font-size:11px;',
                                                'target' => '_blank',
                                            ]) ?>

                                        |

                                        <?= Html::a(Yii::t('pages', 'Restore'),
                                            ['/pages/manage/page/restore-history', 'id' => $item->id], [
                                                'style' => 'font-size:11px; color: red',
                                                'data'  => [
                                                    'confirm' => Yii::t('pages',
                                                        'Are you sure you want to restore this history item?'),
                                                    'method'  => 'post',
                                                ],
                                            ]) ?>

                                    <?php else: ?>
                                        <strong><?= Yii::t('pages', 'Current Edition') ?></strong>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                        <?= Html::a(Yii::t('pages', 'Clear History'),
                            ['/pages/manage/page/clear-history', 'id' => $page->id], [
                                'class' => 'btn btn-danger btn-sm pull-right',
                                'data'  => [
                                    'confirm' => Yii::t('pages', 'Are you sure you want to remove this history?'),
                                    'method'  => 'post',
                                ],
                            ]) ?>
                    <?php else: ?>
                        <div style="padding: 20px 0; text-align: center"><?= Yii::t('pages',
                                'History is empty') ?></div>
                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>

</div>
