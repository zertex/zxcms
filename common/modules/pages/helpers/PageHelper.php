<?php
/**
 * Created by Error202
 * Date: 27.07.2018
 */

namespace common\modules\pages\helpers;

use common\modules\pages\entities\Page;
use common\modules\pages\forms\PageForm;

class PageHelper
{
	public static function saveRevision(Page $model) {
		if (!$model->revision_id) {

			$model->revision_at = time();

			$pageForm = new PageForm($model);

			$page = Page::create(
				$pageForm,
				$model->slug,
				Page::TYPE_REVISION
			);

			$page->revision_at = $model->updated_at;
			$page->revision_id = $model->id;

			$parent = Page::find()->andWhere(['slug' => 'temp'])->andWhere(['depth' => 0])->one();
			$page->appendTo($parent);
			$page->save();
		}
	}
}
