<?php

namespace common\modules\pages\forms;

use core\components\LanguageDynamicModel;
use common\modules\pages\entities\Page;
use core\validators\SlugValidator;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use Yii;

class PageForm extends LanguageDynamicModel
{
    public ?int $type       = null;
    public ?string $title   = null;
    public ?string $slug    = null;
    public ?string $content = null;
    public ?int $parentId   = null;

    public ?string $meta_title          = null;
    public ?string $meta_description    = null;
    public ?string $meta_keywords       = null;

    public ?Page $page = null;

    public function __construct(Page $page = null, array $attributes = [], $config = [])
    {
        if ($page) {
            $this->slug     = $page->slug;
            $this->parentId = $page->parent?->id;

            $this->page = $page;
        }
        parent::__construct($page, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['title'], 'required'],
                [['parentId'], 'integer'],
                [['title', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
                [['content', 'meta_description'], 'string'],
                ['slug', SlugValidator::class],
                [
                    ['slug'],
                    'unique',
                    'targetClass' => Page::class,
                    'filter'      => function (ActiveQuery $query) {
                        if ($this->type != Page::TYPE_PUBLIC) {
                            $query->andWhere($this->type . '=' . Page::TYPE_PUBLIC);
                        }

                        $query->andWhere(['type' => Page::TYPE_PUBLIC]);
                        if ($this->page) {
                            $query->andWhere(['<>', 'id', $this->page->id]);
                        }

                        return $query;
                    }
                ],
            ]
        );
    }

    public function attributeLabels(): array
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'title'            => Yii::t('pages', 'Title'),
                'slug'             => Yii::t('pages', 'Slug'),
                'id'               => Yii::t('pages', 'ID'),
                'content'          => Yii::t('pages', 'Content'),
                'parentId'         => Yii::t('pages', 'Parent Page'),
                'meta_title'       => Yii::t('pages', 'META Title'),
                'meta_description' => Yii::t('pages', 'META Description'),
                'meta_keywords'    => Yii::t('pages', 'META Keywords'),
            ]
        );
    }

    public function attributeHints(): array
    {
        return array_merge(
            parent::attributeHints(),
            [
                'slug' => Yii::t('pages', 'SEO link will be generated automatically if not specified'),
            ]
        );
    }

    public function parentsList(): array
    {
        return ArrayHelper::map(Page::find()->andWhere(['tree' => 1])->orderBy('lft')->all(), 'id', function (Page $page) {
                return ($page->depth > 1 ? str_repeat('-- ', $page->depth - 1) . ' ' : '') . ($page->translation ? $page->translation->title : Yii::t('pages', '- no parent -'));
        });
    }
}
