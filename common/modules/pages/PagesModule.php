<?php

namespace common\modules\pages;

use common\modules\pages\widgets\MenuItemCreatorWidget;
use core\components\modules\ModuleInterface;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Application;

/**
 * page module definition class
 */
class PagesModule extends \yii\base\Module implements ModuleInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\pages\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function bootstrap(Application $app)
    {
        // add migration path
        $app->controllerMap['migrate']['migrationPath'][] = '@common/modules/pages/migrations';

        // add search rules
        $app->params['search_rules'][] = "
			SELECT title, content, CONCAT('/pages/manage/page/view/', page_id) AS url FROM {{pages_lng}}
		";

        $app->getUrlManager()->addRules([
            ['class' => 'common\modules\pages\urls\PageMainUrlRule'],
        ]);

        $app->getUrlManager()->addRules([
            'pages/manage/page/view/<id:\d+>' => 'pages/manage/page/view',
        ]);

        // add languages
        $app->getI18n()->translations = ArrayHelper::merge($app->getI18n()->translations, [
            'pages'       => [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/pages/messages',
            ],
            'page_public' => [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/pages/messages',
            ],
        ]);

        // add menu items
        if (basename($app->getBasePath()) === 'backend') {
            $app->params['adminMenu'][] = [
                'label'   => Yii::t('pages', 'Pages'),
                'icon'    => 'far fa-file-alt',
                'url'     => ['/pages/manage/page/index'],
                'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('PagesManagement')
            ];
        }
    }

    public static function getMenuItemCreator($menu_id): array
    {
        $widgets   = [];
        $widgets[] = [
            'id'      => 'pages',
            'title'   => Yii::t('pages', 'Pages'),
            'content' => MenuItemCreatorWidget::widget([
                'menu_id' => $menu_id,
            ]),
        ];

        return $widgets;
    }
}
