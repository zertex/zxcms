<?php

return [
    'version'     => '1.0.1',
    'name'        => 'links',
    'description' => 'Custom links in web site menu',
    'module'      => 'LinksModule',
];
