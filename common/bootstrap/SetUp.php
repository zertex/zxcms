<?php

namespace common\bootstrap;

//use Elasticsearch\Client;
//use Elasticsearch\ClientBuilder;
//use core\components\modules\ModuleInterface;
use common\modules\languages\entities\Language;
use core\entities\ModuleRecord;

//use League\Flysystem\Adapter\Ftp;
//use League\Flysystem\Filesystem;
use common\modules\shop\cart\ShopCart;
use common\modules\shop\cart\cost\calculator\DynamicCost;
use common\modules\shop\cart\cost\calculator\SimpleCost;
use common\modules\shop\cart\storage\HybridStorage;
use core\dispatchers\AsyncEventDispatcher;
use core\dispatchers\DeferredEventDispatcher;
use core\dispatchers\EventDispatcher;
use core\dispatchers\SimpleEventDispatcher;

//use core\entities\behaviors\FlySystemImageUploadBehavior;
//use shop\entities\Shop\Product\events\ProductAppearedInStock;
use core\jobs\AsyncEventJobHandler;
use core\listeners\user\UserSignupConfirmedListener;
use core\listeners\user\UserSignupRequestedListener;

//use shop\repositories\events\EntityPersisted;
//use shop\repositories\events\EntityRemoved;
use core\services\newsletter\FakeSubscribe;
use core\services\newsletter\MailChimp;
use core\services\newsletter\Newsletter;

//use shop\services\sms\LoggedSender;
//use shop\services\sms\SmsRu;
//use shop\services\sms\SmsSender;
//use shop\services\yandex\ShopInfo;
//use shop\services\yandex\YandexMarket;
use core\events\user\UserSignUpConfirmed;
use core\events\user\UserSignUpRequested;
use core\services\ContactService;
use yii\base\BootstrapInterface;
use yii\base\ErrorHandler;
use yii\caching\Cache;
use yii\di\Container;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\mail\MailerInterface;
use yii\rbac\ManagerInterface;
use yii\queue\Queue;
use Yii;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;

        /*$container->setSingleton(Client::class, function () {
            return ClientBuilder::create()->build();
        });*/

        $container->setSingleton(MailerInterface::class, function () use ($app) {
            return $app->mailer;
        });

        $container->setSingleton(ErrorHandler::class, function () use ($app) {
            return $app->errorHandler;
        });

        $container->setSingleton(Queue::class, function () use ($app) {
            return $app->get('queue');
        });

        $container->setSingleton(Cache::class, function () use ($app) {
            return $app->cache;
        });

        $container->setSingleton(ManagerInterface::class, function () use ($app) {
            return $app->authManager;
        });

        $container->setSingleton(ContactService::class, [], [
            $app->params['adminEmail']
        ]);

        $container->setSingleton(ShopCart::class, function () use ($app) {
            return new ShopCart(
                new HybridStorage($app->get('user'), 'cart', 3600 * 24, $app->db),
                new DynamicCost(new SimpleCost())
            );
        });
/*
        $container->setSingleton(YandexMarket::class, [], [
            new ShopInfo($app->name, $app->name, $app->params['frontendHostInfo']),
        ]);*/

        $container->setSingleton(Newsletter::class, function () use ($app) {
            //return new MailChimp(
            //    new \DrewM\MailChimp\MailChimp($app->params['mailChimpKey']),
            //    $app->params['mailChimpListId']
            //);
            return new FakeSubscribe();
        });

        /*$container->setSingleton(SmsSender::class, function () use ($app) {
            return new LoggedSender(
                new SmsRu($app->params['smsRuKey']),
                \Yii::getLogger()
            );
        });*/

        $container->setSingleton(EventDispatcher::class, DeferredEventDispatcher::class);

        $container->setSingleton(DeferredEventDispatcher::class, function (Container $container) {
            return new DeferredEventDispatcher(new AsyncEventDispatcher($container->get(Queue::class)));
        });

        $container->setSingleton(SimpleEventDispatcher::class, function (Container $container) {
            return new SimpleEventDispatcher($container, [
                UserSignUpRequested::class => [UserSignupRequestedListener::class],
                UserSignUpConfirmed::class => [UserSignupConfirmedListener::class],
                //ProductAppearedInStock::class => [ProductAppearedInStockListener::class],
                //EntityPersisted::class => [
                //    ProductSearchPersistListener::class,
                //    CategoryPersistenceListener::class,
                //],
                //EntityRemoved::class => [
                //    ProductSearchRemoveListener::class,
                //    CategoryPersistenceListener::class,
                //],
            ]);
        });

        $container->setSingleton(AsyncEventJobHandler::class, [], [
            Instance::of(SimpleEventDispatcher::class)
        ]);

        /*
        $container->setSingleton(Filesystem::class, function () use ($app) {
            return new Filesystem(new Ftp($app->params['ftp']));
        });

        $container->set(ImageUploadBehavior::class, FlySystemImageUploadBehavior::class);
        */

        // Set frontend languages
        if (\Yii::$app->moduleManager->isTableExist('{{%languages}}') && file_exists(Yii::getAlias('@common/modules/languages/LanguagesModule.php'))) {
            $language                                = Language::find()->active()->default()->one();
            Yii::$app->params['defaultLanguage']     = $language ? $language->name : 'ru';
            Yii::$app->params['translatedLanguages'] = ArrayHelper::map(Language::find()->active()->all(), 'name', 'title');
        } else {
            Yii::$app->params['defaultLanguage']     = 'ru';
            Yii::$app->params['translatedLanguages'] = ['ru'];
        }

        // Set backend languages
        if (basename($app->getBasePath()) === 'backend') {
            $app->language                   = !$app->user->isGuest && $app->user->identity->user->backend_language ? $app->user->identity->user->backend_language : Yii::$app->params['defaultLanguage'];
            $app->params['frontendLanguage'] = Yii::$app->session->get('frontendLanguage', Yii::$app->params['defaultLanguage']);
        }

        // Connect common modules
        if (\Yii::$app->moduleManager->isTableExist('{{%modules}}')) {
            $modules = ModuleRecord::find()->andWhere(['type' => 'common'])->andWhere(['active' => 1])->all();
            foreach ($modules as $module) {
                Yii::$app->setModule($module->name, [
                    'class' => $module->class,
                ]);
                Yii::$app->getModule($module->name)->bootstrap(Yii::$app);
            }
        } else { // connect all existing modules
            $this->connectExistingModules();
        }
    }

    private function connectExistingModules(): void
    {
        $modules = Yii::$app->moduleManager->getLocalModules();
        //print_r($modules); die;
        foreach ($modules as $module) {
            Yii::$app->setModule($module['name'], [
                'class' => 'common\modules\\' . $module['name'] . '\\' . $module['module'],
            ]);
            Yii::$app->getModule($module['name'])->bootstrap(Yii::$app);
        }
    }
}
