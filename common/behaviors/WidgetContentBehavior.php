<?php
/**
 * Created by Error202
 * Date: 08.08.2018
 */

namespace common\behaviors;

use yii\helpers\Html;
use yii\base\Behavior;

class WidgetContentBehavior extends Behavior
{
    /**
     * Parse Widgets #?= ... ?#
     *
     * @param $text
     *
     * @return string
     */
    public function content($text): string
    {
        $content = preg_replace_callback('/\[\?= (.*)\((.*)\) \?\]/m', function ($m) {
            $array  = [];
            $string = Html::decode($m[2]);
            eval("\$array = $string;");

            return $m[1]($array);
        }, $text);

        return $content;
    }
}
