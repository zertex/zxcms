<?php

use core\components\modules\ModuleManager;
use yii\queue\file\Queue;
use zertex\avatar_generator\AvatarGenerator;

return [
    'aliases'       => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath'    => dirname(dirname(__DIR__)) . '/vendor',
    'controllerMap' => [
        // Common migrations for the whole application
        'migrate' => [
            'class'         => 'yii\console\controllers\MigrateController',
            'migrationPath' => [
                '@console/migrations',
            ],
        ],
    ],
    'components'    => [
        'cache'         => [
            'class'     => 'yii\caching\FileCache',
            'cachePath' => '@common/runtime/cache',
        ],
        'authManager'   => [
            'class'           => 'yii\rbac\DbManager',
            'itemTable'       => '{{%auth_items}}',
            'itemChildTable'  => '{{%auth_item_children}}',
            'assignmentTable' => '{{%auth_assignments}}',
            'ruleTable'       => '{{%auth_rules}}',
        ],
        'queue'         => [
            //'class' => 'yii\queue\redis\Queue',
            //'as log' => 'yii\queue\LogBehavior',
            'class' => Queue::class,
            //'path' => '@runtime/queue',
            'path'  => '@common/runtime/queue',
        ],
        'avatar'        => [
            'class'              => AvatarGenerator::class,
            'images_folder'      => '@staticRoot/images/avatars',
            'images_url'         => '@static/images/avatars',
            'size_width'         => 300,            // default: 300
            'font_size'          => 200,             // default: 200
            'salt'               => 'my_cms_salt',        // salt for image file names
            'texture'            => ['sun', 'rain'],   // texture name
            'texture_over_image' => false,
            'text_over_image'    => false,
        ],
        'moduleManager' => [
            'class' => ModuleManager::class,
        ],
    ],
];
