<?php
return [
    'id' => 'app-common-tests',
    'basePath' => dirname(__DIR__),
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            //'identityClass' => 'common\models\User',
            'identityClass' => 'common\auth\Identity',
        ],
        /*'user' => [
	        'identityClass' => 'common\auth\Identity',
	        'enableAutoLogin' => true,
	        'identityCookie' => [
		        'name' => '_identity',
		        'httpOnly' => true,
		        'domain' => $params['cookieDomain'],
	        ],
	        'loginUrl' => ['auth/login'],
        ],*/
    ],
];
