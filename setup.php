<?php
/**
 * Created by Error202
 * Date: 03.09.2018
 */

namespace zertex;

use PDO;

class Setup
{
    private $_language;
    private $_type;

    private $_db_host;
    private $_db_name;
    private $_db_user;
    private $_db_pass;
    private $_db_connection;

    private $_username;
    private $_password;
    private $_email;

    private $_http_protocol;
    private $_domain;

    private $_systemModules = ['languages', 'pages', 'forms', 'links'];

    private $_l = [
        'ru' => [
            'Connection failed. Try again' => 'Ошибка соединения. Попробуйте снова',
            'MySQL setup complete' => 'Настройка MySQL завершена',
            'Installation complete' => 'Установка завершена',
            'Please set subdomains aliases in you domain DNS:' => 'Настройте DNS домена для поддоменов:',
            'Select initialization type' => 'Укажите тип установки',
            '[d] - Development' => '[d] - Для разработки',
            '[p] - Production' => '[p] - Для работы',
            'Select installer language' => 'Укажите язык установщика',
            'Language [en]: ' => 'Язык [en]',
            'Type [p]: ' => 'Тип [p]',
            'MySQL settings' => 'Параметры соединения с MySQL',
            'Host [localhost]: ' => 'Хост [localhost]: ',
            'Database name: ' => 'Название базы: ',
            'Database name must be set' => 'Название базы не должно быть пустым',
            'User: ' => 'Пользователь: ',
            'User must be set' => 'Имя пользователя не должно быть пустым',
            'Password: ' => 'Пароль: ',
            'Set your HTTP protocol (http/https)' => 'Укажите HTTP протокол (http/https)',
            'HTTP Protocol [http]: ' => 'HTTP протокол [http]: ',
            'Website domain name' => 'Домен сайта',
            'Example: site.com, domain.ru' => 'Например: site.com, domain.ru',
            'Domain name: ' => 'Домен: ',
            'Domain must be set' => 'Назание домена не должно быть пустым',
            'Create admin account' => 'Создание аккаунта администратора',
            'Username: ' => 'Имя пользователя: ',
            'Username must be set' => 'Имя пользователя не должно быть пустым',
            'E-mail must be set' => 'E-mail адрес не должен быть пустым',
            'E-mail must be correct' => 'Ошибка e-mail адреса',
            'Password must be set' => 'Пароль не может быть пустым',
            'Repeat password: ' => 'Повторите пароль: ',
            'Passwords must be equal' => 'Пароли должны совпадать',
            'Admin account complete' => 'Аккаунт администратора создан',
            'Prepare MySQL tables' => 'Подготовка MySQL таблиц',
            'Complete' => 'Готово',
            'Server HTTP configuration' => 'Конфигурация HTTP сервера',
            'Does your server use Apache? [y]: ' => 'Ваш сервер использует Apache? [y]: ',
            'Creating permissions' => 'Создание разрешений',
            'Activating modules' => 'Активация модулей',
        ],
    ];

    public function run()
    {
        // select language
        $this->selectLanguage();

        // select type
        $this->selectType();

        // run init
        if ($this->_type == 'd') {
            shell_exec('php ' . __DIR__ . '/init --env=Development --overwrite=y');
        }
        else {
            shell_exec('php ' . __DIR__ . '/init --env=Production --overwrite=y');
        }

        // config db
        while (!$this->setupMySQL()) {
            echo Console::log($this->l('Connection failed. Try again'), 'red') . PHP_EOL;
        }
        $this->setConfigMySQL();
        echo Console::log($this->l('MySQL setup complete'), 'green') . PHP_EOL;

        // apply migrations
        $this->runMigrations();

        // create admin
        $this->addAdmin();

        // setup domain data
        $this->setConfigDomains();

        // install modules
        $this->activateSystemModules();

        // install system permissions
        $this->addPermissions();

        // Add init settings (name, short_name, theme)
        $this->initSettings();

        // apache htaccess
        $this->apache();

        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('Installation complete'), 'yellow') . PHP_EOL;
        echo PHP_EOL;
        echo Console::log($this->l('Please set subdomains aliases in you domain DNS:'), 'normal') . PHP_EOL;
        echo Console::log('admin.' . $this->_domain, 'normal') . PHP_EOL;
        echo Console::log('static.' . $this->_domain, 'normal') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
    }

    private function selectType() : void
    {
        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('Select initialization type'), 'white') . PHP_EOL;
        echo Console::log($this->l('[d] - Development'), 'normal') . PHP_EOL;
        echo Console::log($this->l('[p] - Production'), 'normal') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
        $this->_type = readline($this->l('Type [p]: ')) ?: 'p';
    }

    private function selectLanguage() : void
    {
        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('Select installer language'), 'white') . PHP_EOL;
        echo Console::log('[ru] - Русский', 'normal') . PHP_EOL;
        echo Console::log('[en] - English', 'normal') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
        $this->_language = readline($this->l('Language [en]: ')) ?: 'en';
    }

    private function setupMySQL() : bool
    {
        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('MySQL settings'), 'white') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
        $this->_db_host = readline($this->l('Host [localhost]: ')) ?: 'localhost';
        while (!$this->_db_name = readline($this->l('Database name: '))) {
            echo Console::log($this->l('Database name must be set'), 'red') . PHP_EOL;
        };
        while (!$this->_db_user = readline($this->l('User: '))) {
            echo Console::log($this->l('User must be set'), 'red') . PHP_EOL;
        };
        $this->_db_pass = readline($this->l('Password: '));

        return $this->checkDatabaseConnection();
    }

    private function checkDatabaseConnection() : bool
    {
        try {
            $this->_db_connection = new PDO('mysql:host=' . $this->_db_host . ';dbname=' . $this->_db_name, $this->_db_user, $this->_db_pass);
            $this->_db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function setConfigMySQL() : void
    {
        $mysql_line = 'mysql:host=' . $this->_db_host . ';dbname=' . $this->_db_name;
        $file = __DIR__ . '/common/config/main-local.php';
        $content = file_get_contents($file);
        $content = preg_replace('/(("|\')dsn("|\')\s*=>\s*)(""|\'\')/', "\\1'$mysql_line'", $content);
        $content = preg_replace('/(("|\')username("|\')\s*=>\s*)(""|\'\')/', "\\1'$this->_db_user'", $content);
        $content = preg_replace('/(("|\')password("|\')\s*=>\s*)(""|\'\')/', "\\1'$this->_db_pass'", $content);
        file_put_contents($file, $content);
    }

    private function setConfigDomains() : void
    {
        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('Set your HTTP protocol (http/https)'), 'white') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
        $this->_http_protocol = readline($this->l('HTTP Protocol [http]: ')) ?: 'http';

        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('Website domain name'), 'white') . PHP_EOL;
        echo Console::log($this->l('Example: site.com, domain.ru'), 'normal') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
        while (!$this->_domain = readline($this->l('Domain name: '))) {
            echo Console::log($this->l('Domain must be set'), 'red') . PHP_EOL;
        };

        $file = __DIR__ . '/common/config/params-local.php';
        $content = file_get_contents($file);

        $site_domain = $this->_http_protocol . '://' . $this->_domain;
        $admin_domain = $this->_http_protocol . '://admin.' . $this->_domain;
        $static_domain = $this->_http_protocol . '://static.' . $this->_domain;
        $cookie_domain = '.' . $this->_domain;

        $content = preg_replace('/(("|\')frontendHostInfo("|\')\s*=>\s*)(""|\'\')/', "\\1'$site_domain'", $content);
        $content = preg_replace('/(("|\')backendHostInfo("|\')\s*=>\s*)(""|\'\')/', "\\1'$admin_domain'", $content);
        $content = preg_replace('/(("|\')staticHostInfo("|\')\s*=>\s*)(""|\'\')/', "\\1'$static_domain'", $content);
        $content = preg_replace('/(("|\')supportEmail("|\')\s*=>\s*)(""|\'\')/', "\\1'$this->_email'", $content);
        $content = preg_replace('/(("|\')adminEmail("|\')\s*=>\s*)(""|\'\')/', "\\1'$this->_email'", $content);
        $content = preg_replace('/(("|\')cookieDomain("|\')\s*=>\s*)(""|\'\')/', "\\1'$cookie_domain'", $content);

        file_put_contents($file, $content);
    }

    private function addAdmin() : void
    {
        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('Create admin account'), 'white') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
        while (!$this->_username = readline($this->l('Username: '))) {
            echo Console::log($this->l('Username must be set'), 'red') . PHP_EOL;
        };
        while (!$this->_email || ($this->_email && !$this->trueEmail($this->_email))) {
            $this->_email = readline('E-mail: ');
            if (!$this->_email) {
                echo Console::log($this->l('E-mail must be set'), 'red') . PHP_EOL;
            }
            if (!$this->trueEmail($this->_email)) {
                echo Console::log($this->l('E-mail must be correct'), 'red') . PHP_EOL;
            }
        }
        $password = null;
        while (!$this->_password || $this->_password != $password) {
            while (!$this->_password = readline($this->l('Password: '))) {
                echo Console::log($this->l('Password must be set'), 'red') . PHP_EOL;
            };
            $password = readline($this->l('Repeat password: '));
            if ($this->_password != $password) {
                echo Console::log($this->l('Passwords must be equal'), 'red') . PHP_EOL;
            }
        };
        shell_exec('php ' . __DIR__ . '/yii user/add-admin "' . $this->_username . '" "' . $this->_email . '" "' . $this->_password . '"');
        echo Console::log($this->l('Admin account complete'), 'green') . PHP_EOL;
    }

    private function initSettings()
    {
        shell_exec('php ' . __DIR__ . '/yii settings/set ru site name "Веб-сайт"');
        shell_exec('php ' . __DIR__ . '/yii settings/set en site name "Website"');
        shell_exec('php ' . __DIR__ . '/yii settings/set ru site short_name "ВС"');
        shell_exec('php ' . __DIR__ . '/yii settings/set en site short_name "WS"');
        shell_exec('php ' . __DIR__ . '/yii settings/set ru design theme "start"');
        shell_exec('php ' . __DIR__ . '/yii settings/set en design theme "start"');
    }

    private function trueEmail($email) : bool
    {
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        return $email ? true : false;
    }

    private function runMigrations() : void
    {
        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('Prepare MySQL tables'), 'white') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
        shell_exec('php ' . __DIR__ . '/yii migrate --interactive=0');
        echo Console::log($this->l('Complete'), 'green') . PHP_EOL;
    }

    private function apache() : void
    {
        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('Server HTTP configuration'), 'white') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
        $apache = readline($this->l('Does your server use Apache? [yes]: ')) ?: 'y';
        if ($apache == 'y' || $apache == 'yes') {
            $this->prepareHtaccess();
        }
    }

    private function prepareHtaccess() : void
    {
        // main
$ssh_rules = <<<SSH
RewriteCond %{HTTPS} off
RewriteCond %{HTTP_HOST} ^(?:www\.)?(.*)$ [NC]
RewriteRule (.*) https://{$this->_domain}%{REQUEST_URI} [L,R=301]

RewriteCond %{HTTPS} on
RewriteCond %{HTTP_HOST} ^(www\.)+(.*)$ [NC]
RewriteRule (.*) https://{$this->_domain}%{REQUEST_URI} [L,R=301]
SSH;

        $ssh_rules = $this->_http_protocol == 'https' ? $ssh_rules : '';

$main_htacces = <<<MH
<IfModule mod_rewrite.c>
    Options +FollowSymlinks
    RewriteEngine On
</IfModule>

<IfModule mod_rewrite.c>

    {$ssh_rules}

    #static
    RewriteCond %{HTTP_HOST} ^static.{$this->_domain}
    RewriteRule ^(.*)$ zxcms/static/$1 [L]

    RewriteCond %{HTTP_HOST} ^admin.{$this->_domain}
    RewriteRule ^(.*)$ zxcms/backend/web/$1 [L]

    # if /admin - backend
    RewriteCond %{HTTP_HOST} ^admin.{$this->_domain}
    RewriteRule ^assets/(.*)$ zxcms/backend/web/assets/$1 [L]

    RewriteCond %{HTTP_HOST} ^admin.{$this->_domain}
    RewriteRule ^css/(.*)$ zxcms/backend/web/css/$1 [L]

    RewriteCond %{HTTP_HOST} ^admin.{$this->_domain}
    RewriteRule ^js/(.*)$ zxcms/backend/web/js/$1 [L]

    RewriteCond %{REQUEST_URI} !^/zxcms/backend/web/(assets|js|css)/
    RewriteCond %{HTTP_HOST} ^admin.{$this->_domain}
    RewriteRule ^.*$ zxcms/backend/web/index.php [L]

    RewriteCond %{REQUEST_URI} ^/(assets|css|js|images)
    RewriteRule ^assets/(.*)$ zxcms/frontend/web/assets/$1 [L]
    RewriteRule ^css/(.*)$ zxcms/frontend/web/css/$1 [L]
    RewriteRule ^js/(.*)$ zxcms/frontend/web/js/$1 [L]
    RewriteRule ^images/(.*)$ zxcms/frontend/web/images/$1 [L]
    RewriteRule ^(.*)$ zxcms/frontend/web/$1 [L]

    RewriteCond %{REQUEST_URI} !^/zxcms/(frontend|backend)/web/(assets|css|js)/
    RewriteCond %{REQUEST_URI} !index.php
    RewriteCond %{REQUEST_FILENAME} !-f [OR]
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^.*$ zxcms/frontend/web/index.php
</IfModule>
MH;

        file_put_contents(__DIR__ . '/../.htaccess', $main_htacces);

        // backend, frontend
$bf_htaccess = <<<BF
RewriteEngine on

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d

RewriteRule . index.php
BF;
        file_put_contents(__DIR__ . '/backend/web/.htaccess', $bf_htaccess);
        file_put_contents(__DIR__ . '/frontend/web/.htaccess', $bf_htaccess);

        // static
$static_htaccess = <<<SH
RewriteEngine on

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . {$this->_http_protocol}://{$this->_domain}/site/error [R=404,L]

ErrorDocument 404 {$this->_http_protocol}://{$this->_domain}/site/error
SH;
        file_put_contents(__DIR__ . '/static/.htaccess', $static_htaccess);
    }

    private function addPermissions() : void
    {
        echo '---------------------' . PHP_EOL;
        echo Console::log($this->l('Creating permissions'), 'white') . PHP_EOL;
        echo '---------------------' . PHP_EOL;
        $systemPermissions = [
            'SettingsManagement' => 'Settings Management',
            'MenuManagement' => 'Menu Management',
            'ModuleManagement' => 'Modules Management',
            'UserManagement' => 'Users Management',
            'Dashboard' => 'Dashboard',
            'SliderManagement' => 'Slider Management',
        ];
        foreach ($systemPermissions as $name => $description) {
            shell_exec('php ' . __DIR__ . '/yii permission/add "' . $name . '" "' . $description . '"');
        }
        echo Console::log($this->l('Complete'), 'green') . PHP_EOL;
    }

    private function activateSystemModules() : void
    {
        Console::log($this->l('Activating system modules: '), 'white');
        shell_exec('php ' . __DIR__ . '/yii module/init');
        foreach ($this->_systemModules as $name) {
            shell_exec('php ' . __DIR__ . '/yii module/activate "' . $name . '"');
            shell_exec('php ' . __DIR__ . '/yii module/set-system "' . $name . '"');
        }
        echo Console::log($this->l('Complete'), 'green') . PHP_EOL;
    }

    private function l($str): string
    {
        return isset($this->_l[$this->_language]) && isset($this->_l[$this->_language][$str]) ? $this->_l[$this->_language][$str] : $str;
    }
}

chdir(dirname(__DIR__));
$setup = new Setup();
$setup->run();


// Colored class

class Console
{

    static $foreground_colors = array(
        'bold'         => '1',
        'dim'          => '2',
        'black'        => '0;30',
        'dark_gray'    => '1;30',
        'blue'         => '0;34',
        'light_blue'   => '1;34',
        'green'        => '0;32',
        'light_green'  => '1;32',
        'cyan'         => '0;36',
        'light_cyan'   => '1;36',
        'red'          => '0;31',
        'light_red'    => '1;31',
        'purple'       => '0;35',
        'light_purple' => '1;35',
        'brown'        => '0;33',
        'yellow'       => '1;33',
        'light_gray'   => '0;37',
        'white'        => '1;37',
        'normal'       => '0;39',
    );

    static $background_colors = array(
        'black'      => '40',
        'red'        => '41',
        'green'      => '42',
        'yellow'     => '43',
        'blue'       => '44',
        'magenta'    => '45',
        'cyan'       => '46',
        'light_gray' => '47',
    );

    static $options = array(
        'underline' => '4',
        'blink'     => '5',
        'reverse'   => '7',
        'hidden'    => '8',
    );
    static $EOF = "\n";

    /**
     * Logs a string to console.
     *
     * @param  string $str Input String
     * @param  string $color Text Color
     * @param  boolean $newline Append EOF?
     * @param  [type]  $background Background Color
     *
     * Formatted output
     */
    public static function log($str = '', $color = 'normal', $newline = false, $background_color = null)
    {
        if (is_bool($color)) {
            $newline = $color;
            $color   = 'normal';
        } elseif (is_string($color) && is_string($newline)) {
            $background_color = $newline;
            $newline          = true;
        }
        $str = $newline ? $str . self::$EOF : $str;
        echo self::$color($str, $background_color);
    }

    /**
     * Anything below this point (and its related variables):
     * Colored CLI Output is: (C) Jesse Donat
     * https://gist.github.com/donatj/1315354
     * -------------------------------------------------------------
     */

    /**
     * Catches static calls (Wildcard)
     *
     * @param  string $foreground_color Text Color
     * @param  array $args Options
     *
     * @return string                   Colored string
     */
    public static function __callStatic($foreground_color, $args)
    {
        $string         = $args[0];
        $colored_string = "";

        // Check if given foreground color found
        if (isset(self::$foreground_colors[$foreground_color])) {
            $colored_string .= "\033[" . self::$foreground_colors[$foreground_color] . "m";
        } else {
            die($foreground_color . ' not a valid color');
        }

        array_shift($args);
        foreach ($args as $option) {
            // Check if given background color found
            if (isset(self::$background_colors[$option])) {
                $colored_string .= "\033[" . self::$background_colors[$option] . "m";
            } elseif (isset(self::$options[$option])) {
                $colored_string .= "\033[" . self::$options[$option] . "m";
            }
        }

        // Add string and end coloring
        $colored_string .= $string . "\033[0m";

        return $colored_string;

    }

    /**
     * Plays a bell sound in console (if available)
     *
     * @param  integer $count Bell play count
     *
     * Bell play string
     */
    public static function bell($count = 1)
    {
        echo str_repeat("\007", $count);
    }
}
