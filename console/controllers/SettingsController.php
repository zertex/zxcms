<?php
/**
 * Created by Error202
 * Date: 13.09.2018
 */

namespace console\controllers;

use core\entities\Settings;
use core\forms\SettingsForm;
use core\services\SettingsService;
use yii\console\Controller;
use yii\db\ActiveRecord;

class SettingsController extends Controller
{
    private SettingsService $settings_service;

    public function __construct(string $id, $module, SettingsService $settings_service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->settings_service = $settings_service;
    }

    public function actionSet($language, $section, $key, $value)
    {
        $setting = $this->findModel($section, $key);
        if ($setting) {
            $translate = $setting->findTranslation($language);

            if (!$translate) {
                $translate = new $setting->virtualClassName();
                $translate->section = $section;
                $translate->key = $key;
                $translate->language = $language;
            }

            $translate->value = $value;
            $translate->save();
        } else {
            $settingForm = new SettingsForm();
            $settingForm->type = 'string';
            $settingForm->section = $section;
            $settingForm->key = $key;
            $settingForm->active = 1;
            $setting = $this->settings_service->create($settingForm);

            $translate = $setting->findTranslation($language);

            if (!$translate) {
                $translate = new $setting->virtualClassName();
                $translate->section = $section;
                $translate->key = $key;
                $translate->language = $language;
            }

            $translate->value = $value;
            $translate->save();
        }
        $this->stdout('Settings updated!' . PHP_EOL);
    }

    /**
     * @param $section
     * @param $key
     * @return array|Settings|ActiveRecord|null
     */
    protected function findModel($section, $key): array|Settings|ActiveRecord|null
    {
        if (($model = Settings::find()->andWhere(['section' => $section])->andWhere(['key' => $key])->one()) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}
