<?php
/**
 * Created by Error202
 * Date: 04.09.2018
 */

namespace console\controllers;

use console\components\ConsoleColor;
use core\components\SearchPerformance;
use core\entities\ModuleRecord;
use core\helpers\FileHelper;
use core\services\ModuleService;
use core\services\PermissionManager;
use Yii;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\db\Exception;

/**
 * Modules management from console
 * Class ModuleController
 * @package console\controllers
 */
class ModuleController extends Controller
{
    private ModuleService $module_service;
    private PermissionManager $permission_manager;

    public function __construct(string $id, $module, ModuleService $module_service, PermissionManager $permission_manager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->module_service = $module_service;
        $this->permission_manager = $permission_manager;
    }

    /**
     * First modules initialization
     */
    public function actionInit() : void
    {
        Yii::$app->moduleManager->getModules();
    }

    /**
     * Activate module and apply it migration if needed
     * @param $name
     */
    public function actionActivate($name)
    {
        $module = ModuleRecord::find()->andWhere(['name' => $name])->one();

        if ($module || $module->isDisabled()) {
            $this->module_service->enable($module);
        }
    }

    public function actionSetSystem($name)
    {
        $module = ModuleRecord::find()->andWhere(['name' => $name])->one();
        if ($module) {
            $this->module_service->setSystem($module);
        }
    }

    public function actionInstall($name)
    {
        $path = Yii::getAlias('@common/modules/' . $name);
        echo ConsoleColor::log('Installing module: ', 'yellow') . ConsoleColor::log($name, 'white') . PHP_EOL;

        // check exists
        if (file_exists($path)) {
            echo ConsoleColor::log('Module ' . $name . ' already exists', 'red') . PHP_EOL;
            return;
        }

        // create folder
        mkdir($path);

        // git clone
        chdir($path);
        echo ConsoleColor::log('Git clone: ', 'normal');
        shell_exec('git clone https://gitlab.com/zertex/zxcms-' . $name . '.git .');
        echo ConsoleColor::log('complete', 'white') . PHP_EOL;

        // get module manifest
        $manifest = require Yii::getAlias('@common/modules/' . $name . '/manifest.php');

        // migration
        echo ConsoleColor::log('Database: ', 'normal');
        if (file_exists($path . '/migrations')) {
            shell_exec('php ' . __DIR__ . '/../../yii migrate --migrationPath=' . $path . '/migrations --interactive=0');
        }
        echo ConsoleColor::log('complete', 'white') . PHP_EOL;

        // add module record with active flag
        $db_module = ModuleRecord::find()->andWhere(['name' => $name])->one();
        if (!$db_module) {
            $this->module_service->create($name, 'common\\modules\\' . $name . '\\' . $manifest['module'], 'common', 1);
        }

        // init permissions
        echo ConsoleColor::log('Permissions: ', 'normal');
        if (isset($manifest['permissions']) && is_array($manifest['permissions'])) {
            $this->assignPermissions($manifest['permissions']);
        }
        echo ConsoleColor::log('complete', 'white') . PHP_EOL;

        // regenerate cp global search
        echo ConsoleColor::log('Search database: ', 'normal');
        SearchPerformance::init();
        echo ConsoleColor::log('complete', 'white') . PHP_EOL;
        echo ConsoleColor::log('Module ' . $name . ' successfully installed', 'light_green') . PHP_EOL;
    }

    /**
     * @param $name
     * @throws ErrorException
     * @throws Exception
     */
    public function actionRemove($name)
    {
        // drop module record
        $db_module = ModuleRecord::find()->andWhere(['name' => $name])->one();

        if ($db_module == ModuleRecord::SYSTEM_YES) {
            echo ConsoleColor::log('Module ' . $name . ' is system. Cannot be deleted.', 'red') . PHP_EOL;
            return;
        }

        if ($db_module) {
            $this->module_service->delete($db_module);
        }

        $path = Yii::getAlias('@common/modules/' . $name);
        echo ConsoleColor::log('Removing module: ', 'yellow') . ConsoleColor::log($name, 'white') . PHP_EOL;

        // get module manifest
        $manifest = require Yii::getAlias('@common/modules/' . $name . '/manifest.php');

        // revert migration
        echo ConsoleColor::log('Database: ', 'normal');
        if (file_exists($path . '/migrations')) {
            shell_exec('php ' . __DIR__ . '/../../yii migrate/down 500 --migrationPath=' . $path . '/migrations --interactive=0');
        }
        echo ConsoleColor::log('clean', 'white') . PHP_EOL;

        // check exists, remove if founded
        echo ConsoleColor::log('Module files: ', 'normal');
        if (file_exists($path)) {
            FileHelper::removeDirectory($path);
        }
        echo ConsoleColor::log('clean', 'white') . PHP_EOL;

        // drop permissions
        echo ConsoleColor::log('Permissions: ', 'normal');
        if (isset($manifest['permissions']) && is_array($manifest['permissions'])) {
            $this->module_service->removePermissions($manifest['permissions']);
        }
        echo ConsoleColor::log('clean', 'white') . PHP_EOL;

        // regenerate cp global search
        echo ConsoleColor::log('Search database: ', 'normal');
        SearchPerformance::init();
        echo ConsoleColor::log('complete', 'white') . PHP_EOL;

        echo ConsoleColor::log('Module ' . $name . ' successfully deleted', 'light_green') . PHP_EOL;
    }

    private function assignPermissions(array $permissions): void
    {
        foreach ($permissions as $permission => $description) {
            if (!$this->permission_manager->permissionExists($permission)) {
                $this->permission_manager->create($permission, $description);
            }
        }
    }
}
