<?php
/**
 * Created by Error202
 * Date: 05.07.2018
 *
 * Init / Update search mysql performance
 */

namespace console\controllers;

use core\components\SearchPerformance;
use yii\console\Controller;
use yii\db\Exception;

class SearchInitController extends Controller {

    /**
     * @throws Exception
     */
    public function actionInit() {
		SearchPerformance::init();
		echo "Search performance complete" . PHP_EOL;
	}
}
