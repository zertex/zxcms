<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings_lng`.
 */
class m180910_182109_create_settings_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%settings_lng}}', [
            'id'       => $this->primaryKey(),
            'section'  => $this->string(),
            'key'      => $this->string(),
            'language' => $this->string(6),
            'value'    => $this->text(),
        ], $tableOptions);

        $this->createIndex('idx_settings_lng_key', '{{%settings_lng}}', ['section', 'key']);
        $this->createIndex('idx_settings_lng_language', '{{%settings_lng}}', 'language');
        $this->addForeignKey('fk_settings_lng_settings_id', '{{%settings_lng}}', ['section', 'key'], '{{%settings}}', ['section', 'key'], 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_settings_lng_settings_id', '{{%settings_lng}}');
        $this->dropIndex('idx_settings_lng_language', '{{%settings_lng}}');
        $this->dropIndex('idx_settings_lng_key', '{{%settings_lng}}');
        $this->dropTable('{{%settings_lng}}');
    }
}
