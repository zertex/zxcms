<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_types`.
 */
class m180110_121107_create_post_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
	    $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%post_types}}', [
            'id' => $this->primaryKey(),
	        'name' => $this->string(64)->notNull(), //a-z
	        'singular' => $this->string(64)->notNull(), // в ед.ч.
	        'plural' => $this->string(64)->notNull(), // во мн.ч.
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%post_types}}');
    }
}
