<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu_items`.
 */
class m180709_202659_create_menu_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = null;
	    if ($this->db->driverName === 'mysql') {
		    $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
	    }

        $this->createTable('{{%menu_items}}', [
            'id' => $this->primaryKey(),
	        'menu_id' => $this->integer()->notNull(),
	        'parent_id' => $this->integer(),
	        'target' => $this->string(20),
	        'css' => $this->string(255),
	        'style' => $this->string(255),
	        'module' => $this->string(255),
	        'url' => $this->string(255),
	        'url_params' => $this->text(), // json id=>1, ...
	        'sort' => $this->integer()->notNull()->defaultValue(1),
        ], $tableOptions);

	    $this->createIndex('idx_menu_item_sort', '{{%menu_items}}', ['menu_id', 'sort']);
	    $this->createIndex('idx_menu_items_menu_id', '{{%menu_items}}', 'menu_id');
	    $this->createIndex('idx_menu_items_parent_id', '{{%menu_items}}', 'parent_id');

	    $this->addForeignKey('frg_menu_items_menu_id', '{{%menu_items}}', 'menu_id', '{{%menu}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropForeignKey('frg_menu_items_menu_id', '{{%menu_items}}');

    	$this->dropIndex('idx_menu_items_parent_id', '{{%menu_items}}');
	    $this->dropIndex('idx_menu_items_menu_id', '{{%menu_items}}');
	    $this->dropIndex('idx_menu_item_sort', '{{%menu_items}}');

        $this->dropTable('{{%menu_items}}');
    }
}
