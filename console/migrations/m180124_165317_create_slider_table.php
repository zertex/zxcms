<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m180124_165317_create_slider_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
	    $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%slider}}', [
            'id' => $this->primaryKey(),
	        'title' => $this->string(128),
            'tagline' => $this->string(255),
	        'image' => $this->string(255),
            'url' => $this->string(255),
            'sort' => $this->integer(11)->defaultValue(100),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%slider}}');
    }
}
