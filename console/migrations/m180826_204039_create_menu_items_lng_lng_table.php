<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu_items_lng_lng`.
 */
class m180826_204039_create_menu_items_lng_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = null;
	    if ($this->db->driverName === 'mysql') {
		    $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
	    }

        $this->createTable('{{%menu_items_lng}}', [
            'id' => $this->primaryKey(),
            'menu_item_id' => $this->integer()->notNull(),
            'language' => $this->string(6)->notNull(),
            'name' => $this->string(255)->notNull(),
            'title_attr' => $this->string(255),
        ], $tableOptions);

	    $this->createIndex('idx_menu_items_lng_language', '{{%menu_items_lng}}', 'language');
	    $this->createIndex('idx_menu_items_lng_page_id', '{{%menu_items_lng}}', 'menu_item_id');
	    $this->addForeignKey('fk_menu_items_lng_id', '{{%menu_items_lng}}', 'menu_item_id', '{{%menu_items}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropForeignKey('fk_menu_items_lng_id', '{{%menu_items_lng}}');
	    $this->dropColumn('idx_menu_items_lng_page_id', '{{%menu_items_lng}}');
	    $this->dropColumn('idx_menu_items_lng_language', '{{%menu_items_lng}}');
        $this->dropTable('{{%menu_items_lng}}');
    }
}
