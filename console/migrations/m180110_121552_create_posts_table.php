<?php

use yii\db\Migration;

/**
 * Handles the creation of table `posts`.
 */
class m180110_121552_create_posts_table extends Migration
{
	public function up()
	{
		$tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

		$this->createTable('{{%posts}}', [
			'id' => $this->primaryKey(),
			'category_id' => $this->integer()->notNull(),
			'published_at' => $this->integer()->unsigned()->notNull(),
			'created_at' => $this->integer()->unsigned()->notNull(),
			'updated_at' => $this->integer()->unsigned()->notNull(),
			'title' => $this->string()->notNull(),
			'description' => $this->text(),
			'content' => 'MEDIUMTEXT',
			'image' => $this->string(),
			'video' => $this->string(),
			'status' => $this->integer()->notNull(),
			'meta_json' => 'TEXT NOT NULL',
			'comments_count' => $this->integer()->notNull()->defaultValue(0),
			'type_id' => $this->integer()->notNull(),
			'views' => $this->integer()->notNull()->defaultValue(0),
			'slug' => $this->string()->notNull(),
		], $tableOptions);

		$this->createIndex('{{%idx-posts-category_id}}', '{{%posts}}', 'category_id');
		$this->createIndex('{{%idx-posts-type_id}}', '{{%posts}}', 'type_id');
		$this->createIndex('{{%idx-posts-slug}}', '{{%posts}}', 'slug', true);
		$this->addForeignKey('{{%fk-posts-category_id}}', '{{%posts}}', 'category_id', '{{%post_categories}}', 'id');
		$this->addForeignKey('{{%fk-posts-type_id_id}}', '{{%posts}}', 'type_id', '{{%post_types}}', 'id');
	}

	public function down()
	{
		$this->dropTable('{{%posts}}');
	}
}
