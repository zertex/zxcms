<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_tag_assignments`.
 */
class m180110_132623_create_post_tag_assignments_table extends Migration
{
	public function up()
	{
		$tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

		$this->createTable('{{%post_tag_assignments}}', [
			'post_id' => $this->integer()->notNull(),
			'tag_id' => $this->integer()->notNull(),
		], $tableOptions);

		$this->addPrimaryKey('{{%pk-post_tag_assignments}}', '{{%post_tag_assignments}}', ['post_id', 'tag_id']);

		$this->createIndex('{{%idx-post_tag_assignments-post_id}}', '{{%post_tag_assignments}}', 'post_id');
		$this->createIndex('{{%idx-post_tag_assignments-tag_id}}', '{{%post_tag_assignments}}', 'tag_id');

		$this->addForeignKey('{{%fk-post_tag_assignments-post_id}}', '{{%post_tag_assignments}}', 'post_id', '{{%posts}}', 'id', 'CASCADE', 'RESTRICT');
		$this->addForeignKey('{{%fk-post_tag_assignments-tag_id}}', '{{%post_tag_assignments}}', 'tag_id', '{{%post_tags}}', 'id', 'CASCADE', 'RESTRICT');
	}

	public function down()
	{
		$this->dropTable('{{%post_tag_assignments}}');
	}
}
