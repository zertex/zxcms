<?php

use yii\db\Migration;

/**
 * Handles the creation of table `session`.
 */
class m180126_061916_create_session_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $tableOptions = null;
	    if ($this->db->driverName === 'mysql') {
		    // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
		    $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
	    }

        $this->createTable('{{%session}}', [
	        'id' => $this->char(40)->notNull(),
	        'expire' => $this->integer(),
	        'data' => $this->binary(),
	        'user_id' => $this->bigInteger()
        ], $tableOptions);

	    $this->addPrimaryKey('session_pk', 'session', 'id');
	    $this->createIndex('idx_session_user_id', '{{%session}}', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropIndex('idx_session_user_id', '{{%session}}');
        $this->dropTable('{{%session}}');
    }
}
