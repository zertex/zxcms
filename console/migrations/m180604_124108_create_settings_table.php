<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180604_124108_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(
            '{{%settings}}',
            [
                //'id'         => $this->primaryKey(),
                'section'    => $this->string(),
                'key'        => $this->string(),
                'type'       => $this->string()->notNull(),
                'active'     => $this->integer(1),
                'created_at' => $this->integer()->unsigned(),
                'updated_at' => $this->integer()->unsigned(),
            ],
            $tableOptions
        );
        $this->addPrimaryKey('idx_setting_key', '{{%settings}}', ['section', 'key']);
        //$this->createIndex('idx_setting_key', '{{%settings}}', ['section', 'key']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        //$this->dropIndex('idx_setting_key', '{{%settings}}');
        $this->dropTable('{{%settings}}');
    }
}
