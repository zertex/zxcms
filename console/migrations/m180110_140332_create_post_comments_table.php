<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_comments`.
 */
class m180110_140332_create_post_comments_table extends Migration
{
	public function up()
	{
		$tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

		$this->createTable('{{%post_comments}}', [
			'id' => $this->primaryKey(),
			'post_id' => $this->integer()->notNull(),
			'user_id' => $this->bigInteger()->notNull(),
			'parent_id' => $this->integer(),
			'created_at' => $this->integer()->unsigned()->notNull(),
			'text' => $this->text()->notNull(),
            'likes' => $this->integer()->defaultValue(0),
			'active' => $this->boolean()->notNull(),
		], $tableOptions);

		$this->createIndex('{{%idx-post_comments-post_id}}', '{{%post_comments}}', 'post_id');
		$this->createIndex('{{%idx-post_comments-user_id}}', '{{%post_comments}}', 'user_id');
		$this->createIndex('{{%idx-post_comments-parent_id}}', '{{%post_comments}}', 'parent_id');

		$this->addForeignKey('{{%fk-post_comments-post_id}}', '{{%post_comments}}', 'post_id', '{{%posts}}', 'id', 'CASCADE');
		$this->addForeignKey('{{%fk-post_comments-user_id}}', '{{%post_comments}}', 'user_id', '{{%users}}', 'id', 'CASCADE');
		$this->addForeignKey('{{%fk-post_comments-parent_id}}', '{{%post_comments}}', 'parent_id', '{{%post_comments}}', 'id', 'CASCADE');
	}

	public function down()
	{
		$this->dropForeignKey('{{%fk-post_comments-parent_id}}', '{{%post_comments}}');
		$this->dropForeignKey('{{%fk-post_comments-user_id}}', '{{%post_comments}}');
		$this->dropForeignKey('{{%fk-post_comments-post_id}}', '{{%post_comments}}');
		$this->dropIndex('{{%idx-post_comments-parent_id}}', '{{%post_comments}}');
		$this->dropIndex('{{%idx-post_comments-user_id}}', '{{%post_comments}}');
		$this->dropIndex('{{%idx-post_comments-post_id}}', '{{%post_comments}}');
		$this->dropTable('{{%post_comments}}');
	}
}
