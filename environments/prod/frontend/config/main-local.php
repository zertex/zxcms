<?php

$config = [
    'components' => [
        'view' => [
            'theme' => [
                'basePath' => '@webroot/themes/sport',
                'baseUrl'  => '@web/themes/sport',
                'pathMap'  => [
                    '@frontend/views'   => '@webroot/themes/sport',
                    '@frontend/widgets' => '@webroot/themes/sport/widgets',
                ],
            ],
        ],

        'authClientCollection' => [
            'class'   => 'yii\authclient\Collection',
            'clients' => [
                'yandex'   => [
                    'class'        => 'yii\authclient\clients\Yandex',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
                'google'   => [
                    'class'        => 'yii\authclient\clients\Google',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
                'facebook' => [
                    'class'        => 'yii\authclient\clients\Facebook',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
                'vk'       => [
                    'class'        => 'yii\authclient\clients\VKontakte',
                    'clientId'     => '',
                    'clientSecret' => '',
                ],
            ],
        ]
    ],
];

return $config;
