<?php
return [
    'backendTranslatedLanguages' => [
        'ru' => 'Русский',
        'en' => 'English',
    ],
    'backendDefaultLanguage'     => 'ru',
];
