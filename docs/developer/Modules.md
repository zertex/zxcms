### Модули

Модули располагаются в папке `common/modules`

После установки папка с модулями содержит только обязательные системные модули.

**Для добавления готовых модулей сделайте следующее:**
+ Создайте в папке `common/modules` папку с названием выбранного модуля
+ Перейдите в эту папку в консоли
+ Клонируйте в нее модуль, например для блога
```bash
git clone https://gitlab.com/zertex/zxcms-blog.git .
``` 
После клонирования перейдите в контрольной панели к списку модулей.
Будет создана запись в таблице `module` о данном модуле. После его включения автоматически будут применены миграции модуля.

**Создание нового модуля**

Создайте в папке `common/modules` папку с названием модуля.
Название должно состоять из строчных букв латинского алфавита и цифр. 

Папка модуля должна содержать основной файл, например, `BlogModule.php` следующего содержания
```php
<?php

namespace common\modules\blog;
use core\components\modules\ModuleInterface;
use yii\helpers\ArrayHelper;

class BlogModule extends \yii\base\Module implements ModuleInterface
{
    // Папка контроллеров модуля
    public $controllerNamespace = 'common\modules\blog\controllers';

    // Инициализация модуля
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }

    // Автозапуск модуля
	public function bootstrap($app)
	{
		// Добавление правила поиска по данному модулю
		$app->params['search_rules'][] = "SELECT title, content, CONCAT('/blog/manage/post/view/', id) AS url FROM {{blog_posts}}";
		
		// Добавление правила роутинга
		$app->getUrlManager()->addRules([
			'blog' => 'blog/post/index',
		]);

        // Добавление правил роутинга, в случае использования классов
		$app->getUrlManager()->addRules([
			['class' => 'common\modules\blog\urls\BlogPostUrlRule'],
			['class' => 'common\modules\blog\urls\BlogCategoryUrlRule'],
		]);

		// Добавление локализации
		$app->getI18n()->translations = ArrayHelper::merge($app->getI18n()->translations, [
			'blog' => [
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@common/modules/blog/messages',
			],
			'blog_public' => [
				'class' => 'yii\i18n\PhpMessageSource',
				'basePath' => '@common/modules/blog/messages',
			],
		]);
		
        // Добавление пунктов в меню администратора
		if (basename($app->getBasePath()) === 'backend') {
			$app->params['adminMenu'][] = [
				'label'   => \Yii::t( 'blog', 'Blog' ),
				'icon'    => 'book',
				'items'   => [
					[
						'label' => \Yii::t( 'blog', 'Categories' ),
						'icon'  => 'caret-right',
						'url'   => [ '/blog/manage/category/index' ]
					],
					[
						'label' => \Yii::t( 'blog', 'Posts' ),
						'icon'  => 'caret-right',
						'url'   => [ '/blog/manage/post/index' ]
					],
					[
						'label' => \Yii::t( 'blog', 'Comments' ),
						'icon'  => 'caret-right',
						'url'   => [ '/blog/manage/comment/index' ]
					],
				],
				'visible' => \Yii::$app->user->can( 'admin' ) || \Yii::$app->user->can( 'BlogManagement' )
			];
		}
	}
}
```