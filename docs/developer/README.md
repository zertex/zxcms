Zertex CMS
==========
###Система управления сайтом

**Для разработчика**

* [Модули](Modules.md)

#### Дополнительные параметры Yii::$app

1. `$app->params['search_rules']` - Массив правил для поиска.

Пример:
```php
$app->params['search_rules'][] = "SELECT title, content, CONCAT('/blog/manage/post/view/', id) AS url FROM {{blog_posts}}";
``` 
Поиск осуществляется по полям TITLE и CONTENT. В качестве URL указывается ссылка на конкрутную запись.

Если название полей отличаются от TITLE и CONTENT, но, сгласно правилам SQL, указывайте ваше поле, затем `as TITLE` или `as CONTENT`

#### Параметры настроек

1. `Yii::$app->params['settings']['site']['short_name']` - Аббревиатура сайта в панели управления

2. `Yii::$app->params['settings']['site']['name']` - Название сайта в панели управления

3. `Yii::$app->params['settings']['design']['theme']` - Тема сайта